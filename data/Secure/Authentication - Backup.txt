User should provide Authentication details in below format.
If the 'Password' value shows 'Secure' keyword, It means its encrypted and safe to use.
Format => [ApplicationName_UserName = Password]

A1SC_automq = Secure : PAT1VHPRCS7FZl8S7FIw9w
A1SC_automq2 = Secure : PAT1VHPRCS7FZl8S7FIw9w
RAR_automq = Secure : R/jqgqs+3CQhwY3K88k+yQ
RAR_automq2 = Secure : R/jqgqs+3CQhwY3K88k+yQ
SFDC_automq2@avaya.com = Avaya@123@
SFDC_automq@avaya.com = Avaya@123@
