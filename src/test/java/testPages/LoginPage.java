
package testPages;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

//import testDrivers.ConfigUtil;
import testDrivers.SecureUtil;
import testEngine.ExcelUtil;
import testEngine.GearBox;
import testEngine.LoadInReports;

public class LoginPage extends GearBox {

	public String UserNamePath, PassWordPath, UserName, Password, SubmitPath, URL, ApplicationName;

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;// = new LinkedHashMap<String, LoadInReports>(); // removed static
	public Map<String, String> testElement;// = new LinkedHashMap<String, String>(); // removed static
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

	public LoginPage(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	synchronized public void SFDCLoginAuthentication(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		try {
			String ApplicationName = excelUtil.GetParameterValue(CurrentTestCase, "SFDC_Model").toString();
			String UserName = "automq2@avaya.com"; // excelUtil.GetParameterValue(CurrentTestCase, "UserName").toString();
//			Password = excelUtil.GetParameterValue("Password").toString();
			Password = secure.getPassword(ApplicationName, UserName);

			HighlightObject("SFDCUserNamePath");
			HighlightObject("SFDCPassWordPath");
			HighlightObject("SFDCSubmitPath");

			AddIOData(CurrentTestCase, "input", "User Name", UserName);
			AddIOData(CurrentTestCase, "input", "Password", Password);

			setText("SFDCUserNamePath", UserName);
			setText("SFDCPassWordPath", Password);
//			wait(1);
			getSnapShot("Login", PageName);
//			wait(1);
			clickButton("SFDCSubmitPath");

//			AddIODataWithLink(CurrentTestCase, "output", "QuoteNumber", "Saurabh", "");

			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
//			throw new ArithmeticException("Failed to Login");
		} catch (Exception e) {
			OnErrorLoadInReport("Unable to login with " + UserName + " user.");
		}

	}

	synchronized public void PSNLoginAuthentication(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		try {
			String ApplicationName = excelUtil.GetParameterValue(CurrentTestCase, "ApplicationName").toString();
			String UserName = excelUtil.GetParameterValue(CurrentTestCase, "PSNUserName").toString();
//			Password = excelUtil.GetParameterValue("Password").toString();
			String Password = secure.getPassword(ApplicationName, UserName);

			HighlightObject("UserNamePath");
			HighlightObject("PassWordPath");
			HighlightObject("SubmitPath");

			AddIOData(CurrentTestCase, "input", "User Name", UserName);
			AddIOData(CurrentTestCase, "input", "Password", Password);

			setText("UserNamePath", UserName);
			setText("PassWordPath", Password);
//			wait(1);
			getSnapShot("Login", PageName);
//			wait(1);
			clickButton("SubmitPath");

//			AddIODataWithLink(CurrentTestCase, "output", "QuoteNumber", "Saurabh", "");

			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
//			throw new ArithmeticException("Failed to Login");
		} catch (Exception e) {
			OnErrorLoadInReport("Unable to login with " + UserName + " user.");
		}

	}

	synchronized public void SFDCLoginWithLaunchOld(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		String URL = excelUtil.GetParameterValue(CurrentTestCase, "SFDC_URL").toString();
		driver.navigate().to(URL);

		List<WebElement> ele = driver.findElements(By.xpath("//input[@id='email']"));
		if (ele.size() > 0) {
			LoginAuthentication(PageName);
		} else {
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
		}
	}

	synchronized public void SFDCLoginWithLaunch(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		String URL = excelUtil.GetParameterValue(CurrentTestCase, "SFDC_URL").toString();
		driver.navigate().to(URL);

		List<WebElement> ele = driver.findElements(By.xpath("//input[@id='username']"));
		if (ele.size() > 0) {
			SFDCLoginAuthentication(PageName);
		} else {
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
		}
	}

	synchronized public void PSNLoginWithLaunch(String PageName) {
		// String psnquotetype = getOutput("PSN Quote Type");
		if (getOutput("PSN Quote Type").equalsIgnoreCase("unlinked")) {
			SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
			try {
				String URL = excelUtil.GetParameterValue(CurrentTestCase, "PSN_URL").toString();
				driver.navigate().to(URL);
				wait(2);
				if (driver.findElements(getPageObject("UserNamePath")).size() > 0) {
					PSNLoginAuthentication(PageName);
				}
				ExecutionStatus = "Pass";
				AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
				System.out.println("Able to login successfully with " + UserName + " user.");
			} catch (Exception e) {
				OnErrorLoadInReport("Unable to login with " + UserName + " user.");
			}
		}
	}

	synchronized public void OCLoginWithLaunch(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		try {
			String URL = excelUtil.GetParameterValue(CurrentTestCase, "OC_URL").toString();
			driver.navigate().to(URL);
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
		} catch (Exception e) {
			OnErrorLoadInReport("Unable to login with " + UserName + " user.");
		}
	}

	synchronized public void APSCLoginWithLaunch(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		try {
			String URL = excelUtil.GetParameterValue(CurrentTestCase, "APSC_URL").toString();
			driver.navigate().to(URL);
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
		} catch (Exception e) {
			OnErrorLoadInReport("Unable to login with " + UserName + " user.");
		}
	}

	// Adding Phase Deal Login for APSC E2E TC 1 and TC 3

	synchronized public void A1SCPhaseDealWithLaunch(String PageName) {
		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		try {
			String inputquote = getOutput("Quote#");
			String BaseURL = "https://app8vo.avaya.com/ec/default.asp?q=" + inputquote.trim();

			String URL = excelUtil.GetParameterValue(CurrentTestCase, "Phase_Deal_URL").toString();
			driver.navigate().to(BaseURL);
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Able to login successfully with " + UserName + " user.");
			System.out.println("Able to login successfully with " + UserName + " user.");
		} catch (Exception e) {
			OnErrorLoadInReport("Unable to login with " + UserName + " user.");
		}
	}

	public void GoogleSearchOperation() {
		String SearchBtnPath = "(//input[@value='Google Search'])[2]";
		clickButton(SearchBtnPath);
//		clickButton("SearchBtnPath");
		AddTextToLog(CurrentTestCase, "User click at Search Button.");

	}

}
