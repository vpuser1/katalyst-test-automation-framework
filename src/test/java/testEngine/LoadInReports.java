package testEngine;

import java.util.Iterator;
import java.util.Map;

public class LoadInReports {

	String SheetName, PageName, ExecutionStatus, TestDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue,
			ExecutionStartTime, ExecutionEndTime, ExecutionTotalTime, TestRemarks, Output, OnFail, TestCase, IOType,
			CurrentTestCase, SourceTestCase, ElementDescription;

	int PageElementsCount, TestRowNum;

	public LoadInReports(String SourceTestCase, String CurrentTestCase, String sheetName, String pageName,
			String executionStatus, String testDescription, String loadInReport, String elementName, String actionTaken,
			String testCaseValue, String Output, String executionStartTime, String executionEndTime, String testRemarks,
			int PageElementsCount, String OnFail, String IOType, int TestRowNum, String ElementDescription) {
		this.SourceTestCase = SourceTestCase;
		this.CurrentTestCase = CurrentTestCase;
		this.SheetName = sheetName;
		this.PageName = pageName;
		this.ExecutionStatus = executionStatus;
		this.TestDescription = testDescription;
		this.LoadInReport = loadInReport;
		this.ElementName = elementName;
		this.ActionTaken = actionTaken;
		this.TestCaseValue = testCaseValue;
		this.Output = Output;
		this.ExecutionStartTime = executionStartTime;
		this.ExecutionEndTime = executionEndTime;
		this.TestRemarks = testRemarks;
		this.PageElementsCount = PageElementsCount;
		this.OnFail = OnFail;
		this.IOType = IOType;
		this.TestRowNum = TestRowNum;
		this.ElementDescription = ElementDescription;
	}

	public String getElementDescription() {
		return ElementDescription;
	}

	public void setElementDescription(String elementDescription) {
		ElementDescription = elementDescription;
	}

	public int getTestRowNum() {
		return TestRowNum;
	}

	public void setTestRowNum(int testRowNum) {
		TestRowNum = testRowNum;
	}

	public String getCurrentTestCase() {
		return CurrentTestCase;
	}

	public void setCurrentTestCase(String currentTestCase) {
		CurrentTestCase = currentTestCase;
	}

	public String getSourceTestCase() {
		return SourceTestCase;
	}

	public void setSourceTestCase(String sourceTestCase) {
		SourceTestCase = sourceTestCase;
	}

	public String getIOType() {
		return IOType;
	}

	public void setIOType(String iOType) {
		IOType = iOType;
	}

	public String getTestCase() {
		return TestCase;
	}

	public void setTestCase(String testCase) {
		TestCase = testCase;
	}

	public String getOnFail() {
		return OnFail;
	}

	public void setOnFail(String onFail) {
		OnFail = onFail;
	}

	public int getPageElementsCount() {
		return PageElementsCount;
	}

	public void setPageElementsCount(int pageElementsCount) {
		PageElementsCount = pageElementsCount;
	}

	public String getOutput() {
		return Output;
	}

	public void setOutput(String output) {
		Output = output;
	}

	public String getSheetName() {
		return SheetName;
	}

	public void setSheetName(String sheetName) {
		SheetName = sheetName;
	}

	public String getPageName() {
		return PageName;
	}

	public void setPageName(String pageName) {
		PageName = pageName;
	}

	public String getExecutionStatus() {
		return ExecutionStatus;
	}

	public void setExecutionStatus(String executionStatus) {
		ExecutionStatus = executionStatus;
	}

	public String getTestDescription() {
		return TestDescription;
	}

	public void setTestDescription(String testDescription) {
		TestDescription = testDescription;
	}

	public String getLoadInReport() {
		return LoadInReport;
	}

	public void setLoadInReport(String loadInReport) {
		LoadInReport = loadInReport;
	}

	public String getElementName() {
		return ElementName;
	}

	public void setElementName(String elementName) {
		ElementName = elementName;
	}

	public String getActionTaken() {
		return ActionTaken;
	}

	public void setActionTaken(String actionTaken) {
		ActionTaken = actionTaken;
	}

	public String getTestCaseValue() {
		return TestCaseValue;
	}

	public void setTestCaseValue(String testCaseValue) {
		TestCaseValue = testCaseValue;
	}

	public String getExecutionStartTime() {
		return ExecutionStartTime;
	}

	public void setExecutionStartTime(String executionStartTime) {
		ExecutionStartTime = executionStartTime;
	}

	public String getExecutionEndTime() {
		return ExecutionEndTime;
	}

	public void setExecutionEndTime(String executionEndTime) {
		ExecutionEndTime = executionEndTime;
	}

	public String getExecutionTotalTime() {
		return ExecutionTotalTime;
	}

	public void setExecutionTotalTime(String executionTotalTime) {
		ExecutionTotalTime = executionTotalTime;
	}

	public String getTestRemarks() {
		return TestRemarks;
	}

	public void setTestRemarks(String testRemarks) {
		TestRemarks = testRemarks;
	}

}
