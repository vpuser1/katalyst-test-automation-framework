package testEngine;

import java.awt.event.InputMethodListener;

import org.openqa.selenium.WebDriver;
import org.testng.IExecutionListener;
import org.testng.IMethodInstance;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

@SuppressWarnings("unused")
public class FileUtil {

	public static String BasePath = System.getProperty("user.dir");
	public static String home = System.getProperty("user.home");
	public static String testWorkbook = BasePath + "//data//Source//KITE.xlsm";
	public static String ChromeDriverPath = BasePath + "//driver//chromedriver_v91//chromedriver.exe";
	public static String IEDriverPath = BasePath + "//driver//iedriver_v11//iedriver32.exe";
	public static String GeckoDriverPath = BasePath + "//driver//geckodriver_79//geckodriver.exe";
	public static String MSEdgeDriverPath = BasePath + "//driver//edgedriver_v86//msedgedriver.exe";
	public static String ChartRunner = BasePath + "//data//Charts//ChartRunner.vbs";
	public static String ChartText = BasePath + "//data//Charts//Charts.txt";
	public static String SnapshotsPath = BasePath + "//data//Snapshots";
	public static String LogoPath = BasePath + "//data//Logo";
	public static String ChartsPath = BasePath + "//data//Charts";
	public static String reportPath = BasePath + "//reports";
	public static String suitreportPath = BasePath + "//reports//SuitReports";
	public static String AuthenticationFilePath = BasePath + "//data//Secure//Authentication.txt";
	public static String EnvironmentFilePath = BasePath + "//data//Settings//Environments.txt";

	public static String sharefolder = "Z:\\Automation\\OSC_Regression\\";
//	public static String sharefolder = BasePath + "\\data\\OSC_Regression\\";
//	public static String OSDBasePath = BasePath + "//data//OSD_Files//OE_Modules";
//	public static String OSDBasePath = sharefolder;

	public static String localfolder = "C:\\Automation\\OSC Model Temp\\";

	public static String EXECUTOR_FILE_PATH = BasePath + "\\data\\Source\\ExecutorReport.txt";
	public static String Katalyst_File_Path = BasePath + "\\data\\Source\\KatalystTestRunner.txt";
	public static String Katalyst_Driver_Sheet = BasePath + "\\data\\Source\\Katalyst_Driver_Sheet.xlsx";
	public static String ReportGeneratorPath = "Z:\\Selenium_Automation\\ExecutionReport\\ReportGenerator.txt";

	public static String DB_CONNECTION = "jdbc:oracle:thin:@//dlqaavmdb01.nonprod.avaya.com:1526/Qaadev";
	public static String DB_USERNAME = "qa_automation";
	public static String DB_PASSWORD = "A2v4a6y8a_11";

	public static String DownloadPath = "C:\\Users\\QATester\\Downloads\\";

}
