package testEngine;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import testDrivers.SecureUtil;
import testPages.LoginPage;

import testPagesUtil.ModelInformationUtil;

public class GearBox extends FileUtil {

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports = new LinkedHashMap<String, LoadInReports>();
	public Map<String, String> testElement = new LinkedHashMap<String, String>();
	public Map<String, HealthCheckUtil> healthCheck = new LinkedHashMap<String, HealthCheckUtil>();
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate, TestStartDateDB, TestEndDateDB;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;
	public String DBApplicationName;

	public StringBuffer EmailHealthCheck = null;

	public EmailReportUtil emailReportUtil;
	public ExcelReportUtil excelReportUtil;
	public Date TestExecutionStartDate, TestExecutionEndDate;
	public String ExecutionStatus = "";
	public String strDate, endDate, PagestrDate, PageendDate, strDateDB;
	public String ImportOSDFlag = "";
	public static String strExecutionDate, endExecutionDate;
	public static String ApplicationName;

	public Map<String, ExcelReport> excelReports = new LinkedHashMap<String, ExcelReport>();

	public Map<String, InputOutputUtil> IOMap = new LinkedHashMap<String, InputOutputUtil>();
	public Map<String, String> TestDataType = new LinkedHashMap<String, String>();
	public static Map<String, String> ExecutorUpdates = new LinkedHashMap<String, String>();
	public static HashMap<String, ModelInformationUtil> ModelInfo = new HashMap<String, ModelInformationUtil>();
	public static Map<String, testSuit> testSuits = new LinkedHashMap<String, testSuit>();
	public static Map<String, String> SnapDetails = new HashMap();
	public static LinkedHashMap<String, String> EnvironmentMap = new LinkedHashMap<String, String>();
	public Map<String, GlobalParametersUtil> GlobalParameters = new LinkedHashMap<String, GlobalParametersUtil>();
	public static Map<String, GlobalObjectParam> GlobalObjects = new LinkedHashMap<String, GlobalObjectParam>();
	public Map<String, TestWorkflowsUtil> TestWorkflows = new LinkedHashMap<String, TestWorkflowsUtil>();
	public Map<String, PageUtil> Page = new LinkedHashMap<String, PageUtil>();
	public ArrayList<String> testArray = new ArrayList<String>();
	public LoginPage loginPage;

	public XSSFWorkbook ExcelWorkbook;
	public XSSFSheet ExcelSheet;
	public String AccessDataFrom = "";

	public SoftAssert softAssertion = new SoftAssert();
	public int WorkFlowCount = 0;

	public boolean TestPresenceFlag = false;

	String ExecutionStartTimeLiteral;

	String AutomationFlag = "Auto-Maint";
	public String lastModifiedDate = "";

	public GearBox(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
//		super();
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		TestStartDate = testStartDate;
		TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		SourceTestCase = sourceTestCase;
		CurrentTestCase = currentTestCase;
		BrowserName = browserName;
	}

	public GearBox() {
	}

	// Initial Setup
	public void InitalSetup(String TestCaseName, String Browser) {
		System.out.println("Called InitialSetup");

		Date ExecutionStartTime = new Date();
		String ExecutionStartTimePattern = "dd-MM-yyyy HH:mm:ss";
		SimpleDateFormat ExecutionStartTimeFormat = new SimpleDateFormat(ExecutionStartTimePattern);
		ExecutionStartTimeLiteral = ExecutionStartTimeFormat.format(ExecutionStartTime).toString();
		System.out.println(ExecutionStartTimeLiteral);

		TestStartDateDB = new Date();
		SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
		strDateDB = formatter1.format(TestStartDateDB);

		// Killing Existing Task Manager Chrome Exe process
		try {
			Runtime.getRuntime().exec("taskKill /F /IM chromedriver.exe");
			System.out.println("Chrome Driver Process Killed from task Manager");
		} catch (IOException e) {
			e.printStackTrace();
		}

		SecureUtil secure = new SecureUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);

		TestStartDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		strDate = formatter.format(TestStartDate);

		FollowedTestsAccessDataFrom(TestCaseName);

		if (TestPresenceFlag == true) {
			SourceTestCase = TestCaseName;
			CurrentTestCase = getFirstTestCase();
			if (getAccessDataFrom().equalsIgnoreCase("source")) {
				CurrentTestCase = SourceTestCase;
			}
			excelUtil = new ExcelUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
			excelUtil.FollowedTestsRecursions(SourceTestCase);

			Collections.reverse(excelUtil.testArray);
			System.out.println("Test Execution Order : " + excelUtil.testArray);
			// + " - " + excelUtil.testArray.size()
			excelUtil.SetGlobalObjects();
			excelUtil.SetTestWorkflow(); // Adjusted based on source Test case
			excelUtil.SetGlobalParameters();
			excelUtil.GetEnvironmentDetails();
			ValidateDuplicatedPagesInSourceFlow(SourceTestCase, getAccessDataFrom(), "initial");

			lastModifiedDate = excelUtil.GetParameterValue(CurrentTestCase, "LAST_MODIFIED_DATE");

			if (Browser == null) {
				BrowserName = excelUtil.GetParameterValue(SourceTestCase, "BrowserName"); // CurrentTestCase
																							// SourceTestCase
				System.out.println("Browser : " + BrowserName);
			} else {
				BrowserName = Browser;
				System.out.println("Browser : " + BrowserName);
			}
			secure.SecureConfiguration();
			String URL;
			try {
				ImportOSDFlag = excelUtil.GetParameterValue(CurrentTestCase, "ImportOSDFlag");
				if (ImportOSDFlag.equalsIgnoreCase("y")) {
					URL = "https://apposcuat1.avaya.com/OSC/home/DirectLogin";
				} else {
					URL = excelUtil.GetParameterValue(CurrentTestCase, "URL");
				}
				driver = launchURL(BrowserName, URL);
				System.out.println("[" + URL + "] URL Launched in Browser [" + BrowserName + "].");
				int DriverLen = driver.getWindowHandle().length();
				String BrowserDriverHandle = driver.getWindowHandle().substring(DriverLen - 10, DriverLen);
				reportTimeStamp = BrowserDriverHandle;
			} catch (Exception e) {
				OnErrorLoadInReport("ERROR : Observing issue with [" + BrowserName + "] driver access.");
			}

			ApplicationName = excelUtil.GetParameterValue(CurrentTestCase, "ApplicationName");
			CreateSnapsFolder(SourceTestCase);

//		**********************ADD New Object Here*********************************
			PagesConfigSetup(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
		}
	}

	// Final Setup
	public void FinalSetup(String TestCaseName, String Browser) throws Exception {
		System.out.println("Called FinalSetup");
		String SourceTestCase, CurrentTestCase;
		String BrowserName;
		SourceTestCase = TestCaseName;
		CurrentTestCase = SourceTestCase;

		TestEndDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		endDate = formatter.format(TestEndDate);

		if (TestPresenceFlag == true) {
			if (Browser == null) {
				BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
//				System.out.println("Browser : " + BrowserName);
			} else {
				BrowserName = Browser;
//				System.out.println("Browser : " + BrowserName);
			}

			ValidateDuplicatedPagesInSourceFlow(SourceTestCase, getAccessDataFrom(), "final");

			excelReportUtil = new ExcelReportUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);
			emailReportUtil = new EmailReportUtil(driver, excelUtil, loadInReports, testElement, gearbox, TestStartDate, TestEndDate, reportTimeStamp, SourceTestCase, CurrentTestCase, BrowserName);

//			Removed HTMLSingleTestReport and added all content to HTMLTestReport		
//			String HTMLMainContent = emailReportUtil.HTMLSingleTestReport(SourceTestCase).toString();  
			String HTMLContent = emailReportUtil.HTMLTestReport(SourceTestCase, reportTimeStamp).toString();
			String EmailContent = emailReportUtil.EmailTestReport(SourceTestCase, reportTimeStamp).toString();

			FileWriter(SourceTestCase, HTMLContent); // Comment on 18 Jan 2021
			String TestCaseDescription = excelUtil.GetParameterValue(CurrentTestCase, "TestDescription");
			String TestDesigner = excelUtil.GetParameterValue(CurrentTestCase, "TestDesigner");
			String TestExecutor = excelUtil.GetParameterValue(CurrentTestCase, "TestExecutioner");

			String TotalExecutionTime = null;
			try {
				TotalExecutionTime = emailReportUtil.getExecutionTime(strDate, endDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String PassPercent = emailReportUtil.GetTestExecutionPassPercentage();
			AddTestsDetailsToLog(ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, PassPercent, BrowserName);

			if (ApplicationName.equalsIgnoreCase("a1s")) {
				AddGeneralInformationToLog(SourceTestCase);
				AddModelInformationToLog(SourceTestCase);
			}
			excelReportUtil.ExcelPageContents();
			excelReportUtil.ExcelReportCreator(ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, PassPercent, gearbox);

			String ExecutionBlockStatus, TEST_QUOTE_STATUS, hostName = "";
			java.net.InetAddress addr;
			try {
				addr = InetAddress.getLocalHost();
				hostName = addr.getHostName();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (PassPercent.contains("100")) {
				ExecutionBlockStatus = "PASS";
				TEST_QUOTE_STATUS = "Ordered";
			} else {
				ExecutionBlockStatus = "FAIL";
				TEST_QUOTE_STATUS = "Not Ordered";

//				String OutputData = "Quote# : " + getOutput("Quote#") + " | OTN# : " + getOutput("OTN#") + " | SSRID# : " + getOutput("SSRID#") + " | ConfigID# : " + getOutput("ConfigID#") + " | APSQRN# : " + getOutput("APS QRN#") + " | WebRef# : " + getOutput("Web Ref#");
//				System.out.println(OutputData);
//
//				String queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + DBApplicationName + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
//						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + ExecutionBlockStatus + ", TEST_QUOTE_STATUS=" + TEST_QUOTE_STATUS + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral
//						+ ", EXECUTION_END_TIME=" + "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration, LAST_MODIFIED_DATE=" + lastModifiedDate;
//				updateExecutionReportinDataBase("insert", queryData);
			}

//			emailReportUtil.SendEmail(SourceTestCase, reportTimeStamp);
//			DeleteChartsTestFolder(SourceTestCase);
//			CloseWebDriver();
		}

		// driver.close();
		// driver.quit();
		/*
		 * try { Kill_ChromeDriver_GeckoDriver_IEDriverserver(); } catch (IOException e)
		 * {
		 * 
		 * }
		 */
	}

	public void ModelExecution(String TestCaseName, String Browser) throws Exception {
		System.out.println("Called Modular Execution...");
		String WorkflowTestPageValue = null, WorkflowTestPageName;
		String ElementName = null, PageName = null, ObjectType, ObjectIdentification, ActionTaken = null, OnFail, Snapshot, TestCaseValue = null, MarkObject, LoadInReport = null, Wait, PageDescription = null, TestRemarks = null, TestCase = null, IOType = null, PageCurrentTestCase,
				PageSourceTestCase, ElementDescription = "";
		String WSSheetName = null, Output = null, CurrentTestCase = null, SourceTestCase = null;
		String LiveActionPreview = "";
		int TestRowNum = 0;
		int AddRecordIndex = 1;
		LiveActionPreview = excelUtil.getEnvironmentValue("ExecutionLiveActionPreview");

		if (excelUtil.GetAllTestWorkflow().size() > 0) {

			for (Entry<String, TestWorkflowsUtil> TestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
				WorkflowTestPageName = TestWorkflow.getKey().toString();
				WorkflowTestPageValue = TestWorkflow.getValue().getPageValue().toString();
				TestRowNum = TestWorkflow.getValue().getTestRowNum();
				CurrentTestCase = WorkflowTestPageName.split("#")[0];
				SourceTestCase = TestCaseName;
				if (getAccessDataFrom().equalsIgnoreCase("source")) {
					CurrentTestCase = SourceTestCase;
				}

//			String BrowserName;
				if (Browser == null) {
					BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
//					System.out.println("Browser : " + BrowserName);
				} else {
					BrowserName = Browser;
//					System.out.println("Browser : " + BrowserName);
				}

				Date StartDate = new Date();
				SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
				PagestrDate = formatter1.format(StartDate);

				WSSheetName = excelUtil.GetSheetNameForPage(WorkflowTestPageValue);
				DBApplicationName = WSSheetName; // .toUpperCase()
				// Set Page Details for all available Pages present in respective Test case
				// workflow.
				excelUtil.SetPageDetails(WSSheetName, WorkflowTestPageValue, SourceTestCase, CurrentTestCase);

				By retElement;
				String retOutput;
				long timeOut;
				int ElementCount;
				ElementCount = excelUtil.GetPage().size();

				String LogPageDescription = excelUtil.GetTestPageDescription();

				if (!LogPageDescription.isEmpty() && LogPageDescription != "") {
					AddPageTextToLog(SourceTestCase, WSSheetName + " => " + WorkflowTestPageValue + " =>  " + LogPageDescription, AddRecordIndex, BrowserName);
				} else {
					AddPageTextToLog(SourceTestCase, WSSheetName + " => " + WorkflowTestPageValue, AddRecordIndex, BrowserName);
				}

				for (Map.Entry<String, PageUtil> page : excelUtil.GetPage().entrySet()) {
//				ElementName = page.getKey();
					ElementName = page.getValue().getElementName();
					PageSourceTestCase = page.getValue().getSourceTestCase();
					PageCurrentTestCase = page.getValue().getCurrentTestCase();
//				TestCase = page.getValue().getTestCase();
					PageName = page.getValue().getPageName().toString();
					ObjectType = page.getValue().getObjectType().toString();
					ObjectIdentification = page.getValue().getObjectIdentification().toString();
					ActionTaken = page.getValue().getActionTaken().toString();
					OnFail = page.getValue().getOnFail().toString();
					MarkObject = page.getValue().getMarkObject().toString();
					LoadInReport = page.getValue().getLoadInReport().toString();
					Snapshot = page.getValue().getSnapshot().toString();
					Wait = page.getValue().getWait().toString();
					TestCaseValue = page.getValue().getTestCaseValue().toString();
					PageDescription = page.getValue().getPageDescription().toString();
					ElementDescription = page.getValue().getElementDescription().toString();

					// Not sure whether to use source or current test case here, trial needs to do
					testElement.put(PageCurrentTestCase, ElementName);
					ExecutionStatus = "Fail";

					if (!ElementDescription.isEmpty() && ElementDescription != "" && !TestCaseValue.isEmpty() && TestCaseValue != "") {
						AddTextToLog(SourceTestCase, "INFO : " + ElementDescription);
						if (LiveActionPreview.equalsIgnoreCase("yes")) {
							HighlightTestStepsDetails(SourceTestCase + " : " + ElementDescription);
						}
					}

					loadInReports.put(PageCurrentTestCase + "#" + ElementName, new LoadInReports(PageSourceTestCase, PageCurrentTestCase, WSSheetName, WorkflowTestPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, PagestrDate, PageendDate,
							TestRemarks, ElementCount, OnFail, IOType, TestRowNum, ElementDescription));

					timeOut = DynamicWait(Wait);

					if (ObjectType.equalsIgnoreCase("function")) {
						retOutput = getFunction(PageCurrentTestCase, ActionTaken, ObjectIdentification, TestCaseValue, timeOut);
						ExecutionStatus = "Pass";

					} else {
						ActionTaken(PageCurrentTestCase, ElementName, ActionTaken, ObjectIdentification, TestCaseValue, timeOut);
						ExecutionStatus = "Pass";
					}
					PostTestOperations(PageSourceTestCase, PageCurrentTestCase, TestCaseValue, PageName, ElementName, MarkObject, ObjectIdentification, Snapshot, ExecutionStatus, PagestrDate, PageendDate, ElementDescription);
				}

				AddRecordIndex++;
			}
		} else {
			String Message = "Browser Page Accessing Issue...";
			if (driver.findElements(By.xpath("//*[@id='main-message']/h1/span")).size() == 1) {
				Message = "Observing [" + driver.findElement(By.xpath("//*[@id='main-message']/h1/span")).getText() + "] message at [" + BrowserName + "] browser...";
			}
			AddPageTextToLog(TestCaseName, "ERROR : " + " => " + "Page Missing" + " =>  " + Message, AddRecordIndex, BrowserName);
			AddTextToLog(TestCaseName, "Observing Browser Failure, please look into it...");
			OnErrorLoadInReport(Message);
		}

	}

	public void TestSuitCaller() {

		TestExecutionEndDate = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		endExecutionDate = formatter.format(TestExecutionEndDate);

		String HTMLTestSuit = emailReportUtil.HTMLTestSuit().toString();
//		System.out.println(HTMLTestSuit);
		TestSuitCreator(HTMLTestSuit);

		testSuitUtil testSuitUtil = new testSuitUtil();
		testSuitUtil.GettestSuitDetails(reportTimeStamp);

		DeleteFolderContents(SnapshotsPath);
		DeleteFolderFiles(home + "/Downloads/");
	}

	public void CloseWebDriver() {
		try {
			driver.close();
		} catch (Exception e) {
			String BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
			String URL = excelUtil.GetParameterValue(CurrentTestCase, "URL");
			driver = launchURL(BrowserName, URL);
			System.out.println("[" + URL + "] URL Launched in Browser [" + BrowserName + "].");
		}
	}

	public void QuiteWebDriver() {
		driver.quit();
	}

//********************************************************************************************************************************************************************************************

	public synchronized void FollowedTestsAccessDataFrom(String PrimaryTestCaseName) {
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
		int RowCount = ExcelSheet.getPhysicalNumberOfRows();
		String RetrievedTestCaseName = "", RetrievedFollowedTestCaseName = "";
//		boolean TestPresenceFlag = false;

		try {
			for (int ro = 1; ro < RowCount; ro++) {
				RetrievedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(0)).toString();
				RetrievedFollowedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(2)).toString();
				AccessDataFrom = GetCellValue(ExcelSheet.getRow(ro).getCell(3)).toString();
//				System.out.println("RetrievedTestCaseName : " + RetrievedTestCaseName + " - - " + ro);
				if (RetrievedTestCaseName.equalsIgnoreCase(PrimaryTestCaseName)) {

					if ((AccessDataFrom.isEmpty() && AccessDataFrom == "") || AccessDataFrom.equalsIgnoreCase("source")) {
						AccessDataFrom = "source";
					}
					if (AccessDataFrom.equalsIgnoreCase("self")) {
						AccessDataFrom = "self";
					}

					if (RetrievedFollowedTestCaseName != "" && !RetrievedFollowedTestCaseName.isEmpty()) {
						TestDataType.put(RetrievedTestCaseName, AccessDataFrom);
						FollowedTestsAccessDataFrom(RetrievedFollowedTestCaseName);
					} else {
						TestDataType.put(RetrievedTestCaseName, AccessDataFrom);
					}
					TestPresenceFlag = true;
				}
			}
			if (TestPresenceFlag == false) {
				OnErrorLoadInReport("Observing issue with FollowedTestsAccessDataFrom functionality, Please check...");
			}
		} catch (Exception e) {
		}

	}

	public XSSFWorkbook GetWorkBook(String WorkbookPath) {
		try {
			FileInputStream file = new FileInputStream(new java.io.File(WorkbookPath));
			ExcelWorkbook = new XSSFWorkbook(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ExcelWorkbook;
	}

	public Object GetCellValue(Cell cell) {
		Object CellValue = "";
		try {
			switch (cell.getCellType()) {

			case Cell.CELL_TYPE_STRING:
				CellValue = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				CellValue = cell.getNumericCellValue();
				break;
			case Cell.CELL_TYPE_BLANK:
				CellValue = "";
				break;
			default:
				CellValue = "";
				break;
			}
		}

		catch (Exception e) {
			CellValue = "";
		}
		return CellValue;
	}

	public String getFirstTestCase() {
		String FirstTestCase = null;
		int testdatasize = TestDataType.size();
		for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
			FirstTestCase = tesdatatype.getKey();
//			tesdatatype.getValue();
//			break;
		}

		return FirstTestCase;
	}

	public String getSourceTestCase() {
		String SourceTestCase = null;
		int testdatasize = TestDataType.size();
		for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
			SourceTestCase = tesdatatype.getKey();
			tesdatatype.getValue();
			break;
		}

		return SourceTestCase;
	}

	public String getAccessDataFrom() {
		String FirstTestCase = null, AccessDataFrom = null;
		int testdatasize = TestDataType.size();
		try {
			for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
				FirstTestCase = tesdatatype.getKey();
				AccessDataFrom = tesdatatype.getValue();
				break;
			}
		} catch (Exception e) {
			OnErrorLoadInReport("Observing issue with getAccessDataFrom functionality...");
		}

		return AccessDataFrom;
	}

	public void ValidateDuplicatedPagesInSourceFlow(String SourceTestCase, String AccessForm, String LevelofExecution) {
		String FirstTestCase = null, AccessDataFrom = null;
		boolean DuplicateFlag = false;
		String WorkflowOuterTestPageName, WorkflowOuterTestPageValue, WorkflowInnerTestPageName, WorkflowInnerTestPageValue, DuplicatePages = "";
		int Count;

		if (AccessForm.equalsIgnoreCase("source")) {
			for (Entry<String, TestWorkflowsUtil> OuterTestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
				WorkflowOuterTestPageName = OuterTestWorkflow.getKey().toString();
				WorkflowOuterTestPageValue = OuterTestWorkflow.getValue().getPageValue().toString();
				Count = 0;

				for (Entry<String, TestWorkflowsUtil> InnerTestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
					WorkflowInnerTestPageName = InnerTestWorkflow.getKey().toString();
					WorkflowInnerTestPageValue = InnerTestWorkflow.getValue().getPageValue().toString();
					if (WorkflowOuterTestPageValue.equalsIgnoreCase(WorkflowInnerTestPageValue)) {
						Count++;
					}

				} // Inner Loop

				if (Count > 1) {
					if (!DuplicatePages.contains(WorkflowOuterTestPageValue)) {
						DuplicatePages = DuplicatePages + ", " + WorkflowOuterTestPageValue;
					}
				}
			} // Outer Loop

			if (!DuplicatePages.isEmpty() && DuplicatePages != "") {
				String Message = "Source testcase [" + SourceTestCase + "] workflow contains duplicate page operations [" + DuplicatePages.substring(1).trim() + "], Please update workflow pages.";

				if (LevelofExecution.equalsIgnoreCase("initial")) {
					System.out.println(Message);
					Assert.fail(Message);
				} else {
					Assert.fail("");
				}

			}

		}

	}

	public WebDriver launchURL(String BrowserName, String URL) {
		Dimension targetSize = new Dimension(1920, 1080);
		switch (BrowserName.toLowerCase()) {

		case "chrome":
			System.setProperty("webdriver.chrome.driver", ChromeDriverPath);
//			WebDriverManager.chromedriver().setup();
			try {
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("w3c", false);

////				options.addArguments("--headless");
//				options.addArguments("--window-size=1920,1080");
//				options.addArguments("--no-sandbox");
//				options.addArguments("--disable-extensions");
//				options.addArguments("--dns-prefetch-disable");
//				options.addArguments("--disable-gpu");
//				options.setPageLoadStrategy(PageLoadStrategy.NORMAL);

//				options.addArguments("--incognito");
				driver = new ChromeDriver(options);
				driver.manage().window().maximize();
//				driver.manage().window().setSize(targetSize);
				driver.get(URL);
			} catch (Exception e1) {
				System.out.println("Driver Launch Issue...");
//				e1.printStackTrace();
			}
			break;
		case "ie":
			System.setProperty("webdriver.ie.driver", IEDriverPath);
			driver = new InternetExplorerDriver();
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);

		case "edge":
			System.setProperty("webdriver.edge.driver", MSEdgeDriverPath);
			driver = new EdgeDriver();
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);
			break;
		case "firefox":
			System.setProperty("webdriver.gecko.driver", GeckoDriverPath);
			FirefoxOptions options = new FirefoxOptions();
			options.setCapability("marionette", true);
//			options.setLegacy(true);
			driver = new FirefoxDriver(options);
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);
			break;
		case "mobilechrome":
			DesiredCapabilities cap = new DesiredCapabilities();
			// Browser Capability
			cap.setCapability("chromedriverExecutable", "C:\\B2BAvayaEnterpriseStore\\driver\\chromedriver\\chromedriver.exe");
			cap.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "vivo 1601");
			cap.setCapability(MobileCapabilityType.UDID, "7HJJ9S5T99999999"); // 192.168.1.203:5555 //7HJJ9S5T99999999
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("w3c", false);
			cap.merge(chromeOptions);

			URL url;
			try {
				url = new URL("http://0.0.0.0:4723/wd/hub");
				driver = new AndroidDriver<MobileElement>(url, cap);
				System.out.println("URL Access");
				driver.navigate().to("https://channelstore-stg.avaya.com/aes");
				driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
				System.out.println("Site Access");

			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			break;

		default:
			System.out.println("Browser [" + BrowserName + "] is not in list, Please validate...");
			Assert.fail("Browser [" + BrowserName + "] is not in list, Please validate...");
		}

		if (driver != null)
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

	public void TestExecutionStartTime() {
		TestExecutionStartDate = new Date();
		SimpleDateFormat Exeformatter = new SimpleDateFormat("HH:mm:ss");
		strExecutionDate = Exeformatter.format(TestExecutionStartDate);

	}

	public void DeleteLocalFiles() {
		File localPath = new File(localfolder);
		for (File file : localPath.listFiles()) {
			if (!file.isDirectory())
				file.delete();
		}
	}

	public String getCurrentTestCase() {
		return CurrentTestCase;
	}

	public void setCurrentTestCase(String currentTestCase) {
		CurrentTestCase = currentTestCase;
	}

	public void PagesConfigSetup(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		loginPage = new LoginPage(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		
	}

	public WebDriver launchURLOldest(String BrowserName, String URL) {
		Dimension targetSize = new Dimension(1920, 1080);
		switch (BrowserName.toLowerCase()) {

		case "chrome":
			System.setProperty("webdriver.chrome.driver", ChromeDriverPath);
			try {
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("w3c", false);
				options.addArguments("--incognito");
				driver = new ChromeDriver(options);
//				driver.manage().window().maximize();
				driver.manage().window().setSize(targetSize);
				driver.get(URL);
			} catch (Exception e1) {
//				System.out.println("Driver Launch Issue...");
				AddTextToLog(CurrentTestCase, "Driver Launch Issue...");
				e1.printStackTrace();
			}
			break;
		case "ie":
			System.setProperty("webdriver.ie.driver", IEDriverPath);
			driver = new InternetExplorerDriver();
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);

		case "edge":
			System.setProperty("webdriver.edge.driver", MSEdgeDriverPath);
			driver = new EdgeDriver();
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);
			break;
		case "firefox":
			System.setProperty("webdriver.gecko.driver", GeckoDriverPath);
			FirefoxOptions options = new FirefoxOptions();
			options.setCapability("marionette", true);
//			options.setLegacy(true);
			driver = new FirefoxDriver(options);
//			driver.manage().window().maximize();
			driver.manage().window().setSize(targetSize);
			driver.get(URL);
			break;
		case "mobilechrome":
			DesiredCapabilities cap = new DesiredCapabilities();
			// Browser Capability
			cap.setCapability("chromedriverExecutable", "C:\\B2BAvayaEnterpriseStore\\driver\\chromedriver\\chromedriver.exe");
			cap.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "vivo 1601");
			cap.setCapability(MobileCapabilityType.UDID, "7HJJ9S5T99999999"); // 192.168.1.203:5555 //7HJJ9S5T99999999
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("w3c", false);
			cap.merge(chromeOptions);

			URL url;
			try {
				url = new URL("http://0.0.0.0:4723/wd/hub");
				driver = new AndroidDriver<MobileElement>(url, cap);
				System.out.println("URL Access");
				driver.navigate().to("https://channelstore-stg.avaya.com/aes");
				driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
				System.out.println("Site Access");

			} catch (MalformedURLException e) {
				e.printStackTrace();
			}

			break;

		default:
			AddTextToLog(CurrentTestCase, "Browser [" + BrowserName + "] is not in list, Please validate...");
//			System.out.println("Browser [" + BrowserName + "] is not in list, Please validate...");
			Assert.fail("Browser [" + BrowserName + "] is not in list, Please validate...");
		}

		if (driver != null)
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

	public String getText(String fieldName) {
		By locator = null;
		locator = getPageObject(fieldName);
		String result = null;
		try {
			result = driver.findElement(locator).getText().trim();
		} catch (Exception e) {
//			LOGGER.error("Error occured while getting text from " + fieldName);
		}
		return result;
	}

	public void printConsole(String message) {
		System.out.println(message);
	}

	public void killBrowserInstance(WebDriver driver) {
		try {
			driver.quit();
//			LOGGER.info("Browser Instance successfully closed");
		} catch (Exception e) {
//			LOGGER.error("Error occured while closing webdriver instance: " + e);
			Assert.fail("Error occured while closing webdriver instance: " + e);
		}
	}

	public By getPageObject(String objPath) {
		By ret = null;
		String key = null, value = null;
		String CallFlag = null;

		try {
			if (!objPath.isEmpty() && objPath != "") {
				if (objPath.contains(":=") || objPath.contains("//")) {
				} else {
					objPath = getObject(objPath);
				}

				if (objPath.contains(":=")) {
					key = objPath.split(":=")[0].trim().toLowerCase();
					value = objPath.split(":=")[1].trim();
					CallFlag = "objectpath";
				} else if (objPath.contains("//")) {
					key = "xpath";
					value = objPath.trim();
					CallFlag = "objectpath";
				} else {
					// Invalid Path Format
//					System.out.println("Invalid Object Identificatin Path Format." + objPath);
					OnErrorLoadInReport("Invalid Object Identificatin Path Format." + objPath);
				}
			} else {
				// Path is not defined
//				System.out.println("Object is Missing, Please Add it.");
				OnErrorLoadInReport("Object is Missing, Please Add it.");
				Assert.fail("Object is Missing, Please Add it.");
			}

			if (CallFlag.equalsIgnoreCase("objectpath")) {

				switch (key.toLowerCase()) {
				case "class":
					ret = By.className(value);
					break;
				case "css":
					ret = By.cssSelector(value);
					break;
				case "id":
					ret = By.id(value);
					break;
				case "link":
					ret = By.linkText(value);
					break;
				case "name":
					ret = By.name(value);
					break;
				case "partiallink":
					ret = By.partialLinkText(value);
					break;
				case "tagname":
					ret = By.tagName(value);
					break;
				case "xpath":
					ret = By.xpath(value);
					break;
				}
			}
		} catch (Exception e) {
			AddTextToLog(SourceTestCase, objPath + " Object is Missing, Please Add it.");
			OnErrorLoadInReport(objPath + " Object is Missing, Please Add it.");
			Assert.fail(objPath + " Object is Missing, Please Add it.");
		}

		return ret;
	}

	public synchronized String getFunction(String TestCase, String FunctionName, String ObjectPath, String Value, long timeOut) throws Exception {
		CurrentTestCase = TestCase;
//		System.out.println("Functional TestCase  " + TestCase + " HashCode " + driver.hashCode());
		loginPage.setCurrentTestCase(CurrentTestCase);

		String Output = null;
		if (!ObjectPath.isEmpty() && ObjectPath != "") {
			waitElementPresence(driver, ObjectPath, timeOut);
		} else {
			driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
		}

		if (!Value.isEmpty() && Value != "") {
			AddIOData(CurrentTestCase, "Input", FunctionName, Value);
		}

		if (!FunctionName.isEmpty() && FunctionName != "") {

			switch (FunctionName) {
			case "ApplicationsDatabaseUpdate":
				ApplicationsDatabaseUpdate(Value);
				break;
			case "LoginAuthentication":
				loginPage.LoginAuthentication(Value);
				break;
			case "LoginWithLaunch":
				loginPage.LoginWithLaunch(Value);
				break;
			case "ClickOkBtn":
				ClickOkBtn();
				break;
			case "SwitchToNextWindow":
				SwitchToNextWindow(Value);
				break;
			case "SwitchToDefaultWindow":
				SwitchToDefaultWindow(Value);
				break;
			case "wait":
				wait(ObjectPath, Value);
				break;
			case "CloseBrowserWindow":
				CloseBrowserWindow(Value);
				break;
			case "GoogleSearchOperation":
				loginPage.GoogleSearchOperation();
				break;

			default:
				ExecutionStatus = "Fail";
				break;
			}
		} else {
			// Function Name Missing
			AddTextToLog(CurrentTestCase, "Function Name Missing.");
//			System.out.println("Function Name Missing.");
		}

		return ExecutionStatus;
	}

	synchronized public void LoginWithLaunch(String PageName) {
	}

	synchronized public void LoginAuthentication(String PageName) {
	}

	public String getConfigValue(String key) {
		Properties props = new Properties();
		File file = new File(".//data//Input_Data.properties");
		FileReader fileR = null;
		String retVal = null;
		try {
			fileR = new FileReader(file);
			props.load(fileR);
			retVal = props.getProperty(key);
//			LOGGER.debug("Key: " + key + " " + "Value: " + retVal);

			fileR.close();
		} catch (Exception e) {
//			LOGGER.error("Error occured while getting value from propertie file: " + e);
			Assert.fail("Error occured while getting value from propertie file: " + e);
		}
		return retVal;
	}

	public synchronized void setText(String objName, String value) {
		try {
			By locator;
			if (objName.contains("//")) {

				if (objName.toLowerCase().contains(":=")) {
					objName = objName.split(":=")[1];
					waitElementVisible(driver, objName);
					driver.findElement(By.xpath(objName)).clear();
					driver.findElement(By.xpath(objName)).sendKeys(value);
					driver.findElement(By.xpath(objName)).sendKeys(Keys.TAB);
				} else {
					waitElementVisible(driver, objName);
					driver.findElement(By.xpath(objName)).clear();
					driver.findElement(By.xpath(objName)).sendKeys(value);
					driver.findElement(By.xpath(objName)).sendKeys(Keys.TAB);
				}
			} else {
				locator = getPageObject(objName);
				waitElementVisible(driver, objName);
				driver.findElement(locator).clear();
				driver.findElement(locator).sendKeys(value);
				driver.findElement(locator).sendKeys(Keys.TAB);
			}

			if (objName.equalsIgnoreCase("password") || objName.toLowerCase().contains("password")) {
				value = "*********";
			}
			AddTextToLog(SourceTestCase, objName + " The text: [" + value + "] entered successfully");
			ExecutionStatus = "Pass";

		} catch (Exception e) {
			ExecutionStatus = "Fail";
//			System.out.println("Error occured while entering value in textbox: " + e);
			OnErrorLoadInReport("Error occured while entering value in textbox: " + objName);
		}
	}

	public synchronized void clickButton(String objName) {
		try {
			By locator;
			if (objName.contains("//")) {
				if (objName.toLowerCase().contains(":=")) {
					objName = objName.split(":=")[1];
					waitElementVisible(driver, objName);
					driver.findElement(By.xpath(objName)).click();
				} else {
					waitElementVisible(driver, objName);
					driver.findElement(By.xpath(objName)).click();
				}
			} else {
				waitElementVisible(driver, objName);
				locator = getPageObject(objName);
				driver.findElement(locator).click();
			}
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Button successfully clicked");
		} catch (Exception e) {
			ExecutionStatus = "Fail";
			e.printStackTrace();
			OnErrorLoadInReport("Error occured while clicking button: " + objName);
		}
	}

	public synchronized void pressTAB(String objName) {
		try {
			wait(2);
			By locator;
			locator = getPageObject(objName);
			driver.findElement(locator).sendKeys(Keys.TAB);
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, "Pressed TAB Succesfully");

		} catch (Exception e) {
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Error occured while pressing TAB Key: " + objName);
		}
	}

	public boolean selectRadioButton_WE(String objName, String key, String value) {

		try {
			WebElement ele = driver.findElement(By.xpath("")); // getPageObjectReplace(objName, key, value)
			if (!ele.isSelected()) {
				ele.click();
				ExecutionStatus = "Pass";
//				LOGGER.info(objName + " has been selected");
//				test.log(LogStatus.PASS, "EXPECTECD: Element " + objName + " should be selected",
//						"Usage: <span style='font-weight:bold;'>ACTUAL:: " + objName + " has been selected</span>");
			} else {
				ExecutionStatus = "Pass";
//				LOGGER.info(objName + " radio button already checked");
//				test.log(LogStatus.INFO, "EXPECTECD: Element " + objName + " already checked",
//						"Usage: <span style='font-weight:bold;'>ACTUAL:: " + objName + " already checked</span>");
			}
			return true;

		} catch (Exception e) {
			ExecutionStatus = "Fail";
//			ErrorMessage = "Selecting radio button failed, Selecting radio button failed  for  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			getScreenShot("selecting radio button " + objName);
//			LOGGER.error("Selecting radio button failed..." + e);
//			LOGGER.error("Selecting radio button failed..." + e);
//			test.log(LogStatus.FAIL, "Selecting radio button failed", "Selecting radio button failed  because  -" + e);
			Assert.fail("Selecting radio button failed..." + e);
			return false;
		}
	}

	public synchronized boolean selectDropDownByVisibleText(String objName, String value) {

		try {
			if (value != null && value.trim().length() > 0) {
				WebElement supplierName = null;
				if (objName.contains("//")) {
					if (objName.toLowerCase().contains(":=")) {
						objName = objName.split(":=")[1];
						waitElementVisible(driver, objName);
						supplierName = driver.findElement(By.xpath(objName));
					} else {
						waitElementVisible(driver, objName);
						supplierName = driver.findElement(By.xpath(objName));
					}
				} else {
					supplierName = driver.findElement(getPageObject(objName));
				}

				boolean isOptionFound = false;
				Select oSelect = new Select(supplierName);
				List<WebElement> optionelems = oSelect.getOptions();
				for (int i = 0; i < optionelems.size(); i++)
					// if(optionelems.get(i).getText().trim().equals(value.trim()))
					if (optionelems.get(i).getText().trim().contains(value.trim())) {
						wait(1);
						oSelect.selectByIndex(i);
						isOptionFound = true;
						break;
					}
				if (isOptionFound) {
//					System.out.println("Value -'" + value + "'- selected from Drop down " + objName + " sucessfully");
					AddTextToLog(SourceTestCase, objName + " The option: [" + value + "] selected successfully");
					ExecutionStatus = "Pass";
				} else {
					ExecutionStatus = "Fail";
//					System.out.println("Unable to find Value -'" + value + "'- from Drop down " + objName);
					OnErrorLoadInReport("Unable to find Value -'" + value + "'- from Drop down " + objName);
				}
			}
			return true;
		} catch (Exception e) {
			OnErrorLoadInReport("Dropdown selection failed..." + objName);
//			Assert.fail("Dropdown selection failed..." + e);
			return false;
		}
	}

	public synchronized void GetElementText(String CurrentTestCase, String ElementName, String objName, String testCaseValue) {
		String value = null;
		int ElementCount = driver.findElements(getPageObject(objName)).size();

		for (int l = 1; l <= 3; l++) {
			if (ElementCount == 1) {
				WebElement ElementObj = driver.findElement(getPageObject(objName));
				value = ElementObj.getText();

				if (value != null && value.trim().length() > 0 && value != "") {
					if (testCaseValue.equalsIgnoreCase("get")) {
						AddIOData(CurrentTestCase, "output", ElementName, value);
					} else {
						AddIODataWithLink(CurrentTestCase, "output", ElementName, value, testCaseValue);
					}
					AddTextToLog(SourceTestCase, "Retrieved [" + ElementName + "] value is [" + value + "]");
					break;
				} else {
					if (testCaseValue.equalsIgnoreCase("get")) {
						AddIOData(CurrentTestCase, "output", ElementName, value);
					} else {
						AddIODataWithLink(CurrentTestCase, "output", ElementName, value, testCaseValue);
					}
				}
			} else {
				AddIOData(CurrentTestCase, "output", ElementName, "Incorrect Object Path");
				AddTextToLog(SourceTestCase, ElementName + " provided incorrect object path, unable to retrieve value.");
			}

		}
	}

	public void clickLink(String objName) {
		try {
			By locator;
			locator = getPageObject(objName);
			driver.findElement(locator).click();
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Link successfully clicked");

		} catch (Exception e) {
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Error occured while clicking link: " + objName);
		}
	}

	public void mouseHover(String objName) {
		try {

			WebElement elem = driver.findElement(getPageObject(objName));
			Actions actions = new Actions(driver);
			actions.moveToElement(elem).build().perform();
//			LOGGER.info(objName + " element is clicked");
//			test.log(LogStatus.PASS, objName + " element is clicked");

		} catch (Exception e) {
//			ErrorMessage = "Exception occured while looking for element :  [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.info("Exception occured while looking for element : " + objName + "  - " + e);
//			LOGGER.error("Exception occured while looking for element : " + objName + "  - " + e);
//			test.log(LogStatus.FAIL, objName + " element is not clicked");

		}
	}

	public void mouseHoverAndClick(String objName) {
		try {

			WebElement elem = driver.findElement(getPageObject(objName));
			Actions actions = new Actions(driver);
			actions.moveToElement(elem).pause(1000).click().perform();
//			LOGGER.info(objName + " element is clicked");
//			test.log(LogStatus.PASS, objName + " element is clicked");

		} catch (Exception e) {
//			LOGGER.error("Exception occured while looking for element : " + objName + "  - " + e);
//			test.log(LogStatus.FAIL, objName + " element is not clicked");

		}
	}

	public void wait(int seconds) {
		try {
			int sec = seconds * 1000;
			Thread.sleep(sec);
		} catch (Exception e) {
//			LOGGER.error("Error Occured while wait");
//			test.log(LogStatus.FAIL, "Error Occured while wait" + e);
		}
	}

//	public void getScreenShot(String testStep) {
//		try {
//			String screenshotB64 = "data:image/png;base64,"
//					+ ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
//
////			test.log(LogStatus.INFO, "Snapshot for  " + testStep + "  : " + test.addBase64ScreenShot(screenshotB64));
//
//		} catch (Exception e) {
////			LOGGER.info("ERROR IN SCREENSHOT." + e);
//			Assert.fail("ERROR IN SCREENSHOT." + e);
//		}
//	}

	public WebDriver switchToWindow(WebDriver driver) {

		try {

			String parentWindowHandle = driver.getWindowHandle();
			Set<String> windowId = driver.getWindowHandles();

			for (String id : windowId) {

				if (!id.equalsIgnoreCase(parentWindowHandle)) {
					driver.switchTo().window(id);
				}
//				LOGGER.info("Window Switched successfully");
			}

		} catch (Exception e) {
//			LOGGER.error("Error Occured while switching window: " + e);
		}
		return driver;
	}

//	public WebElement fluentWait(WebDriver driver, String objName, long timeout, long polling) {
//		WebElement element = null;
//		// try {
//		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
//				.withTimeout(TimeUnit)
//				.pollingEvery(polling, TimeUnit.SECONDS) // set the interval between every 2 tries
//				.ignoring(NoSuchElementException.class); // don't throw this exception
//		// Then - declare the webElement and use a function to find it
//		try {
//			element = wait.until(new Function<WebDriver, WebElement>() {
//				public WebElement apply(WebDriver driver) {
//					return driver.findElement(getPageObject(objName));
//				}
//			});
//		} catch (TimeoutException TE) {
//			System.out.println(TE.getMessage());
////			LOGGER.error(TE.getMessage());
//			throw TE;
//		}
//		return element;
//
//	}

//	public WebElement fluentWait(WebDriver driver, String objName) {
//		WebElement element = null;
//		// try {
//		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS) // set the
//																											// timeout
//				.pollingEvery(3, TimeUnit.SECONDS) // set the interval between every 2 tries
//				.ignoring(NoSuchElementException.class); // don't throw this exception
//		// Then - declare the webElement and use a function to find it
//		try {
//			element = wait.until(new Function<WebDriver, WebElement>() {
//				public WebElement apply(WebDriver driver) {
//					return driver.findElement(getPageObject(objName));
//				}
//			});
//		} catch (TimeoutException TE) {
//			System.out.println(TE.getMessage());
//			LOGGER.error(TE.getMessage());
//			throw TE;
//		}
//		return element;
//	}

	public WebElement waitElementClickable(WebDriver driver, String objName, long timeout) {
		WebElement element = null;
		try {
			By locator;
			locator = getPageObject(objName);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			element = wait.until(ExpectedConditions.elementToBeClickable(locator));
			AddTextToLog(SourceTestCase, objName + " :Element is clickable");
//			LOGGER.info(objName + ":Element is clickable");
//			test.log(LogStatus.PASS, objName + ":Element is clickable");

		} catch (Exception e) {
//			LOGGER.error("Error occured while waiting for Element to be clickable " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be clickable  " + objName + ": " + e);
			OnErrorLoadInReport("Error occured while waiting for Element to be clickable.");
//			Assert.fail("Error occured while waiting for Element to be clickable : " + e);
		}
		return element;

	}

	public WebElement waitElementClickable(WebDriver driver, String objName) {
		WebElement element = null;
		try {
			By locator;
			if (objName.contains("//")) {
				WebDriverWait wait = new WebDriverWait(driver, 150);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				AddTextToLog(SourceTestCase, objName + ":Element is clickable");

			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, 150);
				element = wait.until(ExpectedConditions.elementToBeClickable(locator));
				AddTextToLog(SourceTestCase, objName + " :Element is clickable");
//			LOGGER.info(objName + ":Element is clickable");
//			test.log(LogStatus.PASS, objName + ":Element is clickable");
			}

		} catch (Exception e) {
//			LOGGER.error("Error occured while waiting for Element to be clickable " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be clickable  " + objName + ": " + e);
			OnErrorLoadInReport("Error occured while waiting for Element to be clickable : " + objName);
//			Assert.fail("Error occured while waiting for Element to be clickable : " + e);
		}
		return element;
	}

	public WebElement waitElementVisible(WebDriver driver, String objName, long timeout) {
		WebElement element = null;
		try {
			By locator;
			if (objName.contains("//")) {
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				AddTextToLog(SourceTestCase, objName + ":Element is visible");

			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
				AddTextToLog(SourceTestCase, objName + ":Element is visible");
			}

		} catch (Exception e) {
			OnErrorLoadInReport("Error occured while waiting for Element to be visible : " + e);
//			Assert.fail("Error occured while waiting for Element to be visible : " + e);
		}
		return element;

	}

	public WebElement waitElementVisibleWithoutTermination(WebDriver driver, String objName, long timeout) {
		WebElement element = null;
		try {
			By locator;
			if (objName.contains("//")) {
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				AddTextToLog(SourceTestCase, objName + ":Element is visible");

			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
				AddTextToLog(SourceTestCase, objName + ":Element is visible");
			}

		} catch (Exception e) {
			AddTextToLog(SourceTestCase, objName + ":Element is missing.");
//			OnErrorLoadInReport("Error occured while waiting for Element to be visible : " + e);
//			Assert.fail("Error occured while waiting for Element to be visible : " + e);
		}
		return element;

	}

	public WebElement waitElementPresence(WebDriver driver, String objName, long timeout) {
		WebElement element = null;
		try {
			By locator;
			locator = getPageObject(objName);
			WebDriverWait wait = new WebDriverWait(driver, timeout);
			element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));
//			System.out.println(objName + ":Element is visible");
			ExecutionStatus = "Pass";
//			LOGGER.info(objName + ":Element is visible");
//			test.log(LogStatus.PASS, objName + ":Element is visible");
			AddTextToLog(SourceTestCase, objName + ":Element is visible");

		} catch (Exception e) {
			ExecutionStatus = "Fail";
//			System.out.println("Error occured while waiting for Element to be visible : " + e);
			OnErrorLoadInReport("Error occured while waiting for Element to be visible : " + objName);
//			ErrorMessage = "Error occured while waiting for Element to be visible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be visible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be visible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be visible : " + e);
		}
		return element;

	}

	public WebElement waitElementVisible(WebDriver driver, String objName) {
		WebElement element = null;
		try {
			By locator;
			if (objName.contains("//")) {
				if (objName.toLowerCase().contains(":=")) {
					objName = objName.split(":=")[1];
					WebDriverWait wait = new WebDriverWait(driver, 150);
					element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				} else {
					WebDriverWait wait = new WebDriverWait(driver, 150);
					element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				}

			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, 150);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			}

			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Element is visible");

		} catch (Exception e) {
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Error occured while waiting for Element to be visible : " + objName);
		}
		return element;
	}

	public WebElement waitElementVisibleWithoutTermination(WebDriver driver, String objName) {
		WebElement element = null;
		try {
			By locator;
			if (objName.contains("//")) {
				if (objName.toLowerCase().contains(":=")) {
					objName = objName.split(":=")[1];
					WebDriverWait wait = new WebDriverWait(driver, 150);
					element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				} else {
					WebDriverWait wait = new WebDriverWait(driver, 150);
					element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(objName)));
				}

			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, 150);
				element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			}

			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Element is visible");

		} catch (Exception e) {
//			ExecutionStatus = "Fail";
//			OnErrorLoadInReport("Error occured while waiting for Element to be visible : " + objName);
		}
		return element;
	}

	public void waitElementInvisible(WebDriver driver, String objName, long timeout) {
		try {
			By locator;

			if (objName.contains("//")) {
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(objName)));
			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, timeout);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			}
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Element is invisible");

		} catch (Exception e) {
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Error occured while waiting for Element to be invisible : " + objName);
//			ErrorMessage = "Error occured while waiting for Element to be invisible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be invisible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be invisible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be invisible : " + e);
		}
	}

	public void waitElementInvisible(WebDriver driver, String objName) {
		try {
			By locator;
			if (objName.contains("//")) {
				WebDriverWait wait = new WebDriverWait(driver, 150);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(objName)));
			} else {
				locator = getPageObject(objName);
				WebDriverWait wait = new WebDriverWait(driver, 200);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
			}
			ExecutionStatus = "Pass";
			AddTextToLog(SourceTestCase, objName + ":Element is invisible");
		} catch (Exception e) {
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Error occured while waiting for Element to be invisible : " + objName);
		}
	}

	public WebDriver switchToIFrame(WebDriver driver) {
		driver.switchTo().frame(1);
		return driver;
	}

	public String getKey(Map<String, String> map, String value) {

		Set<String> keys = map.keySet();
		String res = null;
		for (String myKeys : keys) {
			if (myKeys.equalsIgnoreCase(value)) {
				res = myKeys;
			}
		}
		return res;
	}

	public synchronized void ActionTaken(String TestCase, String ElementName, String actionTaken, String ObjectPath, String testCaseValue, long timeout) {
		CurrentTestCase = TestCase;

		if (!testCaseValue.isEmpty() && testCaseValue != "") {

			int ObjCount = driver.findElements(getPageObject(ObjectPath)).size();
			if (ObjCount == 0) {
				waitElementPresence(driver, ObjectPath, timeout);
			}

			if (ElementName.equalsIgnoreCase("Remote_Access_Method")) {
				System.out.println();
			}

			switch (actionTaken.trim().replaceAll(" ", "").toLowerCase()) {
			case "set":
				waitElementPresence(driver, ObjectPath, timeout);
				setText(ObjectPath, testCaseValue);
				AddIOData(CurrentTestCase, "Input", ElementName, testCaseValue);
				break;
			case "click":
				waitElementPresence(driver, ObjectPath, timeout);
				clickButton(ObjectPath);
				break;
			case "select":
				waitElementPresence(driver, ObjectPath, timeout);
				selectDropDownByVisibleText(ObjectPath, testCaseValue);
				AddIOData(CurrentTestCase, "Input", ElementName, testCaseValue);
				break;
			case "get":
				waitElementPresence(driver, ObjectPath, timeout);
				GetElementText(CurrentTestCase, ElementName, ObjectPath, testCaseValue);
				break;
			default:
				AddTextToLog(CurrentTestCase, ElementName + " - Invalid Input, Please provide proper input...");
//				System.out.println(ElementName + " - Invalid Input, Please provide proper input...");
				break;
			}
		}

		wait(2);
	}

	public void FunctionCaller() {
		System.out.println("Able to call FunctionCaller from Config Util Class");
	}

	public void SwitchToNextWindow(String Window) {

		for (int d = 1; d <= 20; d++) {
			String currentHandle = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				if (!handle.equalsIgnoreCase(currentHandle)) {
					driver.switchTo().window(handle);
				}
			}
			if (Window.equalsIgnoreCase(driver.getTitle())) {
//				System.out.println(Window + " Page Activated...");
				AddTextToLog(SourceTestCase, Window + " Page Activated...");
				ExecutionStatus = "Pass";
				break;
			} else {
//				System.out.println(Window + " Page Deactivated...");
				OnErrorLoadInReport(Window + " Page Deactivated...");
			}
		}

	}

	public void SwitchToDefaultWindow(String Window) {

		for (int d = 1; d <= 20; d++) {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			if (Window.equalsIgnoreCase(driver.getTitle())) {
//				System.out.println("[" + Window + "] Page Activated.");
				AddTextToLog(CurrentTestCase, "[" + Window + "] Page Activated.");
				ExecutionStatus = "Pass";
				break;
			} else {
//				System.out.println("[" + Window + "] Page Deactivated.");
				OnErrorLoadInReport("[" + Window + "] Page Deactivated.");
			}
		}
	}

	public void wait(String objPath, String value) {
		long timeout = (long) Double.parseDouble(value);
		waitElementVisible(driver, objPath, timeout);
	}

	public void CloseBrowserWindow(String Window) {

		for (int d = 1; d <= 20; d++) {
			String currentHandle = driver.getWindowHandle();
			for (String handle : driver.getWindowHandles()) {
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				if (handle.equalsIgnoreCase(currentHandle)) {
					driver.switchTo().window(handle);
					ExecutionStatus = "Pass";
				}
			}
			if (Window.equalsIgnoreCase(driver.getTitle())) {
//				System.out.println(Window + " Browser Tab Closed.");
				AddTextToLog(CurrentTestCase, Window + " Browser Tab Closed.");
				driver.close();
				AddTextToLog(SourceTestCase, Window + " Browser Tab Closed.");
				for (String handle : driver.getWindowHandles()) {
					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					if (!handle.equalsIgnoreCase(currentHandle)) {
						driver.switchTo().window(handle);
//						System.out.println("Switched to Next Window.");
						AddTextToLog(SourceTestCase, "Switched to Next Window " + driver.getTitle());
						ExecutionStatus = "Pass";
					}
				}

				break;
			} else {
//				System.out.println(Window + " Browser Tab Unable to Close.");
				OnErrorLoadInReport(Window + " Browser Tab Unable to Close.");
			}
		}
	}

	public void HoverAndSelect(String ObjectPath, String Selection) {

	}

	public void ClickOkBtn() {
		String OkPath = "xpath:=//*[text()='OK']";
		boolean flag = true;
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		try {
			for (int l = 1; l <= 7; l++) {
				if (driver.findElements(getPageObject(OkPath)).size() > 0 && flag) {
					WebDriverWait wait = new WebDriverWait(driver, 50);
					wait.until(ExpectedConditions.elementToBeClickable(getPageObject(OkPath))).click();
					waitElementInvisible(driver, OkPath, 20);
//					System.out.println("Ok Button Clicked..." + l);
					AddTextToLog(CurrentTestCase, "Ok Button Clicked..." + l);
					if (driver.findElements(getPageObject(OkPath)).size() == 0) {
						flag = false;
					}
				}
			}
		} catch (Exception e) {
			ExecutionStatus = "Fail";
//			System.out.println("Unable to Click Ok Button from Create Quote Page.");
			OnErrorLoadInReport("Unable to Click Ok Button from Create Quote Page.");
		}

		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	}

	public void OnErrorLoadInReport(String ErrorMessage) {

//		if (loadInReports.size() == 0) {
////			AddPageTextToLog(SourceTestCase, "ERROR", 1, "Test Case [" + SourceTestCase + "] missing.");
////			AddTextToLog(SourceTestCase, ErrorMessage);
//			loadInReports.put(CurrentTestCase + "#" + "TestCaseMissing",
//					new LoadInReports(SourceTestCase, CurrentTestCase, "NA", "NA", "Fail", "Test Case Missing", "", "", "", "", "", PagestrDate, PageendDate, "Test Case not available in KITE, Please check", 0, "", "", 1, ""));
//			Assert.fail(ErrorMessage);
//		}

		String WSSheetName = null, WorkflowPageValue = null, PageDescription = null, LoadInReport = null, ElementName = null, ActionTaken = null, TestCaseValue = null, Output = null, PagestrDate = null, PageendDate = null, TestRemarks = null, OnFail = null, TestCase = null, IOType = null,
				SourceTestCase = null, CurrentTestCase, ElementDescription = "", ErrMsg;
		int ElementCount = 0;
		int TestRowNum = 0;
		Date EndDate = new Date();
		SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
		PageendDate = formatter2.format(EndDate);
		ExecutorUpdates.put(reportTimeStamp + "#" + "TestRemarks", ErrorMessage);

//		if (RecoveryScenarions().isEmpty() || RecoveryScenarions() == "") {
//			ErrorMessage = RecoveryScenarions();
//		}

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
//			System.out.println("loadInReport.getValue().getExecutionStatus() : " + loadInReport.getValue().getExecutionStatus());
			if (loadInReport.getValue().getExecutionStatus().equalsIgnoreCase("Fail")) {
				// System.out.println("Perform Operations");
//				TestCase = loadInReport.getValue().getTestCase();
				SourceTestCase = loadInReport.getValue().getSourceTestCase();
				CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
				WSSheetName = loadInReport.getValue().getSheetName();
				WorkflowPageValue = loadInReport.getValue().getPageName();
				PageDescription = loadInReport.getValue().getTestDescription();
				LoadInReport = "Yes";// loadInReport.getValue().getLoadInReport();
				ElementName = loadInReport.getValue().getElementName();
				ActionTaken = loadInReport.getValue().getActionTaken();
				TestCaseValue = loadInReport.getValue().getTestCaseValue();
				Output = loadInReport.getValue().getOutput();
				PagestrDate = loadInReport.getValue().getExecutionStartTime();
				OnFail = loadInReport.getValue().getOnFail();
				TestRemarks = ErrorMessage;
				ElementCount = loadInReport.getValue().getPageElementsCount();
				TestRowNum = loadInReport.getValue().getTestRowNum();
				ElementDescription = loadInReport.getValue().getElementDescription();
				ExecutionStatus = "Fail";

				loadInReports.put(CurrentTestCase + "#" + ElementName, new LoadInReports(SourceTestCase, CurrentTestCase, WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, PagestrDate, PageendDate, TestRemarks,
						ElementCount, OnFail, IOType, TestRowNum, ElementDescription));
				ErrMsg = "Assertion Failed for page [" + WorkflowPageValue + "] with element [" + ElementName + "] at [" + ActionTaken + "], Test Execution Terminated...";

				try {
					if (OnFail.equalsIgnoreCase("exit") || OnFail.equalsIgnoreCase("") || OnFail == null) {
						OnFail = "";
//						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription);
//						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription + " | " + ErrMsg);
						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription + " | " + ErrorMessage);
//						getSnapShot(WSSheetName, WorkflowPageValue);
						wait(2);
						getSnapShot(WorkflowPageValue, ErrorMessage);
						AddTextToLog(SourceTestCase, "ERROR : " + ErrorMessage);
						AddTextToLog(SourceTestCase, "ERROR : " + ErrMsg);
						Assert.fail("ERROR : " + ErrMsg);
					} else {
						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription + " | " + ErrorMessage);
						wait(2);
						getSnapShot(WorkflowPageValue, ErrorMessage);
					}
				} catch (Exception e) {
					System.out.println("Exception : " + e);
					if (OnFail.equalsIgnoreCase("exit") || OnFail.equalsIgnoreCase("") || OnFail == null) {
						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription + " | " + ErrorMessage);
						wait(2);
						getSnapShot(WorkflowPageValue, ErrorMessage);
						AddTextToLog(SourceTestCase, "ERROR : " + ErrorMessage);
						AddTextToLog(SourceTestCase, "ERROR : " + ErrMsg);
						Assert.fail("ERROR : " + ErrMsg);
					} else {
						HighlightFailedTestStepsDetails(SourceTestCase + " : " + ElementDescription + " | " + ErrorMessage);
						wait(2);
						getSnapShot(WorkflowPageValue, ErrorMessage);
					}

				}

			}

		}

	}

	public void HighlightObject(String objName) {

		By locator;
		if (objName.contains("//")) {
			if (objName.toLowerCase().contains(":=")) {
				objName = objName.split(":=")[1];
//				System.out.println("Object : " + objName);
				WebElement ele = driver.findElement(By.xpath(objName));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid Green;');", ele);
			} else {
//				System.out.println("Object : " + objName);
				WebElement ele = driver.findElement(By.xpath(objName));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid Green;');", ele);
			}

		} else {
			locator = getPageObject(objName);
//			System.out.println("Object : " + objName + " => " + locator);
			WebElement ele = driver.findElement(locator);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid Green;');", ele);
		}

	}

//	public void HighlightTestStepsDetails(String TestStep) {
//
//		int ElementSize;
//		if (!TestStep.isEmpty() && TestStep != "") {
//			ElementSize = driver.findElements(By.xpath("//div[@id='KatalystHighlightTestStep']")).size();
//			JavascriptExecutor js = (JavascriptExecutor) driver;
//			if (ElementSize == 0) {
//				js.executeScript("var Element = document.createElement(\"div\");\r\n"
//						+ "var BoldEle = document.createElement(\"b\");\r\n" + "var text = document.createTextNode(\""
//						+ TestStep + "\");\r\n" + "Element.appendChild(BoldEle);\r\n" + "BoldEle.appendChild(text);\r\n"
//						+ "document.body.prepend(Element);\r\n" + "Element.setAttribute(\"id\", \"KatalystDiv\");\r\n"
//						+ "BoldEle.setAttribute(\"id\", \"KatalystHighlightTestStep\");\r\n"
//						+ "Element.setAttribute(\"style\", \"background-color: #ffff66;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;\");");
//			} else {
//				js.executeScript("var Element =  document.getElementById(\"KatalystDiv\");\r\n"
//						+ "var BoldEle =  document.getElementById(\"KatalystHighlightTestStep\");\r\n"
//						+ "BoldEle.innerHTML = \"" + TestStep + "\";\r\n"
//						+ "Element.setAttribute(\"style\", \"background-color: #ffff66;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;\");");
//			}
//
//		}
//
//	}

//	public void HighlightTestStepsDetails(String TestStep, String ElementName) {
//
//		if (!TestStep.isEmpty() && TestStep != "") {
//			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("var Element = document.createElement(\"div\");\r\n"
//					+ "var BoldEle = document.createElement(\"b\");\r\n" + "var text = document.createTextNode(\""
//					+ TestStep + "\");\r\n" + "Element.appendChild(BoldEle);\r\n" + "BoldEle.appendChild(text);\r\n"
//					+ "document.body.prepend(Element);\r\n" + "Element.setAttribute(\"id\", \"KatalystDiv_"
//					+ ElementName + "\");\r\n" + "BoldEle.setAttribute(\"id\", \"KatalystHighlightTestStep\");\r\n"
//					+ "Element.setAttribute(\"style\", \"background-color: #ffff66;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;visibility:visible;\");\r\n"
//					+ "var ScriptElement = document.createElement(\"script\");\r\n"
//					+ "ScriptElement.type = 'text/javascript';\r\n"
//					+ "ScriptElement.innerHTML=\"function showdiv(){var element = document. getElementById('KatalystDiv_"
//					+ ElementName
//					+ "');element. parentNode. removeChild(element);} setTimeout('showdiv()', 20000);\";\r\n"
//					+ "document.body.prepend(ScriptElement);");
//
//		}
//
//	}

	public void HighlightTestStepsDetails(String TestStep) {

		if (!TestStep.isEmpty() && TestStep != "") {
			JavascriptExecutor jsDelete = (JavascriptExecutor) driver;
			JavascriptExecutor jsAdd = (JavascriptExecutor) driver;
			jsDelete.executeScript("var ScriptElement = document.createElement(\"script\");\r\n" + "ScriptElement.type = 'text/javascript';\r\n"
					+ "ScriptElement.innerHTML=\"function showdiv(){var element = document. getElementById('KatalystDiv');if (typeof(element) != 'undefined' && element != null){element. parentNode. removeChild(element);}} setTimeout('showdiv()', 000);\";\r\n"
					+ "document.body.prepend(ScriptElement);");
			wait(1);
			jsAdd.executeScript("var Element = document.createElement(\"div\");\r\n" + "var BoldEle = document.createElement(\"b\");\r\n" + "var text = document.createTextNode(\"" + TestStep + "\");\r\n" + "Element.appendChild(BoldEle);\r\n" + "BoldEle.appendChild(text);\r\n"
					+ "document.body.prepend(Element);\r\n" + "Element.setAttribute(\"id\", \"KatalystDiv\");\r\n" + "BoldEle.setAttribute(\"id\", \"KatalystHighlightTestStep\");\r\n"
					+ "Element.setAttribute(\"style\", \"background-color: #ffff66;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;visibility:visible;\");");

		}

	}

	// #ffaa80
	public void HighlightFailedTestStepsDetails(String TestStep) {
		if (!TestStep.isEmpty() && TestStep != "") {
			JavascriptExecutor jsDelete = (JavascriptExecutor) driver;
			JavascriptExecutor jsAdd = (JavascriptExecutor) driver;
			jsDelete.executeScript("var ScriptElement = document.createElement(\"script\");\r\n" + "ScriptElement.type = 'text/javascript';\r\n"
					+ "ScriptElement.innerHTML=\"function showdiv(){var element = document. getElementById('KatalystDiv');if (typeof(element) != 'undefined' && element != null){element. parentNode. removeChild(element);}} setTimeout('showdiv()', 000);\";\r\n"
					+ "document.body.prepend(ScriptElement);");
			wait(1);
			jsAdd.executeScript("var Element = document.createElement(\"div\");\r\n" + "var BoldEle = document.createElement(\"b\");\r\n" + "var text = document.createTextNode(\"" + TestStep + "\");\r\n" + "Element.appendChild(BoldEle);\r\n" + "BoldEle.appendChild(text);\r\n"
					+ "document.body.prepend(Element);\r\n" + "Element.setAttribute(\"id\", \"KatalystDiv\");\r\n" + "BoldEle.setAttribute(\"id\", \"KatalystHighlightTestStep\");\r\n"
					+ "Element.setAttribute(\"style\", \"background-color: #ffaa80;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;visibility:visible;\");");

			System.out.println("var Element = document.createElement(\"div\");\r\n" + "var BoldEle = document.createElement(\"b\");\r\n" + "var text = document.createTextNode(\"" + TestStep + "\");\r\n" + "Element.appendChild(BoldEle);\r\n" + "BoldEle.appendChild(text);\r\n"
					+ "document.body.prepend(Element);\r\n" + "Element.setAttribute(\"id\", \"KatalystDiv\");\r\n" + "BoldEle.setAttribute(\"id\", \"KatalystHighlightTestStep\");\r\n"
					+ "Element.setAttribute(\"style\", \"background-color: #ffaa80;padding-top: 10px;padding-bottom: 10px; text-align: center; font-size: 12px;visibility:visible;\");");

//			String ErrorMessage = TestStep.split("\\|")[1];
//			wait(3);
//			getSnapShot("Katalyst", ErrorMessage);

		}
	}

	public synchronized void getSnapShot(String PageName, String ElementName) {
		String ScreenPath = null;
		String ScreenPathOut = null;
//		ScreenPath = SnapshotsPath + "/" + SourceTestCase + "_" + reportTimeStamp + "/" + CurrentTestCase
//				+ excelUtil.GetTimeStamp() + "#" + PageName + "#" + ElementName + ".png";
		String SnapName = CurrentTestCase + excelUtil.GetTimeStamp();
		ScreenPath = SnapshotsPath + "/" + SourceTestCase + "_" + reportTimeStamp + "/" + SnapName + ".png";

		wait(1);
		try {
			SnapDetails.put(SnapName, PageName + "#" + ElementName);
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			try {
				if (src.exists()) {
					FileUtils.copyFile(src, new File(ScreenPath));
					File screenFile = new File(ScreenPath);
					for (int s = 1; s <= 3; s++) {
						if (screenFile.exists()) {
							wait(1);
							break;
						} else {
							driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						}
					}
				} else {
					src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
					FileUtils.copyFile(src, new File(ScreenPath));
					File screenFile = new File(ScreenPath);
					for (int s = 1; s <= 3; s++) {
						if (screenFile.exists()) {
							wait(1);
							break;
						} else {
							driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						}
					}
				}
			} catch (Exception e) {
				src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(ScreenPath));
				File screenFile = new File(ScreenPath);
				for (int s = 1; s <= 3; s++) {
					if (screenFile.exists()) {
						wait(1);
						break;
					} else {
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					}
				}
			}

//			FileUtils.copyFile(src, new File(ScreenPath));
//			File screenFile = new File(ScreenPath);
//			for (int s = 1; s <= 3; s++) {
//				if (screenFile.exists()) {
//					wait(1);
//					break;
//				} else {
//					driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//				}
//			}
		} catch (Exception e) {
			System.out.println("Exception " + e);
			softAssertion.fail("Unable to get Snapshot for " + excelUtil.GetTimeStamp() + "_" + PageName + "_" + ElementName);
			AddTextToLog(SourceTestCase, "WARNING :: Because of Browser Rendering, Unable to get Snapshot for [" + excelUtil.GetTimeStamp() + "_" + PageName + "_" + ElementName + "]");
		}

//		File screenFile = new File(ScreenPath);
//		for (int s = 1; s <= 3; s++) {
//			if (screenFile.exists()) {
//				wait(1);
//				break;
//			} else {
//				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//			}
//		}
//		wait(1);
	}

//	public String GetTimeStamp() {
//		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH.mm.ss");
//		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		String s = sdf.format(timestamp);
//		String EndCustPostFix = s.replace(".", "");
//		return EndCustPostFix;
//
//	}

	public void DeleteFolderContents(String FilePath) {
//		SnapshotsPath
		File dir = new File(FilePath);
		if (dir.isDirectory() == false) {
//			System.out.println("Not a directory. Do nothing");
			return;
		}
		File[] listFiles = dir.listFiles();
		for (File file : listFiles) {
			File subdirfile = new File(FilePath + "/" + file.getName());
			for (File dirfile : subdirfile.listFiles()) {
				if (dirfile.isFile()) {
					dirfile.delete();
				}
			}
			if (subdirfile.isDirectory()) {
				subdirfile.delete();
			}
		}
	}

	public void DeleteFolderFiles(String FilePath) {
//		SnapshotsPath
		File dir = new File(FilePath);
		if (dir.isDirectory() == false) {
//			System.out.println("Not a directory. Do nothing");
			return;
		}
		File[] listFiles = dir.listFiles();
		for (File file : listFiles) {
			file.delete();
		}
	}

//Added Browser
	public void CreateSnapsFolder(String TestCase) {

//		System.out.println(BrowserName); reportTimeStamp
		String SnapFolderpath;
//		SnapFolderpath = SnapshotsPath + "/" + TestCase + "_" + BrowserName;
		SnapFolderpath = SnapshotsPath + "/" + TestCase + "_" + reportTimeStamp;

		File file = new File(SnapFolderpath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
	}

	public void DeleteChartsTestFolder(String TestCaseName) {
		wait(5);
		System.out.println("Deleting all Charts Files...");
		String ChartTestName = ChartsPath + "/ChartTests/" + TestCaseName;

		File dir = new File(ChartTestName);
		if (dir.isDirectory() == false) {
//			System.out.println("Charts Test Directory Missing At Final Setup, Please check...");
			return;
		}
		File[] listFiles = dir.listFiles();
		for (File file : listFiles) {
			if (file.getName().contains(".png")) {
				file.delete();
			}
		}
		if (dir.isDirectory()) {
			dir.delete();
		}

		String ChartFileName = ChartsPath + "/ChartFiles/" + TestCaseName + ".txt";

		File Filedir = new File(ChartsPath + "/ChartFiles");
		if (Filedir.isDirectory() == false) {
			System.out.println("Charts File Directory Missing At Final Setup, Please check...");
			return;
		}
		File[] ChartlistFiles = Filedir.listFiles();
		for (File file : ChartlistFiles) {
			System.out.println(file.getName());
			if (file.getName().contains(TestCaseName)) {
				file.delete();
			}
		}

		File Snapdir = new File(SnapshotsPath);
		if (Snapdir.isDirectory() == false) {
			System.out.println("Snapshot Directory Missing At Final Setup, Please check...");
			return;
		}
		File[] SnaplistFiles = Snapdir.listFiles();

		System.out.println("BrowserName " + BrowserName);
		for (File file : SnaplistFiles) {
			if (file.getName().equalsIgnoreCase(TestCaseName + "_" + reportTimeStamp)) {
				File subdirfile = new File(SnapshotsPath + "/" + file.getName());
				for (File dirfile : subdirfile.listFiles()) {
					if (dirfile.isFile()) {
						dirfile.delete();
					}
				}
				if (subdirfile.isDirectory()) {
					subdirfile.delete();
				}
			}
		}
		System.out.println();
	}

	public String encodeFileToBase64Binary(String SnapFilePath) {

		File file = new File(SnapFilePath); // "C:/Users/SETU BASAK/Desktop/a.jpg"
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return encodedfile;
	}

	synchronized public void FileWriter(String TestCaseName, String HTMLContent) {

		String HTMLFilePath;
		String ReportTestPath = reportPath + "/" + TestCaseName;

//		int DriverLen = driver.getWindowHandle().length();
//		String BrowserDriverHandle = driver.getWindowHandle().substring(DriverLen - 10, DriverLen);
//		System.out.println(BrowserDriverHandle);

		File file = new File(ReportTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}

//		HTMLFilePath = ReportTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_" + reportTimeStamp
//				+ ".html";
		HTMLFilePath = ReportTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".html";

//		HTMLFilePath = ReportTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_"
//				+ BrowserDriverHandle + ".html";

		try {
			java.io.FileWriter myWriter = new java.io.FileWriter(HTMLFilePath);
			myWriter.write(HTMLContent.toString());
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	synchronized public void TestSuitCreator(String HTMLContent) {

		String LogTestPath = suitreportPath;
		String ReportTestPath = LogTestPath + "/" + "Suits" + "_" + reportTimeStamp + ".html";

//		String ReportTestPath = reportPath + "/" + TestCaseName;

		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("TestSuit directory is created!");
			} else {
				System.out.println("Failed to create TestSuit report Directory!");
			}
		}

		try {
			java.io.FileWriter myWriter = new java.io.FileWriter(ReportTestPath);
			myWriter.write(HTMLContent.toString());
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public void AddTextToLog(String TestCaseName, String Message) {

		String ExcelText, TextFilePath;
		String LogTestPath = reportPath + "/" + TestCaseName;
		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}
		if (reportTimeStamp == null) {
			reportTimeStamp = "Missing";
		}

		TextFilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";
		ExecutorUpdates.put(reportTimeStamp + "#" + "LogTestPath", TextFilePath);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH.mm.ss");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String ts = sdf.format(timestamp);
			java.io.FileWriter Writer = new java.io.FileWriter(TextFilePath, true);
			BufferedWriter myWriter = new BufferedWriter(Writer);
			String substr;
			int strstart = 0;
			int strend;
			int strrange = 160;
			int limit = 164;
			double strlen = Message.length(); // 350
			int count = (int) Math.ceil(strlen / strrange);

			for (int c = 1; c <= count; c++) {
				strend = strrange * c + 1;
				if (strlen >= strend) {
					substr = Message.substring(strstart, strend);
//					System.out.println(substr.length());
					substr = StringUtils.rightPad(substr, limit, " ") + "|";
					myWriter.newLine();
					myWriter.write("|" + ts + " :: " + substr.toString());
					strstart = strend; // + 1

				} else {
					strend = (int) (strlen); // \n
					substr = Message.substring(strstart, strend);
//					System.out.println(substr.length());
					substr = StringUtils.rightPad(substr, limit, " ") + "|";
					myWriter.newLine();
					myWriter.write("|" + ts + " :: " + substr.toString());
				}

				ExcelText = ts + " :: " + substr.toString();
				System.out.println(ExcelText);
			}

			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public void AddPageTextToLog(String TestCaseName, String Message, int AddRecordIndex, String BrowserName) {

		String LogTestPath = reportPath + "/" + TestCaseName;
		String substr, TextFilePath;
		int DashCount;
		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}

		if (reportTimeStamp == null) {
			reportTimeStamp = "Missing";
		}

		TextFilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";

		try {
			Date date = new Date();
			long time = date.getTime();
			Timestamp ts = new Timestamp(time);
			java.io.FileWriter Writer = new java.io.FileWriter(TextFilePath, true);
			BufferedWriter myWriter = new BufferedWriter(Writer);

			if (AddRecordIndex == 1) {
				substr = "+";
				DashCount = 188;
			} else {
				myWriter.newLine();
				substr = "+";
//				substr = "\n+";
				DashCount = 188;
			}

			substr = StringUtils.rightPad(substr, DashCount, "-") + "+";
//			System.out.println(substr);
			myWriter.write(substr);
			myWriter.newLine();
			String title = "|" + Message;
//			String title = "\n|" + Message;
			title = StringUtils.rightPad(title, 188, " ") + "|";
//			System.out.println(title);
			myWriter.write(title);
			if (AddRecordIndex == 1) {
//				System.out.println("\n" + substr);
				myWriter.newLine();
				myWriter.write(substr);
//				myWriter.write("\n" + substr);
			} else {
//				System.out.println(substr);
				myWriter.newLine();
				myWriter.write(substr);
			}

			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public void AddTestsDetailsToLog(String ApplicationName, String TestCaseName, String ExecutionStartTime, String ExecutionEndTimeTime, String TotalExecutionTime, String TestCaseDescription, String TestDesigner, String TestExecutor, String PassPercentage, String Browser) {

		String LogTestPath = reportPath + "/" + TestCaseName;
		String hostName = null, TextFilePath;

		String TestExecutionStatus;

		if (PassPercentage.contains("100")) {
			TestExecutionStatus = "Pass";
		} else {
			TestExecutionStatus = "Fail";
		}

		java.net.InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (reportTimeStamp == null) {
			reportTimeStamp = "Missing";
		}
		UpdatedExecutorDetails(ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, TestExecutionStatus, BrowserName);

		String executionDetails = "";
		try {
			LocalDate date = LocalDate.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
			String todayDate = date.format(formatter);
			executionDetails = todayDate + "|" + hostName + "|" + TestDesigner + "|" + "RAR" + "|" + SourceTestCase + "|" + TestExecutionStatus + "|" + TestStartDate.toString() + "|" + TestEndDate.toString() + "|" + "executionLogsFile" + "|" + "Selenium" + "|";
			writeExecutionDetailsToFile(executionDetails);
		} catch (UnknownHostException e2) {
			e2.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}
		TextFilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";
		try {
			Date date = new Date();
			long time = date.getTime();
			Timestamp ts = new Timestamp(time);
			java.io.FileWriter Writer = new java.io.FileWriter(TextFilePath, true);
			BufferedWriter myWriter = new BufferedWriter(Writer);

			String substr1, substr2, substr3;

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "| Test Application Name";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ApplicationName;
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Test Designer";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TestDesigner;
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  Test Executor";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TestExecutor;
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			if (TestCaseDescription.length() > 90) {
				TestCaseDescription = TestCaseDescription.substring(0, 90);
			}

			myWriter.newLine();
			substr1 = "| Test Case Name";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TestCaseName;
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Test Case Description";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TestCaseDescription;
			substr1 = StringUtils.rightPad(substr1, 122, " ") + "|";
			myWriter.write(substr1);
//			substr1 = "  Execution Start Time";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ExecutionStartTime;
//			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
//			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			String BrowserAlias = "";
			try {
				BrowserAlias = Browser.substring(0, 1).toUpperCase() + Browser.substring(1, Browser.length()).toLowerCase();
			} catch (Exception e) {
			}

			myWriter.newLine();
			substr1 = "| Test Browser";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + BrowserAlias;
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Test Execution Type";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + "End To End";
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  System Informaion";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + hostName;
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);

//			substr1 = "  Total Execution Time";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TotalExecutionTime;
//			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
//			myWriter.write(substr1);

//			substr1 = "\n| Execution Start Time";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ExecutionStartTime;
//			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
//			myWriter.write(substr1);
//			substr1 = " Execution End Time";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ExecutionEndTimeTime;
//			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
//			myWriter.write(substr1);
//			substr1 = "  Total Execution Time";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TotalExecutionTime;
//			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
//			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "| Pass Percentage";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + Double.parseDouble(PassPercentage) + " %";
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			// Newly Added Fail Percentage
			substr1 = " Fail Percentage";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + (100 - Double.parseDouble(PassPercentage)) + " %";
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  Execution Status";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TestExecutionStatus;
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);
//			substr1 = "  System Informaion";
//			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + hostName;
//			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
//			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "| Execution Start Time";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ExecutionStartTime;
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Execution End Time";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + ExecutionEndTimeTime;
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  Total Execution Time";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + " : " + TotalExecutionTime;
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
//			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public static String printDash(int n) {
		String Dash;
		Dash = "+";
		for (int i = 1; i < n; i++) {
			Dash = Dash + "-";
		}
		Dash = Dash + "+";

		return Dash;
	}

	public void AddIOData(String TestCaseName, String IOType, String IOLabel, String IOValue) {
		String IOLink = "";
		String OutputKey, OutputValue;
		OutputKey = testElement.get(TestCaseName);
		OutputValue = IOType + " ; " + IOLabel + " ; " + IOValue + " ; " + IOLink;

		int PageElementsCount = 0;
		String ElementKey, WSSheetName = null, WorkflowPageValue = null, ExecutionStatus = null, PageDescription = null, LoadInReport = null, ElementName = null, ActionTaken = null, TestCaseValue = null, Output = null, StartDate, EndDate, TestRemarks = null, PagestrDate = null, PageendDate = null,
				OnFail = null, SourceTestCase = null, ElementDescription = "";
		int TestRowNum = 0;

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			ElementKey = loadInReport.getKey();
			if ((TestCaseName + "#" + OutputKey).equalsIgnoreCase(ElementKey)) {
				SourceTestCase = loadInReport.getValue().getSourceTestCase();
				WSSheetName = loadInReport.getValue().getSheetName();
				WorkflowPageValue = loadInReport.getValue().getPageName();
				ExecutionStatus = loadInReport.getValue().getExecutionStatus();
				PageDescription = loadInReport.getValue().getTestDescription();
				LoadInReport = loadInReport.getValue().getLoadInReport();
				ElementName = loadInReport.getValue().getElementName();
				ActionTaken = loadInReport.getValue().getActionTaken();
				TestCaseValue = loadInReport.getValue().getTestCaseValue();
				Output = loadInReport.getValue().getOutput(); // Need to add it properly
				StartDate = loadInReport.getValue().getExecutionStartTime();
				EndDate = loadInReport.getValue().getExecutionEndTime();
				TestRemarks = loadInReport.getValue().getTestRemarks();
				PageElementsCount = loadInReport.getValue().getPageElementsCount();
				PagestrDate = loadInReport.getValue().getExecutionStartTime();
				PageendDate = loadInReport.getValue().getExecutionEndTime();
				OnFail = loadInReport.getValue().getOnFail();
				TestRowNum = loadInReport.getValue().getTestRowNum();
				ElementDescription = loadInReport.getValue().getElementDescription();
				if (Output != null) {
					if (!Output.isEmpty() && Output != "") {
						OutputValue = Output + "|" + OutputValue;
					}
				}
			}
		}

		loadInReports.put(TestCaseName + "#" + OutputKey, new LoadInReports(SourceTestCase, TestCaseName, WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, OutputValue, PagestrDate, PageendDate, TestRemarks, PageElementsCount,
				OnFail, IOType, TestRowNum, ElementDescription));
//		System.out.println(OutputValue);
	}

	public void AddIODataWithLink(String TestCaseName, String IOType, String IOLabel, String IOValue, String IOLink) {
		String OutputKey, OutputValue;
		OutputKey = testElement.get(TestCaseName);

		if (!IOLink.isEmpty() && IOLink != "") {
			OutputValue = IOType + " ; " + IOLabel + " ; " + IOValue + " ; " + IOLink;
		} else {
			OutputValue = IOType + " ; " + IOLabel + " ; " + IOValue + " ; " + IOLink;
		}

		int PageElementsCount = 0, TestRowNum = 0;
		String ElementKey, WSSheetName = null, WorkflowPageValue = null, ExecutionStatus = null, PageDescription = null, LoadInReport = null, ElementName = null, ActionTaken = null, TestCaseValue = null, Output = null, StartDate, EndDate, TestRemarks = null, PagestrDate = null, PageendDate = null,
				OnFail = null, SourceTestCase = null, ElementDescription = "";

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			ElementKey = loadInReport.getKey();
			if ((TestCaseName + "#" + OutputKey).equalsIgnoreCase(ElementKey)) {
				SourceTestCase = loadInReport.getValue().getSourceTestCase();
				WSSheetName = loadInReport.getValue().getSheetName();
				WorkflowPageValue = loadInReport.getValue().getPageName();
				ExecutionStatus = loadInReport.getValue().getExecutionStatus();
				PageDescription = loadInReport.getValue().getTestDescription();
				LoadInReport = loadInReport.getValue().getLoadInReport();
				ElementName = loadInReport.getValue().getElementName();
				ActionTaken = loadInReport.getValue().getActionTaken();
				TestCaseValue = loadInReport.getValue().getTestCaseValue();
				Output = loadInReport.getValue().getOutput(); // Need to add it properly
				StartDate = loadInReport.getValue().getExecutionStartTime();
				EndDate = loadInReport.getValue().getExecutionEndTime();
				TestRemarks = loadInReport.getValue().getTestRemarks();
				PageElementsCount = loadInReport.getValue().getPageElementsCount();
				PagestrDate = loadInReport.getValue().getExecutionStartTime();
				PageendDate = loadInReport.getValue().getExecutionEndTime();
				OnFail = loadInReport.getValue().getOnFail();
				TestRowNum = loadInReport.getValue().getTestRowNum();
				ElementDescription = loadInReport.getValue().getElementDescription();

				if (Output != null) {
					if (!Output.isEmpty() && Output != "") {
						OutputValue = Output + "|" + OutputValue;
					}
				}
			}
		}

//		IOMap.put(OutputKey, new InputOutputUtil(TestCaseName, WorkflowPageValue, ElementName, IOType, OutputValue, ""));

		loadInReports.put(TestCaseName + "#" + OutputKey, new LoadInReports(SourceTestCase, TestCaseName, WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, OutputValue, PagestrDate, PageendDate, TestRemarks, PageElementsCount,
				OnFail, IOType, TestRowNum, ElementDescription));

	}

	public void UpdateLoadinReportWithOutput(String ProvCurrentTestCase, String ElementName, String ExecutionStatus, String PageStartDate, String PageEndDate) {

		int PageElementsCount = 0, TestRowNum = 0;
		String ElementKey, WSSheetName = null, WorkflowPageValue = null, PageDescription = null, LoadInReport = null, ActionTaken = null, TestCaseValue = null, Output = null, StartDate, EndDate, TestRemarks = null, PagestrDate = null, PageendDate = null, OnFail = null, IOType = null,
				SourceTestCase = null, CurrentTestCase = null, ElementDescription = "";

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			ElementKey = loadInReport.getKey();
			if ((ProvCurrentTestCase + "#" + ElementName).equalsIgnoreCase(ElementKey)) {
				SourceTestCase = loadInReport.getValue().getSourceTestCase();
				CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
				WSSheetName = loadInReport.getValue().getSheetName();
				WorkflowPageValue = loadInReport.getValue().getPageName();
//				ExecutionStatus = loadInReport.getValue().getExecutionStatus();
				PageDescription = loadInReport.getValue().getTestDescription();
				LoadInReport = loadInReport.getValue().getLoadInReport();
				ElementName = loadInReport.getValue().getElementName();
				ActionTaken = loadInReport.getValue().getActionTaken();
				TestCaseValue = loadInReport.getValue().getTestCaseValue();
				Output = loadInReport.getValue().getOutput(); // Need to add it properly
				StartDate = loadInReport.getValue().getExecutionStartTime();
				EndDate = loadInReport.getValue().getExecutionEndTime();
				TestRemarks = loadInReport.getValue().getTestRemarks();
				PageElementsCount = loadInReport.getValue().getPageElementsCount();
//				PagestrDate = loadInReport.getValue().getExecutionStartTime();
				OnFail = loadInReport.getValue().getOnFail();
				IOType = loadInReport.getValue().getIOType();
				TestRowNum = loadInReport.getValue().getTestRowNum();
				ElementDescription = loadInReport.getValue().getElementDescription();
			}
		}

		loadInReports.put(CurrentTestCase + "#" + ElementName, new LoadInReports(SourceTestCase, CurrentTestCase, WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, PageStartDate, PageEndDate, TestRemarks,
				PageElementsCount, OnFail, IOType, TestRowNum, ElementDescription));
		// TestCaseName is replaced with Current Test Case - July 13th
	}

	public void InputOutputDetails() {

		for (Entry<String, InputOutputUtil> IO : IOMap.entrySet()) {
			System.out.println("IO Map Key " + IO.getKey());
		}

	}

	public void PostTestOperations(String SourceTestCase, String CurrentTestCase, String testCaseValue, String PageName, String ElementName, String MarkObject, String ObjectIdentification, String Snapshot, String ExecutionStatus, String PagestrDate, String PageendDate, String ElementDescription) {
		String EleDesc;

		if (!testCaseValue.isEmpty() && testCaseValue != "") {
			// Highlight Object
			try {
				if (!MarkObject.isEmpty() && MarkObject != "") {
					if (MarkObject.equalsIgnoreCase("yes")) {
						if (!ObjectIdentification.isEmpty() && ObjectIdentification != "") {
							if (driver.findElements(getPageObject(ObjectIdentification)).size() > 0) {
								HighlightObject(ObjectIdentification);
							}
						}
					} else if (driver.findElements(getPageObject(MarkObject)).size() > 0) {
						HighlightObject(MarkObject);
					}
				}
			} catch (Exception e) {
				softAssertion.fail("Please provide proper object path..." + MarkObject);
			}
		}

		if (!ElementDescription.isEmpty() || ElementDescription != "") {
			EleDesc = ElementDescription;
		} else {
			EleDesc = ElementName;
		}

		if (Snapshot.equalsIgnoreCase("yes")) {
			getSnapShot(PageName, EleDesc);
		}

		if (ExecutionStatus.equalsIgnoreCase("Pass")) {
			// Page Execution End Time
			Date EndDate = new Date();
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
			PageendDate = formatter2.format(EndDate);
			UpdateLoadinReportWithOutput(CurrentTestCase, ElementName, ExecutionStatus, PagestrDate, PageendDate);
		}

	}

	public long DynamicWait(String Wait) {
		long timeOut;
		if (Wait.isEmpty() || Wait == "") {
			timeOut = 1;
		} else {
			timeOut = (long) Double.parseDouble(Wait);
		}
		return timeOut;
	}

	public String getObject(String ProvideObjectName) {
		String ObjectName = "", SystemBrowserObject = "", MobileBrowserObject = "", MobileApplicationObject = "", ObjectPath = "";
		boolean ObjectFlag = false;
		By ret = null;

		for (Map.Entry<String, GlobalObjectParam> Objects : GlobalObjects.entrySet()) {
			ObjectName = Objects.getKey();
			if (ObjectName.equalsIgnoreCase(ProvideObjectName)) {
				if (BrowserName.equalsIgnoreCase("chrome") || BrowserName.equalsIgnoreCase("ie") || BrowserName.equalsIgnoreCase("firefox") || BrowserName.equalsIgnoreCase("edge")) {
					ObjectPath = Objects.getValue().getSystemBrowserObject(); // SystemBrowserObject
				} else if (BrowserName.toLowerCase().contains("mobile")) {
					if (BrowserName.toLowerCase().contains("app")) {
						ObjectPath = Objects.getValue().getMobileApplicationObject(); // MobileApplicationObject
					} else if (BrowserName.toLowerCase().contains("chrome")) {
						ObjectPath = Objects.getValue().getMobileBrowserObject(); // MobileBrowserObject
					}
				}

				if (ObjectPath != "" && !ObjectPath.isEmpty()) {
					ret = getPageObject(ObjectPath);
				}

//				System.out.println("BrowserName : " + BrowserName + " ObjectPath " + ObjectPath);
				ObjectFlag = true;
			}
		}

		if (ObjectFlag == false) {
			OnErrorLoadInReport("[" + ProvideObjectName + "] object is not available in Global Object sheet, Please validate it.");
		}

		if (ret == null) {
			OnErrorLoadInReport("[" + ProvideObjectName + "] object provides incorrect object path [" + ObjectPath + "], Please validate it.");
		}

		return ObjectPath;
	}

	public double getTotalWorkflowPages(String PrimaryTestCaseName, ArrayList<String> WorkflowPageArray) {
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
		int RowCount = ExcelSheet.getPhysicalNumberOfRows();
		int ColumnCount = 0;
		double WorkFlowLocalCount = 0;

		String RetrievedTestCaseName = "", RetrievedFollowedTestCaseName = "", PageValue = "";
		try {
			for (int ro = 1; ro <= RowCount; ro++) {
				ColumnCount = ExcelSheet.getRow(ro).getPhysicalNumberOfCells() + 5;
				RetrievedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(0)).toString();
				RetrievedFollowedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(2)).toString();

				if (RetrievedTestCaseName.equalsIgnoreCase(PrimaryTestCaseName)) {
					for (int c = 4; c < ColumnCount; c++) {
						PageValue = GetCellValue(ExcelSheet.getRow(ro).getCell(c)).toString();
						if (PageValue != "" && !PageValue.isEmpty()) {
							// WorkFlowCount++;
							WorkflowPageArray.add(PageValue);
						}
					}

					if (RetrievedFollowedTestCaseName != "" && !RetrievedFollowedTestCaseName.isEmpty()) {
						getTotalWorkflowPages(RetrievedFollowedTestCaseName, WorkflowPageArray);
					} else {
						break;
					}
				}

				WorkFlowLocalCount = WorkFlowCount;
			}
		} catch (Exception e) {
		}

//		System.out.println("WorkflowPageArray " + WorkflowPageArray);
//		System.out.println("WorkflowPageArray Count " + WorkflowPageArray.size());
		// return WorkflowPageArray.size();
		ArrayList<String> newList = removeDuplicates(WorkflowPageArray);
		return newList.size();

	}

	public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
		ArrayList<T> newList = new ArrayList<T>();
		for (T element : list) {
			if (!newList.contains(element)) {
				newList.add(element);
			}
		}
		return newList;
	}

	public String getOutput(String OutputParam) {
//		String OutputKey, OutputValue;
		String IOType, IOLabel, IOValue = "-", IOLink, TestCaseName, IOData;
		int PageElementsCount = 0, TestRowNum = 0;
		String Output = "-";
		boolean foundflag = false;

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			TestCaseName = loadInReport.getValue().getTestCase();
			Output = loadInReport.getValue().getOutput(); // Need to add it properly
//			System.out.println("Output  : " + Output);
//			if (TestCaseName.equalsIgnoreCase(CurrentTestCase)) {
			if (Output != null) {
				if (!Output.isEmpty() && Output != "") {
					if (Output.contains("|")) {
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								if (IOType.equalsIgnoreCase("output")) {
									IOLabel = IOData.split(";")[1].trim();
									if (OutputParam.equalsIgnoreCase(IOLabel)) {
										IOValue = IOData.split(";")[2].trim();
										IOLink = IOData.split(";")[3].trim();
										System.out.println(IOType + " ; " + IOLabel + " ; " + IOValue);
										foundflag = true;
										break;
									}
								}
							}
						}
					} else {
						if (Output.contains(OutputParam)) {
							if (Output.contains(";")) {
								IOType = Output.split(";")[0];
								IOLabel = Output.split(";")[1];
								IOValue = Output.split(";")[2];

							} else {
								// Fail
							}
						}
					}

				}
			}
//			}

			if (foundflag) {
				break;
			}

		}
		System.out.println("Retrieved [" + OutputParam + "] value is [" + IOValue + "]");
		return IOValue;
	}

	public String getBrowserAlias(String BrowserName) {
		String Alias = "";

		switch (BrowserName.toLowerCase()) {
		case "chrome":
			Alias = "OnChrome";
			break;
		case "ie":
			Alias = "OnIE";
			break;
		case "firefox":
			Alias = "OnFirefox";
			break;
		case "edge":
			Alias = "OnEdge";
			break;
		case "opera":
			Alias = "OnOpera";
			break;

		default:
			break;
		}

		return Alias;

	}

	public void AddGeneralInformationToLog(String TestCaseName) {
//		, String UserName, String Account, String AppVersion
		String LogTestPath = reportPath + "/" + TestCaseName;
		String hostName = null, TextFilePath;

		String UserName = null, Account = null, AppVersion = null;

		UserName = getOutput("UserName");
		Account = getOutput("Account");
		AppVersion = getOutput("AppVersion");

		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}

		TextFilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";
		try {
			Date date = new Date();
			long time = date.getTime();
			Timestamp ts = new Timestamp(time);
			java.io.FileWriter Writer = new java.io.FileWriter(TextFilePath, true);
			BufferedWriter myWriter = new BufferedWriter(Writer);

			String substr1, substr2, substr3;
			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "| User Name";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + ApplicationName
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Account";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + TestDesigner
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  Application Version";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + TestExecutor
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "| " + UserName; // UserName
			substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " " + Account; // Test Case Description
			substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
			substr1 = StringUtils.rightPad(substr1, 60, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  " + AppVersion; // Execution Start Time
			substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
			substr1 = StringUtils.rightPad(substr1, 61, " ") + "|";
			myWriter.write(substr1);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
			myWriter.write(substr3);

			myWriter.newLine();
//			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public void AddModelInformationToLog(String TestCaseName) {

		String LogTestPath = reportPath + "/" + TestCaseName;
		String hostName = null, TextFilePath;

		File file = new File(LogTestPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Report " + TestCaseName + " directory is created!");
			} else {
				System.out.println("Failed to create " + TestCaseName + " report directory!");
			}
		}

		TextFilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";
		try {
			Date date = new Date();
			long time = date.getTime();
			Timestamp ts = new Timestamp(time);
			java.io.FileWriter Writer = new java.io.FileWriter(TextFilePath, true);
			BufferedWriter myWriter = new BufferedWriter(Writer);
			String substr1, substr2, substr3, substr4;

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 40, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 40, "-") + "+";
			myWriter.write(substr3);
			substr4 = "-";
			substr4 = StringUtils.rightPad(substr4, 40, "-") + "+";
			myWriter.write(substr4);

			myWriter.newLine();
			substr1 = "| Model Name";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + ApplicationName
			substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
			myWriter.write(substr1);
			substr1 = " Model Version";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + TestDesigner
			substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  Release Date";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + TestExecutor
			substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
			myWriter.write(substr1);
			substr1 = "  InFile Date";
			substr1 = StringUtils.rightPad(substr1, 25, " ") + ""; // + TestExecutor
			substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
			myWriter.write(substr1);

//			substr1 = "\n+";
//			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
//			myWriter.write(substr1);
//			substr2 = "-";
//			substr2 = StringUtils.rightPad(substr2, 60, "-") + "+";
//			myWriter.write(substr2);
//			substr3 = "-";
//			substr3 = StringUtils.rightPad(substr3, 61, "-") + "+";
//			myWriter.write(substr3);

			myWriter.newLine();
			substr1 = "+";
			substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
			myWriter.write(substr1);
			substr2 = "-";
			substr2 = StringUtils.rightPad(substr2, 40, "-") + "+";
			myWriter.write(substr2);
			substr3 = "-";
			substr3 = StringUtils.rightPad(substr3, 40, "-") + "+";
			myWriter.write(substr3);
			substr4 = "-";
			substr4 = StringUtils.rightPad(substr4, 40, "-") + "+";
			myWriter.write(substr4);

//			ModelInfo

			for (Map.Entry<String, ModelInformationUtil> modelInfo : ModelInfo.entrySet()) {
				myWriter.newLine();
				substr1 = "| " + modelInfo.getValue().getModelName(); // UserName
				substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
				substr1 = StringUtils.rightPad(substr1, 65, " ") + "|";
				myWriter.write(substr1);
				substr1 = " " + modelInfo.getValue().getModelVersion(); // Test Case Description
				substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
				substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
				myWriter.write(substr1);
				substr1 = "  " + modelInfo.getValue().getReleaseDate(); // Execution Start Time
				substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
				substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
				myWriter.write(substr1);
				substr1 = "  " + modelInfo.getValue().getInfileDate(); // Execution Start Time
				substr1 = StringUtils.rightPad(substr1, 25, " ") + "";
				substr1 = StringUtils.rightPad(substr1, 40, " ") + "|";
				myWriter.write(substr1);

				myWriter.newLine();
				substr1 = "+";
				substr1 = StringUtils.rightPad(substr1, 65, "-") + "+";
				myWriter.write(substr1);
				substr2 = "-";
				substr2 = StringUtils.rightPad(substr2, 40, "-") + "+";
				myWriter.write(substr2);
				substr3 = "-";
				substr3 = StringUtils.rightPad(substr3, 40, "-") + "+";
				myWriter.write(substr3);
				substr4 = "-";
				substr4 = StringUtils.rightPad(substr4, 40, "-") + "+";
				myWriter.write(substr4);

			}
			ModelInfo.clear();
			myWriter.newLine();
//			myWriter.write("\n");
			myWriter.close();
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public void PageLoadTimeValidation() {
		String Header1Text = "", Header2Text = "", ErrorMesage = "";
		if (driver.findElements(By.xpath("(//h1)[1]")).size() == 1) {
			Header1Text = driver.findElement(By.xpath("(//h1)[1]")).getText();
		}

		if (driver.findElements(By.xpath("(//h2)[1]")).size() == 1) {
			Header2Text = driver.findElement(By.xpath("(//h2)[1]")).getText();
		}

		if (Header1Text.toLowerCase().contains("error")) {
			ErrorMesage = Header1Text + " | " + Header2Text;
			OnErrorLoadInReport(ErrorMesage);
			getSnapShot("ERROR", ErrorMesage);
		}

	}

	public void writeExecutorDetails(String text) throws IOException {
		if (EXECUTOR_FILE_PATH.isEmpty() && EXECUTOR_FILE_PATH == "") {
			AddTextToLog(SourceTestCase, "Executor File Path Is Empty...");
		}
		File file = new File(EXECUTOR_FILE_PATH);
		java.io.FileWriter fr = null;

		if (file.exists()) {
		} else {
			file.createNewFile();
		}

		fr = new java.io.FileWriter(file, true);
		BufferedWriter br = new BufferedWriter(fr);

		br.write(text);
		br.newLine();

		br.close();
		fr.close();
	}

	public void UpdatedExecutorDetails(String ApplicationName, String TestCaseName, String ExecutionStartTime, String ExecutionEndTimeTime, String TotalExecutionTime, String TestCaseDescription, String TestDesigner, String TestExecutor, String PassPercentage, String Browser) {
//		ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, TestExecutionStatus, BrowserName

		String ExecutorTestCaseName = "-", ExecutorTestExecutionTime = "-", ExecutorTestRemarks = "-", ExecutorStrOutput = "-", ExecutorTestExecutionStatus = "-", ExecutorWExecutionStartTime = "-", ExecutorLogTestPath = "-", ExecutorReportFilePath = "-", ExecutorInput = "-",
				ExecutorExeQuoteStatus = "-";
		try {
			String TextFilePath = "-", TestExecutionTime = "-", TestRemarks = "-", StrOutput = "-", TestExecutionStatus = "-", ReportFile = "-", Input = "-";
			TextFilePath = reportPath + "/" + SourceTestCase + "/" + SourceTestCase + "_" + reportTimeStamp + ".txt";

			ExecutorUpdates.put(reportTimeStamp + "#" + "TestCaseName", SourceTestCase);
			ExecutorUpdates.put(reportTimeStamp + "#" + "TestExecutionTime", TotalExecutionTime);
			ExecutorUpdates.put(reportTimeStamp + "#" + "LogTestPath", TextFilePath);

			if (ExecutorUpdates.get(reportTimeStamp + "#" + "TestRemarks") == null || ExecutorUpdates.get(reportTimeStamp + "#" + "TestRemarks").isEmpty()) {
				ExecutorUpdates.put(reportTimeStamp + "#" + "TestRemarks", TestRemarks);
			}

			if (ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput") == null || ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput").isEmpty()) {
				ExecutorUpdates.put(reportTimeStamp + "#" + "StrOutput", StrOutput);
			}
			ExecutorUpdates.put(reportTimeStamp + "#" + "TestExecutionStatus", PassPercentage);
			ExecutorUpdates.put(reportTimeStamp + "#" + "ExecutionStartTime", TestStartDate.toString());
			if (ExecutorUpdates.get(reportTimeStamp + "#" + "ReportFile") == null || ExecutorUpdates.get(reportTimeStamp + "#" + "ReportFile").isEmpty()) {
				ExecutorUpdates.put(reportTimeStamp + "#" + "ReportFile", ReportFile);
			}
			if (ExecutorUpdates.get(reportTimeStamp + "#" + "Input") == null || ExecutorUpdates.get(reportTimeStamp + "#" + "Input").isEmpty()) {
				ExecutorUpdates.put(reportTimeStamp + "#" + "Input", Input);
			}

			if (ExecutorUpdates.size() > 0) {
				ExecutorTestCaseName = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "TestCaseName"));
				ExecutorTestExecutionTime = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "TestExecutionTime"));
				ExecutorTestRemarks = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "TestRemarks"));
				if (ExecutorTestRemarks.contains("|")) {
					ExecutorTestRemarks = ExecutorTestRemarks.replaceAll("\\|", "#");
				}
				String stroutput = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput"));
				if (stroutput.contains("\n")) {
					ExecutorStrOutput = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput")).replaceAll("\n", " ");
				} else {
					ExecutorStrOutput = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput"));
				}
				ExecutorTestExecutionStatus = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "TestExecutionStatus"));
				ExecutorWExecutionStartTime = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "ExecutionStartTime"));
				ExecutorLogTestPath = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "LogTestPath"));
				ExecutorReportFilePath = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "ReportFile"));
				ExecutorInput = ConvertNullToBlank(ExecutorUpdates.get(reportTimeStamp + "#" + "Input"));
			}
		} catch (Exception e) {
			AddTextToLog(SourceTestCase, "ERROR : Observing failure while collecting data for Executor.");
		}

		try {
			writeExecutorDetails(ExecutorTestCaseName + "|" + ExecutorTestRemarks + "|" + ExecutorStrOutput + "|" + ExecutorTestExecutionStatus + "|" + ExecutorLogTestPath + "|" + ExecutorReportFilePath + "|" + ExecutorWExecutionStartTime + "|" + TestEndDate.toString() + "|" + ExecutorExeQuoteStatus
					+ "|" + ExecutorInput + "|" + ExecutorTestExecutionTime);
		} catch (IOException e) {
			System.out.println("Unable To Write Content To Source File...");
			e.printStackTrace();
		}

	}

	public String ConvertNullToBlank(String value) {
		if (value == null) {
			value = "-";
		}
		return value;
	}

	public String RecoveryScenarions() {
		String ErrorMessage = "";
		ValidateConfiguratorError();

		return ErrorMessage;
	}

	public String ValidateConfiguratorError() {
		wait(1);
		String Error_Message = "";
		if (driver.findElements(By.xpath("//p[text()='Avaya One Source Configurator Error']")).size() > 0) {
			if (driver.findElements(By.xpath("//td[text()='Error Message:']/following-sibling::td")).size() > 0) {
				String ErrorMessage = driver.findElement(By.xpath("//td[text()='Error Message:']/following-sibling::td")).getText();
				String Quotenum = driver.findElement(By.xpath("//td[text()='Quote Reference Number:']/following-sibling::td")).getText();
				Error_Message = "Observing Avaya One Source Configurator Error : [" + ErrorMessage + "] and Quote Number [" + Quotenum + "]";
//				OnErrorLoadInReport(Error_Message);
				AddIOData(CurrentTestCase, "Output", "Quote #", Quotenum);
			}
		}

		if (driver.findElements(By.xpath("//span[text()='Current Configuration has generated Errors!']")).size() > 0 && driver.findElements(By.xpath("//div[contains(text(),'Stop design')]")).size() > 0) {
			Error_Message = "Current Configuration has generated Errors, Please update Quote Details.";
			String ErrorXpath = "//div[contains(text(),'ERROR ')]/following-sibling::div/div[1]";
			String ResolutionXpath = "//div[contains(text(),'ERROR ')]/following-sibling::div/div[3]";
			wait(3);
			// waitElementVisible(driver, "//div[@id='WarningMsgContent']/div[2]", 10);
			driver.findElement(By.xpath("//div[@id='WarningMsgContent']/div[2]")).click();
			waitElementVisible(driver, "//div[contains(text(),'ERROR ')]", 10);
			driver.findElement(By.xpath("//div[contains(text(),'ERROR ')]")).click();
			wait(2);
			String ErrorText = driver.findElement(By.xpath(ErrorXpath)).getText();
			String ResolutionText = driver.findElement(By.xpath(ResolutionXpath)).getText();
			AddTextToLog(CurrentTestCase, Error_Message);
			AddTextToLog(CurrentTestCase, "A1SC CONFIGURATION ERROR : " + ErrorText);
			AddTextToLog(CurrentTestCase, "A1SC CONFIGURATION RESOLUTION : " + ResolutionText);
//			OnErrorLoadInReport(Error_Message);
		}

		return Error_Message;

	}

	public void writeExecutionDetailsToFile(String text) throws IOException {
		File file = new File(ReportGeneratorPath);
		java.io.FileWriter fr = null;
		fr = new java.io.FileWriter(file, true);
		BufferedWriter br = new BufferedWriter(fr);

		br.write(text);
		br.newLine();

		br.close();
		fr.close();
	}

	public void Kill_ChromeDriver_GeckoDriver_IEDriverserver() throws IOException {
		Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe /T");
		Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe /T");
	}

	public static void updateExecutionReportinDataBase(String queryType, String queryData) {

		String DB_CONNECTION = "jdbc:oracle:thin:@//dlqaavmdb01.nonprod.avaya.com:1526/Qaadev";
		String DB_USERNAME = "qa_automation";
		String DB_PASSWORD = "A2v4a6y8a_11";

		System.out.println("queryData : " + queryData);
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection(DB_CONNECTION, DB_USERNAME, DB_PASSWORD);

			// INSERT INTO
			if (queryType.toLowerCase().contentEquals("insert")) {
				String VMName = "", userName = "", excutionDate = "", projectName = "", applicationName = "";
				String moduleName = "", submoduleName = "", testScenarioName = "", testcasename = "";
				String inputDetails = "", outputDetails = "", testExecutionStatus = "", testQuoteStatus = "";
				String totalExecutionTime = "", executionStartTime = "", executionEndTime = "", logReport = "-", excelReport = "-", htmlReport = "-", projectReport = "-", envirnment = "IQ1", automationFlag = "Automation", releasePack = "", lastmodifieddate = "";

				Statement stmt = con.createStatement();
				// VM NAME, USER NAME, EXECUTION DATE, PROJECT NAME, APPLICATION NAME, MODULE
				// NAME, SUB MODULE NAME, TEST SCENARIO NAME
				// TEST CASE NAME, INPUT DETAILS, OUTPUT DETAILS, TEST EXECUTION STATUS, TEST
				// QUOTE STATUS, TOTAL EXECUTION TIME, EXECUTION START TIME, EXECUTION END TIME
				String updateExecutionReport = "INSERT INTO AUTOMATION_TEST_DATA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?)";
				PreparedStatement preparedStmt = con.prepareStatement(updateExecutionReport);

//				queryData = "VM_NAME, USER_NAME, EXECUTION_DATE, PROJECT_NAME, APPLICATION_NAME, MODULE_NAME, SUB_MODULE_NAME, TEST_SCENARIO_NAME, TEST_CASE_NAME, "
//						+ "INPUT_DETAILS, OUTPUT_DETAILS, TEST_EXECUTION_STATUS, TEST_QUOTE_STATUS, TOTAL_EXECUTION_TIME, EXECUTION_START_TIME, EXECUTION_END_TIME, LOG_REPORT, EXCEL_REPORT, HTML_REPORT, PROJECT_REPORT, UAT_ENV, AUTOMATION_FLAG";

				String[] queryArray = queryData.split(",");
				for (int i = 0; i < queryArray.length; i++) {
					String QueryData = queryArray[i].split("=")[0].trim();
					System.out.println(QueryData);
					switch (QueryData) {
					case "VM_NAME":
						VMName = queryArray[i].split("=")[1];
						break;
					case "USER_NAME":
						userName = queryArray[i].split("=")[1];
						break;
					case "EXECUTION_DATE":
						excutionDate = queryArray[i].split("=")[1];
						break;
					case "PROJECT_NAME":
						projectName = queryArray[i].split("=")[1];
						break;
					case "APPLICATION_NAME":
						applicationName = queryArray[i].split("=")[1];
						break;
					case "MODULE_NAME":
						moduleName = queryArray[i].split("=")[1];
						break;
					case "SUB_MODULE_NAME":
						submoduleName = queryArray[i].split("=")[1];
						break;
					case "TEST_SCENARIO_NAME":
						testScenarioName = queryArray[i].split("=")[1];
						break;
					case "TEST_CASE_NAME":
						testcasename = queryArray[i].split("=")[1];
						break;
					case "INPUT_DETAILS":
						inputDetails = queryArray[i].split("=")[1];
						break;
					case "OUTPUT_DETAILS":
						outputDetails = queryArray[i].split("=")[1];
						break;
					case "TEST_EXECUTION_STATUS":
						testExecutionStatus = queryArray[i].split("=")[1];
						break;
					case "TEST_QUOTE_STATUS":
						testQuoteStatus = queryArray[i].split("=")[1];
						break;
					case "TOTAL_EXECUTION_TIME":
						totalExecutionTime = queryArray[i].split("=")[1];
						break;
					case "EXECUTION_START_TIME":
						executionStartTime = queryArray[i].split("=")[1];
						break;
					case "EXECUTION_END_TIME":
						executionEndTime = queryArray[i].split("=")[1];
						break;
					case "LOG_REPORT":
						logReport = queryArray[i].split("=")[1];
						break;
					case "EXCEL_REPORT":
						excelReport = queryArray[i].split("=")[1];
						break;
					case "HTML_REPORT":
						htmlReport = queryArray[i].split("=")[1];
						break;
					case "PROJECT_REPORT":
						projectReport = queryArray[i].split("=")[1];
						break;
					case "UAT_ENV":
						envirnment = queryArray[i].split("=")[1];
						break;
					case "AUTOMATION_FLAG":
						automationFlag = queryArray[i].split("=")[1];
						break;
					case "RELEASE_PACK":
						releasePack = queryArray[i].split("=")[1];
						break;
					case "LAST_MODIFIED_DATE":
						lastmodifieddate = queryArray[i].split("=")[1];
						break;

					}
				}

				Date ExecutionDate = new Date();
				String ExecutionDatePattern = "dd-MM-yyyy";
				SimpleDateFormat ExecutionDateFormat = new SimpleDateFormat(ExecutionDatePattern);
				excutionDate = ExecutionDateFormat.format(ExecutionDate).toString();

				Date ExecutionEndTime = new Date();
				String ExecutionEndTimePattern = "dd-MM-yyyy HH:mm:ss";
				SimpleDateFormat ExecutionEndTimeFormat = new SimpleDateFormat(ExecutionEndTimePattern);
				executionEndTime = ExecutionEndTimeFormat.format(ExecutionEndTime).toString();

				preparedStmt.setString(1, VMName);
				preparedStmt.setString(2, userName);
				System.out.println(excutionDate);
				preparedStmt.setString(3, excutionDate); // Done
				preparedStmt.setString(4, projectName);
				preparedStmt.setString(5, applicationName);
				preparedStmt.setString(6, moduleName);
				preparedStmt.setString(7, submoduleName);
				preparedStmt.setString(8, testScenarioName);
				preparedStmt.setString(9, testcasename);
				preparedStmt.setString(10, inputDetails);
				preparedStmt.setString(11, outputDetails);
				preparedStmt.setString(12, testExecutionStatus);
				preparedStmt.setString(13, testQuoteStatus);
				preparedStmt.setString(14, totalExecutionTime);
				preparedStmt.setString(15, executionStartTime);
				preparedStmt.setString(16, executionEndTime); // Done
				preparedStmt.setString(17, logReport);
				preparedStmt.setString(18, excelReport);
				preparedStmt.setString(19, htmlReport);
				preparedStmt.setString(20, projectReport);
				preparedStmt.setString(21, envirnment);
				preparedStmt.setString(22, automationFlag);
				preparedStmt.setString(23, releasePack);
				preparedStmt.setString(24, lastmodifieddate);

				preparedStmt.execute();
				preparedStmt.close();
				con.close();
				System.out.println("Data inserted successfully in database...");

			} else if (queryType.toLowerCase().contentEquals("update")) { // UPDATE TABLE

			} else if (queryType.toLowerCase().contentEquals("delete")) { // DELETE ENTRIES OLDER THAN xx Date

			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Database Exception, ClassNotFoundException: " + e);
//			Assert.fail("Database Exception, ClassNotFoundException: " + e);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Database Exception, SQLException: " + e);
//			Assert.fail("Database Exception, SQLException: " + e);
		}

	}

	public void ApplicationsDatabaseUpdate(String ApplicationName) {
		try {
			String hostName = "";
			String queryData = "";
			String OutputData = "";
			java.net.InetAddress addr;
			try {
				addr = InetAddress.getLocalHost();
				hostName = addr.getHostName();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String TotalExecutionTime = null;
			try {
				TestEndDateDB = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
				String endDateDB = formatter.format(TestEndDateDB);

				TotalExecutionTime = getExecutionTime(strDateDB, endDateDB);
			} catch (ParseException e) {
				e.printStackTrace();
			}

//			OutputData = "Quote# : " + getOutput("Quote#") + " | OTN# : " + getOutput("OTN#") + " | SSRID# : " + getOutput("SSRID#") + " | ConfigID# : " + getOutput("ConfigID#") + " | APSQRN# : " + getOutput("APS QRN#") + " | WebRef# : " + getOutput("Web Ref#");
			System.out.println(OutputData);

			switch (ApplicationName.toLowerCase().replaceAll(" ", "")) {
			case "a1s":
				OutputData = "Quote# : " + getOutput("Quote#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;
			case "sfdc":
				OutputData = "OTN# : " + getOutput("OTN#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;
			case "psn":
				OutputData = "SSRID# : " + getOutput("SSRID#") + " | APSQRN# : " + getOutput("APS QRN#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;
			case "quotecenter":
				OutputData = "Quote# : " + getOutput("Quote#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;
			case "apsconfig":
				OutputData = "ConfigID# : " + getOutput("ConfigID#") + " | APSQRN# : " + getOutput("APS QRN#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;
			case "ordercenter":
				OutputData = "WebRef# : " + getOutput("Web Ref#");
				queryData = "VM_NAME=" + hostName + ", USER_NAME=" + "Automation Team" + ", EXECUTION_DATE=" + "-" + ", PROJECT_NAME=RAR, APPLICATION_NAME=" + ApplicationName.toUpperCase() + ", MODULE_NAME=NA, SUB_MODULE_NAME=APSC Linked, TEST_SCENARIO_NAME=" + SourceTestCase + ", TEST_CASE_NAME="
						+ SourceTestCase + ", " + "INPUT_DETAILS=NA, OUTPUT_DETAILS=" + OutputData + ", TEST_EXECUTION_STATUS=" + "PASS" + ", TEST_QUOTE_STATUS=" + "NA" + ", TOTAL_EXECUTION_TIME=" + TotalExecutionTime + ", EXECUTION_START_TIME=" + ExecutionStartTimeLiteral + ", EXECUTION_END_TIME="
						+ "-" + ", LOG_REPORT=NA,EXCEL_REPORT=NA, HTML_REPORT=NA, PROJECT_REPORT=NA, UAT_ENV=IQ1, AUTOMATION_FLAG=" + AutomationFlag + ", RELEASE_PACK=Automation Migration,LAST_MODIFIED_DATE=" + lastModifiedDate;
				break;

			default:
				OnErrorLoadInReport("Invalid Option, Observing issue while accessing [" + ApplicationName + "] applications database details.");
				break;
			}

			updateExecutionReportinDataBase("insert", queryData);
			Date ExecutionStartTime = new Date();
			String ExecutionStartTimePattern = "dd-MM-yyyy HH:mm:ss";
			SimpleDateFormat ExecutionStartTimeFormat = new SimpleDateFormat(ExecutionStartTimePattern);
			ExecutionStartTimeLiteral = ExecutionStartTimeFormat.format(ExecutionStartTime).toString();

			TestStartDateDB = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			strDateDB = formatter.format(TestStartDateDB);

		} catch (Exception e) {
			OnErrorLoadInReport("Observing issue while updating [" + ApplicationName + "] applications database details.");
		}

	}

	synchronized public String getExecutionTime(String strDate, String endDate) throws ParseException {
		String dateStart = strDate;
		String dateStop = endDate;
		String TotalTime = "";
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		long diff, diffSeconds = 0, diffMinutes = 0, diffHours = 0, diffDays;
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			diff = d2.getTime() - d1.getTime();
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (diffHours != 0) { // !min.isEmpty() && min != "0"
			TotalTime = TotalTime + diffHours + " Hours, ";
		}

		if (diffMinutes != 0) { // !min.isEmpty() && min != "0"
			TotalTime = TotalTime + diffMinutes + " Minutes, ";
		}

		if (diffSeconds != 0) { // !sec.isEmpty() && sec != "0"
			TotalTime = TotalTime + diffSeconds + " Seconds.";
		} else {
//			TotalTime = "2 Seconds.";
			TotalTime = TotalTime + "2 Seconds.";
		}
		return TotalTime;
	}

}
