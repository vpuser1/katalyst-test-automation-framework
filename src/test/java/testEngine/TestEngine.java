package testEngine;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestEngine extends GearBox {
	String TestCaseName;
	
	@BeforeSuite
	public void EngineStart() {
		TestExecutionStartTime();
//		DeleteLocalFiles();
	}
	
	@Parameters({ "TestCaseName", "Browser" })
	@BeforeTest
	public void EngineStartUp(String TestCaseName, @Optional String Browser) {
		
	//	TestCaseName = System.getProperty("TestCaseName");
	//	Browser = "Chrome";
		InitalSetup(TestCaseName, Browser);
		if (TestPresenceFlag == false) {
			System.out.println(TestCaseName);
		}
	}

	@Parameters({ "TestCaseName", "Browser" })
	@Test
	public void MainSetup(String TestCaseName, @Optional String Browser) throws Exception {
		System.out.println("Called Main Module");
	//	TestCaseName = System.getProperty("TestCaseName");
	//	Browser = "Chrome";
		if (TestPresenceFlag == true) {
			
			ModelExecution(TestCaseName, Browser);
		} else {
			AddPageTextToLog(TestCaseName, "ERROR" + " => " + "Test Case Missing", 1, "");
			AddTextToLog(TestCaseName, "Test Case [" + TestCaseName + "] Missing...");

		}
	}

	@Parameters({ "TestCaseName", "Browser" })
	@AfterTest(alwaysRun = true)
	public void EngineShutDown(String TestCaseName, @Optional String Browser) throws Exception {
	//	TestCaseName = System.getProperty("TestCaseName");
	//	Browser = "Chrome";
		FinalSetup(TestCaseName, Browser);
		
		if (TestPresenceFlag == false) {
			AddTestsDetailsToLog("NA", TestCaseName, TestStartDate.toString(), TestEndDate.toString(), "2 Seconds", "NA", "NA", "NA", "0", "NA");
		}
	}

	@AfterSuite
	public void EngineStop() {
		if (TestPresenceFlag == true) {
//			QuiteWebDriver();
			TestSuitCaller();
			
		}
	}

}
