//package testEngine;
//
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import org.testng.TestNG;
//import org.testng.xml.XmlClass;
//import org.testng.xml.XmlSuite;
//import org.testng.xml.XmlTest;
//
//public class TestNGRunner extends ExcelUtil {
//
//	public static LinkedHashMap<String, String> ExecutionMap = new LinkedHashMap<String, String>();
//	String TestCaseName, Browser = null, MultiBrowserExeType, LocalExecutionFlag = null, LocalExecutionBrowser = "", ExecutionFlag = null, TPRMultiBrowserExeType, TPRMultiBrowser, TPRExecutionType, ExecutionType = "Parallel";
//
////	public TestNGRunner(String SourceTestCase, String CurrentTestCase) {
////		super(SourceTestCase, CurrentTestCase);
////	}
//
//	int GlobalThreadCount;
//	String GlobalBrowser, GlobalExecutionFlag, GlobalExecutionType;
//
////	public static void main(String[] args) throws IOException {
////		new TestNGRunner("", "").TestRunner();
////	}
//
//	public void TestRunner() {
//
//		GetEnvironmentDetails();
//		GlobalExecutionFlag = getEnvironmentValue("ExecutionFlag");
//		GlobalBrowser = getEnvironmentValue("Browser");
//		GlobalExecutionType = getEnvironmentValue("ExecutionType");
//		GlobalThreadCount = Integer.parseInt(getEnvironmentValue("Thread_Count")); // Will be applicable for single
//
//		// browser flows
//		String SheetName = "Sheet1";
//		int TestCount = 0, BrowserCount, BrowserThreadCount = 0;
//		try {
////			String xlPath = "./Katalyst_Driver_Sheet.xlsx";
//			String xlPath = "./TestRunner.xlsx";
//			int rowCount = getRowCount(xlPath, SheetName);
//			int executionFlagColindex = getColumnWithCellText(xlPath, SheetName, "Execute");
//
//			for (int index = 1; index < rowCount; index++) {
//				LocalExecutionFlag = getCellData(xlPath, SheetName, index, executionFlagColindex);
//				TestCaseName = getCellData(xlPath, SheetName, index, 2);
//				LocalExecutionBrowser = getCellData(xlPath, SheetName, index, 2);
//				if (LocalExecutionFlag.equalsIgnoreCase("yes") && !TestCaseName.isEmpty() && TestCaseName != "") {
//					ExecutionMap.put(TestCaseName, LocalExecutionFlag + "_" + LocalExecutionBrowser);
//					TestCount++;
//				}
//			}
//
//			XmlSuite xmlSuite = new XmlSuite();
//			xmlSuite.setName("Katalyst Automation Execution Status : ");
//			xmlSuite.setParallel("Tests");
//
//			List<XmlSuite> listSuite = new ArrayList<XmlSuite>();
//			listSuite.add(xmlSuite);
//			int index = 2;
////			for (int index = 2; index < rowCount; index++) {
//			for (Map.Entry<String, String> execution : ExecutionMap.entrySet()) {
//				TestCaseName = execution.getKey().trim();
//				LocalExecutionFlag = execution.getValue().trim().split("_")[0];
//				LocalExecutionBrowser = execution.getValue().trim().split("_")[1];
//
////				LocalExecutionFlag = getCellData(xlPath, SheetName, index, executionFlagColindex)̥;
////				TestCaseName = getCellData(xlPath, SheetName, index, 0);
////				LocalExecutionBrowser = getCellData(xlPath, SheetName, index, 1);
//
//				if (LocalExecutionFlag.equalsIgnoreCase("yes")) {
//
//					FollowedTestsRecursions(TestCaseName);
//					SetGlobalParametersT();
//					TPRMultiBrowser = GetParameterValue(TestCaseName, "MultiBrowser");
//					TPRExecutionType = GetParameterValue(TestCaseName, "ExecutionType");
//
//					if (TPRMultiBrowser.equalsIgnoreCase("yes")) {
////						TestCaseName, Browser, BrowserThreadCount
//						if (GlobalExecutionFlag.equalsIgnoreCase("global")) {
////					 	Use Global Mapping
//							Browser = GlobalBrowser;
//							BrowserThreadCount = Browser.split(";").length;
//							ExecutionType = GlobalExecutionType;
//
//						} else if (GlobalExecutionFlag.equalsIgnoreCase("executor")) {
////					 	Use Executor Mapping
//							if (TestCount > 1) {
////						 	Use Global Mapping
//								Browser = GlobalBrowser;
//								BrowserThreadCount = Browser.split(";").length;
//								ExecutionType = GlobalExecutionType;
//
//							} else {
////						 	Use Local Mapping
//								Browser = getCellData(xlPath, SheetName, index, 1);
//								BrowserThreadCount = Browser.split(";").length;
//								ExecutionType = TPRExecutionType;
//							}
//						} else {
////							Existing TPR Browser
//							Browser = GetParameterValue(TestCaseName, "BrowserName");
//							BrowserThreadCount = 1;
//						}
//						System.out.println("Executing TestCase [" + TestCaseName + "] on browsers [" + Browser + "].");
//
//						if (BrowserThreadCount >= 1) {
//							if (ExecutionType.equalsIgnoreCase("parallel")) {
//								xmlSuite.setThreadCount(BrowserThreadCount);
//							} else if (ExecutionType.equalsIgnoreCase("sequential")) {
//								xmlSuite.setThreadCount(1);
//							} else {
//								xmlSuite.setThreadCount(GlobalThreadCount);
//							}
//
//							for (int i = 0; i < BrowserThreadCount; i++) {
//								if (!Browser.split(";")[i].isEmpty() && Browser.split(";")[i] != "") {
//									XmlTest xmlTest = new XmlTest(xmlSuite);
//									xmlTest.setName(TestCaseName + "_Test_" + index + "_" + i);
//									xmlTest.addParameter("TestCaseName", TestCaseName);
//									xmlTest.addParameter("Browser", Browser.split(";")[i]);
//									XmlClass xmlClass = new XmlClass();
//									xmlClass.setName("testEngine.TestEngine");
//									List<XmlClass> listClass = new ArrayList<XmlClass>();
//									listClass.add(xmlClass);
//									xmlTest.setXmlClasses(listClass);
//								}
//							}
//
//						} else {
//							xmlSuite.setThreadCount(GlobalThreadCount);
//							XmlTest xmlTest = new XmlTest(xmlSuite);
//							xmlTest.setName(TestCaseName + "_Test_" + index);
//							xmlTest.addParameter("TestCaseName", TestCaseName);
//							xmlTest.addParameter("Browser", Browser);
//							XmlClass xmlClass = new XmlClass();
//							xmlClass.setName("testEngine.TestEngine");
//							List<XmlClass> listClass = new ArrayList<XmlClass>();
//							listClass.add(xmlClass);
//							xmlTest.setXmlClasses(listClass);
//						}
//
//					} else {
////					Existing TPR Browser
//						xmlSuite.setThreadCount(GlobalThreadCount);
//						Browser = GetParameterValue(TestCaseName, "BrowserName");
//						System.out.println("Executing TestCase [" + TestCaseName + "] on browsers [" + Browser + "].");
//						XmlTest xmlTest = new XmlTest(xmlSuite);
//						xmlTest.setName(TestCaseName + "_Test_" + index);
//						xmlTest.addParameter("TestCaseName", TestCaseName);
//						xmlTest.addParameter("Browser", Browser);
//						XmlClass xmlClass = new XmlClass();
//						xmlClass.setName("testEngine.TestEngine");
//						List<XmlClass> listClass = new ArrayList<XmlClass>();
//						listClass.add(xmlClass);
//						xmlTest.setXmlClasses(listClass);
//
//					}
//				}
//				index++;
//
//			} // Row Count
//
//			TestNG testng = new TestNG();
//			testng.setXmlSuites(listSuite);
//			testng.run();
//
//		}
//
//		catch (
//
//		Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//}
