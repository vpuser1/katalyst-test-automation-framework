package testEngine;

public class ExcelReport { // extends GearBox

	String SheetName, PageName, ExecutionStatus, TestDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, ExecutionStartTime, ExecutionEndTime, ExecutionTotalTime, TestRemarks, Output, OnFail, TestCase, IOType;

	public ExcelReport(String sheetName, String pageName, String executionStatus, String testDescription, String executionStartTime, String executionEndTime, String testRemarks, String output, String testCase, String IOType) {
//		super();
		SheetName = sheetName;
		PageName = pageName;
		ExecutionStatus = executionStatus;
		TestDescription = testDescription;
		ExecutionStartTime = executionStartTime;
		ExecutionEndTime = executionEndTime;
		TestRemarks = testRemarks;
		Output = output;
		TestCase = testCase;
		this.ElementName = ElementName;

	}

	public String getIOType() {
		return IOType;
	}

	public void setIOType(String iOType) {
		IOType = iOType;
	}

	public String getSheetName() {
		return SheetName;
	}

	public void setSheetName(String sheetName) {
		SheetName = sheetName;
	}

	public String getPageName() {
		return PageName;
	}

	public void setPageName(String pageName) {
		PageName = pageName;
	}

	public String getExecutionStatus() {
		return ExecutionStatus;
	}

	public void setExecutionStatus(String executionStatus) {
		ExecutionStatus = executionStatus;
	}

	public String getTestDescription() {
		return TestDescription;
	}

	public void setTestDescription(String testDescription) {
		TestDescription = testDescription;
	}

	public String getLoadInReport() {
		return LoadInReport;
	}

	public void setLoadInReport(String loadInReport) {
		LoadInReport = loadInReport;
	}

	public String getElementName() {
		return ElementName;
	}

	public void setElementName(String elementName) {
		ElementName = elementName;
	}

	public String getActionTaken() {
		return ActionTaken;
	}

	public void setActionTaken(String actionTaken) {
		ActionTaken = actionTaken;
	}

	public String getTestCaseValue() {
		return TestCaseValue;
	}

	public void setTestCaseValue(String testCaseValue) {
		TestCaseValue = testCaseValue;
	}

	public String getExecutionStartTime() {
		return ExecutionStartTime;
	}

	public void setExecutionStartTime(String executionStartTime) {
		ExecutionStartTime = executionStartTime;
	}

	public String getExecutionEndTime() {
		return ExecutionEndTime;
	}

	public void setExecutionEndTime(String executionEndTime) {
		ExecutionEndTime = executionEndTime;
	}

	public String getExecutionTotalTime() {
		return ExecutionTotalTime;
	}

	public void setExecutionTotalTime(String executionTotalTime) {
		ExecutionTotalTime = executionTotalTime;
	}

	public String getTestRemarks() {
		return TestRemarks;
	}

	public void setTestRemarks(String testRemarks) {
		TestRemarks = testRemarks;
	}

	public String getOutput() {
		return Output;
	}

	public void setOutput(String output) {
		Output = output;
	}

	public String getOnFail() {
		return OnFail;
	}

	public void setOnFail(String onFail) {
		OnFail = onFail;
	}

	public String getTestCase() {
		return TestCase;
	}

	public void setTestCase(String testCase) {
		TestCase = testCase;
	}

}
