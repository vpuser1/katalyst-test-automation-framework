package testEngine;

public class GlobalParametersUtil {
	
	
	String TestCaseName, ParameterName, ParameterValue;

	public GlobalParametersUtil(String testCaseName, String parameterName, String parameterValue) {
		TestCaseName = testCaseName;
		ParameterName = parameterName;
		ParameterValue = parameterValue;
	}

	public String getTestCaseName() {
		return TestCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		TestCaseName = testCaseName;
	}

	public String getParameterName() {
		return ParameterName;
	}

	public void setParameterName(String parameterName) {
		ParameterName = parameterName;
	}

	public String getParameterValue() {
		return ParameterValue;
	}

	public void setParameterValue(String parameterValue) {
		ParameterValue = parameterValue;
	}
	
	
	

}
