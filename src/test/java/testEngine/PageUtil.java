package testEngine;

public class PageUtil { // extends ExcelUtil

	String ElementName, PageName, ObjectType, ObjectIdentification, ActionTaken, OnFail, Snapshot, TestCaseValue, MarkObject, LoadInReport, Wait, PageDescription, TestCase, SourceTestCase, CurrentTestCase, ElementDescription;

	public PageUtil(String SourceTestCase, String CurrentTestCase, String elementName, String pageName, String objectType, String objectIdentification, String actionTaken, String onFail, String snapshot, String testCaseValue, String markObject,
			String LoadInReport, String Wait, String PageDescription, String ElementDescription) {
//		super(SourceTestCase, CurrentTestCase);
		this.SourceTestCase = SourceTestCase;
		this.CurrentTestCase = CurrentTestCase;
		this.ElementName = elementName;
		this.PageName = pageName;
		this.ObjectType = objectType;
		this.ObjectIdentification = objectIdentification;
		this.ActionTaken = actionTaken;
		this.OnFail = onFail;
		this.Snapshot = snapshot;
		this.TestCaseValue = testCaseValue;
		this.MarkObject = markObject;
		this.LoadInReport = LoadInReport;
		this.Wait = Wait;
		this.PageDescription = PageDescription;
		this.ElementDescription = ElementDescription;
	}

	public String getElementDescription() {
		return ElementDescription;
	}

	public void setElementDescription(String elementDescription) {
		ElementDescription = elementDescription;
	}

	public String getSourceTestCase() {
		return SourceTestCase;
	}

	public void setSourceTestCase(String sourceTestCase) {
		SourceTestCase = sourceTestCase;
	}

	public String getCurrentTestCase() {
		return CurrentTestCase;
	}

	public void setCurrentTestCase(String currentTestCase) {
		CurrentTestCase = currentTestCase;
	}

	public String getTestCase() {
		return TestCase;
	}

	public void setTestCase(String testCase) {
		TestCase = testCase;
	}

	public String getWait() {
		return Wait;
	}

	public void setWait(String wait) {
		Wait = wait;
	}

	public String getMarkObject() {
		return MarkObject;
	}

	public void setMarkObject(String markObject) {
		MarkObject = markObject;
	}

	public String getLoadInReport() {
		return LoadInReport;
	}

	public void setLoadInReport(String loadInReport) {
		LoadInReport = loadInReport;
	}

	public String getElementName() {
		return ElementName;
	}

	public void setElementName(String elementName) {
		ElementName = elementName;
	}

	public String getPageName() {
		return PageName;
	}

	public void setPageName(String pageName) {
		PageName = pageName;
	}

	public String getObjectType() {
		return ObjectType;
	}

	public void setObjectType(String objectType) {
		ObjectType = objectType;
	}

	public String getObjectIdentification() {
		return ObjectIdentification;
	}

	public void setObjectIdentification(String objectIdentification) {
		ObjectIdentification = objectIdentification;
	}

	public String getActionTaken() {
		return ActionTaken;
	}

	public void setActionTaken(String actionTaken) {
		ActionTaken = actionTaken;
	}

	public String getOnFail() {
		return OnFail;
	}

	public void setOnFail(String onFail) {
		OnFail = onFail;
	}

	public String getSnapshot() {
		return Snapshot;
	}

	public void setSnapshot(String snapshot) {
		Snapshot = snapshot;
	}

	public String getTestCaseValue() {
		return TestCaseValue;
	}

	public void setTestCaseValue(String testCaseValue) {
		TestCaseValue = testCaseValue;
	}

	public String getPageDescription() {
		return PageDescription;
	}

	public void setPageDescription(String pageDescription) {
		PageDescription = pageDescription;

	}

}
