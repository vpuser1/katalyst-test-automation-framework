package testEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.format.CellTextFormatter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

public class testSuitUtil extends GearBox {

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;
	public Map<String, String> testElement;
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

	public testSuitUtil(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}
	
	public testSuitUtil() {
		super();
	}

	public void testSultDetails() {
		String WSSheetName, WorkflowPageValue, ExecutionStatus, ExecutionBlockStatus = null, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, IOType, TestCaseName;
		String CapturedPage = "";
		String outflag = "";
		int PageElementsCount, count;
		count = 1;
		String PageIO = "";
		Map<String, LoadInReports> PageMapIO = null;

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			TestCaseName = loadInReport.getValue().getTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageDescription = loadInReport.getValue().getTestDescription();
			LoadInReport = loadInReport.getValue().getLoadInReport();
			ElementName = loadInReport.getValue().getElementName();
			ActionTaken = loadInReport.getValue().getActionTaken();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();
			Output = loadInReport.getValue().getOutput(); // Need to add it properly
			StartDate = loadInReport.getValue().getExecutionStartTime();
			EndDate = loadInReport.getValue().getExecutionEndTime();
			TestRemarks = loadInReport.getValue().getTestRemarks();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();

			System.out.println(TestCaseName + " | " + WSSheetName + " | " + WorkflowPageValue + " | " + ExecutionStatus + " | " + PageDescription + " | " + Output + " | " + StartDate + " | " + EndDate + " | " + TestRemarks);

		}
	}

	public void GettestSuitDetails(String reportTimeStamp) {

		String TestCaseName, TestDescription = null, TestExecutionStatus = null, TestOutputDetails = null, TestExecutionTime = null, TestRemarks = null, LogPath = null;
		Map<String, ExcelReport> testAllDetails = null;

		XSSFWorkbook ExcelWorkbook = null;
		XSSFSheet ExcelSheet = null;
		XSSFRow ExcelRows;
		XSSFCell ExcelColumns;
		int RowNum, ColumnNum;
		FileOutputStream fileOut = null;

		String LogTestPath = suitreportPath;
		String ExcelPath = LogTestPath + "/" + "Suits" + "_" + reportTimeStamp + ".xlsx";

		File file = new File(suitreportPath);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}

		try {
			fileOut = new FileOutputStream(new java.io.File(ExcelPath));
			ExcelWorkbook = new XSSFWorkbook();
			ExcelSheet = ExcelWorkbook.createSheet("AutomationReport");
		} catch (IOException e) {
			e.printStackTrace();
		}

		XSSFCellStyle style1 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style2 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style3 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style4 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style5 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style6 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Passstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Failstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle PassSubstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle FailSubstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Logstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle IOstyle = ExcelWorkbook.createCellStyle();

		CellRangeAddress Headerregion = new CellRangeAddress(0, 0, 0, 5);
		CellRangeAddress Inforegion = new CellRangeAddress(0, 100, 0, 5);
//		CellRangeAddress Dataregion = new CellRangeAddress(1, 3, 0, 5);

		Font font = ExcelWorkbook.createFont();
		font.setBold(true);

		style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(153, 204, 255)));
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setAlignment(HorizontalAlignment.CENTER);
		style1.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		style1.setBorderRight(CellStyle.BORDER_THICK);
		style1.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style1.setBorderBottom(CellStyle.BORDER_THICK);
		style1.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style1.setWrapText(true);
		style1.setFont(font);

		style2.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		style3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style3.setAlignment(HorizontalAlignment.LEFT);
		style3.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		style3.setWrapText(true);
		style3.setFont(font);

		style5.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style5.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style5.setAlignment(HorizontalAlignment.LEFT);
		style5.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		style5.setWrapText(true);

		style4.setFillForegroundColor(new XSSFColor(new java.awt.Color(153, 204, 255)));
		style4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style4.setAlignment(CellStyle.ALIGN_CENTER);
		style4.setBorderRight(CellStyle.BORDER_THICK);
		style4.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style4.setBorderBottom(CellStyle.BORDER_THICK);
		style4.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style4.setWrapText(true);
		style4.setFont(font);

		style6.setFillForegroundColor(new XSSFColor(new java.awt.Color(230, 230, 255)));
		style6.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style6.setAlignment(CellStyle.ALIGN_LEFT);
		style6.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		style6.setAlignment(CellStyle.ALIGN_LEFT);
		style6.setBorderTop(CellStyle.BORDER_THICK);
		style6.setTopBorderColor(IndexedColors.WHITE.getIndex());
		style6.setBorderRight(CellStyle.BORDER_THICK);
		style6.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style6.setBorderBottom(CellStyle.BORDER_THICK);
		style6.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style6.setWrapText(true);
		style6.setFont(font);

		Passstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 255, 128)));
		Passstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Passstyle.setAlignment(HorizontalAlignment.CENTER);
		Passstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		Passstyle.setAlignment(CellStyle.ALIGN_CENTER);
		Passstyle.setBorderRight(CellStyle.BORDER_THICK);
		Passstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Passstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Passstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Passstyle.setWrapText(true);
		Passstyle.setFont(font);

		PassSubstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 255, 128)));
		PassSubstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		PassSubstyle.setAlignment(HorizontalAlignment.LEFT);
		PassSubstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		PassSubstyle.setAlignment(CellStyle.ALIGN_LEFT);
		PassSubstyle.setBorderRight(CellStyle.BORDER_THICK);
		PassSubstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		PassSubstyle.setBorderBottom(CellStyle.BORDER_THICK);
		PassSubstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		PassSubstyle.setWrapText(true);
		PassSubstyle.setFont(font);

		Failstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 153, 153)));
		Failstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Failstyle.setAlignment(HorizontalAlignment.CENTER);
		Failstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		Failstyle.setAlignment(CellStyle.ALIGN_CENTER);
		Failstyle.setBorderRight(CellStyle.BORDER_THICK);
		Failstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Failstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Failstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Failstyle.setWrapText(true);
		Failstyle.setFont(font);

		FailSubstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 153, 153)));
		FailSubstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		FailSubstyle.setAlignment(HorizontalAlignment.LEFT);
		FailSubstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		FailSubstyle.setAlignment(CellStyle.ALIGN_LEFT);
		FailSubstyle.setBorderRight(CellStyle.BORDER_THICK);
		FailSubstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		FailSubstyle.setBorderBottom(CellStyle.BORDER_THICK);
		FailSubstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		FailSubstyle.setWrapText(true);
		FailSubstyle.setFont(font);

		IOstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 245, 231)));
		IOstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		IOstyle.setAlignment(HorizontalAlignment.LEFT);
		IOstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
		IOstyle.setAlignment(CellStyle.ALIGN_LEFT);
		IOstyle.setBorderRight(CellStyle.BORDER_THICK);
		IOstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		IOstyle.setBorderBottom(CellStyle.BORDER_THICK);
		IOstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		IOstyle.setWrapText(true);
		IOstyle.setFont(font);

		Logstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 245, 231)));
		Logstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Logstyle.setBorderTop(CellStyle.BORDER_THICK);
		Logstyle.setTopBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setBorderRight(CellStyle.BORDER_THICK);
		Logstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Logstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setWrapText(true);

		ExcelSheet.setColumnWidth(0, 22 * 256);
		ExcelSheet.setColumnWidth(1, 30 * 256);
		ExcelSheet.setColumnWidth(2, 20 * 256);
		ExcelSheet.setColumnWidth(3, 25 * 256);
		ExcelSheet.setColumnWidth(4, 25 * 256);
		ExcelSheet.setColumnWidth(5, 30 * 256);

		for (int r = 0; r < 500; r++) {
			Row row = ExcelSheet.createRow(r);
			for (int c = 0; c < 30; c++) {
				Cell infocell = row.createCell(c);
				infocell.setCellStyle(style2);
			}
		}

		String hostName = null;
		java.net.InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		Cell cell = ExcelSheet.getRow(0).getCell(0);
		ExcelSheet.addMergedRegion(Headerregion);
		cell.setCellValue("EXECUTION DETAILS");
		cell.setCellStyle(style1);
		ExcelSheet.getRow(0).setHeightInPoints(30);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderRight(CellStyle.BORDER_THICK, Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setRightBorderColor(IndexedColors.WHITE.getIndex(), Headerregion, ExcelSheet, ExcelWorkbook);

		ExcelSheet.getRow(1).getCell(0).setCellValue("Application Name");
		ExcelSheet.getRow(1).getCell(2).setCellValue("System Informaion");
		ExcelSheet.getRow(1).getCell(4).setCellValue("Total Executin Time");

		ExcelSheet.getRow(1).getCell(1).setCellValue("Offer Enablement");
		ExcelSheet.getRow(1).getCell(3).setCellValue(hostName);
		ExcelSheet.getRow(1).getCell(5).setCellValue("Total Executin Time");

		ExcelSheet.getRow(2).getCell(0).setCellValue("Test Designer");
		ExcelSheet.getRow(2).getCell(2).setCellValue("Test Executor");
		ExcelSheet.getRow(2).getCell(4).setCellValue("Test Count");

		ExcelSheet.getRow(2).getCell(1).setCellValue("Saurabh Sawalapurkar");
		ExcelSheet.getRow(2).getCell(3).setCellValue("Saurabh Sawalapurkar");
		ExcelSheet.getRow(2).getCell(5).setCellValue(testSuits.size());

		for (int r = 1; r < 3; r++) {
			for (int c = 0; c <= 5; c++) {
				if (c == 1 || c == 3 || c == 5) {
					ExcelSheet.getRow(r).getCell(c).setCellStyle(style3);
				} else {
					ExcelSheet.getRow(r).getCell(c).setCellStyle(style5);
				}
			}
		}

//		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Dataregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Inforegion, ExcelSheet, ExcelWorkbook);
//		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Dataregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Inforegion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderRight(CellStyle.BORDER_THICK, Inforegion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setRightBorderColor(IndexedColors.WHITE.getIndex(), Inforegion, ExcelSheet, ExcelWorkbook);

		ExcelSheet.getRow(5).getCell(0).setCellValue("TESTCASE NAME");
		ExcelSheet.getRow(5).getCell(1).setCellValue("TEST DESCRIPTION");
		ExcelSheet.getRow(5).getCell(2).setCellValue("STATUS");
		ExcelSheet.getRow(5).getCell(3).setCellValue("OUTPUT DETAILS");
		ExcelSheet.getRow(5).getCell(4).setCellValue("TOTAL EXECUTION TIME");
		ExcelSheet.getRow(5).getCell(5).setCellValue("REMARKS");

		for (int c = 0; c <= 5; c++) {
			ExcelSheet.getRow(5).getCell(c).setCellStyle(style4);
		}

		int dr = 6;
		int OptRow;
		try {
			for (Entry<String, testSuit> testSuit : testSuits.entrySet()) {
//				TestCaseName = testSuit.getKey();
				TestCaseName = testSuit.getValue().getTestCaseName();
				TestDescription = testSuit.getValue().getTestDescription();
				TestExecutionStatus = testSuit.getValue().getTestExecutionStatus();
				TestOutputDetails = testSuit.getValue().getTestOutputDetails();
				TestExecutionTime = testSuit.getValue().getTestExecutionTime();
				TestRemarks = testSuit.getValue().getTestRemarks();
				testAllDetails = testSuit.getValue().getTestIODetails();
				LogPath = testSuit.getValue().getTestCaseLogPath();

				System.out.println(TestCaseName + " | " + TestDescription + " | " + TestExecutionStatus + " | " + TestOutputDetails + " | " + TestExecutionTime + " | " + TestRemarks + " | " + testAllDetails + " | " + LogPath);

				ExcelSheet.getRow(dr).getCell(0).setCellValue(TestCaseName);
				ExcelSheet.getRow(dr).getCell(1).setCellValue(TestDescription);

//				System.out.println(LogPath);
				File logfile = new File(LogPath);
				String FileAddress = logfile.toString(); // .replace("\\", "/")
//				System.out.println(logfile);
//				System.out.println(FileAddress);
				FileInputStream inpFile = null;

//			ExcelSheet.getRow(dr).getCell(2).setCellValue(TestExecutionStatus);
				if (logfile.exists()) {
					ExcelSheet.getRow(dr).getCell(2).setCellFormula("HYPERLINK(\"" + FileAddress + "\", \"" + TestExecutionStatus + "\")");
				}
				ExcelSheet.getRow(dr).getCell(4).setCellValue(TestExecutionTime);
				OptRow = dr;

				if (TestExecutionStatus.equalsIgnoreCase("pass")) {
					for (int dc = 0; dc <= 5; dc++) {
						if (dc == 1 || dc == 3 || dc == 5) {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(PassSubstyle);
						} else {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(Passstyle);
						}
					}
				}
				if (TestExecutionStatus.equalsIgnoreCase("fail")) {
					for (int dc = 0; dc <= 5; dc++) {
						if (dc == 1 || dc == 3 || dc == 5) {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(FailSubstyle);
						} else {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(Failstyle);
						}
					}
				}

				int iodr = dr + 1, iolabel;
				iolabel = iodr; // + 1

				ExcelSheet.getRow(iolabel).getCell(0).setCellValue("CHECKPOINTS");
				ExcelSheet.getRow(iolabel).getCell(1).setCellValue("PAGE");
				ExcelSheet.getRow(iolabel).getCell(2).setCellValue("INPUT DETAILS");
				ExcelSheet.getRow(iolabel).getCell(4).setCellValue("OUTPUT DETAILS");

				ExcelSheet.addMergedRegion(new CellRangeAddress(iolabel, iolabel, 2, 3));
				ExcelSheet.addMergedRegion(new CellRangeAddress(iolabel, iolabel, 4, 5));

				for (int c = 0; c <= 5; c++) {
					ExcelSheet.getRow(iolabel).getCell(c).setCellStyle(style6);
				}

				dr = iolabel + 1;
				int trfrom = iolabel, trto;

				String WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, IOType, PageOutput = "", PageRemark = null;

				for (Entry<String, ExcelReport> excelReport : testAllDetails.entrySet()) {
					WSSheetName = excelReport.getValue().getSheetName();
					WorkflowPageValue = excelReport.getValue().getPageName();
					if (!WSSheetName.isEmpty() && WSSheetName != "" && !WorkflowPageValue.isEmpty() && WorkflowPageValue != "") {
						Output = excelReport.getValue().getOutput();
						PageRemark = excelReport.getValue().getTestRemarks();

						if (PageRemark == null) {
							PageRemark = "";
						}
						if (Output == null) {
							Output = "";
						}

						ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 2, 3));
						ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 4, 5));
//						System.out.println("dr value " + dr);
						ExcelSheet.getRow(dr).getCell(0).setCellValue(WSSheetName);
						ExcelSheet.getRow(dr).getCell(1).setCellValue(WorkflowPageValue);

						String IOData, IOLabel, IOValue, StrOutput = "", StrInput = "", IOLink;
						int IOCount = 0, OCount = 0, ICount = 0;
						if (Output != null) {
							if (Output.contains("|")) {
								for (int io = 0; io < Output.split("\\|").length; io++) {
									IOData = Output.split("\\|")[io];
									if (IOData.contains(";")) {
										IOType = IOData.split(";")[0].trim();
										IOLabel = IOData.split(";")[1].trim();
										IOValue = IOData.split(";")[2].trim();
										IOLink = IOData.split(";")[3].trim();
										if (IOType.equalsIgnoreCase("output")) {
											if (!StrOutput.isEmpty() && StrOutput != "") {
												StrOutput = StrOutput + "\r\n" + IOLabel + " = " + IOValue;
											} else {
												StrOutput = IOLabel + " = " + IOValue;
											}
											OCount = OCount + 1;
										} else if (IOType.equalsIgnoreCase("input")) {
											if (!StrInput.isEmpty() && StrInput != "") {
												StrInput = StrInput + "\r\n" + IOLabel + " = " + IOValue;
											} else {
												StrInput = IOLabel + " = " + IOValue;
											}
											ICount = ICount + 1;
										}
									}
								}
								if (OCount >= ICount) {
									IOCount = OCount;
								} else {
									IOCount = ICount;
								}

							} else {
								IOData = Output;
								if (IOData.contains(";")) {
									IOType = IOData.split(";")[0].trim();
									IOLabel = IOData.split(";")[1].trim();
									IOValue = IOData.split(";")[2].trim();
									IOLink = IOData.split(";")[3].trim();
									if (IOType.equalsIgnoreCase("output")) {
										StrOutput = IOLabel + " = " + IOValue;
									} else if (IOType.equalsIgnoreCase("input")) {
										StrInput = IOLabel + " = " + IOValue;
									}
									IOCount = 1;
								}
							}
						}

						if (!StrOutput.isEmpty() && StrOutput != "") {
							ExcelSheet.getRow(dr).getCell(4).setCellValue(StrOutput);

							if (!PageOutput.isEmpty() && PageOutput != "") {
								PageOutput = PageOutput + "\r\n" + StrOutput;
							} else {
								PageOutput = StrOutput;
							}

						} else {
							ExcelSheet.getRow(dr).getCell(4).setCellValue("-");
						}
						if (!StrInput.isEmpty() && StrInput != "") {
							ExcelSheet.getRow(dr).getCell(2).setCellValue(StrInput);
						} else {
							ExcelSheet.getRow(dr).getCell(2).setCellValue("-");
						}

						for (int dc = 0; dc <= 5; dc++) {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(IOstyle);
						}

						ExcelSheet.getRow(dr).setHeightInPoints(IOCount * 20);
						dr = dr + 1;
					}
				}

				ExcelSheet.getRow(OptRow).getCell(3).setCellValue(PageOutput);
				ExcelSheet.getRow(OptRow).getCell(5).setCellValue(PageRemark.trim());

				trto = dr - 1;
				ExcelSheet.groupRow(trfrom, trto);
				ExcelSheet.setRowGroupCollapsed(trfrom, true);

				dr = trto + 1;

			}
		} catch (Exception e) {
			System.out.println("Excel Suite File Format Issue, Saving file... ");
		}

		try {
			ExcelWorkbook.write(fileOut);
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
