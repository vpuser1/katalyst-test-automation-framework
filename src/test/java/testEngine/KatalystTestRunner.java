package testEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.TestNG;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.beust.jcommander.Parameter;

public class KatalystTestRunner extends ExcelUtil {

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;
	public Map<String, String> testElement;
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;
	

	public KatalystTestRunner(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase,
			String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	public static LinkedHashMap<String, String> KatalystExecutionMap = new LinkedHashMap<String, String>();
	public static LinkedHashMap<String, String> BrowserExecutionMap = new LinkedHashMap<String, String>();
//	String TestCaseName, Browser = null, MultiBrowserExeType, LocalExecutionFlag = null, LocalExecutionBrowser = "", ExecutionFlag = null, TPRMultiBrowserExeType, TPRMultiBrowser, TPRExecutionType, ExecutionType = "Parallel";
	int GlobalThreadCount;
	String GlobalBrowser, GlobalExecutionFlag, GlobalExecutionType;

	String ExecutionMode = "Global", ExecutionBrowser = "", ExecutionType = "", ExecutionTestsFile = "", ExecutionLiveActionPreview = "", ExecutionThread = "1";

	public static void main(String[] args) throws IOException {
		new KatalystTestRunner(null, null, null, null, null, null, null, "", "", "", "").TestRunner();
	}

//	Direct Through KatalystTestRunner = Katalyst_Driver_Sheet
//	Through Executor = Katalyst_Driver_File
	
	public void TestRunner() {
		GetEnvironmentDetails();
//		"KatalystExecutor | " & appname & " | " & testCaseName & " | " & ExecutorThread & " | " & scriptpath & " | " & ExecutionTestsFile & " | " & ExecutionBrowser & " | " & ExecutionLiveActionPreview
		try {
			String TestCaseName = "";
			File KatalystExecutorFilePath = new File(Katalyst_File_Path);
			BufferedReader br;
			String st;
			int TestCount = 1;

			if (KatalystExecutorFilePath.exists()) {
				try {
					br = new BufferedReader(new FileReader(KatalystExecutorFilePath));
					while ((st = br.readLine()) != null) {
						TestCaseName = st.split("\\|")[2].toString().trim();
						if (TestCount == 1) {
							ExecutionMode = st.split("\\|")[0].toString().trim();
							ExecutionThread = st.split("\\|")[3].toString().trim();
							ExecutionTestsFile = st.split("\\|")[5].toString().trim();
							ExecutionBrowser = st.split("\\|")[6].toString().trim();
							ExecutionLiveActionPreview = st.split("\\|")[7].toString().trim();
						}
						KatalystExecutionMap.put(TestCaseName, st);
						System.out.println(st);
						TestCount++;
					}

					KatalystExecutorFilePath.delete();

				} catch (Exception e) {
					System.out.println("Observing Some Exception with Executor Format, Using Global Configuration Approach...");
				}
			}
			
			try {
				TestCaseName = System.getProperty("TestCaseName");
			} catch (Exception e) {
				TestCaseName = "";
			}
			
			if (TestCaseName!=null && !TestCaseName.contentEquals("")) {
				KatalystExecutionMap.put(TestCaseName, "yes");
				ExecutionBrowser = System.getProperty("BrowserName");
				ExecutionTestsFile = getEnvironmentValue("ExecutionTestsFile");
				ExecutionThread = getEnvironmentValue("ExecutionThread");
				ExecutionLiveActionPreview = getEnvironmentValue("ExecutionLiveActionPreview");
			} else {

			String SheetName = "Driver_File";
			if (ExecutionMode.equalsIgnoreCase("global")) {
				String xlPath = Katalyst_Driver_Sheet;
				String LocalExecutionFlag;
				int rowCount = getRowCount(xlPath, SheetName);
				int executionFlagColindex = getColumnWithCellText(xlPath, SheetName, "Execute");

					for (int index = 1; index < rowCount; index++) {
						LocalExecutionFlag = getCellData(xlPath, SheetName, index, executionFlagColindex);
						TestCaseName = getCellData(xlPath, SheetName, index, 1);
						if (LocalExecutionFlag.equalsIgnoreCase("yes") && !TestCaseName.isEmpty() && TestCaseName != "") {
							KatalystExecutionMap.put(TestCaseName, LocalExecutionFlag);
							TestCount++;
						}
					}

				ExecutionBrowser = getEnvironmentValue("ExecutionBrowser");
				ExecutionTestsFile = getEnvironmentValue("ExecutionTestsFile");
				ExecutionThread = getEnvironmentValue("ExecutionThread");
				ExecutionLiveActionPreview = getEnvironmentValue("ExecutionLiveActionPreview");
			} else if (ExecutionMode.equalsIgnoreCase("executor")) {
//				Already Achieved Above
			} else if (ExecutionMode.equalsIgnoreCase("local")) {

			}
			}
//		chrome;Edge;IE
//		chrome:4;Edge:8;IE:6
			int BrowserMapCount = 0;
			String BrowserMap, Browsername = "", BrowserNum = "";

			if (ExecutionBrowser.contains(";")) {
				BrowserMapCount = ExecutionBrowser.split(";").length;
				for (int b = 0; b < BrowserMapCount; b++) {
					BrowserMap = ExecutionBrowser.split(";")[b];
					if (BrowserMap.contains(":")) {
						Browsername = BrowserMap.split(":")[0];
						BrowserNum = BrowserMap.split(":")[1];
						BrowserExecutionMap.put(Browsername, BrowserNum);
					} else {
						Browsername = BrowserMap;
						BrowserExecutionMap.put(Browsername, "1");
					}
				}
			} else {
				BrowserMap = ExecutionBrowser;
				if (BrowserMap.contains(":")) {
					Browsername = BrowserMap.split(":")[0];
					BrowserNum = BrowserMap.split(":")[1];
					BrowserExecutionMap.put(Browsername, BrowserNum);
				} else {
					Browsername = BrowserMap;
					BrowserExecutionMap.put(Browsername, "1");
				}
			}

			XmlSuite xmlSuite = new XmlSuite();
			xmlSuite.setName("Katalyst Automation Execution Status : ");
			xmlSuite.setParallel("Tests");
			xmlSuite.setThreadCount(Integer.parseInt(ExecutionThread));
			List<XmlSuite> listSuite = new ArrayList<XmlSuite>();
			listSuite.add(xmlSuite);
			int index = 2, BrowserIndex;
			String BrowserName;
			
			for (Map.Entry<String, String> KatalystExecutionMap : KatalystExecutionMap.entrySet()) {
				TestCaseName = KatalystExecutionMap.getKey().trim();
//				System.out.println("TestCaseName : "  + TestCaseName);
				for (Map.Entry<String, String> BrowserExecutionMap : BrowserExecutionMap.entrySet()) {
					BrowserName = BrowserExecutionMap.getKey().toString().trim();
					BrowserIndex = Integer.parseInt(BrowserExecutionMap.getValue().toString().trim());
					for (int i = 1; i <= BrowserIndex; i++) {
//						System.out.println(TestCaseName + "_" + BrowserName + "_Test_" + index + "_" + i);
						XmlTest xmlTest = new XmlTest(xmlSuite);
						xmlTest.setName(TestCaseName + "_" + BrowserName + "_Test_" + index + "_" + i);
						xmlTest.addParameter("TestCaseName", TestCaseName);
						xmlTest.addParameter("Browser", BrowserName);
						XmlClass xmlClass = new XmlClass();
						xmlClass.setName("testEngine.TestEngine");
						List<XmlClass> listClass = new ArrayList<XmlClass>();
						listClass.add(xmlClass);
						xmlTest.setXmlClasses(listClass);
					}
				}
				index++;
			}
			
			TestNG testng = new TestNG();
			testng.setXmlSuites(listSuite);
			testng.run();
			KatalystExecutorFilePath.delete();

		} catch (Exception e) {
			System.out.println("Please validate the details...");
		}
	}
}