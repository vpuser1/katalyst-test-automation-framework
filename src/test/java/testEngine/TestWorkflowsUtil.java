package testEngine;

public class TestWorkflowsUtil {

	String TestCaseName, PageName, PageValue, BeforeTest, FollowTest, AfterTest;
	int TestRowNum;

	public TestWorkflowsUtil(String testCaseName, String pageName, String pageValue, String beforeTest,
			String followTest, String afterTest, int TestRowNum) {
		TestCaseName = testCaseName;
		PageName = pageName;
		PageValue = pageValue;
		BeforeTest = beforeTest;
		FollowTest = followTest;
		AfterTest = afterTest;
		this.TestRowNum = TestRowNum;
	}

	public int getTestRowNum() {
		return TestRowNum;
	}

	public void setTestRowNum(int testRowNum) {
		TestRowNum = testRowNum;
	}

	public String getTestCaseName() {
		return TestCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		TestCaseName = testCaseName;
	}

	public String getPageName() {
		return PageName;
	}

	public void setPageName(String pageName) {
		PageName = pageName;
	}

	public String getPageValue() {
		return PageValue;
	}

	public void setPageValue(String pageValue) {
		PageValue = pageValue;
	}

	public String getBeforeTest() {
		return BeforeTest;
	}

	public void setBeforeTest(String beforeTest) {
		BeforeTest = beforeTest;
	}

	public String getFollowTest() {
		return FollowTest;
	}

	public void setFollowTest(String followTest) {
		FollowTest = followTest;
	}

	public String getAfterTest() {
		return AfterTest;
	}

	public void setAfterTest(String afterTest) {
		AfterTest = afterTest;
	}

}
