package testEngine;

import java.util.Map;

public class testSuit {

	public String TestCaseName, TestDescription, TestExecutionStatus, TestOutputDetails, TestExecutionTime, TestRemarks, TestStartTime, TestEndTime, TestCaseLogPath;
	public Map<String, ExcelReport> testIODetails;

	public testSuit(String testCaseName, String testDescription, String testExecutionStatus, String testOutputDetails,
			String TestExecutionTime, String testRemarks, Map<String, ExcelReport> testIODetails, String TestCaseLogPath) {
		super();
		TestCaseName = testCaseName;
		TestDescription = testDescription;
		TestExecutionStatus = testExecutionStatus;
		TestOutputDetails = testOutputDetails;
		this.TestExecutionTime = TestExecutionTime;
		TestRemarks = testRemarks;
		this.testIODetails = testIODetails;
		this.TestCaseLogPath = TestCaseLogPath;
	}

	public String getTestCaseLogPath() {
		return TestCaseLogPath;
	}

	public void setTestCaseLogPath(String testCaseLogPath) {
		TestCaseLogPath = testCaseLogPath;
	}

	public String getTestStartTime() {
		return TestStartTime;
	}

	public void setTestStartTime(String testStartTime) {
		TestStartTime = testStartTime;
	}

	public String getTestEndTime() {
		return TestEndTime;
	}

	public void setTestEndTime(String testEndTime) {
		TestEndTime = testEndTime;
	}

	public String getTestCaseName() {
		return TestCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		TestCaseName = testCaseName;
	}

	public String getTestDescription() {
		return TestDescription;
	}

	public void setTestDescription(String testDescription) {
		TestDescription = testDescription;
	}

	public String getTestExecutionStatus() {
		return TestExecutionStatus;
	}

	public void setTestExecutionStatus(String testExecutionStatus) {
		TestExecutionStatus = testExecutionStatus;
	}

	public String getTestOutputDetails() {
		return TestOutputDetails;
	}

	public void setTestOutputDetails(String testOutputDetails) {
		TestOutputDetails = testOutputDetails;
	}

	public String getTestExecutionTime() {
		return TestExecutionTime;
	}

	public void setTestExecutionTime(String testExecutionTime) {
		TestExecutionTime = testExecutionTime;
	}

	public String getTestRemarks() {
		return TestRemarks;
	}

	public void setTestRemarks(String testRemarks) {
		TestRemarks = testRemarks;
	}

	public Map<String, ExcelReport> getTestIODetails() {
		return testIODetails;
	}

	public void setTestIODetails(Map<String, ExcelReport> testIODetails) {
		this.testIODetails = testIODetails;
	}

}
