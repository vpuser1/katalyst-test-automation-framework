package testEngine;

public class HealthCheckUtil {

	String ApplicationName, TestCaseName, Output, StartTime, EndTime, TotalExecutionTime, Status, Remarks, AppExecutionTime;

	public HealthCheckUtil(String applicationName, String testCaseName, String output, String startTime, String endTime, String totalExecutionTime, String status, String remarks, String appExecutionTime) {
		super();
		ApplicationName = applicationName;
		TestCaseName = testCaseName;
		Output = output;
		StartTime = startTime;
		EndTime = endTime;
		TotalExecutionTime = totalExecutionTime;
		Status = status;
		Remarks = remarks;
		AppExecutionTime = appExecutionTime;
	}

	public String getAppExecutionTime() {
		return AppExecutionTime;
	}

	public void setAppExecutionTime(String appExecutionTime) {
		AppExecutionTime = appExecutionTime;
	}

	public String getApplicationName() {
		return ApplicationName;
	}

	public void setApplicationName(String applicationName) {
		ApplicationName = applicationName;
	}

	public String getTestCaseName() {
		return TestCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		TestCaseName = testCaseName;
	}

	public String getOutput() {
		return Output;
	}

	public void setOutput(String output) {
		Output = output;
	}

	public String getStartTime() {
		return StartTime;
	}

	public void setStartTime(String startTime) {
		StartTime = startTime;
	}

	public String getEndTime() {
		return EndTime;
	}

	public void setEndTime(String endTime) {
		EndTime = endTime;
	}

	public String getTotalExecutionTime() {
		return TotalExecutionTime;
	}

	public void setTotalExecutionTime(String totalExecutionTime) {
		TotalExecutionTime = totalExecutionTime;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

}
