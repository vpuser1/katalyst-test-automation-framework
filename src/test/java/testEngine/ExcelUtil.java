package testEngine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.mail.search.AddressTerm;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class ExcelUtil extends GearBox {

	public FileInputStream fi;
	public XSSFWorkbook ExcelWorkbook;
	public XSSFSheet ExcelSheet;
	XSSFRow ExcelRows, rowGlobal;
	XSSFCell ExcelColumns, cellGlobal;
	int RowNum, ColumnNum;

	String RetrievedTestCase = null, ElementName, ObjectType, ObjectIdentification, ActionTaken, OnFail, Snapshot, MarkObject, LoadInReport, TestCaseValue, PageName, Wait, PageDescription = "", ElementDescription;

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;// = new LinkedHashMap<String, LoadInReports>(); // removed static
	public Map<String, String> testElement;// = new LinkedHashMap<String, String>(); // removed static
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

//***************************************************************************************************************************************************

	public ExcelUtil(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	public XSSFWorkbook GetWorkBook(String WorkbookPath) {
		try {
			FileInputStream file = new FileInputStream(new java.io.File(WorkbookPath));
			ExcelWorkbook = new XSSFWorkbook(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ExcelWorkbook;
	}

	XSSFSheet GetExcelSheet(String SheetName) {
		ExcelSheet = ExcelWorkbook.getSheet(SheetName);
		return ExcelSheet;
	}

//***************************************************************************************************************************************************

//	public void SetGlobalParameters() {
//		String RetrievedTestCase = null, ParameterName, ParameterValue;
//		ExcelSheet = GetWorkBook(testWorkbook).getSheet("GlobalParameters");
//		
//		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
//		// Row Iterator
//		while (RowIterator.hasNext()) {
//			Row row = (Row) RowIterator.next();
//			Iterator<Cell> CellIterator = row.cellIterator();
//			RowNum = row.getRowNum();
//			if (RowNum > 0) {
//				// Cell Iterator
//				while (CellIterator.hasNext()) {
//					Cell cell = (Cell) CellIterator.next();
//					ColumnNum = cell.getColumnIndex();
//					if (ColumnNum == 0) {
//						RetrievedTestCase = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
//					}
//					if (RetrievedTestCase.equalsIgnoreCase(TestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
//						ParameterName = ExcelSheet.getRow(0).getCell(ColumnNum).getStringCellValue();
//						ParameterValue = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
////						System.out.println(RetrievedTestCase + " => " + ParameterName + " : " + ParameterValue);
//						GlobalParameters.putIfAbsent(ParameterName, ParameterValue.toString());
//					}
//
//				}
//			}
//		}
//}

	public void SetGlobalParameters() {
		String RetrievedTestCase = null, ParameterName, ParameterValue, ProvidedTestCase, ParamObjectType = "";
		String SheetName = null;
		String PageName = null;
		XSSFWorkbook wb = GetWorkBook(testWorkbook);

		boolean flag = false;
		for (int i = 0; i < wb.getNumberOfSheets(); i++) {
			SheetName = wb.getSheetAt(i).getSheetName();
			if (!SheetName.equalsIgnoreCase("GlobalParameters") && !SheetName.equalsIgnoreCase("TestWorkflow") && !SheetName.equalsIgnoreCase("GlobalObjects") && !SheetName.equalsIgnoreCase("Devices")) {
				ExcelSheet = GetWorkBook(testWorkbook).getSheet(SheetName);
				for (int f = 0; f < testArray.size(); f++) {
					ProvidedTestCase = testArray.get(f).toString();
					Iterator<Row> RowIterator = ExcelSheet.rowIterator();
					while (RowIterator.hasNext()) {
						Row row = (Row) RowIterator.next();
						Iterator<Cell> CellIterator = row.cellIterator();
						RowNum = row.getRowNum();
						if (RowNum > 0) {
							// Cell Iterator
							while (CellIterator.hasNext()) {
								Cell cell = (Cell) CellIterator.next();
								ColumnNum = cell.getColumnIndex();
								if (ColumnNum == 0) {
									RetrievedTestCase = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
								}
								if (RetrievedTestCase.equalsIgnoreCase(ProvidedTestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
									ParameterName = GetCellValue(ExcelSheet.getRow(0).getCell(ColumnNum)).toString();
									ParameterValue = GetCellValue(ExcelSheet.getRow(RowNum).getCell(ColumnNum)).toString();

									try {
										ParamObjectType = GetCellValue(ExcelSheet.getRow(2).getCell(ColumnNum)).toString();
									} catch (Exception e) {
										ParamObjectType = "";
									}
									if (ParamObjectType.equalsIgnoreCase("GlobalParameter")) {
//										System.out.println("Parameter : " + ProvidedTestCase + "#" + ParameterName + " => " + ParameterValue.toString());
										GlobalParameters.putIfAbsent(ProvidedTestCase + "#" + ParameterName, new GlobalParametersUtil(ProvidedTestCase, ParameterName, ParameterValue.toString())); // ParameterValue.toString()
									}
								}

							}
						}
					}
				}

			}

		}

	}

	public void SetGlobalParametersT() {
		String RetrievedTestCase = null, ParameterName, ParameterValue, ProvidedTestCase;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("GlobalParameters");

		for (int f = 0; f < testArray.size(); f++) {
			ProvidedTestCase = testArray.get(f).toString();

			Iterator<Row> RowIterator = ExcelSheet.rowIterator();
			// Row Iterator
			while (RowIterator.hasNext()) {
				Row row = (Row) RowIterator.next();
				Iterator<Cell> CellIterator = row.cellIterator();
				RowNum = row.getRowNum();
				if (RowNum > 0) {
					// Cell Iterator
					while (CellIterator.hasNext()) {
						Cell cell = (Cell) CellIterator.next();
						ColumnNum = cell.getColumnIndex();
						if (ColumnNum == 0) {
							RetrievedTestCase = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
						}
						if (RetrievedTestCase.equalsIgnoreCase(ProvidedTestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
							ParameterName = ExcelSheet.getRow(0).getCell(ColumnNum).getStringCellValue();
							ParameterValue = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
//						System.out.println(RetrievedTestCase + " => " + ParameterName + " : " + ParameterValue);
//							GlobalParametersUtil.putIfAbsent(ParameterName, ParameterValue.toString());
							GlobalParameters.putIfAbsent(ProvidedTestCase + "#" + ParameterName, new GlobalParametersUtil(ProvidedTestCase, ParameterName, ParameterValue.toString())); // ParameterValue.toString()
						}

					}
				}
			}
		}

	}

	public void SetGlobalParametersForFlowedTest(String TestCaseName) {
		String RetrievedTestCase = null, ParameterName, ParameterValue;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("GlobalParameters");
		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
		// Row Iterator
		while (RowIterator.hasNext()) {
			Row row = (Row) RowIterator.next();
			Iterator<Cell> CellIterator = row.cellIterator();
			RowNum = row.getRowNum();
			if (RowNum > 0) {
				// Cell Iterator
				while (CellIterator.hasNext()) {
					Cell cell = (Cell) CellIterator.next();
					ColumnNum = cell.getColumnIndex();
					if (ColumnNum == 0) {
						RetrievedTestCase = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
					}
					if (RetrievedTestCase.equalsIgnoreCase(TestCaseName) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
						ParameterName = ExcelSheet.getRow(0).getCell(ColumnNum).getStringCellValue();
						ParameterValue = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
						System.out.println(RetrievedTestCase + " => " + ParameterName + " : " + ParameterValue);
//						GlobalParameters.putIfAbsent(ParameterName, ParameterValue.toString());
					}

				}
			}
		}
	}

	public String GetParameterValue(String CurrentTestCase, String ExcelParameterName) {
		String ExcelParameterValue = "";
		boolean Parameterflag = false;
		// TestCase + "#" +
		for (Entry<String, GlobalParametersUtil> GlobalParameter : GlobalParameters.entrySet()) {
			if (GlobalParameter.getKey().toString().equalsIgnoreCase(CurrentTestCase + "#" + ExcelParameterName)) {
//				System.out.println(GlobalParameter.getKey() + "   " + GlobalParameter.getValue().toString());
				ExcelParameterValue = GlobalParameter.getValue().getParameterValue().toString();
				Parameterflag = true;
				break;
			}
		}
		if (Parameterflag == false) {
			ExcelParameterValue = "";
		}

		return ExcelParameterValue;
	}

	public String GetPageParameterValue(String ExcelParameterName) {
		String ExcelParameterValue = null;
		boolean Parameterflag = false;
		for (Map.Entry<String, PageUtil> PageParameter : GetPage().entrySet()) {
			if (PageParameter.getKey().toString().equalsIgnoreCase(ExcelParameterName)) {
//				System.out.println(GlobalParameter.getKey() + "   " + GlobalParameter.getValue().toString());
				ExcelParameterValue = PageParameter.getValue().toString();
				Parameterflag = true;
			}
		}
		if (Parameterflag == false) {
			ExcelParameterValue = "";
		}

		return ExcelParameterValue;
	}

	public String GetTestPageDescription() {
		String ExcelParameterValue = null;
		boolean Parameterflag = false;
		for (Map.Entry<String, PageUtil> PageParameter : GetPage().entrySet()) {
//			if (PageParameter.getKey().toString().equalsIgnoreCase(ExcelParameterName)) {
//				System.out.println(GlobalParameter.getKey() + "   " + GlobalParameter.getValue().toString());
			ExcelParameterValue = PageParameter.getValue().getPageDescription();
			Parameterflag = true;
//			}
		}
		if (Parameterflag == false) {
			ExcelParameterValue = "";
		}

		return ExcelParameterValue;
	}

//***************************************************************************************************************************************************

	public void SetTestWorkflowOld() {
		String RetrievedTestCase = null, PageName, PageValue, BeforeTest, FollowTest, AfterTest;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
		boolean flag = false, FollowTestFlag = false;
		// Row Iterator
		while (RowIterator.hasNext()) {
			Row row = (Row) RowIterator.next();
			Iterator<Cell> CellIterator = row.cellIterator();
			RowNum = row.getRowNum();
			if (RowNum > 0) {
				// Cell Iterator
				while (CellIterator.hasNext()) {
					Cell cell = (Cell) CellIterator.next();
					ColumnNum = cell.getColumnIndex();
					if (ColumnNum > 3) {

						RetrievedTestCase = GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString();
//						System.out.println("RetrievedTestCase : " + RetrievedTestCase + " - " + ColumnNum);
						// Get Data Based On Provided Test Case
						if (RetrievedTestCase.equalsIgnoreCase(SourceTestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
							PageName = GetCellValue(ExcelSheet.getRow(0).getCell(ColumnNum)).toString();
							PageValue = GetCellValue(ExcelSheet.getRow(RowNum).getCell(ColumnNum)).toString();
							BeforeTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString();
							FollowTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(2)).toString();
							AfterTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(3)).toString();

							if (FollowTest == "" && FollowTest.isEmpty()) {
								if (!PageName.isEmpty() && !PageValue.isEmpty()) {
//									System.out.println(RowNum + " | " + ColumnNum + " | " + RetrievedTestCase + " | " + PageName + " | " + PageValue);
									TestWorkflows.put(RetrievedTestCase + "#" + PageName, new TestWorkflowsUtil(RetrievedTestCase, PageName, PageValue, BeforeTest, FollowTest, AfterTest, RowNum));

								}
								FollowTestFlag = false;
							}
							if (FollowTest != "" && !FollowTest.isEmpty()) {
								FollowTestFlag = true;
								SetTestWorkflowForFollowedTests(SourceTestCase);
								flag = true;
								break;
							}

						}

					}
				}
				if (flag) {
					break;
				}

			}
		}

	}

	public void SetTestWorkflow() {
		String RetrievedTestCase = null, PageName, PageValue, BeforeTest, FollowTest, AfterTest;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
//		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
		boolean flag = false, FollowTestFlag = false;
		int TotalRowNum = 0, TotalColNum = 0;
		TotalRowNum = ExcelSheet.getPhysicalNumberOfRows();
//		while (RowIterator.hasNext()) {
		for (int r = 0; r < TotalRowNum; r++) {
//			Row row = (Row) RowIterator.next();
//			Iterator<Cell> CellIterator = row.cellIterator();
			RowNum = r;
			TotalColNum = ExcelSheet.getRow(r).getPhysicalNumberOfCells();
//			RowNum = row.getRowNum();
			if (RowNum > 0) {
				// Cell Iterator
//				while (CellIterator.hasNext()) {
				for (int c = 0; c < TotalColNum + 4; c++) {
//					Cell cell = (Cell) CellIterator.next();
					ColumnNum = c;
//					ColumnNum = cell.getColumnIndex();
					if (ColumnNum > 1) {

						RetrievedTestCase = GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString();
//						System.out.println("RetrievedTestCase : " + RetrievedTestCase + " - " + ColumnNum);
						// Get Data Based On Provided Test Case
						if (RetrievedTestCase.equalsIgnoreCase(SourceTestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
							PageName = GetCellValue(ExcelSheet.getRow(0).getCell(ColumnNum)).toString();
							PageValue = GetCellValue(ExcelSheet.getRow(RowNum).getCell(ColumnNum)).toString();
							BeforeTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString();
							FollowTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(2)).toString();
							AfterTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(3)).toString();

							if (FollowTest == "" && FollowTest.isEmpty()) {
								if (!PageName.isEmpty() && !PageValue.isEmpty()) {
//									System.out.println(RowNum + " | " + ColumnNum + " | " + RetrievedTestCase + " | " + PageName + " | " + PageValue);
									TestWorkflows.put(RetrievedTestCase + "#" + PageName, new TestWorkflowsUtil(RetrievedTestCase, PageName, PageValue, BeforeTest, FollowTest, AfterTest, RowNum));

								}
								FollowTestFlag = false;
							}
							if (FollowTest != "" && !FollowTest.isEmpty()) {
								FollowTestFlag = true;
								SetTestWorkflowForFollowedTests(SourceTestCase);
								flag = true;
								break;
							}

						}

					}
				}
				if (flag) {
					break;
				}

			}
		} // Totoal Rows

	}

	public void SetTestWorkflowForFollowedTests(String PrimaryTestCaseName) { // , String FollowedTestCaseName
//		FollowedTestsRecursions(PrimaryTestCaseName);
//		Collections.reverse(testArray);
//		System.out.println("After Reverse..." + testArray);
		String RetrievedTestCase = null, PageName, PageValue, BeforeTest, FollowTest, AfterTest, ProvidedTestCase;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");

		for (int f = 0; f < testArray.size(); f++) {
			ProvidedTestCase = testArray.get(f).toString();
			Iterator<Row> RowIterator = ExcelSheet.rowIterator();
			boolean flag = false;
			// Row Iterator

			while (RowIterator.hasNext()) {
				Row row = (Row) RowIterator.next();
				Iterator<Cell> CellIterator = row.cellIterator();
				RowNum = row.getRowNum();
				if (RowNum > 0) {
					// Cell Iterator
					while (CellIterator.hasNext()) {
						Cell cell = (Cell) CellIterator.next();
						ColumnNum = cell.getColumnIndex();
						if (ColumnNum > 3) {

							RetrievedTestCase = GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString();
							// Get Data Based On Provided Test Case
							if (RetrievedTestCase.equalsIgnoreCase(ProvidedTestCase) && !RetrievedTestCase.isEmpty() && ColumnNum > 0) {
								PageName = GetCellValue(ExcelSheet.getRow(0).getCell(ColumnNum)).toString();
								PageValue = GetCellValue(ExcelSheet.getRow(RowNum).getCell(ColumnNum)).toString();
								BeforeTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString();
								FollowTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(2)).toString();
								AfterTest = GetCellValue(ExcelSheet.getRow(RowNum).getCell(3)).toString();

								if (!PageName.isEmpty() && !PageValue.isEmpty()) {
//									System.out.println(RowNum + " | " + ColumnNum + " | " + RetrievedTestCase + " | " + PageName + " | " + PageValue);
									TestWorkflows.put(RetrievedTestCase + "#" + PageName, new TestWorkflowsUtil(RetrievedTestCase, PageName, PageValue, BeforeTest, FollowTest, AfterTest, RowNum));
									flag = true;
//									break;

								}

							}

						}
					}

				}
				if (flag) {
					break;
				}
			}

		}
	}

	public void FollowedTestsRecursions(String PrimaryTestCaseName) {
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
		int RowCount = ExcelSheet.getPhysicalNumberOfRows();
		String RetrievedTestCaseName = "", RetrievedFollowedTestCaseName = "";
		try {
			for (int ro = 1; ro < RowCount; ro++) {
				RetrievedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(0)).toString();
				RetrievedFollowedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(2)).toString();
//				System.out.println(ro + " - " + RetrievedTestCaseName);
//				if (ro == 122) {
//					System.out.println();
//				}

				if (RetrievedTestCaseName != null) {
					if (RetrievedTestCaseName.equalsIgnoreCase(PrimaryTestCaseName)) {
						if (RetrievedFollowedTestCaseName != "" && !RetrievedFollowedTestCaseName.isEmpty()) {
//						System.out.println("RetrievedTestCaseName " + RetrievedTestCaseName);
							testArray.add(RetrievedTestCaseName);
							FollowedTestsRecursions(RetrievedFollowedTestCaseName);
						} else {
//						System.out.println("RetrievedTestCaseName " + RetrievedTestCaseName);
							testArray.add(RetrievedTestCaseName);
						}
					}
				}
			}
		} catch (Exception e) {
			OnErrorLoadInReport("ERROR : Observing issue in [FollowedTestsRecursions] function for [" + PrimaryTestCaseName + "] testcase.");
		}

	}

	public String GetTestWorkflow(String ExcelPageName) {
		String ExcelPageValue = null;
		boolean Pageflag = false;
		for (Map.Entry<String, TestWorkflowsUtil> TestWorkflow : TestWorkflows.entrySet()) {
			if (TestWorkflow.getKey().toString().equalsIgnoreCase(ExcelPageName)) {
//				System.out.println(TestWorkflow.getKey() + "   " + TestWorkflow.getValue().toString());
				ExcelPageValue = TestWorkflow.getValue().toString();
				Pageflag = true;
			}
		}
		if (Pageflag == false) {
			ExcelPageValue = "Fail";
		}
		return ExcelPageValue;
	}

	public Map<String, TestWorkflowsUtil> GetAllTestWorkflow() {
		return TestWorkflows;
	}

//***************************************************************************************************************************************************

	public void SetPageDetails(String SheetName, String SheetsPageName, String SourceTestCase, String CurrentTestCase) {

		int TestRowNum = 0;
		int ColumnNum = 0;
		boolean TestPageFlag = false;

		ExcelSheet = GetWorkBook(testWorkbook).getSheet(SheetName);

		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
		while (RowIterator.hasNext()) {
			Row row = (Row) RowIterator.next();
			RowNum = row.getRowNum();
			try {
				RetrievedTestCase = ExcelSheet.getRow(RowNum).getCell(ColumnNum).getStringCellValue();
			} catch (Exception E) {
				RetrievedTestCase = "";
			}
			if (RetrievedTestCase.equalsIgnoreCase(CurrentTestCase) && !RetrievedTestCase.isEmpty()) {
				TestRowNum = RowNum;
				TestPageFlag = true;
				break;
			}
		}

		if (TestPageFlag == false) {

			System.out.println("Test Case [" + CurrentTestCase + "] is missing in Page [" + PageName + "].");

		}

		if (TestPageFlag == true) {
			RetrievePageTestCaseDetails(TestRowNum, SheetsPageName, SourceTestCase);
		}
	}

	public void RetrievePageTestCaseDetails(int TestRowNum, String SheetsPageName, String SourceTestCase) {
		String PageDescription = "";
		int ColumnCount = 0;
		Page.clear(); // Clear the Page Content
		Iterator<Row> NewRowIterator = ExcelSheet.rowIterator();
		while (NewRowIterator.hasNext()) {
			Row row = (Row) NewRowIterator.next();
			ColumnCount = row.getPhysicalNumberOfCells();
			Iterator<Cell> CellIterator = row.cellIterator();
			RowNum = row.getRowNum();
			ColumnNum = 1;

			if (RowNum == TestRowNum) {
//				while (CellIterator.hasNext()) {
				while (ColumnNum <= ColumnCount + 1) {
//					Cell cell = (Cell) CellIterator.next();
//					ColumnNum = cell.getColumnIndex();
					if (ColumnNum > 0) {
						try {
							ElementName = GetCellValue(ExcelSheet.getRow(0).getCell(ColumnNum)).toString();
//							System.out.println("ElementName " + ElementName);
						} catch (Exception e) {
//							e.printStackTrace();
							ElementName = "";
						}
						if (!ElementName.isEmpty() && ColumnNum > 0) {
							PageName = GetCellValue(ExcelSheet.getRow(1).getCell(ColumnNum)).toString();
							ObjectType = GetCellValue(ExcelSheet.getRow(2).getCell(ColumnNum)).toString();
							if (ElementName.equalsIgnoreCase(SheetsPageName)) {
								if (ObjectType.equalsIgnoreCase("PageDescription")) {
									PageDescription = GetCellValue(ExcelSheet.getRow(10).getCell(ColumnNum)).toString();
								}
							}
							if (SheetsPageName.equalsIgnoreCase(PageName)) {
								String TestCase = GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString();
//								ObjectType = GetCellValue(ExcelSheet.getRow(2).getCell(ColumnNum)).toString();
								ObjectIdentification = GetCellValue(ExcelSheet.getRow(3).getCell(ColumnNum)).toString();
								ActionTaken = GetCellValue(ExcelSheet.getRow(4).getCell(ColumnNum)).toString();
								OnFail = GetCellValue(ExcelSheet.getRow(5).getCell(ColumnNum)).toString();
								MarkObject = GetCellValue(ExcelSheet.getRow(6).getCell(ColumnNum)).toString();
								LoadInReport = GetCellValue(ExcelSheet.getRow(7).getCell(ColumnNum)).toString();
								Snapshot = GetCellValue(ExcelSheet.getRow(8).getCell(ColumnNum)).toString();
								Wait = GetCellValue(ExcelSheet.getRow(9).getCell(ColumnNum)).toString();
								ElementDescription = GetCellValue(ExcelSheet.getRow(10).getCell(ColumnNum)).toString();
								TestCaseValue = GetCellValue(ExcelSheet.getRow(RowNum).getCell(ColumnNum)).toString();
//								PageDescription = GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString();
//								System.out.println(ColumnNum + " " + ElementName + " 	" + PageName + " " + ObjectType + " " + ObjectIdentification + " " + ActionTaken + " " + OnFail + " " + Snapshot + " " + TestCaseValue);
								Page.put(TestCase + "#" + ElementName, new PageUtil(SourceTestCase, TestCase, ElementName, PageName, ObjectType, ObjectIdentification, ActionTaken, OnFail, Snapshot, TestCaseValue, MarkObject, LoadInReport, Wait, PageDescription, ElementDescription));
							}
						}

						ColumnNum++;
					}
				}
			}
		}
	}

	public Map<String, PageUtil> GetPage() {
//		for (Map.Entry<String, PageUtil> page : Page.entrySet()) {
//		System.out.println(page.getKey() + "   " + page.getValue().getPageName());
//		}
		return Page;
	}

	public Object GetCellValue(Cell cell) {
		Object CellValue = "";
		try {
			switch (cell.getCellType()) {

			case Cell.CELL_TYPE_STRING:
				CellValue = cell.getStringCellValue();
				break;
			case Cell.CELL_TYPE_NUMERIC:
				CellValue = cell.getNumericCellValue();
				break;
			case Cell.CELL_TYPE_BLANK:
				CellValue = "";
				break;
			default:
				CellValue = "";
				break;
			}
		}

		catch (Exception e) {
			CellValue = "";
		}
		return CellValue;
	}

	public boolean IsSheetPresent(String SheetName) {
		boolean status = false;
		for (int i = 0; i < GetWorkBook(testWorkbook).getNumberOfSheets(); i++) {
			if (GetWorkBook(testWorkbook).getSheetAt(i).getSheetName().equalsIgnoreCase(SheetName)) {
				status = true;
				break;
			} else {

			}
		}
		return status;
	}

	public String GetSheetNameForPage(String Page) {
		String SheetName = null;
		String PageName = null;
		XSSFWorkbook wb = GetWorkBook(testWorkbook);

		boolean flag = false;
		for (int i = 0; i < wb.getNumberOfSheets(); i++) {
			SheetName = wb.getSheetAt(i).getSheetName();
			if (!SheetName.equalsIgnoreCase("GlobalParameters") && !SheetName.equalsIgnoreCase("TestWorkflow") && !SheetName.equalsIgnoreCase("GlobalObjects") && !SheetName.equalsIgnoreCase("Devices")) {
				int ColumnCount = wb.getSheet(SheetName).getRow(0).getPhysicalNumberOfCells();
				for (int c = 2; c < ColumnCount; c++) {
					PageName = wb.getSheet(SheetName).getRow(1).getCell(c).getStringCellValue();
					if (PageName.equalsIgnoreCase(Page)) {
//						System.out.println("Sheet Name = " + SheetName);
						flag = true;
						break;
					}
				}

				if (flag == true) {
					break;
				}
			}

		}

		if (IsSheetPresent(SheetName) == true) {
			System.out.println("\n[" + SheetName + "] Sheet Present in WorkBook");
		} else {
			System.out.println("\n[" + SheetName + "] Sheet Missing From WorkBook");
			Assert.fail("[" + SheetName + "] Sheet Missing From WorkBook");
//			System.exit(0);
		}

		return SheetName;
	}

	public String GetTimeStamp() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy.HH.mm.ss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String s = sdf.format(timestamp);
		String EndCustPostFix = s.replace(".", "");
		return EndCustPostFix;

	}

	public void SetGlobalObjects() {
		String ObjectName = "", SystemBrowserObject = "", MobileBrowserObject = "", MobileApplicationObject = "", TestCaseName = CurrentTestCase;
		int RowNum, ColumnNum;
		ExcelSheet = GetWorkBook(testWorkbook).getSheet("GlobalObjects");
		Iterator<Row> RowIterator = ExcelSheet.rowIterator();
		// Row Iterator
		while (RowIterator.hasNext()) {
			Row row = (Row) RowIterator.next();
			Iterator<Cell> CellIterator = row.cellIterator();
			RowNum = row.getRowNum();
			if (RowNum > 0) {
				// Cell Iterator
				while (CellIterator.hasNext()) {
					Cell cell = (Cell) CellIterator.next();
					ColumnNum = cell.getColumnIndex();
					if (ColumnNum == 0) {
						if (GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString() == null) {
							ObjectName = "";
						} else {
							ObjectName = GetCellValue(ExcelSheet.getRow(RowNum).getCell(0)).toString();
						}

						if (GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString() == null) {
							SystemBrowserObject = "";
						} else {
							SystemBrowserObject = GetCellValue(ExcelSheet.getRow(RowNum).getCell(1)).toString();
						}

						if (GetCellValue(ExcelSheet.getRow(RowNum).getCell(2)).toString() == null) {
							MobileBrowserObject = "";
						} else {
							MobileBrowserObject = GetCellValue(ExcelSheet.getRow(RowNum).getCell(2)).toString();
						}

						if (GetCellValue(ExcelSheet.getRow(RowNum).getCell(3)).toString() == null) {
							MobileApplicationObject = "";
						} else {
							MobileApplicationObject = GetCellValue(ExcelSheet.getRow(RowNum).getCell(3)).toString();
						}

//						System.out.println("Global Objects Added..." + ObjectName + " | " + SystemBrowserObject + " | " + MobileBrowserObject + " | " + MobileApplicationObject);
						GlobalObjects.put(ObjectName, new GlobalObjectParam(ObjectName, SystemBrowserObject, MobileBrowserObject, MobileApplicationObject, TestCaseName));

					}
				}
			}
		}
	}

	public int getRowCount(String xlfile, String xlsheet) throws IOException {
		fi = new FileInputStream(xlfile);
		ExcelWorkbook = new XSSFWorkbook(fi);
		ExcelSheet = ExcelWorkbook.getSheet(xlsheet);
		int rowcount = ExcelSheet.getLastRowNum() + 1;
		ExcelWorkbook.close();
		fi.close();
		return rowcount;
	}

	public int getColumnWithCellText(String xlfile, String xlsheet, String text) throws IOException {

		int colcount;
		int rowIndex;

		fi = new FileInputStream(xlfile);
		ExcelWorkbook = new XSSFWorkbook(fi);
		ExcelSheet = ExcelWorkbook.getSheet(xlsheet);
		int rowcount = ExcelSheet.getLastRowNum() + 1;

		for (rowIndex = 0; rowIndex < rowcount; rowIndex++) {
			rowGlobal = ExcelSheet.getRow(rowIndex);
			colcount = rowGlobal.getLastCellNum() + 1;
			for (int colIndex = 0; colIndex < colcount; colIndex++) {
				cellGlobal = rowGlobal.getCell(colIndex);
				try {
					DataFormatter formatter = new DataFormatter();
					String cellData = formatter.formatCellValue(cellGlobal);
					cellData = cellData.replaceAll("\n", " ").trim();
					if (cellData.equals(text)) {
						return colIndex;
					}
				} catch (Exception e) {

				}
			}
		}
		ExcelWorkbook.close();
		fi.close();
		return -1;
	}

	public String getCellData(String xlfile, String xlsheet, int rownum, int colnum) throws IOException {
		fi = new FileInputStream(xlfile);
		ExcelWorkbook = new XSSFWorkbook(fi);
		ExcelSheet = ExcelWorkbook.getSheet(xlsheet);
		rowGlobal = ExcelSheet.getRow(rownum);
		cellGlobal = rowGlobal.getCell(colnum);
		String data;
		try {
			DataFormatter formatter = new DataFormatter();
			String cellData = formatter.formatCellValue(cellGlobal);
			return cellData;
		} catch (Exception e) {
			data = "";
		}
		ExcelWorkbook.close();
		fi.close();
		return data;
	}

	public void GetEnvironmentDetails() {
		String value, Key;
		EnvironmentMap.clear();
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(new File(EnvironmentFilePath)));
			String line = reader.readLine();
			while (line != null) {
				if (line.contains("=")) {
					Key = line.split("=")[0];
					try {
						value = line.split("=")[1];
					} catch (Exception e) {
						value = "";
					}
					EnvironmentMap.put(Key, value);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getEnvironmentValue(String ProvKey) {
		String value = "", Key;

		for (Entry<String, String> env : EnvironmentMap.entrySet()) {
			Key = env.getKey().trim();
			if (ProvKey.trim().equalsIgnoreCase(Key)) {
				value = env.getValue().trim();
				break;
			}
		}

		return value;
	}

	public void addnewsheet(String filePath, String SheetName) {
		try {
			System.out.println("file path is: " + filePath);
			FileInputStream inputStream = new FileInputStream(new File(filePath));
			Workbook workbook = WorkbookFactory.create(inputStream);
			Sheet newSheet = workbook.createSheet(SheetName);
			// Workbook wb = new HSSFWorkbook();
			FileOutputStream fileOut = new FileOutputStream(filePath);
			workbook.write(fileOut);
			workbook.close();
			fileOut.close();
			System.out.println("New worksheet " + SheetName + " has been added");
		} catch (Exception e) {
			System.out.println("Unable to add new worksheet " + SheetName + " at " + filePath);
		}

	}

	public void updateColumnName(String filePath, String SheetName) throws Exception {
		System.out.println("file path is: " + filePath);
		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook wb = WorkbookFactory.create(excelFile);
		Sheet sheet = wb.getSheet(SheetName);
		// String cellData = "";
		int colNum = 0;
		boolean colFound = false;
		boolean rowFound = false;
		try {
			Row row = sheet.getRow(0);
			for (int j = 0; j < row.getLastCellNum(); j++) {
				Cell c = row.getCell(j);
				if (c != null) {
					c.setCellType(Cell.CELL_TYPE_STRING);
					String targetcolumnname = c.getStringCellValue().toUpperCase();
//					System.out.println("target column name is: " + targetcolumnname);
					targetcolumnname = targetcolumnname.trim().replace(" ", "_");
//					System.out.println("target column name is: " + targetcolumnname);
					c.setCellValue(targetcolumnname);
				}
			}
			FileOutputStream outputStream = new FileOutputStream(filePath);
			wb.write(outputStream);
			wb.close();
			outputStream.close();
			System.out.println("Local Actual report column name updated.");
		} catch (Exception e) {
			System.out.println("Error while updating column name, Exception : " + e.getMessage());
		}
	}

}
