package testEngine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;

//import testDrivers.ConfigUtil;

public class ExcelReportUtil extends GearBox {

	public LinkedHashMap<String, String> ExcellogMap;
	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;
	public Map<String, String> testElement;
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

	public ExcelReportUtil(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	public void ExcelPageContents() {
		String WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, TestCaseName, IOType, CurrentTestCase, SourceTestCase, ElementDescription = "";
		String CapturedPage = "";
		String outflag = "";
		int PageElementsCount, count;
		String PageIO = "";
		String PageRemark = "";
		count = 1;

		// Middle Main Page Block
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageDescription = loadInReport.getValue().getTestDescription();
			LoadInReport = loadInReport.getValue().getLoadInReport();
			ElementName = loadInReport.getValue().getElementName();
			ActionTaken = loadInReport.getValue().getActionTaken();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();
			Output = loadInReport.getValue().getOutput();
			StartDate = loadInReport.getValue().getExecutionStartTime();
			EndDate = loadInReport.getValue().getExecutionEndTime();
			TestRemarks = loadInReport.getValue().getTestRemarks();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();
			TestCaseName = loadInReport.getValue().getTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			IOType = loadInReport.getValue().getIOType();
			ElementDescription = loadInReport.getValue().getElementDescription();

			if (CapturedPage.contains(WorkflowPageValue)) {
				outflag = "true"; // Working on same page
				if (Output != null) {
					PageIO = PageIO + "|" + Output;
				}

				if (TestRemarks != null) {
					PageRemark = TestRemarks; // PageRemark + "|" +
				}

			} else {
				count = 1;
				PageIO = Output;
				PageRemark = TestRemarks;
			}

			if (count == PageElementsCount || ExecutionStatus.equalsIgnoreCase("Fail")) {
				String Key = CurrentTestCase + "#" + ElementName; // WSSheetName + "#" + WorkflowPageValue;
				excelReports.put(Key, new ExcelReport(WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, StartDate, EndDate, PageRemark, PageIO, CurrentTestCase, ElementName));

			} else {

			}

			count = count + 1;
			CapturedPage = CapturedPage + "," + WorkflowPageValue;
		}

	}

	public void GetExcelPageInfo() {
		String WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, TestCaseName;

		for (Entry<String, ExcelReport> excelReport : excelReports.entrySet()) {
			WSSheetName = excelReport.getValue().getSheetName();
			WorkflowPageValue = excelReport.getValue().getPageName();
			ExecutionStatus = excelReport.getValue().getExecutionStatus();
			PageDescription = excelReport.getValue().getTestDescription();
			Output = excelReport.getValue().getOutput();
			StartDate = excelReport.getValue().getExecutionStartTime();
			EndDate = excelReport.getValue().getExecutionEndTime();
			TestRemarks = excelReport.getValue().getTestRemarks();
			TestCaseName = excelReport.getValue().getTestCase();

//			System.out.println(TestCaseName + " | " + WSSheetName + " | " + WorkflowPageValue + " | " + ExecutionStatus
//					+ " | " + PageDescription + " | " + Output + " | " + StartDate + " | " + EndDate + " | "
//					+ TestRemarks);
		}
	}

	public void ExcelReportCreator(String ApplicationName, String TestCaseName, String ExecutionStartTime, String ExecutionEndTimeTime, String TotalExecutionTime, String TestCaseDescription, String TestDesigner, String TestExecutor, String PassPercentage, GearBox gearbox) throws Exception {

		String ExecutionBlockStatus;
		int LogCount = 0;
		if (PassPercentage.contains("100")) {
			ExecutionBlockStatus = "Pass";
		} else {
			ExecutionBlockStatus = "Fail";
		}

		RARHealthCheckInitialSetup(TestCaseName); // Set Initial Health Check structure...

		String LogTestPath = reportPath + "/" + TestCaseName;
//		String ExcelPath = LogTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_" + reportTimeStamp
//				+ ".xlsx";
//		String FilePath = LogTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_" + reportTimeStamp
//				+ ".txt";

		String ExcelPath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".xlsx";
		String FilePath = LogTestPath + "/" + TestCaseName + "_" + reportTimeStamp + ".txt";

//		int DriverLen = driver.getWindowHandle().length();
//		String BrowserDriverHandle = driver.getWindowHandle().substring(DriverLen - 10, DriverLen);
//		System.out.println(BrowserDriverHandle);
//
//		String ExcelPath = LogTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_"
//				+ BrowserDriverHandle + ".xlsx";
//		String FilePath = LogTestPath + "/" + TestCaseName + "_" + getBrowserAlias(BrowserName) + "_"
//				+ BrowserDriverHandle + ".txt";
//
//		testSuits.put(TestCaseName + "#" + ExecutionStartTime, new testSuit(TestCaseName, TestCaseDescription,
//				ExecutionBlockStatus, "", TotalExecutionTime, "", excelReports, FilePath));

		XSSFWorkbook ExcelWorkbook = null;
		XSSFSheet ExcelSheet = null;
		XSSFRow ExcelRows;
		XSSFCell ExcelColumns;
		int RowNum, ColumnNum;
		FileOutputStream fileOut = null;

		// Retrieve Log Details From Text Log File...
		LogCount = GetLogDetails(FilePath);

		try {
			fileOut = new FileOutputStream(new java.io.File(ExcelPath));
			ExcelWorkbook = new XSSFWorkbook();
			ExcelSheet = ExcelWorkbook.createSheet("AutomationReport");
		} catch (IOException e) {
			e.printStackTrace();
		}

		XSSFCellStyle style1 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style2 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style3 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style4 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style5 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle style6 = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Passstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Failstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle PassSubstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle FailSubstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle Logstyle = ExcelWorkbook.createCellStyle();
		XSSFCellStyle IOstyle = ExcelWorkbook.createCellStyle();

		CellRangeAddress Headerregion = new CellRangeAddress(0, 0, 0, 8);
		CellRangeAddress Inforegion = new CellRangeAddress(0, 100, 0, 8);
		CellRangeAddress Dataregion = new CellRangeAddress(1, 3, 0, 8);
		CellRangeAddress Pageregion = new CellRangeAddress(5, 5, 1, 2);
		CellRangeAddress Outputregion = new CellRangeAddress(5, 5, 4, 5);

		Font font = ExcelWorkbook.createFont();
		font.setBold(true);

		style1.setFillForegroundColor(new XSSFColor(new java.awt.Color(153, 204, 255)));
		style1.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style1.setAlignment(HorizontalAlignment.LEFT);
		style1.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		style1.setBorderRight(CellStyle.BORDER_THICK);
		style1.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style1.setBorderBottom(CellStyle.BORDER_THICK);
		style1.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style1.setWrapText(true);
		style1.setFont(font);

		style2.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		style3.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style3.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style3.setAlignment(HorizontalAlignment.LEFT);
		style3.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		style3.setWrapText(true);
		style3.setFont(font);

		style5.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style5.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style5.setAlignment(HorizontalAlignment.LEFT);
		style5.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		style5.setWrapText(true);

		style4.setFillForegroundColor(new XSSFColor(new java.awt.Color(153, 204, 255)));
		style4.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style4.setAlignment(CellStyle.ALIGN_LEFT);
		style4.setBorderRight(CellStyle.BORDER_THICK);
		style4.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style4.setBorderBottom(CellStyle.BORDER_THICK);
		style4.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style4.setWrapText(true);
		style4.setFont(font);

		style6.setFillForegroundColor(new XSSFColor(new java.awt.Color(230, 230, 255)));
		style6.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style6.setAlignment(CellStyle.ALIGN_LEFT);
		style6.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		style6.setAlignment(CellStyle.ALIGN_LEFT);
		style6.setBorderTop(CellStyle.BORDER_THICK);
		style6.setTopBorderColor(IndexedColors.WHITE.getIndex());
		style6.setBorderRight(CellStyle.BORDER_THICK);
		style6.setRightBorderColor(IndexedColors.WHITE.getIndex());
		style6.setBorderBottom(CellStyle.BORDER_THICK);
		style6.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		style6.setWrapText(true);
		style6.setFont(font);

		Passstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 255, 128)));
		Passstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Passstyle.setAlignment(HorizontalAlignment.LEFT);
		Passstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		Passstyle.setAlignment(CellStyle.ALIGN_LEFT);
		Passstyle.setBorderRight(CellStyle.BORDER_THICK);
		Passstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Passstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Passstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Passstyle.setWrapText(true);
		Passstyle.setFont(font);

		PassSubstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(128, 255, 128)));
		PassSubstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		PassSubstyle.setAlignment(HorizontalAlignment.LEFT);
		PassSubstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		PassSubstyle.setAlignment(CellStyle.ALIGN_LEFT);
		PassSubstyle.setBorderRight(CellStyle.BORDER_THICK);
		PassSubstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		PassSubstyle.setBorderBottom(CellStyle.BORDER_THICK);
		PassSubstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		PassSubstyle.setWrapText(true);
		PassSubstyle.setFont(font);

		Failstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 153, 153)));
		Failstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Failstyle.setAlignment(HorizontalAlignment.LEFT);
		Failstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		Failstyle.setAlignment(CellStyle.ALIGN_LEFT);
		Failstyle.setBorderRight(CellStyle.BORDER_THICK);
		Failstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Failstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Failstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Failstyle.setWrapText(true);
		Failstyle.setFont(font);

		FailSubstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 153, 153)));
		FailSubstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		FailSubstyle.setAlignment(HorizontalAlignment.LEFT);
		FailSubstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		FailSubstyle.setAlignment(CellStyle.ALIGN_LEFT);
		FailSubstyle.setBorderRight(CellStyle.BORDER_THICK);
		FailSubstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		FailSubstyle.setBorderBottom(CellStyle.BORDER_THICK);
		FailSubstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		FailSubstyle.setWrapText(true);
		FailSubstyle.setFont(font);

		IOstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 245, 231)));
		IOstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		IOstyle.setAlignment(HorizontalAlignment.LEFT);
		IOstyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.TOP);
		IOstyle.setAlignment(CellStyle.ALIGN_LEFT);
		IOstyle.setBorderRight(CellStyle.BORDER_THICK);
		IOstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		IOstyle.setBorderBottom(CellStyle.BORDER_THICK);
		IOstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		IOstyle.setWrapText(true);
		IOstyle.setFont(font);

		Logstyle.setFillForegroundColor(new XSSFColor(new java.awt.Color(254, 245, 231)));
		Logstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		Logstyle.setBorderTop(CellStyle.BORDER_THICK);
		Logstyle.setTopBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setBorderRight(CellStyle.BORDER_THICK);
		Logstyle.setRightBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setBorderBottom(CellStyle.BORDER_THICK);
		Logstyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
		Logstyle.setWrapText(true);

		for (int r = 0; r < LogCount; r++) {
			Row row = ExcelSheet.createRow(r);
			for (int c = 0; c < 30; c++) {
				Cell infocell = row.createCell(c);
				infocell.setCellStyle(style2);
			}
		}

		Cell cell = ExcelSheet.getRow(0).getCell(0);
		ExcelSheet.addMergedRegion(Headerregion);
		cell.setCellValue("EXECUTION DETAILS");
		cell.setCellStyle(style1);

		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderRight(CellStyle.BORDER_THICK, Headerregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setRightBorderColor(IndexedColors.WHITE.getIndex(), Headerregion, ExcelSheet, ExcelWorkbook);

		ExcelSheet.setColumnWidth(0, 22 * 256);
		ExcelSheet.setColumnWidth(1, 24 * 256);
		ExcelSheet.setColumnWidth(2, 5 * 256);
		ExcelSheet.setColumnWidth(3, 25 * 256);
		ExcelSheet.setColumnWidth(4, 25 * 256);
		ExcelSheet.setColumnWidth(5, 22 * 256);
		ExcelSheet.setColumnWidth(6, 20 * 256);
		ExcelSheet.setColumnWidth(7, 28 * 256);
		ExcelSheet.setColumnWidth(8, 30 * 256);

		ExcelSheet.getRow(0).setHeightInPoints(30);

		String hostName = null;
		java.net.InetAddress addr;
		try {
			addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		ExcelSheet.getRow(1).getCell(0).setCellValue("Test Case Name");
		ExcelSheet.getRow(2).getCell(0).setCellValue("Test Case Description");
		ExcelSheet.getRow(3).getCell(0).setCellValue("Test Designer");

		ExcelSheet.getRow(1).getCell(3).setCellValue("Application Name");
		ExcelSheet.getRow(2).getCell(3).setCellValue("Pass Percentage");
		ExcelSheet.getRow(3).getCell(3).setCellValue("Test Executor");

		ExcelSheet.getRow(1).getCell(5).setCellValue("System Informaion");
		ExcelSheet.getRow(2).getCell(5).setCellValue("Fail Percentage");
		ExcelSheet.getRow(3).getCell(5).setCellValue("Test Count");

		ExcelSheet.getRow(1).getCell(7).setCellValue("Execution Start Time");
		ExcelSheet.getRow(2).getCell(7).setCellValue("Execution End Time Time");
		ExcelSheet.getRow(3).getCell(7).setCellValue("Total Execution Time");

		ExcelSheet.getRow(1).getCell(1).setCellValue(TestCaseName);
		ExcelSheet.getRow(2).getCell(1).setCellValue(TestCaseDescription);
		ExcelSheet.getRow(3).getCell(1).setCellValue(TestDesigner);

		ExcelSheet.getRow(1).getCell(4).setCellValue(ApplicationName);
		ExcelSheet.getRow(2).getCell(4).setCellValue(PassPercentage + " %");
		ExcelSheet.getRow(3).getCell(4).setCellValue(TestExecutor);

		double FailPercentage = 100 - Double.parseDouble(PassPercentage);

		ExcelSheet.getRow(1).getCell(6).setCellValue(hostName);
		ExcelSheet.getRow(2).getCell(6).setCellValue(FailPercentage + " %");
		ExcelSheet.getRow(3).getCell(6).setCellValue("1");

		ExcelSheet.getRow(1).getCell(8).setCellValue(ExecutionStartTime);
		ExcelSheet.getRow(2).getCell(8).setCellValue(ExecutionEndTimeTime);
		ExcelSheet.getRow(3).getCell(8).setCellValue(TotalExecutionTime);

		ExcelSheet.addMergedRegion(new CellRangeAddress(1, 1, 1, 2));
		ExcelSheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 2));
		ExcelSheet.addMergedRegion(new CellRangeAddress(3, 3, 1, 2));

		for (int r = 1; r < 4; r++) {
			for (int c = 0; c <= 8; c++) {
				if (c == 1 || c == 4 || c == 6 || c == 8) {
					ExcelSheet.getRow(r).getCell(c).setCellStyle(style3);
				} else {
					ExcelSheet.getRow(r).getCell(c).setCellStyle(style5);
				}
			}
		}

		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Dataregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderBottom(CellStyle.BORDER_THICK, Inforegion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Dataregion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBottomBorderColor(IndexedColors.WHITE.getIndex(), Inforegion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setBorderRight(CellStyle.BORDER_THICK, Inforegion, ExcelSheet, ExcelWorkbook);
		RegionUtil.setRightBorderColor(IndexedColors.WHITE.getIndex(), Inforegion, ExcelSheet, ExcelWorkbook);

		ExcelSheet.getRow(5).getCell(0).setCellValue("CHECKPOINTS");
		ExcelSheet.getRow(5).getCell(1).setCellValue("PAGE");
		ExcelSheet.getRow(5).getCell(3).setCellValue("STATUS");
		ExcelSheet.getRow(5).getCell(4).setCellValue("DESCRIPTION");
		ExcelSheet.getRow(5).getCell(6).setCellValue("OUTPUT DETAILS");
		ExcelSheet.getRow(5).getCell(7).setCellValue("EXECUTION TIME");
		ExcelSheet.getRow(5).getCell(8).setCellValue("REMARKS");
		ExcelSheet.addMergedRegion(Pageregion);
		ExcelSheet.addMergedRegion(Outputregion);

		for (int c = 0; c <= 8; c++) {
			ExcelSheet.getRow(5).getCell(c).setCellStyle(style4);
		}

		String WSSheetName, WorkflowPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks = null, IOType, StrOutput = "";
		int dr = 6;

		for (Entry<String, ExcelReport> excelReport : excelReports.entrySet()) {
			WSSheetName = excelReport.getValue().getSheetName();
			WorkflowPageValue = excelReport.getValue().getPageName();

			if (!WSSheetName.isEmpty() && WSSheetName != "" && !WorkflowPageValue.isEmpty() && WorkflowPageValue != "") {

				ExecutionStatus = excelReport.getValue().getExecutionStatus();
				PageDescription = excelReport.getValue().getTestDescription();
				Output = excelReport.getValue().getOutput();
				StartDate = excelReport.getValue().getExecutionStartTime();
				EndDate = excelReport.getValue().getExecutionEndTime();
				TestRemarks = excelReport.getValue().getTestRemarks();
				TestCaseName = excelReport.getValue().getTestCase();
				IOType = excelReport.getValue().getIOType();

				String ExecutionTime = null;
				try {
					ExecutionTime = getExecutionTime(StartDate, EndDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 1, 2));
				ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 4, 5));

				ExcelSheet.getRow(dr).getCell(0).setCellValue(WSSheetName);
				ExcelSheet.getRow(dr).getCell(1).setCellValue(WorkflowPageValue);
				ExcelSheet.getRow(dr).getCell(3).setCellValue(ExecutionStatus);
				ExcelSheet.getRow(dr).getCell(4).setCellValue(PageDescription);

				String IOData, IOLabel, IOValue, IOLink;
				int IOCount = 1;
				if (Output != null) {
					if (Output.contains("|")) {
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								try {
									IOType = IOData.split(";")[0].trim();
								} catch (Exception e) {
									IOType = "";
								}
								try {
									IOLabel = IOData.split(";")[1].trim();
								} catch (Exception e) {
									IOLabel = "";
								}
								try {
									IOValue = IOData.split(";")[2].trim();
								} catch (Exception e) {
									IOValue = "";
								}
								try {
									IOLink = IOData.split(";")[3].trim();
								} catch (Exception e) {
									IOLink = "";
								}
								if (IOType.equalsIgnoreCase("output")) {
									if (!StrOutput.isEmpty() && StrOutput != "") {
										StrOutput = StrOutput + "\r\n" + IOLabel + " = " + IOValue;
									} else {
										StrOutput = IOLabel + " = " + IOValue;
									}
								}
								IOCount = IOCount + 1;
							}
						}
					} else {
						IOData = Output;
						if (IOData.contains(";")) {
							try {
								IOType = IOData.split(";")[0].trim();
							} catch (Exception e) {
								IOType = "";
							}
							try {
								IOLabel = IOData.split(";")[1].trim();
							} catch (Exception e) {
								IOLabel = "";
							}
							try {
								IOValue = IOData.split(";")[2].trim();
							} catch (Exception e) {
								IOValue = "";
							}
							try {
								IOLink = IOData.split(";")[3].trim();
							} catch (Exception e) {
								IOLink = "";
							}
							if (IOType.equalsIgnoreCase("output")) {
								StrOutput = IOLabel + " = " + IOValue;
							}
							IOCount = 3;
						}
					}
				}

//				ExcelSheet.getRow(dr).getCell(6).setCellValue(StrOutput);
				ExcelSheet.getRow(dr).getCell(7).setCellValue(ExecutionTime);
				ExcelSheet.getRow(dr).getCell(8).setCellValue(TestRemarks);

//				healthCheck.put(WSSheetName, new HealthCheckUtil(WSSheetName, TestCaseName, StrOutput, StartDate, EndDate, TotalExecutionTime, ExecutionStatus, TestRemarks));

				ExecutorUpdates.put(reportTimeStamp + "#" + "TestCaseName", TestCaseName);
				ExecutorUpdates.put(reportTimeStamp + "#" + "TestExecutionTime", TotalExecutionTime);

				if (TestRemarks == null) {
					TestRemarks = "";
				}
				ExecutorUpdates.put(reportTimeStamp + "#" + "TestRemarks", TestRemarks);
				if (StrOutput == null) {
					StrOutput = "";
				}
				ExecutorUpdates.put(reportTimeStamp + "#" + "StrOutput", StrOutput);
				ExecutorUpdates.put(reportTimeStamp + "#" + "TestExecutionStatus", ExecutionStatus);
				ExecutorUpdates.put(reportTimeStamp + "#" + "ExecutionStartTime", ExecutionStartTime);

				if (ExecutionStatus.equalsIgnoreCase("pass")) {
					for (int dc = 0; dc <= 8; dc++) {
						if (dc == 4 || dc == 6 || dc == 8) {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(PassSubstyle);
						} else {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(Passstyle);
						}
					}
				}
				if (ExecutionStatus.equalsIgnoreCase("fail")) {
					for (int dc = 0; dc <= 8; dc++) {
						if (dc == 4 || dc == 6 || dc == 8) {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(FailSubstyle);
						} else {
							ExcelSheet.getRow(dr).getCell(dc).setCellStyle(Failstyle);
						}
					}
				}

				String PageKey = WSSheetName + "#" + WorkflowPageValue;
				String Logtext;
				int logRowNum;
				int rnum = dr + 1;
				int fromRow = rnum;

				for (Entry<String, String> excellogmap : ExcellogMap.entrySet()) {
//				System.out.println(excellogmap.getKey());
					if (excellogmap.getKey().contains(PageKey)) {
						Logtext = excellogmap.getValue();
						if (!Logtext.isEmpty() && Logtext != "") {
							ExcelSheet.getRow(rnum).getCell(0).setCellValue(Logtext);
							for (int lc = 0; lc <= 8; lc++) {
								ExcelSheet.getRow(rnum).getCell(lc).setCellStyle(Logstyle);
							}
							ExcelSheet.addMergedRegion(new CellRangeAddress(rnum, rnum, 0, 8));
							rnum = rnum + 1;
						}
					}

				}
				int toRow = rnum - 1;
				if (fromRow > toRow) {
					fromRow = toRow;
				}
				ExcelSheet.groupRow(fromRow, toRow);
				ExcelSheet.setRowGroupCollapsed(fromRow, true);
				dr = toRow + 1;
			}
		}

//		if (TestRemarks != null) {
//			if (!TestRemarks.isEmpty() && TestRemarks != "") {
		testSuits.put(TestCaseName + "#" + reportTimeStamp, new testSuit(TestCaseName, TestCaseDescription, ExecutionBlockStatus, StrOutput, TotalExecutionTime, TestRemarks, excelReports, FilePath));
//			}
//		}

		// Output Details

		int iodr = dr + 1, iolabel;
		iolabel = iodr + 1;

		ExcelSheet.getRow(iodr).getCell(0).setCellValue("INPUT/OUTPUT DETAILS");
		ExcelSheet.getRow(iodr).getCell(0).setCellStyle(style1);
		ExcelSheet.addMergedRegion(new CellRangeAddress(iodr, iodr, 0, 8));

		ExcelSheet.getRow(iolabel).getCell(0).setCellValue("CHECKPOINTS");
		ExcelSheet.getRow(iolabel).getCell(1).setCellValue("PAGE");
		ExcelSheet.getRow(iolabel).getCell(3).setCellValue("INPUT DETAILS");
		ExcelSheet.getRow(iolabel).getCell(6).setCellValue("OUTPUT DETAILS");

		ExcelSheet.addMergedRegion(new CellRangeAddress(iolabel, iolabel, 1, 2));
		ExcelSheet.addMergedRegion(new CellRangeAddress(iolabel, iolabel, 3, 5));
		ExcelSheet.addMergedRegion(new CellRangeAddress(iolabel, iolabel, 6, 8));

		for (int c = 0; c <= 8; c++) {
			ExcelSheet.getRow(iolabel).getCell(c).setCellStyle(style6);
		}

		String ExecutionTime = "";
		dr = iolabel + 1;
		int trfrom = dr, trto;
		String ApplicationOutput = "", AppStartDate, AppEndDate;
		for (Entry<String, ExcelReport> excelReport : excelReports.entrySet()) {
			WSSheetName = excelReport.getValue().getSheetName();
			WorkflowPageValue = excelReport.getValue().getPageName();
			if (!WSSheetName.isEmpty() && WSSheetName != "" && !WorkflowPageValue.isEmpty() && WorkflowPageValue != "") {

				ExecutionStatus = excelReport.getValue().getExecutionStatus();
				PageDescription = excelReport.getValue().getTestDescription();
				Output = excelReport.getValue().getOutput();
				StartDate = excelReport.getValue().getExecutionStartTime();
				EndDate = excelReport.getValue().getExecutionEndTime();
				TestRemarks = excelReport.getValue().getTestRemarks();
				TestCaseName = excelReport.getValue().getTestCase();
				IOType = excelReport.getValue().getIOType();

				ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 1, 2));
				ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 3, 5));
				ExcelSheet.addMergedRegion(new CellRangeAddress(dr, dr, 6, 8));

				ExcelSheet.getRow(dr).getCell(0).setCellValue(WSSheetName);
				ExcelSheet.getRow(dr).getCell(1).setCellValue(WorkflowPageValue);

				String IOData, IOLabel, IOValue, StrInput = "", IOLink;
				int IOCount = 0, OCount = 0, ICount = 0;
				StrOutput = "";
				if (Output != null) {
					if (Output.contains("|")) {
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								try {
									IOType = IOData.split(";")[0].trim();
								} catch (Exception e) {
									IOType = "";
								}
								try {
									IOLabel = IOData.split(";")[1].trim();
								} catch (Exception e) {
									IOLabel = "";
								}
								try {
									IOValue = IOData.split(";")[2].trim();
								} catch (Exception e) {
									IOValue = "";
								}
								try {
									IOLink = IOData.split(";")[3].trim();
								} catch (Exception e) {
									IOLink = "";
								}

								if (IOLabel.equalsIgnoreCase("password")) {
									IOValue = "*********";
								}
								if (IOValue.equalsIgnoreCase("#Desc")) {
									IOLabel = "";
									IOValue = "";
								}

								if (IOType.equalsIgnoreCase("output")) {
									if (!StrOutput.isEmpty() && StrOutput != "") {
										if (!IOLabel.isEmpty() && IOLabel != "") {
											StrOutput = StrOutput + "\r\n" + IOLabel + " = " + IOValue;
										}
									} else {
										if (!IOLabel.isEmpty() && IOLabel != "") {
											StrOutput = IOLabel + " = " + IOValue;
										}
									}
									OCount = OCount + 1;
								} else if (IOType.equalsIgnoreCase("input")) {
									if (!StrInput.isEmpty() && StrInput != "") {
										if (!IOLabel.isEmpty() && IOLabel != "") {
											StrInput = StrInput + "\r\n" + IOLabel + " = " + IOValue;
										}
									} else {
										if (!IOLabel.isEmpty() && IOLabel != "") {
											StrInput = IOLabel + " = " + IOValue;
										}
									}
									ICount = ICount + 1;
								}
//								IOCount = IOCount + 1;
							}
						}
						if (OCount >= ICount) {
							IOCount = OCount;
						} else {
							IOCount = ICount;
						}

					} else {
						IOData = Output;
						if (IOData.contains(";")) {
							try {
								IOType = IOData.split(";")[0].trim();
							} catch (Exception e) {
								IOType = "";
							}
							try {
								IOLabel = IOData.split(";")[1].trim();
							} catch (Exception e) {
								IOLabel = "";
							}
							try {
								IOValue = IOData.split(";")[2].trim();
							} catch (Exception e) {
								IOValue = "";
							}
							try {
								IOLink = IOData.split(";")[3].trim();
							} catch (Exception e) {
								IOLink = "";
							}

							if (IOLabel.equalsIgnoreCase("password")) {
								IOValue = "*********";
							}

							if (IOValue.equalsIgnoreCase("#Desc")) {
								IOLabel = "";
								IOValue = "";
							}

							if (IOType.equalsIgnoreCase("output")) {
								if (!IOLabel.isEmpty() && IOLabel != "") {
									StrOutput = IOLabel + " = " + IOValue;
								}
							} else if (IOType.equalsIgnoreCase("input")) {
								if (!IOLabel.isEmpty() && IOLabel != "") {
									StrInput = IOLabel + " = " + IOValue;
								}
							}
							IOCount = 1;
						}
					}
				}

				if (!StrOutput.isEmpty() && StrOutput != "") {
					ExcelSheet.getRow(dr).getCell(6).setCellValue(StrOutput);
				} else {
					ExcelSheet.getRow(dr).getCell(6).setCellValue("-");
				}
				if (!StrInput.isEmpty() && StrInput != "") {
					ExcelSheet.getRow(dr).getCell(3).setCellValue(StrInput);
				} else {
					ExcelSheet.getRow(dr).getCell(3).setCellValue("-");
				}

				for (int dc = 0; dc <= 8; dc++) {
					ExcelSheet.getRow(dr).getCell(dc).setCellStyle(IOstyle);
				}

				ExcelSheet.getRow(dr).setHeightInPoints(IOCount * 15);
				dr = dr + 1;

				try {
					if (healthCheck.get(WSSheetName).getOutput() != null) {
						if (healthCheck.get(WSSheetName).getOutput() != "" && !healthCheck.get(WSSheetName).getOutput().isEmpty()) {
							StrOutput = healthCheck.get(WSSheetName).getOutput() + " " + StrOutput;
						}
					}
				} catch (Exception e) {
					// System.out.println(healthCheck.get(WSSheetName).getOutput());
				}

				try {
					if (healthCheck.get(WSSheetName).getStartTime() != null) {
						if (healthCheck.get(WSSheetName).getStartTime() != "" && !healthCheck.get(WSSheetName).getStartTime().isEmpty()) {
							StartDate = healthCheck.get(WSSheetName).getStartTime() + " | " + StartDate;
						}
					}
				} catch (Exception e) {
					// System.out.println(healthCheck.get(WSSheetName).getStartTime());
				}

				try {
					if (healthCheck.get(WSSheetName).getEndTime() != null) {
						if (healthCheck.get(WSSheetName).getEndTime() != "" && !healthCheck.get(WSSheetName).getEndTime().isEmpty()) {
							EndDate = healthCheck.get(WSSheetName).getEndTime() + " | " + EndDate;
						}
					}
				} catch (Exception e) {
					// System.out.println(healthCheck.get(WSSheetName).getEndTime());
				}

				if (StartDate.contains("|")) {
					AppStartDate = StartDate.split("\\|")[0].trim().toString();
				} else {
					AppStartDate = StartDate;
				}

				if (EndDate.contains("|")) {
					AppEndDate = EndDate.split("\\|")[EndDate.split("\\|").length - 1].trim().toString();
				} else {
					AppEndDate = EndDate;
				}

				try {
					ExecutionTime = getExecutionTime(AppStartDate, AppEndDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				System.out.println(WSSheetName + " StartDate : " + StartDate);
				System.out.println(WSSheetName + " EndDate : " + EndDate);
				System.out.println("ExecutionTime : " + ExecutionTime);
				healthCheck.put(WSSheetName, new HealthCheckUtil(WSSheetName, TestCaseName, StrOutput, StartDate, EndDate, TotalExecutionTime, ExecutionStatus, TestRemarks, ExecutionTime));

			}
		}

		trto = dr;
		if (trfrom > trto) {
			trfrom = trto;
		}
		ExcelSheet.groupRow(trfrom - 1, trto);
		ExcelSheet.setRowGroupCollapsed(trfrom - 1, true);

		try {
			ExcelWorkbook.write(fileOut);
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		

	}

	public int GetLogDetails(String FilePath) {

		ExcellogMap = new LinkedHashMap<String, String>();
		String st, SheetName = null, PageName = null, PageDescr = null, logtxt;
		int logcount = 0;
		int linecount = 0;
		File file = new File(FilePath);
		if (file.exists()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				while ((st = br.readLine()) != null) {
					if (st.contains("|")) {
						st = st.replace("|", "").replace("+", "").replace("-", "");

						if (st.contains("=>")) {
							SheetName = st.split("=>")[0];
							PageName = st.split("=>")[1];
//							PageDescr = st.split("=>")[2];
							logcount = 0;
						} else if (st.contains("::")) {
							logtxt = st;
							logcount = logcount + 1;
							String Key = SheetName.trim() + "#" + PageName.trim() + "#" + logcount;
							ExcellogMap.put(Key, logtxt);

						}
					}
					linecount++;
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return linecount;
	}

	synchronized public String getExecutionTime(String strDate, String endDate) throws ParseException {
		String dateStart = strDate;
		String dateStop = endDate;
		String TotalTime = "";
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		long diff, diffSeconds = 0, diffMinutes = 0, diffHours, diffDays;
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			diff = d2.getTime() - d1.getTime();
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (diffMinutes != 0) { // !min.isEmpty() && min != "0"
			TotalTime = TotalTime + diffMinutes + " Minutes, ";
		}

		if (diffSeconds != 0) { // !sec.isEmpty() && sec != "0"
			TotalTime = TotalTime + diffSeconds + " Seconds.";
		} else {
			TotalTime = "0 Seconds.";
		}
		return TotalTime;
	}

	public String getBrowserAlias(String BrowserName) {
		String Alias = "";

		switch (BrowserName.toLowerCase()) {
		case "chrome":
			Alias = "OnChrome";
			break;
		case "ie":
			Alias = "OnIE";
			break;
		case "firefox":
			Alias = "OnFirefox";
			break;
		case "edge":
			Alias = "OnEdge";
			break;
		case "opera":
			Alias = "OnOpera";
			break;

		default:
			break;
		}

		return Alias;

	}

	public void RARHealthCheckInitialSetup(String TestcaseName) {
		healthCheck.put("Application1", new HealthCheckUtil("A1S", TestcaseName, "", "", "", "", "Skipped", "", ""));
		
	}

	
	public String readFileAsString(String fileName) throws Exception {
		String data = "";
		data = new String(Files.readAllBytes(Paths.get(fileName)));
		return data;
	}

	public void removeUsedTC(String d) throws IOException {
		String[] arrOfStr = d.split("@");
		String finaldata = "";

		for (int i = 1; i < arrOfStr.length; i++) {
			String[] arrOfStr1 = arrOfStr[i].split("!");
			if (arrOfStr1[0].equalsIgnoreCase("unused")) {
				finaldata = finaldata + "@" + arrOfStr[i];
			}
		}

		System.out.println(finaldata);
		FileWriter myWriter = new FileWriter("Z:\\Automation\\RARExecutionReports\\TestReport.txt");
		myWriter.write(finaldata);
		myWriter.close();
		System.out.println("Successfully wrote to the RAR Report file.");
	}

	public void writeHTMLReport(String reportdata, String path) {
		try {
			FileWriter myWriter = new FileWriter(path);
			myWriter.write(reportdata);
			myWriter.close();
			System.out.println("Successfully wrote to the RAR Report file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
//		 try {
//			  
//	            // Open given file in append mode.
//	            BufferedWriter out = new BufferedWriter(
//	                   new FileWriter(path, true));
//	            out.write(reportdata);
//	            out.close();
//	        }
//	        catch (IOException e) {
//	            System.out.println("exception occoured" + e);
//	        }
	}

}
