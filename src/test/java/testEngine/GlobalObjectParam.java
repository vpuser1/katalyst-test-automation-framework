package testEngine;

public class GlobalObjectParam {

	String ObjectName, SystemBrowserObject, MobileBrowserObject, MobileApplicationObject, TestCaseName;

	public GlobalObjectParam(String objectName, String systemBrowserObject, String mobileBrowserObject,
			String mobileApplicationObject, String TestCaseName) {
		super();
		ObjectName = objectName;
		SystemBrowserObject = systemBrowserObject;
		MobileBrowserObject = mobileBrowserObject;
		MobileApplicationObject = mobileApplicationObject;
		this.TestCaseName = TestCaseName;
	}

	public String getTestCaseName() {
		return TestCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		TestCaseName = testCaseName;
	}

	public String getObjectName() {
		return ObjectName;
	}

	public void setObjectName(String objectName) {
		ObjectName = objectName;
	}

	public String getSystemBrowserObject() {
		return SystemBrowserObject;
	}

	public void setSystemBrowserObject(String systemBrowserObject) {
		SystemBrowserObject = systemBrowserObject;
	}

	public String getMobileBrowserObject() {
		return MobileBrowserObject;
	}

	public void setMobileBrowserObject(String mobileBrowserObject) {
		MobileBrowserObject = mobileBrowserObject;
	}

	public String getMobileApplicationObject() {
		return MobileApplicationObject;
	}

	public void setMobileApplicationObject(String mobileApplicationObject) {
		MobileApplicationObject = mobileApplicationObject;
	}

}
