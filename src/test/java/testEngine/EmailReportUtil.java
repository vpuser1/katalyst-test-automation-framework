
package testEngine;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.openqa.selenium.WebDriver;

//import testDrivers.ConfigUtil;

@SuppressWarnings("unused")
public class EmailReportUtil extends GearBox {

	public StringBuffer HTMLPageSubBlock = null, emailPageBlock = null, HTMLPageBuffer, HTMLTestScriptBlock = null, EmailHTMLPageSubBlock = null, EmailBody = null;
	public static StringBuffer HTMLSuitBuffer;

	public Date TestStartTime = null, TestEndTime = null;
	public String TestCompletionPer = null, TestTotalTime = null;
	public Map<String, String> TestStatus = new LinkedHashMap<String, String>();
	public MultiPartEmail email = new MultiPartEmail();
	public String strDate, endDate;
	public ArrayList<String> WorkflowPageArray = new ArrayList<String>();
	public String PassPercentageValue;

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;
	public Map<String, String> testElement;
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

	public EmailReportUtil(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase, String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	public StringBuffer HTMLSingleTestReport(String SourceTestCase) { // This Function Creates a HTML Web Page with
																		// CSS/Styles
//		System.out.println("HTMLSingleTestReport Start");
		PassPercentageValue = GetTestExecutionPassPercentage();
//		System.out.println("PassPercentageValue :" + PassPercentageValue);
		HTMLPageSubBlock = new StringBuffer();
		HTMLPageSubBlock = HTMLTestStyle(PassPercentageValue); // Style;
		HTMLPageSubBlock = HTMLMainHeader();
		HTMLPageSubBlock = HTMLTestExecutionDetails(PassPercentageValue, SourceTestCase); // Test Details
		HTMLPageSubBlock = HTMLPageBlock(); // Page Block
//		System.out.println("HTMLSingleTestReport End");
		return HTMLPageSubBlock;
	}

//	NO Use, directly added sub function in main...
	public StringBuffer EmailSingleTestReport(String SourceTestCase) { // This Function Creates a HTML Web Page without
																		// CSS/Styles

		PassPercentageValue = GetTestExecutionPassPercentage();
		HTMLPageSubBlock = new StringBuffer();
//		Email Report Didnt require Style but considering single function block will add it.
//		HTMLPageSubBlock = HTMLTestStyle(PassPercentageValue); // ****************
		HTMLPageSubBlock = EmailMainHeader(PassPercentageValue);
		HTMLPageSubBlock = EmailTestExecutionDetails(PassPercentageValue, SourceTestCase); // Test Details
		HTMLPageSubBlock = EmailPageBlock(PassPercentageValue); // Page Block
		HTMLPageSubBlock = EmailPageOutputBlock(PassPercentageValue);

//		System.out.println("HTMLPageSubBlock : " + HTMLPageSubBlock.toString());
		return HTMLPageSubBlock;
	}

	public StringBuffer HTMLTestSuit() {
		HTMLSuitBuffer = new StringBuffer();
		HTMLSuitBuffer = HTMLTestStyle();
		HTMLSuitBuffer = HTMLTestSuitHeadBlock();
		HTMLSuitBuffer = HTMLTestBlock();

		HTMLSuitBuffer.append("</body>");
		HTMLSuitBuffer.append("</html>");

		return HTMLSuitBuffer;
	}

	public StringBuffer HTMLPageBlock() {
		String WSSheetName, WorkflowPageValue, ExecutionStatus, ExecutionBlockStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, IOType, SourceTestCase, CurrentTestCase, ElementDescription = "";
		String CapturedPage = "";
		String outflag = "", WorkflowTestPage = "", OutputValue = null;
		int PageElementsCount, count, TestRowNum;
		count = 1;
//		 Header Block
		HTMLPageSubBlock = EmailPageHeader();
//		 Middle Main Page Block
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
//			System.out.println(loadInReport.getKey());
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageDescription = loadInReport.getValue().getTestDescription();
			LoadInReport = loadInReport.getValue().getLoadInReport();
			ElementName = loadInReport.getValue().getElementName();
			ActionTaken = loadInReport.getValue().getActionTaken();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();
			Output = loadInReport.getValue().getOutput(); // Need to add it properly
			StartDate = loadInReport.getValue().getExecutionStartTime();
			EndDate = loadInReport.getValue().getExecutionEndTime();
			TestRemarks = loadInReport.getValue().getTestRemarks();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();
			IOType = loadInReport.getValue().getIOType();
			TestRowNum = loadInReport.getValue().getTestRowNum();
			ElementDescription = loadInReport.getValue().getElementDescription();
//			System.out.println("Output :=> " + WorkflowPageValue + " | " + Output);
			WorkflowTestPage = CurrentTestCase + "@" + WorkflowPageValue;
//			System.out.println("WorkflowTestPage : " + WorkflowTestPage);
			if (CapturedPage.contains(WorkflowTestPage)) { // WorkflowPageValue
				outflag = "true"; // Working on same page
				OutputValue = OutputValue + "|" + Output;
			} else {
				count = 1;
				OutputValue = Output;
				ExecutionBlockStatus = GetExecutionStatus(WSSheetName, WorkflowPageValue, CurrentTestCase);
				EmailPageSubFirstBlock(WSSheetName, WorkflowPageValue, ExecutionBlockStatus, PageDescription, LoadInReport);
			}

			HTMLPageSubBlock(LoadInReport, ElementName, ActionTaken, TestCaseValue, ExecutionStatus);
			if (count == PageElementsCount || ExecutionStatus.equalsIgnoreCase("Fail")) {
				EmailPageSubLastBlock(IOType, OutputValue, StartDate, EndDate, TestRemarks, ExecutionStatus);
				OutputValue = "";
			}
			count = count + 1;
			CapturedPage = CapturedPage + "," + WorkflowTestPage;
		}

//		 Tail Block
		HTMLPageSubBlock = EmailPageTail();
		return HTMLPageSubBlock;
	}

	public StringBuffer EmailPageBlock(String PassPercentageValue) {
		String WSSheetName, WorkflowPageValue, ExecutionStatus, ExecutionBlockStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, IOType, SourceTestCase, CurrentTestCase, ElementDescription = "";
		String CapturedPage = "";
		String outflag = "", WorkflowTestPage = "";
		int PageElementsCount, count, TestRowNum;
		count = 1;
//		 Header Block
		EmailBody = EmailHTMLPageHeader(PassPercentageValue);
//		 Middle Main Page Block
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
//			System.out.println(loadInReport.getKey());
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageDescription = loadInReport.getValue().getTestDescription();
			LoadInReport = loadInReport.getValue().getLoadInReport();
			ElementName = loadInReport.getValue().getElementName();
			ActionTaken = loadInReport.getValue().getActionTaken();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();
			Output = loadInReport.getValue().getOutput(); // Need to add it properly
			StartDate = loadInReport.getValue().getExecutionStartTime();
			EndDate = loadInReport.getValue().getExecutionEndTime();
			TestRemarks = loadInReport.getValue().getTestRemarks();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();
			IOType = loadInReport.getValue().getIOType();
			TestRowNum = loadInReport.getValue().getTestRowNum();
			ElementDescription = loadInReport.getValue().getElementDescription();

			WorkflowTestPage = CurrentTestCase + "@" + WorkflowPageValue;
//			System.out.println("WorkflowTestPage : " + WorkflowTestPage);
			if (CapturedPage.contains(WorkflowTestPage)) { // WorkflowPageValue
				outflag = "true"; // Working on same page
			} else {
				count = 1;
				ExecutionBlockStatus = GetExecutionStatus(WSSheetName, WorkflowPageValue, CurrentTestCase);
				EmailHTMLPageSubFirstBlock(WSSheetName, WorkflowPageValue, ExecutionBlockStatus, PageDescription, LoadInReport);
			}

			EmailHTMLPageSubBlock(LoadInReport, ElementName, ActionTaken, TestCaseValue, ExecutionStatus);

			if (count == PageElementsCount || ExecutionStatus.equalsIgnoreCase("Fail")) {
				EmailHTMLPageSubLastBlock(IOType, Output, StartDate, EndDate, TestRemarks, ExecutionStatus);
			}

			count = count + 1;
			CapturedPage = CapturedPage + "," + WorkflowTestPage;
		}

		// Tail Block
		EmailBody = EmailHTMLPageTail();

		return EmailBody;

	}

	public StringBuffer EmailPageSubFirstBlock(String WSSheetName, String WorkflowPageValue, String ExecutionStatus, String PageDescription, String LoadInReport) {
		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td class='checkPage'><b>" + WSSheetName + "<b></td>"); // width='7%'
		HTMLPageSubBlock.append("<td class='checkPage'><b>" + WorkflowPageValue + "<b></td>"); // width='7%'
		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			HTMLPageSubBlock.append("<td class='checkStatus' style=' width: 7%; color:rgb(49, 146, 81);'><b>" + ExecutionStatus + "<b></td>"); // width='5%'
			HTMLPageSubBlock.append("<td>");
		} else {
			HTMLPageSubBlock.append("<td class='checkStatus' style='width: 7%; color:rgb(146, 67, 49);' ><b>" + ExecutionStatus + "<b></td>"); // width='5%'
			HTMLPageSubBlock.append("<td style='width: 40%;'>");
		}
//		HTMLPageSubBlock.append("<td style='width: 40%;'>"); // width='40%' Need to vary it based on pass and fail
		HTMLPageSubBlock.append("<div>");
		HTMLPageSubBlock.append("<table class='Flowdetails'>");

		if (!PageDescription.isEmpty() && PageDescription != "") {
			HTMLPageSubBlock.append("<tr>");
			HTMLPageSubBlock.append("<td colspan=3 class='descCell'><span><b>" + PageDescription + "</b></span></td>");
			HTMLPageSubBlock.append("</tr>");
		}

		if (!LoadInReport.isEmpty() && LoadInReport != "" && LoadInReport.equalsIgnoreCase("yes")) {
			HTMLPageSubBlock.append("<tr>");
			HTMLPageSubBlock.append("<td class='ElemensCell'><b>Element Name</b></td>");
			HTMLPageSubBlock.append("<td  class='ElemensCell'><b>Action Taken</b></td>");
			HTMLPageSubBlock.append("<td  class='ElemensCell'><b>Value</b></td>");
			HTMLPageSubBlock.append("</tr>");
		}

		return HTMLPageSubBlock;
	}

	public StringBuffer EmailHTMLPageSubFirstBlock(String WSSheetName, String WorkflowPageValue, String ExecutionStatus, String PageDescription, String LoadInReport) {
		String CellBG = "#F2F3F4;";
		EmailBody.append("<tr>");
		EmailBody.append("<td style='vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + WSSheetName + "<b></td>"); // width='7%'
		EmailBody.append("<td style='vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + WorkflowPageValue + "<b></td>"); // width='7%'
		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			EmailBody.append("<td style='width: 7%; color:rgb(49, 146, 81);vertical-align: top;padding-right: 15px;padding-top: 10px;height: auto;text-align: center;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + ExecutionStatus + "<b></td>"); // width='5%'
			EmailBody.append("<td>");
		} else {
			EmailBody.append("<td style='width: 7%; color:rgb(146, 67, 49);vertical-align: top;padding-right: 15px;padding-top: 10px;height: auto;text-align: center;font: 13px sans-serif;background-color: " + CellBG + "' ><b>" + ExecutionStatus + "<b></td>"); // width='5%'
			EmailBody.append("<td style='width: 40%;'>");
		}
//		EmailBody.append("<td style='width: 40%;'>"); // width='40%' Need to vary it based on pass and fail
		EmailBody.append("<div>");
		EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");

		if (!PageDescription.isEmpty() && PageDescription != "") {
			EmailBody.append("<tr>");
			EmailBody.append("<td colspan=3 style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: rgb(201, 209, 209);padding-bottom: 5px;'><span><b>" + PageDescription + "</b></span></td>");
			EmailBody.append("</tr>");
		}

		if (!LoadInReport.isEmpty() && LoadInReport != "" && LoadInReport.equalsIgnoreCase("yes")) {
			EmailBody.append("<tr>");
			EmailBody.append("<td style='background-color: rgb(209, 228, 228);padding-bottom: 4px; width: 36%;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'><b>Element Name</b></td>");
			EmailBody.append("<td style='background-color: rgb(209, 228, 228);padding-bottom: 4px; width: 36%;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'><b>Action Taken</b></td>");
			EmailBody.append("<td style='background-color: rgb(209, 228, 228);padding-bottom: 4px; width: 36%;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'><b>Value</b></td>");
			EmailBody.append("</tr>");
		}

		return EmailBody;
	}

	public StringBuffer EmailPageSubLastBlock(String IOType, String Output, String StartDate, String EndDate, String TestRemarks, String ExecutionStatus) {
		String ExecutionTime = null;
		try {
			ExecutionTime = getExecutionTime(StartDate, EndDate);
//			System.out.println("Page execution Time = " + ExecutionTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (Output == null) {
			Output = "";
		}
		if (TestRemarks == null) {
			TestRemarks = "";
		}

		if (IOType == null) {
			IOType = "";
		}

		HTMLPageSubBlock.append("</table>");
		HTMLPageSubBlock.append("</div>");
		HTMLPageSubBlock.append("</td>");

		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			HTMLPageSubBlock.append("<td class='checkPage'  style='width: auto;'><b><div><table class='Outputetails'>"); // width='15%'
		} else {
			HTMLPageSubBlock.append("<td class='checkPage'  style='width: auto;'><b><div><table class='Outputetails'>"); // width='15%'
		}

		String IOData, IOLabel = null, IOValue = null, IOLink = null;
		if (Output.contains("|")) {
			for (int io = 0; io < Output.split("\\|").length; io++) {
				IOData = Output.split("\\|")[io];
				if (IOData.contains(";")) {
					IOType = IOData.split(";")[0].trim();
					IOLabel = IOData.split(";")[1].trim();
					IOValue = IOData.split(";")[2].trim();
					IOLink = IOData.split(";")[3].trim();

					if (IOType == null) {
						IOType = "";
					}
					if (IOLink == null) {
						IOLink = "";
					}

					if (IOType.equalsIgnoreCase("Output") && IOLink != "") {
						if (IOLink.toLowerCase().contains("http")) {
							HTMLPageSubBlock.append("<tr><td><b> " + IOLabel + " </b></td><td><b> &nbsp; <a id='Varlink' href='" + IOLink + "'  target='_blank'>" + IOValue + "</a> </b></td><tr>");
						} // will get link here}
						else {
							HTMLPageSubBlock.append("<tr ><td ><b> " + IOLabel + " </b></td><td ><b> &nbsp;" + IOValue + " </b></td><tr>");
						}
					}
				} else if (IOType.equalsIgnoreCase("Output") && IOLink == "") {
					HTMLPageSubBlock.append("<tr><td><b> " + IOLabel + " </b></td><td><b> &nbsp;" + IOValue + " </b></td><tr>");
				} else {
					// Do Nothing
				}

			}
		} else

		{
			IOData = Output;
			if (IOData.contains(";")) {
				IOType = IOData.split(";")[0].trim();
				IOLabel = IOData.split(";")[1].trim();
				IOValue = IOData.split(";")[2].trim();
				IOLink = IOData.split(";")[3].trim();
				if (IOType.equalsIgnoreCase("Output") && IOLink != "") {
//					HTMLPageSubBlock.append("<tr ><td style='border-style:hidden;'><b> " + IOLabel
//							+ " </b></td><td style='border-style:hidden;'><b> : &nbsp;" + IOValue + " </b></td><tr>");
					if (IOLink.toLowerCase().contains("http")) {
						HTMLPageSubBlock.append("<tr ><td><b> " + IOLabel + " </b></td><td><b> &nbsp;<a id='Varlink' href='" + IOLink + "'  target='_blank'>" + IOValue + "</a> </b></td><tr>");
					} // will get link here}
					else {
						HTMLPageSubBlock.append("<tr ><td><b> " + IOLabel + " </b></td><td><b> &nbsp;" + IOValue + " </b></td><tr>");
					}
				} else if (IOType.equalsIgnoreCase("Output") && IOLink == "") {
					HTMLPageSubBlock.append("<tr ><td><b> " + IOLabel + " </b></td><td><b> &nbsp;" + IOValue + " </b></td><tr>");
				} else {
					// Do Nothing
				}
			}

		}

		HTMLPageSubBlock.append("</table></div></b></td>");

		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			HTMLPageSubBlock.append("<td class='checkPage' style='width: 12%; text-align: left;'><b>" + ExecutionTime + "</b></td> "); // width='10%'
			HTMLPageSubBlock.append("<td class='checkPage' style='text-align: left;'><b>" + TestRemarks + "</b></td>");
		} else {
			HTMLPageSubBlock.append("<td class='checkPage' style='width: 12%; text-align: left;'><b>" + ExecutionTime + "</b></td> "); // width='10%'
			HTMLPageSubBlock.append("<td class='checkPage' style=' text-align: left;'><b>" + TestRemarks + "</b></td>");
		}

		HTMLPageSubBlock.append("</tr>");

		return HTMLPageSubBlock;
	}

	public StringBuffer EmailHTMLPageSubLastBlock(String IOType, String Output, String StartDate, String EndDate, String TestRemarks, String ExecutionStatus) {
		String CellBG = "#F2F3F4;";
		String ExecutionTime = null;
		try {
			ExecutionTime = getExecutionTime(StartDate, EndDate);
//			System.out.println("Page execution Time = " + ExecutionTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (Output == null) {
			Output = "";
		}
		if (TestRemarks == null) {
			TestRemarks = "";
		}

		EmailBody.append("</table>");
		EmailBody.append("</div>");
		EmailBody.append("</td>");

		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			EmailBody.append("<td style='width: auto;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG
					+ "'><b><div><table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>"); // width='15%'
		} else {
			EmailBody.append("<td  style='width: 10%;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG
					+ "'><b><div><table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>"); // width='15%'
		}

		String IOData, IOLabel = null, IOValue = null, IOLink = null;
		if (Output.contains("|")) {
			for (int io = 0; io < Output.split("\\|").length; io++) {
				IOData = Output.split("\\|")[io];
				if (IOData.contains(";")) {
					IOType = IOData.split(";")[0].trim();
					IOLabel = IOData.split(";")[1].trim();
					IOValue = IOData.split(";")[2].trim();
					IOLink = IOData.split(";")[3].trim();

					if (IOType.equalsIgnoreCase("Output") && IOLink != "") {
						if (IOLink.toLowerCase().contains("http")) {
							EmailBody.append("<tr><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b> &nbsp; <a id='Varlink' href='" + IOLink + "'  target='_blank'>" + IOValue + "</a> </b></td><tr>");
						} // will get link here}
						else {
							EmailBody.append("<tr ><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b> &nbsp;" + IOValue + " </b></td><tr>");
						}
					}
				} else if (IOType.equalsIgnoreCase("Output") && IOLink == "") {
					EmailBody.append("<tr><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b> &nbsp;" + IOValue + " </b></td><tr>");
				} else {
					// Do Nothing
				}

			}
		} else

		{
			IOData = Output;
			if (IOData.contains(";")) {
				IOType = IOData.split(";")[0].trim();
				IOLabel = IOData.split(";")[1].trim();
				IOValue = IOData.split(";")[2].trim();
				IOLink = IOData.split(";")[3].trim();
				if (IOType.equalsIgnoreCase("Output") && IOLink != "") {
//					EmailBody.append("<tr ><td style='border-style:hidden;'><b> " + IOLabel
//							+ " </b></td><td style='border-style:hidden;'><b> : &nbsp;" + IOValue + " </b></td><tr>");
					if (IOLink.toLowerCase().contains("http")) {
						EmailBody.append("<tr ><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b>  &nbsp;<a id='Varlink' href='" + IOLink + "'  target='_blank'>" + IOValue + "</a> </b></td><tr>");
					} // will get link here}
					else {
						EmailBody.append("<tr ><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b> &nbsp;" + IOValue + " </b></td><tr>");
					}
				} else if (IOType.equalsIgnoreCase("Output") && IOLink == "") {
					EmailBody.append("<tr ><td style='width: auto;'><b> " + IOLabel + " </b></td><td style='width: auto;'><b> &nbsp;" + IOValue + " </b></td><tr>");
				} else {
					// Do Nothing
				}
			}

		}

		EmailBody.append("</table></div></b></td>");

		if (ExecutionStatus.equalsIgnoreCase("pass")) {
			EmailBody.append("<td style='width: 12%; text-align: left;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + ExecutionTime + "</b></td> "); // width='10%'
			EmailBody.append("<td style='text-align: left;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + TestRemarks + "</b></td>");
		} else {
			EmailBody.append("<td style='width: 12%; text-align: left;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + ExecutionTime + "</b></td> "); // width='10%'
			EmailBody.append("<td style=' text-align: left;vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "'><b>" + TestRemarks + "</b></td>");
		}

		EmailBody.append("</tr>");
		return EmailBody;

	}

	public StringBuffer HTMLPageSubBlock(String LoadInReport, String ElementName, String ActionTaken, String TestCaseValue, String ExecutionStatus) {
		if (TestCaseValue.equalsIgnoreCase("#desc")) {
			TestCaseValue = "";
		}
		if (!LoadInReport.isEmpty() && LoadInReport != "" && LoadInReport.equalsIgnoreCase("yes")) {
			if (ExecutionStatus.equalsIgnoreCase("fail")) {
				HTMLPageSubBlock.append("<tr>");
				HTMLPageSubBlock.append("<td class='Elements' style='color:brown;'>" + ElementName + "</td>"); // style='color:brown;'
				HTMLPageSubBlock.append("<td class='Elements' style='color:brown;'>" + ActionTaken + "</td>");
				HTMLPageSubBlock.append("<td class='Elements' style='color:brown;'>" + TestCaseValue + "</td>");
				HTMLPageSubBlock.append("</tr>");
			} else {
				HTMLPageSubBlock.append("<tr>");
				HTMLPageSubBlock.append("<td class='Elements'>" + ElementName + "</td>");
				HTMLPageSubBlock.append("<td class='Elements'>" + ActionTaken + "</td>");
				HTMLPageSubBlock.append("<td class='Elements'>" + TestCaseValue + "</td>");
				HTMLPageSubBlock.append("</tr>");
			}
		}

		return HTMLPageSubBlock;
	}

	public StringBuffer EmailHTMLPageSubBlock(String LoadInReport, String ElementName, String ActionTaken, String TestCaseValue, String ExecutionStatus) {
		if (TestCaseValue.equalsIgnoreCase("#desc")) {
			TestCaseValue = "";
		}
		if (!LoadInReport.isEmpty() && LoadInReport != "" && LoadInReport.equalsIgnoreCase("yes")) {
			if (ExecutionStatus.equalsIgnoreCase("fail")) {
				EmailBody.append("<tr>");
				EmailBody.append("<td style='padding-bottom: 5px;color:brown;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + ElementName + "</td>"); // style='color:brown;'
				EmailBody.append("<td style='padding-bottom: 5px;color:brown;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + ActionTaken + "</td>");
				EmailBody.append("<td style='padding-bottom: 5px;color:brown;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + TestCaseValue + "</td>");
				EmailBody.append("</tr>");
			} else {
				EmailBody.append("<tr>");
				EmailBody.append("<td style='padding-bottom: 5px;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + ElementName + "</td>");
				EmailBody.append("<td style='padding-bottom: 5px;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + ActionTaken + "</td>");
				EmailBody.append("<td style='padding-bottom: 5px;padding-left: 14px;padding-top: 8px;height: auto;text-align: left;font: 13px sans-serif;'>" + TestCaseValue + "</td>");
				EmailBody.append("</tr>");
			}
		}
		return EmailBody;
	}

	public StringBuffer EmailPageHeader() { // Done
		HTMLPageSubBlock.append("<a name='Exedetails'></a>");
		HTMLPageSubBlock.append("<table class='Exedetails'> ");
		HTMLPageSubBlock.append("<tr class='headrow'>");
		HTMLPageSubBlock.append("<td ><b>MODULES<b></td>"); // CHECKPOINTS
		HTMLPageSubBlock.append("<td ><b>PAGE<b></td>");
		HTMLPageSubBlock.append("<td ><b>STATUS<b></td>");
		HTMLPageSubBlock.append("<td ><b>DESCRIPTIONS<b></td>"); // TEST DETAILS
		HTMLPageSubBlock.append("<td ><b>OUTPUT<b></td>"); // DETAILS
		HTMLPageSubBlock.append("<td ><b>TIME<b></td>");
		HTMLPageSubBlock.append("<td ><b>REMARKS<b></td>");
		HTMLPageSubBlock.append("</tr>");

		return HTMLPageSubBlock;
	}

	public StringBuffer EmailHTMLPageHeader(String PassPercentageValue) {
		String HeadFoot = "", HeadRow = "";
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		String CellBG = "#F2F3F4;";
		HeadRow = "background-color: rgb(12, 11, 11);";
		HeadFoot = "background-color: rgb(12, 11, 11);";

		EmailBody.append("<a name='Exedetails'></a>");
		EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-color: " + CellBG + "'> ");
		EmailBody.append("<tr style='" + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;border-collapse: separate;'>");
		EmailBody.append("<td style='padding-left: 14px;'><b>MODULES<b></td>"); // CHECKPOINTS
		EmailBody.append("<td style='padding-left: 14px;'><b>PAGE<b></td>");
		EmailBody.append("<td style='padding-left: 14px;'><b>STATUS<b></td>");
		EmailBody.append("<td style='padding-left: 14px;'><b>DESCRIPTIONS<b></td>"); // TEST DETAILS
		EmailBody.append("<td style='padding-left: 14px;'><b>OUTPUT<b></td>"); // DETAILS
		EmailBody.append("<td style='padding-left: 14px;'><b>TIME<b></td>");
		EmailBody.append("<td style='padding-left: 14px;'><b>REMARKS<b></td>");
		EmailBody.append("</tr>");

		return EmailBody;
	}

	public StringBuffer EmailPageTail() {
		HTMLPageSubBlock.append("</table>");
		return HTMLPageSubBlock;
	}

	public StringBuffer EmailHTMLPageTail() {
		EmailBody.append("</table>");
		return EmailBody;
	}

	public StringBuffer HTMLMainHeader() {
		HTMLPageSubBlock.append("<body><table class='MainHeader'>");
		HTMLPageSubBlock.append("<tr class='HeaderBlock'><td style='text-align: left;padding-left: 14px; width: 10%;'>");
		File LogoFile = new File(LogoPath + "//" + "KatalystLogo.png");
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(LogoFile);
			byte[] bytes = new byte[(int) LogoFile.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String LogoBase64Code = encodedfile;
		HTMLPageSubBlock.append("<img src='data:image/png;base64, " + LogoBase64Code + "' alt='Red dot' style='height:200px; width : 200px' />");
		HTMLPageSubBlock.append("</td>");
		HTMLPageSubBlock.append("<td style='text-align: left;padding-left: 14px; padding-top: 20px; width: 50px;'>");
		HTMLPageSubBlock.append("<div style='font: bold 60px Cookie; color: rgb(255, 255, 255); '>K<span style='font: bold 50px Cookie; color: rgb(132, 230, 230);'>atalyst</span> </div>");
		HTMLPageSubBlock.append("<div style='padding-left: 108px;color: rgb(240, 233, 233); font: bold 15px Cookie;'>BDD + TDD</div></td> ");
		HTMLPageSubBlock.append("</tr></table>");
		return HTMLPageSubBlock;
	}

	public StringBuffer EmailMainHeader(String PassPercentageValue) {

		String HeadFoot = "", HeadRow = "";
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		HeadRow = "background-color: rgb(12, 11, 11);";
		HeadFoot = "background-color: rgb(12, 11, 11);";
		String CellBG = "#F2F3F4;";

		EmailBody.append("<html><body style='height: 100%; width: 100%; padding: 0;margin: 0;'><table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;" + HeadFoot + "'>");
		EmailBody.append("<tr style='height: 120px;vertical-align: top;'><td style='text-align: left;padding-left: 14px; width: 10%;padding-top: 10px;padding-bottom: 10px;'>");
		File LogoFile = new File(LogoPath + "//" + "KatalystLogo.png");
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(LogoFile);
			byte[] bytes = new byte[(int) LogoFile.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String LogoBase64Code = encodedfile;
		EmailBody.append("<img src='data:image/png;base64, " + LogoBase64Code + "' alt='Red dot' style='height:200px; width : 200px' />");
		EmailBody.append("</td>");
		EmailBody.append("<td style='text-align: left;padding-left: 28px; padding-top: 20px; width: 50px;'>");
		EmailBody.append("<div style='font: bold 60px Cookie; color: rgb(255, 255, 255); '>K<span style='font: bold 50px Cookie; color: rgb(132, 230, 230);'>atalyst</span> </div>");
		EmailBody.append("<div style='padding-left: 108px;color: rgb(240, 233, 233); font: bold 15px Cookie;'>BDD + TDD</div></td> ");
		EmailBody.append("</tr></table>");
		return EmailBody;
	}

	public StringBuffer HTMLTestExecutionDetails(String PassPercentageValue, String SourceTestCase) {
		String TCName = excelUtil.SourceTestCase;
		String TCDesc = excelUtil.GetParameterValue(SourceTestCase, "TestDescription").toString();
		String TestDesigner = excelUtil.GetParameterValue(SourceTestCase, "TestDesigner").toString();
		String TestExecutor = excelUtil.GetParameterValue(SourceTestCase, "TestExecutioner").toString();
		TestStartTime = TestStartDate;
		TestEndTime = TestEndDate;
		String hostName = null;
		SimpleDateFormat Startformatter = new SimpleDateFormat("HH:mm:ss");
		strDate = Startformatter.format(TestStartTime);
		SimpleDateFormat Endformatter = new SimpleDateFormat("HH:mm:ss");
		endDate = Endformatter.format(TestEndTime);
		try {
			TestTotalTime = getExecutionTime(strDate, endDate);
			System.out.println("Test Total Time : " + TestTotalTime);
			java.net.InetAddress addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
			ExecutorUpdates.put(reportTimeStamp + "#" + "TestExecutionTime", TestTotalTime);
		} catch (Exception e) {
			System.out.println(strDate + "  " + endDate);
			e.printStackTrace();
		}
//		<!-- *************************************************************************************** -->
//		<!-- *************************************************************************************** -->
		HTMLPageSubBlock.append("<a name='Testdetails'></a>");
		HTMLPageSubBlock.append("<table class='Testdetails'>");
		HTMLPageSubBlock.append("<tbody>");

		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td colspan='6' class='headrow' >"); // style='color:rgb(49, 146, 81);'
		HTMLPageSubBlock.append("<b>" + ApplicationName.toUpperCase() + " - EXECUTION DETAILS</b>");
		HTMLPageSubBlock.append("</td>");
		HTMLPageSubBlock.append("</tr>");

		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td >&nbsp;&nbsp; Test Case Name</td>");
		HTMLPageSubBlock.append("<td><b >" + TCName + "</b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; System Informaion</td>");
		HTMLPageSubBlock.append("<td><b>" + hostName + "</b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Execution Start Time</td>");
		HTMLPageSubBlock.append("<td><b>" + TestStartTime + "</b></td>");
		HTMLPageSubBlock.append("</tr>");

		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td>&nbsp;&nbsp; Test Case Description</td>");
		HTMLPageSubBlock.append("<td><b>" + TCDesc + "</b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Pass Percentage</td>");
		Double IntValue = Double.parseDouble(PassPercentageValue);
		HTMLPageSubBlock.append("<td><b>" + IntValue + " % </b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Execution End Time</td>");
		HTMLPageSubBlock.append("<td><b>" + TestEndTime + "</b></td>");
		HTMLPageSubBlock.append("</tr>");

		String BrowserAlias = BrowserName.substring(0, 1).toUpperCase() + BrowserName.substring(1, BrowserName.length()).toLowerCase();
//		System.out.println(BrowserAlias);

		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td>&nbsp;&nbsp; Test Browser</td>");
		HTMLPageSubBlock.append("<td><b style='padding-left: 1px;'>" + BrowserAlias + "</b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Fail Percentage</td>");
		HTMLPageSubBlock.append("<td><b style='padding-left: 1px;'>" + (100 - IntValue) + " % </b ></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Total Execution Time</td>");
		HTMLPageSubBlock.append("<td><b>" + TestTotalTime + "</b></td>");
		HTMLPageSubBlock.append("</tr>");

		HTMLPageSubBlock.append("<tr >");
		HTMLPageSubBlock.append("<td>&nbsp;&nbsp; Test Execution Type</td>");
		HTMLPageSubBlock.append("<td><b style='padding-left: 1px;'>End To End</b></td>");
		HTMLPageSubBlock.append("<td style='padding-left: 8px;'>Test Designer</td>");
		HTMLPageSubBlock.append("<td><b>" + TestDesigner + "</b></td>");
		HTMLPageSubBlock.append("<td>&nbsp; Test Executor</td>");
		HTMLPageSubBlock.append("<td><b>" + TestExecutor + "</b></td>");
		HTMLPageSubBlock.append("</tr>");

		HTMLPageSubBlock.append("</tbody>");
		HTMLPageSubBlock.append("</table>");
		HTMLPageSubBlock.append("<br/><br/>");

//		<!-- *************************************************************************************** -->
//		<!-- *************************************************************************************** -->
		return HTMLPageSubBlock;
	}

	public StringBuffer HTMLTestStyle(String PassPercentageValue) {
		String HeadFoot = "", HeadRow = "";
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		String CellBG = "#F2F3F4;";

		HTMLPageSubBlock.append("<html lang='en'>");
		HTMLPageSubBlock.append("<head>");
		HTMLPageSubBlock.append("<title>Automation Execution Report</title>");

		HTMLPageSubBlock = EmailTestHeader(PassPercentageValue);

//		HTMLPageSubBlock.append("</head>");
		HTMLPageSubBlock.append("<style>");

		HTMLPageSubBlock.append("::-webkit-scrollbar { width: 10px;}");
		HTMLPageSubBlock.append("::-webkit-scrollbar-track {box-shadow: inset 0 0 5px gray; border-radius: 10px;}");
		HTMLPageSubBlock.append("::-webkit-scrollbar-thumb { background-image: " + HeadFoot + " border-radius: 10px;}");
		HTMLPageSubBlock.append("::-webkit-scrollbar-thumb:hover {background-image: " + HeadRow + "}");

		HTMLPageSubBlock.append("#ChartTable{border: 2px solid white;width: 100%;border-collapse: collapse;}");
		HTMLPageSubBlock.append("#SnapTd {color: black;border: 1px solid white}"); // #F2F3F4
		HTMLPageSubBlock.append("#Varlink{text-decoration:None;color: black;}");
		HTMLPageSubBlock.append("#Varlink:hover{text-decoration:None;color: rgb(179, 47, 87);}");
		HTMLPageSubBlock.append("#VarContactlink{color: white;}");
		HTMLPageSubBlock.append("#VarContactlink:hover{text-decoration:None;color: rgb(179, 47, 87);}");
		HTMLPageSubBlock.append(".Chartdetails { box-sizing: border-box; width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image: " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Chartdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Chartdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Snapdetails {box-sizing: border-box;width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image: " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Snapdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Snapdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "padding-left: 14px;}");
		HTMLPageSubBlock.append("#PieTd {color: " + CellBG + "width: 30%;}");
		HTMLPageSubBlock.append("#piechart_3d{width: 400px; height: 350px;}");
		HTMLPageSubBlock.append("#barchart_values{width: 450px; height: 350px;}");
		HTMLPageSubBlock.append("#donutchart{width: 400px; height: 350px;}");
		HTMLPageSubBlock.append("html, body{height:100%;width:100%;padding:0;margin:0;}");
		HTMLPageSubBlock.append(".IOdetails {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".IOdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".IOdetails .Secondheadrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 14px;text-align: left;}");
		HTMLPageSubBlock.append(".IOdetails .Secondheadrow td{padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;}");
		HTMLPageSubBlock.append(".IOdetails .checkPage{vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 10%;}");

		HTMLPageSubBlock.append(".IOdetails .checkPageFeature{vertical-align: top;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 25%;}");

		HTMLPageSubBlock.append(".IOdetails .checkDiv{vertical-align: top;background-color: " + CellBG + "width: 35%;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");

		HTMLPageSubBlock.append(".IOdetails .featuredetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");

//		HTMLPageSubBlock.append(
//				".IOdetails .inputdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");

		HTMLPageSubBlock.append(".IOdetails .featuredetails .tdHead{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails .tdHead{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails .tdTail{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
//		HTMLPageSubBlock.append(
//				".IOdetails .outputdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails .tdHead{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails .tdTail{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;}");
		HTMLPageSubBlock.append(".Exedetails {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-color: " + CellBG + "}");
//		padding: 210px 0px 30px 20px;
		HTMLPageSubBlock.append(".Exedetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Exedetails .headrow td{padding-left: 14px;}");

		HTMLPageSubBlock.append(".Exedetails .checkPage{vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Exedetails .checkStatus{vertical-align: top;padding-right: 15px;padding-top: 10px;height: auto;text-align: center;font: 13px sans-serif;background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .descCell{background-color: rgb(201, 209, 209);;padding-bottom: 5px;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .ElemensCell{background-color: rgb(209, 228, 228);padding-bottom: 4px; width: 36%;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .Elements{padding-bottom: 5px;}");
		HTMLPageSubBlock.append(".Exedetails .Outputetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".Exedetails .Outputetails .OutputTd{vertical-align: top;padding-left: 14px;padding-top: 10px;}");
		HTMLPageSubBlock.append(".Testdetails {box-sizing: border-box;width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Testdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Testdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".MainHeader {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
		HTMLPageSubBlock.append(".MainHeader .HeaderBlock {height: 120px;vertical-align: top;}");
		HTMLPageSubBlock.append(".HeaderBlock td {vertical-align: top;padding-top: 10px;padding-right: 15px;font: 15px sans-serif;text-align: right;text-align: top ;color: white;}");
		HTMLPageSubBlock.append(".MainFooter {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
//		padding: 210px 0px 30px 20px;
		HTMLPageSubBlock.append(".MainFooter h3 {color: rgb(132, 230, 230);font: bold 20px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter div {color: rgb(132, 230, 230);font: bold 20px 'Cookie', normal;padding-top: 1px;}");
		HTMLPageSubBlock.append(".MainFooter .footer-company-about {line-height: 20px;color: #010b13;font-size: 13px;font-weight: normal;margin: 0;}");
		HTMLPageSubBlock.append(".MainFooter .footer-company-about span {display: block;color: #130101;font-size: 18px;font-weight: bold;margin-bottom: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .FooterLinks a {display: inline-block;line-height: 1.8;text-decoration: none;color: rgb(247, 247, 247);font: bold 15px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter .FooterLinks span {display: inline-block;line-height: 1.8;text-decoration: none;color: rgb(247, 247, 247);font: bold 15px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter .CompanyNameFooter {color: #c5cfcf;font-size: 14px;font-weight: normal;margin: 0;}");
		HTMLPageSubBlock.append(".MainFooter .Katalyst {width: 22%;text-align: left;vertical-align: top;padding-left: 14px;padding-top: 15px;padding-bottom: 40px;}");
		HTMLPageSubBlock.append(".MainFooter .LogoStart {font: bold 20px 'Cookie', cursive;color: rgb(255, 255, 255);}");
		HTMLPageSubBlock.append(".MainFooter .LogoEnd {font: bold 20px 'Cookie', cursive;color: rgb(132, 230, 230);}");
		HTMLPageSubBlock.append(".MainFooter .TeamContact{width: 16%;text-align: left;vertical-align: top;padding-left: 30px;padding-top: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .TeamContact a{vertical-align: top;padding-top: 1px;padding-bottom: 5px;padding-left: 5px;}");
		HTMLPageSubBlock.append(".MainFooter .AboutApplication{width: auto;text-align: left;vertical-align: top;padding-left: 0px;padding-top: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .ApplicationAboutFooter {line-height: 20px;color: #c5cfcf;font-size: 14px;font-weight: normal;padding-top: 12px;margin: 0;}");

		HTMLPageSubBlock.append(".container {position: relative;}");
		HTMLPageSubBlock.append(".mySlides {display: none;padding-left: 12px;}"); // padding-right: 10%;
		HTMLPageSubBlock.append(".cursor {cursor: pointer;}");
		HTMLPageSubBlock.append(".prev,.next {cursor: pointer;position: absolute;top: 40%;width: auto;padding: 0px 10px 2px;border-radius: 3px 1px 1px 3px;color: rgb(236, 227, 227);font-weight: bold;font-size: 35px;height: 50px;text-decoration: none;}");
		HTMLPageSubBlock.append(".prev:hover,.next:hover {background-color: rgba(81, 82, 72, 0.8);}");
		HTMLPageSubBlock.append(".numbertext {color: #111111;font-size: 16px;padding-top: 470px;padding-left: 1150px;position: absolute;top: 0;font-weight: bold;width: 80px;}");
		HTMLPageSubBlock.append(".caption-container {text-align: center;color: rgb(36, 33, 33);font-weight: bold;padding-right: 26.8%;background-color: white;} .row:after {content: '';display: table;clear: both;}");
		HTMLPageSubBlock.append(".column {float: left;width: 80%;}");
		HTMLPageSubBlock.append(".demo {opacity: 0.6;}");
		HTMLPageSubBlock.append(".active,.demo:hover {opacity: 1;}");

		HTMLPageSubBlock.append("</style>");
		HTMLPageSubBlock.append("</head>");

		return HTMLPageSubBlock;
	}

	public StringBuffer EmailTestExecutionDetails(String PassPercentageValue, String SourceTestCase) {

		String HeadFoot = "", HeadRow = "";
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		String CellBG = "#F2F3F4;";
		HeadRow = "background-color: rgb(12, 11, 11);";
		HeadFoot = "background-color: rgb(12, 11, 11);";

		String TCName = excelUtil.SourceTestCase;
		String TCDesc = excelUtil.GetParameterValue(SourceTestCase, "TestDescription").toString();
		String TestDesigner = excelUtil.GetParameterValue(SourceTestCase, "TestDesigner").toString();
		String TestExecutor = excelUtil.GetParameterValue(SourceTestCase, "TestExecutor").toString();
		TestStartTime = TestStartDate;
		TestEndTime = TestEndDate;
		String hostName = null;
		SimpleDateFormat Startformatter = new SimpleDateFormat("HH:mm:ss");
		strDate = Startformatter.format(TestStartTime);
		SimpleDateFormat Endformatter = new SimpleDateFormat("HH:mm:ss");
		endDate = Endformatter.format(TestEndTime);
		try {
			TestTotalTime = getExecutionTime(strDate, endDate);
			System.out.println("TestTotalTime : " + TestTotalTime);
			java.net.InetAddress addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (Exception e) {
			System.out.println(strDate + "  " + endDate);
			e.printStackTrace();
		}
//		<!-- *************************************************************************************** -->
//		<!-- *************************************************************************************** -->
		EmailBody.append("<a name='Testdetails'></a>");
		EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;" + HeadFoot + "'>");
		EmailBody.append("<tbody>");

		EmailBody.append("<tr>");
		EmailBody.append("<td colspan='6' style='" + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;' >"); // style='color:rgb(49,
																																												// 146,
																																												// 81);'
		EmailBody.append("<b>" + ApplicationName.toUpperCase() + " - EXECUTION DETAILS</b>");
		EmailBody.append("</td>");
		EmailBody.append("</tr>");

		EmailBody.append("<tr>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp;&nbsp; Test Case Name</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b >" + TCName + "</b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; System Informaion</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + hostName + "</b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Execution Start Time</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TestStartTime + "</b></td>");
		EmailBody.append("</tr>");

//		Double IntValue = Double.parseDouble(PassPercentageValue);
		EmailBody.append("<tr>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp;&nbsp; Test Case Description</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TCDesc + "</b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Pass Percentage</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + IntValue + " % </b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Execution End Time</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TestEndTime + "</b></td>");
		EmailBody.append("</tr>");

		String BrowserAlias = BrowserName.substring(0, 1).toUpperCase() + BrowserName.substring(1, BrowserName.length()).toLowerCase();
//		System.out.println(BrowserAlias);

		EmailBody.append("<tr>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp;&nbsp; Test Browser</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b style='padding-left: 1px;'>" + BrowserAlias + "</b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Fail Percentage</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b style='padding-left: 1px;'>" + (100 - IntValue) + " % </b ></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Total Execution Time</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TestTotalTime + "</b></td>");
		EmailBody.append("</tr>");

		EmailBody.append("<tr >");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp;&nbsp; Test Execution Type</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b style='padding-left: 1px;'>End To End</b></td>");
		EmailBody.append("<td style='padding-left: 8px;padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>Test Designer</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TestDesigner + "</b></td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'>&nbsp; Test Executor</td>");
		EmailBody.append("<td style='padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "'><b>" + TestExecutor + "</b></td>");
		EmailBody.append("</tr>");

		EmailBody.append("</tbody>");
		EmailBody.append("</table>");
		EmailBody.append("<br/><br/>");

//		<!-- *************************************************************************************** -->
//		<!-- *************************************************************************************** -->
		return EmailBody;

	}

	public StringBuffer EmailHTMLTestStyle(String PassPercentageValue) {
		String HeadFoot = "", HeadRow = "";
//		int IntValue = (int) Math.round(Double.parseDouble(PassPercentageValue));
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		String CellBG = "#F2F3F4;";

		HTMLPageSubBlock.append("<html lang='en'>");
		HTMLPageSubBlock.append("<head>");
		HTMLPageSubBlock.append("<title>Automation Execution Report</title>");

		HTMLPageSubBlock = EmailTestHeader(PassPercentageValue);

//		HTMLPageSubBlock.append("</head>");
		HTMLPageSubBlock.append("<style>");

		HTMLPageSubBlock.append("#ChartTable{border: 2px solid white;width: 100%;border-collapse: collapse;}");
		HTMLPageSubBlock.append("#SnapTd {color: black;border: 1px solid white}"); // #F2F3F4
		HTMLPageSubBlock.append("#Varlink{text-decoration:None;color: black;}");
		HTMLPageSubBlock.append("#Varlink:hover{text-decoration:None;color: rgb(179, 47, 87);}");
		HTMLPageSubBlock.append("#VarContactlink{color: white;}");
		HTMLPageSubBlock.append("#VarContactlink:hover{text-decoration:None;color: rgb(179, 47, 87);}");
		HTMLPageSubBlock.append(".Chartdetails { box-sizing: border-box; width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image: " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Chartdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Chartdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Snapdetails {box-sizing: border-box;width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image: " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Snapdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Snapdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "padding-left: 14px;}");
		HTMLPageSubBlock.append("#PieTd {color: " + CellBG + "width: 30%;}");
		HTMLPageSubBlock.append("#piechart_3d{width: 400px; height: 350px;}");
		HTMLPageSubBlock.append("#barchart_values{width: 450px; height: 350px;}");
		HTMLPageSubBlock.append("#donutchart{width: 400px; height: 350px;}");
		HTMLPageSubBlock.append("html, body{height:100%;width:100%;padding:0;margin:0;}");
		HTMLPageSubBlock.append(".IOdetails {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".IOdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".IOdetails .Secondheadrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 14px;text-align: left;}");
		HTMLPageSubBlock.append(".IOdetails .Secondheadrow td{padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;}");
		HTMLPageSubBlock.append(".IOdetails .checkPage{vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 12%;}");
		HTMLPageSubBlock.append(".IOdetails .checkDiv{vertical-align: top;background-color: " + CellBG + "width: 35%;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
//		HTMLPageSubBlock.append(
//				".IOdetails .inputdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails .tdHead{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;}");
		HTMLPageSubBlock.append(".IOdetails .inputdetails .tdTail{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
//		HTMLPageSubBlock.append(
//				".IOdetails .outputdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails .tdHead{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;}");
		HTMLPageSubBlock.append(".IOdetails .outputdetails .tdTail{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;}");
		HTMLPageSubBlock.append(".Exedetails {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-color: " + CellBG + "}");
//		padding: 210px 0px 30px 20px;
		HTMLPageSubBlock.append(".Exedetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Exedetails .headrow td{padding-left: 14px;}");

		HTMLPageSubBlock.append(".Exedetails .checkPage{vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Exedetails .checkStatus{vertical-align: top;padding-right: 15px;padding-top: 10px;height: auto;text-align: center;font: 13px sans-serif;background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails td{padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .descCell{background-color: rgb(201, 209, 209);;padding-bottom: 5px;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .ElemensCell{background-color: rgb(209, 228, 228);padding-bottom: 4px; width: 36%;}");
		HTMLPageSubBlock.append(".Exedetails .Flowdetails .Elements{padding-bottom: 5px;}");
		HTMLPageSubBlock.append(".Exedetails .Outputetails{box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;}");
		HTMLPageSubBlock.append(".Exedetails .Outputetails .OutputTd{vertical-align: top;padding-left: 14px;padding-top: 10px;}");
		HTMLPageSubBlock.append(".Testdetails {box-sizing: border-box;width: 100%;text-align: center;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
		HTMLPageSubBlock.append(".Testdetails .headrow{background-image: " + HeadRow + "color: white;height: 40px;font-size: 15px;text-align: left;padding-left: 14px;border-collapse: separate;}");
		HTMLPageSubBlock.append(".Testdetails td{padding-top: 4px;height: 25px;text-align: left;font: 13px sans-serif;background-color: rgb(255, 255, 255);background-color: " + CellBG + "}");
		HTMLPageSubBlock.append(".MainHeader {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
		HTMLPageSubBlock.append(".MainHeader .HeaderBlock {height: 120px;vertical-align: top;}");
		HTMLPageSubBlock.append(".HeaderBlock td {vertical-align: top;padding-top: 10px;padding-right: 15px;font: 15px sans-serif;text-align: right;text-align: top ;color: white;}");
		HTMLPageSubBlock.append(".MainFooter {box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;background-image:  " + HeadFoot + "}");
//		padding: 210px 0px 30px 20px;
		HTMLPageSubBlock.append(".MainFooter h3 {color: rgb(132, 230, 230);font: bold 20px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter div {color: rgb(132, 230, 230);font: bold 20px 'Cookie', normal;padding-top: 1px;}");
		HTMLPageSubBlock.append(".MainFooter .footer-company-about {line-height: 20px;color: #010b13;font-size: 13px;font-weight: normal;margin: 0;}");
		HTMLPageSubBlock.append(".MainFooter .footer-company-about span {display: block;color: #130101;font-size: 18px;font-weight: bold;margin-bottom: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .FooterLinks a {display: inline-block;line-height: 1.8;text-decoration: none;color: rgb(247, 247, 247);font: bold 15px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter .FooterLinks span {display: inline-block;line-height: 1.8;text-decoration: none;color: rgb(247, 247, 247);font: bold 15px 'Cookie', normal;}");
		HTMLPageSubBlock.append(".MainFooter .CompanyNameFooter {color: #c5cfcf;font-size: 14px;font-weight: normal;margin: 0;}");
		HTMLPageSubBlock.append(".MainFooter .Katalyst {width: 22%;text-align: left;vertical-align: top;padding-left: 14px;padding-top: 15px;padding-bottom: 40px;}");
		HTMLPageSubBlock.append(".MainFooter .LogoStart {font: bold 20px 'Cookie', cursive;color: rgb(255, 255, 255);}");
		HTMLPageSubBlock.append(".MainFooter .LogoEnd {font: bold 20px 'Cookie', cursive;color: rgb(132, 230, 230);}");
		HTMLPageSubBlock.append(".MainFooter .TeamContact{width: 16%;text-align: left;vertical-align: top;padding-left: 30px;padding-top: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .TeamContact a{vertical-align: top;padding-top: 1px;padding-bottom: 5px;padding-left: 5px;}");
		HTMLPageSubBlock.append(".MainFooter .AboutApplication{width: auto;text-align: left;vertical-align: top;padding-left: 0px;padding-top: 20px;}");
		HTMLPageSubBlock.append(".MainFooter .ApplicationAboutFooter {line-height: 20px;color: #c5cfcf;font-size: 14px;font-weight: normal;padding-top: 12px;margin: 0;}");
		HTMLPageSubBlock.append("</style>");
		HTMLPageSubBlock.append("</head>");

		return HTMLPageSubBlock;
	}

	public StringBuffer HTMLTestScriptAtBody() {

		HTMLTestScriptBlock.append("<script>");
		HTMLTestScriptBlock.append("var tables = document.getElementsByClassName('Exedetails');");
		HTMLTestScriptBlock.append("for (var i = 0; i < tables.length; i++) {");
		HTMLTestScriptBlock.append("resizableGrid(tables[i]);}");
		HTMLTestScriptBlock.append("function resizableGrid(table) {");
		HTMLTestScriptBlock.append("var row = table.getElementsByTagName('tr')[0],");
		HTMLTestScriptBlock.append("cols = row ? row.children : undefined;");
		HTMLTestScriptBlock.append("if (!cols) return;");
		HTMLTestScriptBlock.append("for (var i = 0; i < cols.length; i++) {");
		HTMLTestScriptBlock.append("var div = createDiv(table.offsetHeight);");
		HTMLTestScriptBlock.append("cols[i].appendChild(div);");
		HTMLTestScriptBlock.append("cols[i].style.position = 'relative';");
		HTMLTestScriptBlock.append("setListeners(div);}}");

		HTMLTestScriptBlock.append("function createDiv(height) {");
		HTMLTestScriptBlock.append("var div = document.createElement('div');");
		HTMLTestScriptBlock.append("div.style.top = 0;");
		HTMLTestScriptBlock.append("div.style.right = 0;");
		HTMLTestScriptBlock.append("div.style.width = '1px';");
		HTMLTestScriptBlock.append("div.style.position = 'absolute';");
		HTMLTestScriptBlock.append("div.style.cursor = 'col-resize';");
//		HTMLTestScriptBlock.append("div.style.backgroundColor = '#F2F3F4';");
		HTMLTestScriptBlock.append("div.style.userSelect = 'none';");
		HTMLTestScriptBlock.append("div.style.height = height + 'px';");
		HTMLTestScriptBlock.append("return div;}");

		HTMLTestScriptBlock.append("function setListeners(div) {");
		HTMLTestScriptBlock.append("var pageX, curCol, nxtCol, curColWidth, nxtColWidth;");
		HTMLTestScriptBlock.append("div.addEventListener('mousedown', function (e) {");
		HTMLTestScriptBlock.append("curCol = e.target.parentElement;");
		HTMLTestScriptBlock.append("nxtCol = curCol.nextElementSibling;");
		HTMLTestScriptBlock.append("pageX = e.pageX;");
		HTMLTestScriptBlock.append("curColWidth = curCol.offsetWidth;");
		HTMLTestScriptBlock.append("if (nxtCol) nxtColWidth = nxtCol.offsetWidth;});");
		HTMLTestScriptBlock.append("document.addEventListener('mousemove', function (e) {");
		HTMLTestScriptBlock.append("if (curCol) {");
		HTMLTestScriptBlock.append("var diffX = e.pageX - pageX;");
		HTMLTestScriptBlock.append("if (nxtCol) nxtCol.style.width = nxtColWidth - diffX + 'px';");
		HTMLTestScriptBlock.append("curCol.style.width = curColWidth + diffX + 'px';}});");
		HTMLTestScriptBlock.append("document.addEventListener('mouseup', function (e) {");
		HTMLTestScriptBlock.append("curCol = undefined;");
		HTMLTestScriptBlock.append("nxtCol = undefined;");
		HTMLTestScriptBlock.append("pageX = undefined;");
		HTMLTestScriptBlock.append("nxtColWidth = undefined;");
		HTMLTestScriptBlock.append("curColWidth = undefined;});}");

		HTMLTestScriptBlock.append("var slideIndex = 1; var slideMax = 3;");
		HTMLTestScriptBlock.append("showSlides(slideIndex);");
		HTMLTestScriptBlock.append("function plusSlides(n) {showSlides((slideIndex += n));}");
		HTMLTestScriptBlock.append("function currentSlide(n) {showSlides((slideIndex = n));}");
		HTMLTestScriptBlock.append("function showSlides(n) {var i;");
		HTMLTestScriptBlock.append("var slides = document.getElementsByClassName('mySlides');");
		HTMLTestScriptBlock.append("var dots = document.getElementsByClassName('demo');");
		HTMLTestScriptBlock.append("var captionText = document.getElementById('caption');");
		HTMLTestScriptBlock.append("if (n > slides.length) {slideIndex = 1;}");
		HTMLTestScriptBlock.append("if (n < 1) {slideIndex = slides.length;}");
		HTMLTestScriptBlock.append("for (i = 0; i < slides.length; i++) {slides[i].style.display = 'none';}");
		HTMLTestScriptBlock.append("for (i = 0; i < dots.length; i++) {dots[i].style.display = 'none';}");
//		HTMLTestScriptBlock.append("for (i = 0; i < slideMax; i++) {dots[i].style.display = 'block';}");
//		Newly added
		HTMLTestScriptBlock.append("if (slides.length < slideMax) {for (i = 0; i < slides.length; i++) {dots[i].style.display = 'block';}} ");
		HTMLTestScriptBlock.append("else {for (i = 0; i < slideMax; i++) {dots[i].style.display = 'block';}}");

		HTMLTestScriptBlock.append("for (i = 0; i < dots.length; i++) {dots[i].className = dots[i].className.replace(' active', '');}");

		HTMLTestScriptBlock.append("if (n > slideMax) {for (i = 0; i < dots.length; i++) {dots[i].style.display = 'none';}");
		HTMLTestScriptBlock.append("if (n <= dots.length) {for (i = n; i > n - slideMax; i--) {dots[i - 1].style.display = 'block';}");
		HTMLTestScriptBlock.append("} else {for (i = 0; i < slideMax; i++) {dots[i].style.display = 'block';}}}");

		HTMLTestScriptBlock.append("if (n < 1) {for (i = 0; i < dots.length; i++) {dots[i].style.display = 'none';}");
//		HTMLTestScriptBlock
//				.append("for (i = dots.length; i > dots.length - slideMax; i--) {dots[i - 1].style.display = 'block';}}");

		HTMLTestScriptBlock.append("if (dots.length > slideMax) { for (i = dots.length; i > dots.length - slideMax; i--) {dots[i - 1].style.display = 'block';}}");
		HTMLTestScriptBlock.append("else {for (i = 0; i < dots.length; i++) {dots[i].style.display = 'block';}}}");

		HTMLTestScriptBlock.append("slides[slideIndex - 1].style.display = 'block';");
		HTMLTestScriptBlock.append("dots[slideIndex - 1].className += ' active';");
		HTMLTestScriptBlock.append("captionText.innerHTML = dots[slideIndex - 1].alt;}");

		HTMLTestScriptBlock.append("</script>");

		return HTMLTestScriptBlock;
	}

	public StringBuffer EmailTestHeader(String PassPercentageValue) {

//		int IntValue = (int) Math.round(Double.parseDouble(PassPercentageValue));
		Double IntValue = Double.parseDouble(PassPercentageValue);
		HTMLPageSubBlock.append("<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>");
		HTMLPageSubBlock.append("<script type='text/javascript'>");
		HTMLPageSubBlock.append("google.charts.load('current', {packages:['corechart']});");
		HTMLPageSubBlock.append("google.charts.setOnLoadCallback(drawPieChart);");
		HTMLPageSubBlock.append("google.charts.setOnLoadCallback(drawBarChart);");
		HTMLPageSubBlock.append("google.charts.setOnLoadCallback(drawDonutChart);");

		HTMLPageSubBlock.append("function drawPieChart() {");
		HTMLPageSubBlock.append("  var data = google.visualization.arrayToDataTable([");
		HTMLPageSubBlock.append("['Execution Status', 'Execution Percentage'],");
		HTMLPageSubBlock.append("['Pass',     " + IntValue + "],");
		HTMLPageSubBlock.append("['Fail',      " + (100 - IntValue) + "],");
		HTMLPageSubBlock.append("]);");
		HTMLPageSubBlock.append("var options = {");
		HTMLPageSubBlock.append("is3D: true,");
		HTMLPageSubBlock.append("slices: {");
		HTMLPageSubBlock.append("0: { color: 'rgb(14, 223, 84)' },");
		HTMLPageSubBlock.append("1: { color: 'rgb(223, 56, 14)' }");
		HTMLPageSubBlock.append("},");
		HTMLPageSubBlock.append("backgroundColor: '#F2F3F4',");

		HTMLPageSubBlock.append("chartArea:{left:60,top:45,width:'70%',height:'75%'},");
		HTMLPageSubBlock.append("legend: { position: 'none' },");
		HTMLPageSubBlock.append("};");
		HTMLPageSubBlock.append("var chart_div = document.getElementById('piechart_3d');");
		HTMLPageSubBlock.append("var chart = new google.visualization.PieChart(chart_div);");

		HTMLPageSubBlock.append("chart.draw(data, options);");
		HTMLPageSubBlock.append("}");

//		HTMLPageSubBlock.append("google.visualization.events.addListener(chart, 'ready', function () {");
//		HTMLPageSubBlock.append("chart_div.innerHTML = '<a><img src=' + chart.getImageURI() + '> </a>';");
//		HTMLPageSubBlock.append("console.log(chart_div.innerHTML);});");

		HTMLPageSubBlock.append("function drawBarChart() {");
		HTMLPageSubBlock.append("var data = google.visualization.arrayToDataTable([");
		HTMLPageSubBlock.append("['Status', 'Percentage', { role: 'style' } ],");
		HTMLPageSubBlock.append("['Pass', " + IntValue + ", 'rgb(14, 223, 84)'],");
		HTMLPageSubBlock.append("['Fail', " + (100 - IntValue) + ", 'rgb(223, 56, 14)'],");
		HTMLPageSubBlock.append("]);");
		HTMLPageSubBlock.append("var view = new google.visualization.DataView(data);");
		HTMLPageSubBlock.append("view.setColumns([0, 1,");
		HTMLPageSubBlock.append("{ calc: 'stringify',");
		HTMLPageSubBlock.append("sourceColumn: 1,");
		HTMLPageSubBlock.append("type: 'string',");
		HTMLPageSubBlock.append("role: 'annotation' },");
		HTMLPageSubBlock.append("2]);");
		HTMLPageSubBlock.append("var options = {");
		HTMLPageSubBlock.append("width: 500,");
		HTMLPageSubBlock.append("height: 300,");
		HTMLPageSubBlock.append("bar: {groupWidth: '95%'},");
		HTMLPageSubBlock.append("legend: { position: 'none' },");
		HTMLPageSubBlock.append("backgroundColor: '#F2F3F4',");

		HTMLPageSubBlock.append("chartArea:{left:35,top:60,width:'75%',height:'65%'},");
		HTMLPageSubBlock.append("};");
		HTMLPageSubBlock.append("var chart = new google.visualization.BarChart(document.getElementById('barchart_values'));");
		HTMLPageSubBlock.append("chart.draw(view, options);}");

		HTMLPageSubBlock.append("function drawDonutChart() {");
		HTMLPageSubBlock.append("var data = google.visualization.arrayToDataTable([");
		HTMLPageSubBlock.append("['Total Execution', 'Total Count'],");
		HTMLPageSubBlock.append("['Pass',     " + IntValue + "],");
		HTMLPageSubBlock.append("['Fail',    " + (100 - IntValue) + "],");
		HTMLPageSubBlock.append("]);");
		HTMLPageSubBlock.append("var options = {");
		HTMLPageSubBlock.append("pieHole: 0.4,");
		HTMLPageSubBlock.append("slices: {");
		HTMLPageSubBlock.append("0: { color: 'rgb(14, 223, 84)' },");
		HTMLPageSubBlock.append("1: { color: 'rgb(223, 56, 14)' }");
		HTMLPageSubBlock.append("},");
		HTMLPageSubBlock.append("backgroundColor: '#F2F3F4',");
		HTMLPageSubBlock.append("chartArea:{left:40,top:40,width:'100%',height:'75%'},");
		HTMLPageSubBlock.append("legend: { position: 'none' },");
		HTMLPageSubBlock.append("};");
		HTMLPageSubBlock.append("var chart = new google.visualization.PieChart(document.getElementById('donutchart'));");
		HTMLPageSubBlock.append("chart.draw(data, options);");
		HTMLPageSubBlock.append("}  ");
		HTMLPageSubBlock.append("</script>");

		return HTMLPageSubBlock;
	}

	public StringBuffer HTMLTestCharts(String PieChart, String DonutChart, String BarChart) {

		HTMLPageSubBlock.append("<br ></br>");
		HTMLPageSubBlock.append("<a name='ChartDetails'></a>");
		HTMLPageSubBlock.append("<table class='Chartdetails'>");

		HTMLPageSubBlock.append("<tr><td colspan='6' class='headrow'>");
		HTMLPageSubBlock.append("<b>FLOW STATUS CHARTS<b></td></tr>");

		HTMLPageSubBlock.append("<tr>");
		HTMLPageSubBlock.append("<td>");
		HTMLPageSubBlock.append("<div id='piechart_3d' ></div>");
		HTMLPageSubBlock.append("</td >");
		HTMLPageSubBlock.append("<td>");
		HTMLPageSubBlock.append("<div id='barchart_values' ></div>");
		HTMLPageSubBlock.append("</td>");
		HTMLPageSubBlock.append("<td>");
		HTMLPageSubBlock.append("<div id='donutchart' ></div>");
		HTMLPageSubBlock.append("</td>");
		HTMLPageSubBlock.append("</tr>");
		HTMLPageSubBlock.append("</table>");

		return HTMLPageSubBlock;
	}

	@SuppressWarnings({ "static-access", "resource" })
	public StringBuffer HTMLTestSnap(String TestCaseName) {

		HTMLPageBuffer.append("<br ></br>");
		HTMLPageBuffer.append("<a name='SnapDetails'></a>");
		HTMLPageBuffer.append("<table class='Snapdetails'>");
		String SnapBase64Code = null;
		String SnapTestPath = SnapshotsPath + "//" + TestCaseName;// "C:\\Users\\Saurabh.Sawalapurkar\\Desktop\\Report
		// Generator\\Snapshot";

		HTMLPageBuffer.append("<tr><td colspan='6' class='headrow'>");
		HTMLPageBuffer.append("<b>FLOW STATUS CHARTS<b></td></tr>");
		String SnapPage = "";
		String SnapElement = "";

		File dir = new File(SnapTestPath);
		if (dir.isDirectory() == false) {
//			System.out.println("Charts Test Directory Missing At Final Setup, Please check...");
		}
		File[] listFiles = dir.listFiles();
		for (File file : listFiles) {
			if (file.getName().contains(".png")) {
				for (Entry<String, String> snapdetails : SnapDetails.entrySet()) {
					if (file.getName().contains(snapdetails.getKey())) {
						SnapPage = snapdetails.getValue().split("#")[0];
						SnapElement = snapdetails.getValue().split("#")[1];
					}
				}

				if (file.getName().contains("#")) {
					SnapPage = file.getName().split("#")[1];
					SnapElement = file.getName().split("#")[2].replace(".png", "");
				}
				HTMLPageBuffer.append("<tr><td><b>Screenshot : " + SnapPage + " &#8658; " + SnapElement + "</b></td></tr>");
//				System.out.println(SnapTestPath + "//" + file.getName());
//				SnapBase64Code =  encodeFileToBase64Binary(SnapTestPath + "//" + file.getName());
				File Snapfile = new File(SnapTestPath + "//" + file.getName()); // "C:/Users/SETU BASAK/Desktop/a.jpg"
				String encodedfile = null;
				try {
					FileInputStream fileInputStreamReader = new FileInputStream(Snapfile);
					byte[] bytes = new byte[(int) file.length()];
					fileInputStreamReader.read(bytes);
					encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				SnapBase64Code = encodedfile;

				if (excelUtil.GetParameterValue(CurrentTestCase, "BrowserName").toLowerCase().contains("mobile")) {
					HTMLPageBuffer.append("<tr><td><p> <img src='data:image/png;base64, " + SnapBase64Code + "' alt='Red dot' height='550' width='400' />  </p></td></tr>");
				} else {
					HTMLPageBuffer.append("<tr><td ><p> <img src='data:image/png;base64, " + SnapBase64Code + "' alt='Red dot' height='550' width='900' />  </p></td></tr>");
				}

			}
		}

		HTMLPageBuffer.append("</table>");

		return HTMLPageBuffer;
	}

	@SuppressWarnings({ "static-access", "resource" })
	public StringBuffer HTMLTestSnapWithSlider(String TestCaseName, String reportTimeStamp) {

		HTMLPageBuffer.append("<br ></br>");
		HTMLPageBuffer.append("<a name='SnapDetails'></a>");
		HTMLPageBuffer.append("<table class='Snapdetails'>");
		String SnapBase64Code = null;
//		System.out.println(Browser);
//		String SnapTestPath =  SnapshotsPath + "//" + TestCaseName + "_" + Browser;
		String SnapTestPath = SnapshotsPath + "//" + TestCaseName + "_" + reportTimeStamp;
		// "C:\\Users\\Saurabh.Sawalapurkar\\Desktop\\Report
		// Generator\\Snapshot";

		HTMLPageBuffer.append("<tr><td colspan='6' class='headrow'>");
		HTMLPageBuffer.append("<b>FLOW SCREENSHOT<b></td></tr>");
		HTMLPageBuffer.append("<tr><td style='background-color: white;'>");

		HTMLPageBuffer.append("<div class='caption-container'>");
		HTMLPageBuffer.append("<div ");
		HTMLPageBuffer.append(" id='caption'");
		HTMLPageBuffer.append("style='");
		HTMLPageBuffer.append("height: 20px;");
		HTMLPageBuffer.append("text-align: left;");
		HTMLPageBuffer.append("padding-top: 8px;'></div></div>");

		HTMLPageBuffer.append("</td></tr></table>");

		HTMLPageBuffer.append("<table style='background-color: #F2F3F4;'><tr>");
		HTMLPageBuffer.append("<td style='vertical-align: top; padding-bottom: 0px;'>");

		HTMLPageBuffer.append("<div class='container'>");
		String SnapPage = "";
		String SnapElement = "";
		File dir = new File(SnapTestPath);
		if (dir.isDirectory() == false) {
//			System.out.println("Charts Test Directory Missing At Final Setup, Please check...");
		}
		File[] listFiles = dir.listFiles();
		int imgCount = listFiles.length;
		int imgindex = 1;
		for (File file : listFiles) {
			if (file.getName().contains(".png")) {
				SnapPage = "";
				SnapElement = "";
				for (Entry<String, String> snapdetails : SnapDetails.entrySet()) {
					if (file.getName().contains(snapdetails.getKey())) {
						if (snapdetails.getValue().contains("#")) {
							try {
								SnapPage = snapdetails.getValue().split("#")[0];
							} catch (Exception e) {
								SnapPage = "";
							}
							try {
								SnapElement = snapdetails.getValue().split("#")[1];
							} catch (Exception e) {
								SnapElement = "";
							}
							break;
						}
					}
				}

//				if (file.getName().contains("#")) {
//					SnapPage = file.getName().split("#")[1];
//					SnapElement = file.getName().split("#")[2].replace(".png", "");
//				}

				File Snapfile = new File(SnapTestPath + "//" + file.getName());
				String encodedfile = null;
				try {
					FileInputStream fileInputStreamReader = new FileInputStream(Snapfile);
					byte[] bytes = new byte[(int) file.length()];
					fileInputStreamReader.read(bytes);
					encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				SnapBase64Code = encodedfile;

				if (excelUtil.GetParameterValue(CurrentTestCase, "BrowserName").toLowerCase().contains("mobile")) {
					HTMLPageBuffer.append("<tr><td><p> <img src='data:image/png;base64, " + SnapBase64Code + "' alt='Red dot' height='550' width='400' />  </p></td></tr>");
				} else {
//					HTMLPageBuffer.append("<tr><td ><p> <img src='data:image/png;base64, " + SnapBase64Code
//							+ "' alt='Red dot' height='550' width='900' />  </p></td></tr>");

					HTMLPageBuffer.append("<div class='mySlides'>");
					HTMLPageBuffer.append("<div class='numbertext'>" + imgindex + " / " + imgCount + "</div>");
					HTMLPageBuffer.append("<img src='data:image/png;base64, " + SnapBase64Code + "' alt='Red dot' style='width: 1195; height: 510;' />");
					HTMLPageBuffer.append("</div>");

				}
				imgindex++;

			}
		}

		HTMLPageBuffer.append("<div style='padding-left: 1%;' >"); // style='padding-left: 14px;'
		HTMLPageBuffer.append("<a class='prev' href='#SnapDetails' onclick='plusSlides(-1)'>&lsaquo;</a>");
		HTMLPageBuffer.append("</div>");
		HTMLPageBuffer.append("<div style='padding-left: 97.3%;'>");
		HTMLPageBuffer.append("<a class='next' href='#SnapDetails'  onclick='plusSlides(1)'>&rsaquo;</a>");
		HTMLPageBuffer.append("</div>");
		HTMLPageBuffer.append("</div>");
		HTMLPageBuffer.append("</td><td style='vertical-align: top; padding-top: 2px;'>");
		HTMLPageBuffer.append("<div>"); // padding-right: 10%; style='padding-left: 14px; '
		HTMLPageBuffer.append("<div class='row'>");
		HTMLPageBuffer.append("<div>");

		imgindex = 1;
		for (File file : listFiles) {
			if (file.getName().contains(".png")) {
				SnapPage = "";
				SnapElement = "";
				for (Entry<String, String> snapdetails : SnapDetails.entrySet()) {
					if (file.getName().contains(snapdetails.getKey())) {
						SnapPage = snapdetails.getValue().split("#")[0];
						SnapElement = snapdetails.getValue().split("#")[1];
						break;
					}
				}
//				if (file.getName().contains("#")) {
//					SnapPage = file.getName().split("#")[1];
//					SnapElement = file.getName().split("#")[2].replace(".png", "");
//				}
//				HTMLPageBuffer.append("<tr><td><b>Snapshot : " + SnapPage + " => " + SnapElement + "</b></td></tr>");
				File Snapfile = new File(SnapTestPath + "//" + file.getName());
				String encodedfile = null;
				try {
					FileInputStream fileInputStreamReader = new FileInputStream(Snapfile);
					byte[] bytes = new byte[(int) file.length()];
					fileInputStreamReader.read(bytes);
					encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				SnapBase64Code = encodedfile;

				if (excelUtil.GetParameterValue(CurrentTestCase, "BrowserName").toLowerCase().contains("mobile")) {
					HTMLPageBuffer.append("<tr><td><p> <img src='data:image/png;base64, " + SnapBase64Code + "' alt='Red dot' height='550' width='400' />  </p></td></tr>");
				} else {
//					HTMLPageBuffer.append("<tr><td ><p> <img src='data:image/png;base64, " + SnapBase64Code
//							+ "' alt='Red dot' height='550' width='900' />  </p></td></tr>");

					HTMLPageBuffer.append("<div class='column'>");
					HTMLPageBuffer.append("<img class='demo cursor'");
					HTMLPageBuffer.append("src='data:image/png;base64, " + SnapBase64Code + "'");
					HTMLPageBuffer.append("style='width: 300; height: 170;'");
					HTMLPageBuffer.append("onclick='currentSlide(" + imgindex + ")'");
					HTMLPageBuffer.append("alt='" + "Screenshot : " + SnapPage + " &#8658; " + SnapElement + "'/>");
					HTMLPageBuffer.append("</div>");

				}
				imgindex++;
			}
		}
		HTMLPageBuffer.append("</div></div></div>");

		HTMLPageBuffer.append("</td></tr></table>");

		return HTMLPageBuffer;
	}

	public StringBuffer HTMLFooter() {

		HTMLPageBuffer.append("<br/><br/>");
		HTMLPageBuffer.append("<table class='MainFooter'>");
		HTMLPageBuffer.append("<tr>");
		HTMLPageBuffer.append("<td class='Katalyst'>");
		HTMLPageBuffer.append("<div class='LogoStart'>K<span class='LogoEnd'>atalyst</span></div>");
		HTMLPageBuffer.append("<p class='FooterLinks' >");
		HTMLPageBuffer.append("<a id='VarContactlink' href='#Testdetails'>Context</a>&nbsp; <span>|</span>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='#Exedetails'>Workflows<a>&nbsp; <span>|</span>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='#IOdetails'>In-Out</a>&nbsp; <span>|</span>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='#ChartDetails'>Charts</a>&nbsp; <span>|</span>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='#SnapDetails'>Snaps</a> ");
		HTMLPageBuffer.append("</p>");
		HTMLPageBuffer.append("<p class='CompanyNameFooter'>");
		HTMLPageBuffer.append("© 2020 Katalyst Learning Solutions.");
		HTMLPageBuffer.append("</p>");
		HTMLPageBuffer.append("</td>");
		HTMLPageBuffer.append("<td class='TeamContact'>");
		HTMLPageBuffer.append("<div>Offshore Team Contact</div>");
		HTMLPageBuffer.append("<p class='FooterLinks'>");
		HTMLPageBuffer.append("<img class='img1' src='' width='20' alt='Unavailable'/> &nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='mailto:ssawalapurka@avaya.com?cc=amalge@avaya.com;sbhutkar@avaya.com;wang351@avaya.com'>Saurabh Sawalapurkar </a><br />");
		HTMLPageBuffer.append("<img class='img1' src='' width='20' alt='Unavailable'/>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='mailto:amalge@avaya.com?cc=ssawalapurka@avaya.com;sbhutkar@avaya.com;wang351@avaya.com'>Aditya Malge</a><br />");
		HTMLPageBuffer.append("<img class='img1' src='' width='20' alt='Unavailable'/>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='mailto:sbhutkar@avaya.com?cc=ssawalapurka@avaya.com;amalge@avaya.com;wang351@avaya.com'>Shubham Bhutkar</a></p></td>");
		HTMLPageBuffer.append("<td class='TeamContact'>");
		HTMLPageBuffer.append("<div>Onshore Team Contact</div>");
		HTMLPageBuffer.append("<p class='FooterLinks'>");
		HTMLPageBuffer.append("<img class='img1' src='' width='20' alt='Unavailable'/>&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='mailto:wang351@avaya.com?cc=ssawalapurka@avaya.com;amalge@avaya.com;sbhutkar@avaya.com'> Yue Wang</a></p></td>");
		HTMLPageBuffer.append("<td class='TeamContact'>");
		HTMLPageBuffer.append("<div>Dev Team Contact</div>");
		HTMLPageBuffer.append("<p class='FooterLinks'>");
		HTMLPageBuffer.append("<img class='img1' src='' width='20' alt='Unavailable' />&nbsp;");
		HTMLPageBuffer.append("<a id='VarContactlink' href='mailto:wang351@avaya.com?cc=ssawalapurka@avaya.com;amalge@avaya.com;sbhutkar@avaya.com' >Dhawal Kodrani</a></p></td>");
		HTMLPageBuffer.append("<td class='AboutApplication'>");
		HTMLPageBuffer.append("<div>About Application</div>");
		HTMLPageBuffer.append("<p class='ApplicationAboutFooter'>");
		HTMLPageBuffer.append("We will provide applications basic summary or general flow descriptions, which will help any handler to get an overview.");
		HTMLPageBuffer.append("</p></td></tr></table>");

		return HTMLPageBuffer;
	}

	public StringBuffer HTMLFooterScript() {

		HTMLPageBuffer.append("<script>");
		// document.getElementById("currentdate").innerText = Date()
		HTMLPageBuffer.append("var emailpath ='https://www.freepnglogos.com/uploads/email-png/blue-email-box-circle-png-transparent-icon-2.png';");
		HTMLPageBuffer.append("var imgele = document.getElementsByClassName('img1');");
		HTMLPageBuffer.append("var els = imgele.length;");
		HTMLPageBuffer.append("for (var index = 0; index < els; index++) {");
		HTMLPageBuffer.append("imgele[index].src= emailpath;");
		HTMLPageBuffer.append("}");
		HTMLPageBuffer.append("</script>");

		return HTMLPageBuffer;
	}

	public StringBuffer PageOutputBlock() {
		// Done
		String WSSheetName, WorkflowPageValue, ExecutionStatus, ExecutionBlockStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, IOType;
		String CapturedPage = "", ElementDescription = "";
		String outflag = "";
		int PageElementsCount, count, TestRowNum;
		count = 1;

		HTMLPageSubBlock.append("<br></br>");
		HTMLPageSubBlock.append("<a name='IOdetails'></a>");
		HTMLPageSubBlock.append("<table class='IOdetails'>");
//		HTMLPageSubBlock.append("<tr >");
//		HTMLPageSubBlock.append("<td colspan='4'><b>INPUT/OUTPUT DETAILS</b></td>");	
//		HTMLPageSubBlock.append("</tr>");
		HTMLPageSubBlock.append("<tr  class='Secondheadrow'>");
		HTMLPageSubBlock.append("<td ><b>MODULES<b></td>"); // CHECKPOINTS
		HTMLPageSubBlock.append("<td ><b>PAGE<b></td>");
		HTMLPageSubBlock.append("<td ><b>FEATURES<b></td>");
		HTMLPageSubBlock.append("<td ><b>INPUT<b></td>"); // DETAILS
		HTMLPageSubBlock.append("<td ><b>OUTPUT<b></td>"); // DETAILS
		HTMLPageSubBlock.append("</tr>");

		String strIO = "", SourceTestCase, CurrentTestCase, WorkflowTestPage = "", EleDesc = "";
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			Output = loadInReport.getValue().getOutput();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();
			TestRowNum = loadInReport.getValue().getTestRowNum();
			ElementDescription = loadInReport.getValue().getElementDescription();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();

			WorkflowTestPage = CurrentTestCase + "@" + WorkflowPageValue;

			if (CapturedPage.contains(WorkflowTestPage)) {
				if (Output != null) {
					strIO = strIO + "|" + Output;
				}
				if (ElementDescription != null && TestCaseValue != "" && !TestCaseValue.isEmpty()) {
					EleDesc = EleDesc + "|" + ElementDescription;
				}

			} else {
				count = 1;
				strIO = Output;
				if (TestCaseValue != "" && !TestCaseValue.isEmpty()) {
					EleDesc = ElementDescription;
				} else {
					EleDesc = "";
				}

				HTMLPageSubBlock.append("<tr>");
				HTMLPageSubBlock.append("<td class='checkPage'>" + WSSheetName + "</td>");
				HTMLPageSubBlock.append("<td class='checkPage'>" + WorkflowPageValue + "</td>"); // align='center'
			}

			String IOData, IOLabel, IOValue, StrOutput = "", StrInput = "", IOLink, EleDescLabel;
			int IOCount = 0, OCount = 0, ICount = 0;

			if (count == PageElementsCount || ExecutionStatus.equalsIgnoreCase("Fail")) {
				if (EleDesc.contains("|")) {
					HTMLPageSubBlock.append("<td class='checkPageFeature'><div>");
					HTMLPageSubBlock.append("<table class='featuredetails'>");
					boolean EleDescflag = false;
					for (int ed = 0; ed < EleDesc.split("\\|").length; ed++) {
						EleDescLabel = EleDesc.split("\\|")[ed];
						HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + EleDescLabel + "</td></tr>");
						EleDescflag = true;
					}
//					if (EleDescflag == false) {
//						HTMLPageSubBlock.append("<tr><td></td></tr>");
//					}
					HTMLPageSubBlock.append("</table></div></td>");
				} else {
					HTMLPageSubBlock.append("<td class='checkPageFeature'><div>");
					HTMLPageSubBlock.append("<table class='featuredetails'>");
					HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + EleDesc + "</td></tr>");
					HTMLPageSubBlock.append("</table></div></td>");

				}

				Output = strIO;
				if (Output != null) {
					if (Output.contains("|")) {
						HTMLPageSubBlock.append("<td class='checkDiv'><div>");
						HTMLPageSubBlock.append("<table class='inputdetails'>");
						boolean InputFlag = false;
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								IOLabel = IOData.split(";")[1].trim();
								IOValue = IOData.split(";")[2].trim();
								IOLink = IOData.split(";")[3].trim();

								if (IOLabel.equalsIgnoreCase("password")) {
									IOValue = "*********";
								}
//								#Desc
								if (IOValue.equalsIgnoreCase("#Desc")) {
									IOLabel = "";
									IOValue = "";
								}

								if (IOType.equalsIgnoreCase("input") && IOLabel != "") {
									HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + IOLabel + "</td>");
									HTMLPageSubBlock.append("<td class='tdTail'>" + IOValue + "</td></tr>");
									InputFlag = true;
								}

							}
						}
						if (InputFlag == false) {
							HTMLPageSubBlock.append("<tr><td></td></tr>");
						}

						HTMLPageSubBlock.append("</table></div></td>");
					}

					if (Output.contains("|")) {
						HTMLPageSubBlock.append("<td class='checkDiv'><div>");
						HTMLPageSubBlock.append("<table class='outputdetails'>");
						boolean Outputflag = false;
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								IOLabel = IOData.split(";")[1].trim();
								IOValue = IOData.split(";")[2].trim();
								IOLink = IOData.split(";")[3].trim();

								if (IOValue.equalsIgnoreCase("#Desc")) {
									IOLabel = "";
									IOValue = "";
								}
								if (IOType.equalsIgnoreCase("output") && IOLabel != "") {
									HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + IOLabel + "</td>");
									HTMLPageSubBlock.append("<td class='tdTail'>" + IOValue + "</td></tr>");
									Outputflag = true;
								}

							}
						}
						if (Outputflag == false) {
							HTMLPageSubBlock.append("<tr><td></td></tr>");
						}

						HTMLPageSubBlock.append("</table></div></td>");
					} else {
						IOData = Output;
						if (IOData.contains(";")) {
							IOType = IOData.split(";")[0].trim();
							IOLabel = IOData.split(";")[1].trim();
							IOValue = IOData.split(";")[2].trim();
							IOLink = IOData.split(";")[3].trim();

							if (IOLabel.equalsIgnoreCase("password")) {
								IOValue = "*********";
							}

							if (IOValue.equalsIgnoreCase("#Desc")) {
								IOLabel = "";
								IOValue = "";
							}
							if (IOType.equalsIgnoreCase("input") && IOLabel != "") {
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='inputdetails'>");
								HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + IOLabel + "</td>");
								HTMLPageSubBlock.append("<td class='tdTail'>" + IOValue + "</td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");

								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='inputdetails'>");
								HTMLPageSubBlock.append("<tr><td></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
							}

							if (IOType.equalsIgnoreCase("output") && IOLabel != "") {
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr><td></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");

								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr ><td class='tdHead'>" + IOLabel + "</td>");
								HTMLPageSubBlock.append("<td class='tdTail'>" + IOValue + "</td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
							}

							if (IOType.equalsIgnoreCase("")) {
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr><td ></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr><td ></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
							}

							if (IOLabel == "" && IOLabel.isEmpty()) {
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr><td ></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
								HTMLPageSubBlock.append("<td class='checkDiv'><div>");
								HTMLPageSubBlock.append("<table class='outputdetails'>");
								HTMLPageSubBlock.append("<tr><td ></td></tr>");
								HTMLPageSubBlock.append("</table></div></td>");
							}

							IOCount = 1;
						}
					}
				}

				else {
					HTMLPageSubBlock.append("<td class='checkDiv'><div>");
					HTMLPageSubBlock.append("<table class='outputdetails'>");
					HTMLPageSubBlock.append("<tr><td></td></tr>");
					HTMLPageSubBlock.append("</table></div></td>");
					HTMLPageSubBlock.append("<td class='checkDiv'><div>");
					HTMLPageSubBlock.append("<table class='outputdetails'>");
					HTMLPageSubBlock.append("<tr><td></td></tr>");
					HTMLPageSubBlock.append("</table></div></td>");
				}
			}
			count = count + 1;
			CapturedPage = CapturedPage + "," + WorkflowTestPage;
		}

		HTMLPageSubBlock.append("</table>");
		return HTMLPageSubBlock;
	}

	public StringBuffer HTMLTestReport(String TestCaseName, String reportTimeStamp) {
		String imagePiePath = ChartsPath + "/ChartTests/" + TestCaseName + "/PieChart.png";
		String imageDonutPath = ChartsPath + "/ChartTests/" + TestCaseName + "/DonutChart.png";
		String imageBarPath = ChartsPath + "/ChartTests/" + TestCaseName + "/BarChart.png";

//		System.out.println("HTMLSingleTestReport Start");
		PassPercentageValue = GetTestExecutionPassPercentage();
//		System.out.println("PassPercentageValue :" + PassPercentageValue);
		HTMLPageSubBlock = new StringBuffer();
		HTMLPageSubBlock = HTMLTestStyle(PassPercentageValue); // Style;
		HTMLPageSubBlock = HTMLMainHeader();
		HTMLPageSubBlock = HTMLTestExecutionDetails(PassPercentageValue, TestCaseName); // Test Details
		HTMLPageSubBlock = HTMLPageBlock(); // Page Block
//		System.out.println("HTMLSingleTestReport End");
		HTMLPageSubBlock = PageOutputBlock(); // Done

		HTMLPageBuffer = new StringBuffer();
		HTMLPageBuffer = HTMLPageSubBlock;// EmailTestReport(TestCaseName); // Style
		HTMLPageBuffer = HTMLTestCharts(imagePiePath, imageDonutPath, imageBarPath);
//		HTMLPageBuffer = HTMLTestSnap(TestCaseName);
		HTMLPageBuffer = HTMLTestSnapWithSlider(TestCaseName, reportTimeStamp);
		HTMLPageBuffer = HTMLFooter();

//		 For Table Movement
		HTMLTestScriptBlock = new StringBuffer();
		HTMLTestScriptBlock = HTMLTestScriptAtBody();

		HTMLPageBuffer.append("</body>");
		HTMLPageBuffer = HTMLFooterScript();
		HTMLPageBuffer.append(HTMLTestScriptBlock);
		HTMLPageBuffer.append("</html>");

		return HTMLPageBuffer;
	}

	// **********************************************************************************************************************************************************************

	public StringBuffer EmailTestReport(String TestCaseName, String reportTimeStamp) {
		String imagePiePath = ChartsPath + "/ChartTests/" + TestCaseName + "/PieChart.png";
		String imageDonutPath = FileUtil.ChartsPath + "/ChartTests/" + TestCaseName + "/DonutChart.png";
		String imageBarPath = ChartsPath + "/ChartTests/" + TestCaseName + "/BarChart.png";

//		HTMLPageSubBlock = EmailSingleTestReport(TestCaseName);
//		Above function EmailSingleTestReport replaced with below block
		PassPercentageValue = GetTestExecutionPassPercentage();

		EmailBody = new StringBuffer();

//		Email Report Didn't require Style but considering single function block will add it.
//		HTMLPageSubBlock = HTMLTestStyle(PassPercentageValue); // ****************

		EmailBody = EmailMainHeader(PassPercentageValue);
		EmailBody = EmailTestExecutionDetails(PassPercentageValue, SourceTestCase); // Test Details
		EmailBody = EmailPageBlock(PassPercentageValue); // Page Block
		EmailBody = EmailPageOutputBlock(PassPercentageValue);

//		HTMLPageBuffer = new StringBuffer();
//		HTMLPageBuffer = HTMLPageSubBlock;// EmailTestReport(TestCaseName); // Style

//		HTMLPageBuffer = EmailTestCharts(imagePiePath, imageDonutPath, imageBarPath);
////	HTMLPageBuffer = HTMLTestSnap(TestCaseName); // Old Version
//		HTMLPageBuffer = HTMLTestSnapWithSlider(TestCaseName, reportTimeStamp);
//		HTMLPageBuffer = HTMLFooter();

//		For Table Movement
//		HTMLTestScriptBlock = new StringBuffer();
//		HTMLTestScriptBlock = HTMLTestScriptAtBody();

		EmailBody.append("<br></br></body>");
//		HTMLPageBuffer = HTMLFooterScript();
//		HTMLPageBuffer.append(HTMLTestScriptBlock);
		EmailBody.append("</html>");

//		System.out.println("Email Body Content : " + EmailBody.toString());

		return EmailBody;
	}

	public StringBuffer EmailPageOutputBlock(String PassPercentageValue) {

		String WSSheetName, WorkflowPageValue, ExecutionStatus, ExecutionBlockStatus, PageDescription, LoadInReport, ElementName, ActionTaken, TestCaseValue, Output, StartDate, EndDate, TestRemarks, IOType;
		String CapturedPage = "", ElementDescription = "";
		String outflag = "";
		int PageElementsCount, count, TestRowNum;
		count = 1;

		String HeadFoot = "", HeadRow = "";
		Double IntValue = Double.parseDouble(PassPercentageValue);
//		System.out.println(IntValue);
		if (IntValue < 100) {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12),black, rgb(223, 56, 14));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(146, 67, 49));";
		} else {
			HeadFoot = "background-image:linear-gradient(to bottom right, rgb(12, 12, 12), black, rgb(14, 223, 84));";
			HeadRow = "background-image:linear-gradient(to bottom right, rgb(0, 0, 0), rgb(36, 32, 32), rgb(49, 146, 81));";
		}
		String CellBG = "#F2F3F4;";
		HeadRow = "background-color: rgb(12, 11, 11);";
		HeadFoot = "background-color: rgb(12, 11, 11);";

		EmailBody.append("<br></br>");
		EmailBody.append("<a name='IOdetails'></a>");
		EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
//		EmailBody.append("<tr >");
//		EmailBody.append("<td colspan='4'><b>INPUT/OUTPUT DETAILS</b></td>");	
//		EmailBody.append("</tr>");
		EmailBody.append("<tr style='" + HeadRow + "color: white;height: 40px;font-size: 14px;text-align: left;'>");
		EmailBody.append("<td style='padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;'><b>MODULES<b></td>"); // CHECKPOINTS
		EmailBody.append("<td style='padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;'><b>PAGE<b></td>");
		EmailBody.append("<td style='padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;'><b>FEATURES<b></td>");
		EmailBody.append("<td style='padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;'><b>INPUT<b></td>"); // DETAILS
		EmailBody.append("<td style='padding-left: 14px;padding-top: 4px;height: auto;text-align: left;font: 14px sans-serif;width: auto;'><b>OUTPUT<b></td>"); // DETAILS
		EmailBody.append("</tr>");

		String strIO = "", SourceTestCase, CurrentTestCase, WorkflowTestPage = "", EleDesc = "";
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			Output = loadInReport.getValue().getOutput();
			PageElementsCount = loadInReport.getValue().getPageElementsCount();
			TestRowNum = loadInReport.getValue().getTestRowNum();
			ElementDescription = loadInReport.getValue().getElementDescription();
			TestCaseValue = loadInReport.getValue().getTestCaseValue();

			WorkflowTestPage = CurrentTestCase + "@" + WorkflowPageValue;

			if (CapturedPage.contains(WorkflowTestPage)) {
				if (Output != null) {
					strIO = strIO + "|" + Output;
				}
				if (ElementDescription != null && TestCaseValue != "" && !TestCaseValue.isEmpty()) {
					EleDesc = EleDesc + "|" + ElementDescription;
				}

			} else {
				count = 1;
				strIO = Output;
				if (TestCaseValue != "" && !TestCaseValue.isEmpty()) {
					EleDesc = ElementDescription;
				} else {
					EleDesc = "";
				}

				EmailBody.append("<tr>");
				EmailBody.append("<td style='vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + " width: 10%;'>" + WSSheetName + "</td>");
				EmailBody.append("<td style='vertical-align: top;padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 10%;'>" + WorkflowPageValue + "</td>"); // align='center'
			}

			String IOData, IOLabel, IOValue, StrOutput = "", StrInput = "", IOLink, EleDescLabel;
			int IOCount = 0, OCount = 0, ICount = 0;

			if (count == PageElementsCount || ExecutionStatus.equalsIgnoreCase("Fail")) {
				if (EleDesc.contains("|")) {
					EmailBody.append("<td style='vertical-align: top;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 25%;'><div>");
					EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
					boolean EleDescflag = false;
					for (int ed = 0; ed < EleDesc.split("\\|").length; ed++) {
						EleDescLabel = EleDesc.split("\\|")[ed];
						EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + EleDescLabel + "</td></tr>");
						EleDescflag = true;
					}
//					if (EleDescflag == false) {
//						EmailBody.append("<tr><td></td></tr>");
//					}
					EmailBody.append("</table></div></td>");
				} else {
					EmailBody.append("<td style='vertical-align: top;height: auto;text-align: left;font: 13px sans-serif;background-color: " + CellBG + "width: 25%;'><div>");
					EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
					EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + EleDesc + "</td></tr>");
					EmailBody.append("</table></div></td>");

				}

				Output = strIO;
				if (Output != null) {
					if (Output.contains("|")) {
						EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
						EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
						boolean InputFlag = false;
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								IOLabel = IOData.split(";")[1].trim();
								IOValue = IOData.split(";")[2].trim();
								IOLink = IOData.split(";")[3].trim();

								if (IOLabel.equalsIgnoreCase("password")) {
									IOValue = "*********";
								}
//								#Desc
								if (IOValue.equalsIgnoreCase("#Desc")) {
									IOLabel = "";
									IOValue = "";
								}

								if (IOType.equalsIgnoreCase("input") && IOLabel != "") {
									EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + IOLabel + "</td>");
									EmailBody.append("<td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;'>" + IOValue + "</td></tr>");
									InputFlag = true;
								}

							}
						}
						if (InputFlag == false) {
							EmailBody.append("<tr><td></td></tr>");
						}

						EmailBody.append("</table></div></td>");
					}

					if (Output.contains("|")) {
						EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
						EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
						boolean Outputflag = false;
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								IOLabel = IOData.split(";")[1].trim();
								IOValue = IOData.split(";")[2].trim();
								IOLink = IOData.split(";")[3].trim();

								if (IOValue.equalsIgnoreCase("#Desc")) {
									IOLabel = "";
									IOValue = "";
								}
								if (IOType.equalsIgnoreCase("output") && IOLabel != "") {
									EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + IOLabel + "</td>");
									EmailBody.append("<td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;'>" + IOValue + "</td></tr>");
									Outputflag = true;
								}

							}
						}
						if (Outputflag == false) {
							EmailBody.append("<tr><td></td></tr>");
						}

						EmailBody.append("</table></div></td>");
					} else {
						IOData = Output;
						if (IOData.contains(";")) {
							IOType = IOData.split(";")[0].trim();
							IOLabel = IOData.split(";")[1].trim();
							IOValue = IOData.split(";")[2].trim();
							IOLink = IOData.split(";")[3].trim();

							if (IOLabel.equalsIgnoreCase("password")) {
								IOValue = "*********";
							}

							if (IOValue.equalsIgnoreCase("#Desc")) {
								IOLabel = "";
								IOValue = "";
							}
							if (IOType.equalsIgnoreCase("input") && IOLabel != "") {
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + IOLabel + "</td>");
								EmailBody.append("<td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;'>" + IOValue + "</td></tr>");
								EmailBody.append("</table></div></td>");

								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td></td></tr>");
								EmailBody.append("</table></div></td>");
							}

							if (IOType.equalsIgnoreCase("output") && IOLabel != "") {
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td></td></tr>");
								EmailBody.append("</table></div></td>");

								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr ><td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 30%;'>" + IOLabel + "</td>");
								EmailBody.append("<td style='padding-left: 14px;padding-top: 10px;height: auto;text-align: left;font: 13px sans-serif;width: 70%;'>" + IOValue + "</td></tr>");
								EmailBody.append("</table></div></td>");
							}

							if (IOType.equalsIgnoreCase("")) {
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td ></td></tr>");
								EmailBody.append("</table></div></td>");
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td ></td></tr>");
								EmailBody.append("</table></div></td>");
							}

							if (IOLabel == "" && IOLabel.isEmpty()) {
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td ></td></tr>");
								EmailBody.append("</table></div></td>");
								EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
								EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
								EmailBody.append("<tr><td ></td></tr>");
								EmailBody.append("</table></div></td>");
							}

							IOCount = 1;
						}
					}
				}

				else {
					EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
					EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
					EmailBody.append("<tr><td></td></tr>");
					EmailBody.append("</table></div></td>");
					EmailBody.append("<td style='vertical-align: top;background-color: " + CellBG + "width: 35%;'><div>");
					EmailBody.append("<table style='box-sizing: border-box;width: 100%;text-align: left;font: 13px sans-serif;border-collapse: collapse;'>");
					EmailBody.append("<tr><td></td></tr>");
					EmailBody.append("</table></div></td>");
				}
			}
			count = count + 1;
			CapturedPage = CapturedPage + "," + WorkflowTestPage;
		}

		EmailBody.append("</table>");
		return EmailBody;
	}

	synchronized public String getExecutionTime(String strDate, String endDate) throws ParseException {
		String dateStart = strDate;
		String dateStop = endDate;
		String TotalTime = "";
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		long diff, diffSeconds = 0, diffMinutes = 0, diffHours = 0, diffDays;
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(dateStart);
			d2 = format.parse(dateStop);
			diff = d2.getTime() - d1.getTime();
			diffSeconds = diff / 1000 % 60;
			diffMinutes = diff / (60 * 1000) % 60;
			diffHours = diff / (60 * 60 * 1000) % 24;
			diffDays = diff / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (diffHours != 0) { // !min.isEmpty() && min != "0"
			TotalTime = TotalTime + diffHours + " Hours, ";
		}

		if (diffMinutes != 0) { // !min.isEmpty() && min != "0"
			TotalTime = TotalTime + diffMinutes + " Minutes, ";
		}

		if (diffSeconds != 0) { // !sec.isEmpty() && sec != "0"
			TotalTime = TotalTime + diffSeconds + " Seconds.";
		} else {
//			TotalTime = "2 Seconds.";
			TotalTime = TotalTime + "2 Seconds.";
		}
		return TotalTime;
	}

	
	public String GetTestExecutionPassPercentage() {

//		double TotalWorkflowPages = excelUtil.GetAllTestWorkflow().size();
		double PassCount = 0, FailCount = 0, PassPercentage;
		String SheetName, ExecutionStatus, PageName, TestCaseName = "", SourceTestCase = null, CurrentTestCase = null;
		String Passvalue = "0", ElementDescription = "";
		TestStatus.clear();

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
//			TestCaseName = loadInReport.getValue().getTestCase();
			SourceTestCase = loadInReport.getValue().getSourceTestCase();
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			SheetName = loadInReport.getValue().getSheetName();
			PageName = loadInReport.getValue().getPageName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			ElementDescription = loadInReport.getValue().getElementDescription();

//			System.out.println(PageName + " => " + ExecutionStatus);
			if (PageName != null) {
				if (!PageName.isEmpty() && PageName != "") {
					TestStatus.put(CurrentTestCase + "#" + PageName, ExecutionStatus);
				}
			}
		}
		WorkflowPageArray.clear();
		double TotalWorkflowPages = getTotalWorkflowPages(SourceTestCase, WorkflowPageArray); // loadInReports.size();
		for (Map.Entry<String, String> status : TestStatus.entrySet()) {
			status.getKey();

			if (status.getValue().equalsIgnoreCase("Pass")) {
				PassCount++;
			} else if (status.getValue().equalsIgnoreCase("Fail") || status.getValue().equalsIgnoreCase("NA")) {
				FailCount++;
			}
		}

		if (TotalWorkflowPages != 0) {
			PassPercentage = (PassCount / TotalWorkflowPages) * 100;
			DecimalFormat df2 = new DecimalFormat("#.##");
			df2.setRoundingMode(RoundingMode.UP);
			Passvalue = String.valueOf(df2.format(PassPercentage));
		}

//		System.out.println("Passvalue " + Passvalue);
		return Passvalue;

	}

	public String GetTestExecutionFailPercentage() {

		double PassCount = 0, FailCount = 0, PassPercentage, FailPercentage;
		String SheetName, ExecutionStatus, PageName;
		TestStatus.clear();
		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			SheetName = loadInReport.getValue().getSheetName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageName = loadInReport.getValue().getPageName();
			TestStatus.put(PageName, ExecutionStatus);
		}

		double TotalWorkflowPages = loadInReports.size();
		for (Map.Entry<String, String> status : TestStatus.entrySet()) {
			status.getKey();

			if (status.getValue().equalsIgnoreCase("Pass")) {
				PassCount++;
			} else if (status.getValue().equalsIgnoreCase("Fail") || status.getValue().equalsIgnoreCase("NA")) {
				FailCount++;
			}
		}
		FailPercentage = (FailCount / TotalWorkflowPages) * 100;
		return String.valueOf(FailPercentage);
	}

	public double GetTestExecutionFailCounter() {

		double PassCount = 0, FailCount = 0, PassPercentage, FailPercentage;
		String SheetName, ExecutionStatus, PageName;

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			SheetName = loadInReport.getValue().getSheetName();
			ExecutionStatus = loadInReport.getValue().getExecutionStatus();
			PageName = loadInReport.getValue().getPageName();
			TestStatus.put(PageName, ExecutionStatus);
		}

		double TotalWorkflowPages = loadInReports.size();
		for (Map.Entry<String, String> status : TestStatus.entrySet()) {
			status.getKey();

			if (status.getValue().equalsIgnoreCase("Pass")) {
				PassCount++;
			} else if (status.getValue().equalsIgnoreCase("Fail") || status.getValue().equalsIgnoreCase("NA")) {
				FailCount++;
			}
		}
		FailPercentage = (FailCount / TotalWorkflowPages) * 100;
		return FailCount;
	}

	public void GenerateCharts(String TestCaseName) {

		String FilePath = ChartsPath + "/ChartFiles/" + TestCaseName + ".txt"; // excelUtil.ChartText;

		File file = new File(FilePath);

		if (file.exists()) {
			java.io.FileWriter myWriter;
			try {
				myWriter = new java.io.FileWriter(FilePath);
				myWriter.write(PassPercentageValue); // GetTestExecutionPassPercentage()
//				myWriter.write("15");
				myWriter.close();
//				System.out.println("Successfully wrote to the file.");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {

			try {
				file.createNewFile();
				java.io.FileWriter myWriter = new java.io.FileWriter(FilePath);
				myWriter.write(PassPercentageValue); // GetTestExecutionPassPercentage()
//				myWriter.write("15");
				myWriter.close();
//				System.out.println("Successfully wrote to the file.");

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Thread.sleep(5000);
			Runtime.getRuntime().exec(new String[] { "wscript.exe", excelUtil.ChartRunner });
			Thread.sleep(5000);
//			System.out.println("Image generated 2");
		} catch (Exception e) {
			System.out.println(e);
			System.exit(0);
		}

	}

	public synchronized String GetExecutionStatus(String ProvSheetName, String ProvPageName, String ProvCurrentTestCase) {
		String ExecutionStatus = "Pass";
		String WSSheetName, WorkflowPageValue, ElementName, CurrentTestCase;

		for (Map.Entry<String, LoadInReports> loadInReport : loadInReports.entrySet()) {
			CurrentTestCase = loadInReport.getValue().getCurrentTestCase();
			WSSheetName = loadInReport.getValue().getSheetName();
			WorkflowPageValue = loadInReport.getValue().getPageName();
			ElementName = loadInReport.getValue().getElementName();
			if (ProvSheetName.equalsIgnoreCase(WSSheetName) && ProvPageName.equalsIgnoreCase(WorkflowPageValue) && ProvCurrentTestCase.equalsIgnoreCase(CurrentTestCase)) {
				ExecutionStatus = loadInReport.getValue().getExecutionStatus();
				if (ExecutionStatus.equalsIgnoreCase("fail")) {
					break;
				}
			}
		}

		return ExecutionStatus;
	}

	public StringBuffer HTMLTestStyle() {
		HTMLSuitBuffer.append("<html lang='en'>");
		HTMLSuitBuffer.append("<head>");
		HTMLSuitBuffer.append("<title>Automation Execution Report</title>");
		HTMLSuitBuffer.append("</head>");
		HTMLSuitBuffer.append("<style>");
		HTMLSuitBuffer.append("#ChartTable{border: 2px solid white;width: 100%;border-collapse: collapse;}");
		HTMLSuitBuffer.append("#PieTd {color: #F2F3F4;border: 1px solid white;width: 30%;}");
		HTMLSuitBuffer.append("#SnapTd {color: black;border: 1px solid white}"); // #F2F3F4
		HTMLSuitBuffer.append("td{color: black; border : 1px solid white;}");
		HTMLSuitBuffer.append("#DynamicPage{text-align: center;border-collapse: collapse;}");
		HTMLSuitBuffer.append("#MainTable {border : 2px solid white;width: 100%;border-collapse: collapse;}");
		HTMLSuitBuffer.append("#SubTable{border : 1px solid white;width: 100%;text-align: left;vertical-align: middle;border-collapse: collapse;}");
		HTMLSuitBuffer.append("#ReportHeader{border : 1px solid white; background-color : #85C1E9;text-align: center; border-collapse: collapse;}");
		HTMLSuitBuffer.append("span{text-align: left;}");
//		HTMLSuitBuffer.append(".bordered, .n-bordered{ border: 1px solid black; border-collapse: collapse;}");
//		HTMLSuitBuffer.append(".n-bordered{border: none;}");
//		HTMLSuitBuffer.append(".bordered td, .n-bordered td{border: 1px solid black;}");
		HTMLSuitBuffer.append(".n-bordered tr:first-child td{border-top: none;}");
		HTMLSuitBuffer.append(".n-bordered tr:last-child td{border-bottom: none;}");
		HTMLSuitBuffer.append(".n-bordered tr td:first-child{border-left: none;}");
		HTMLSuitBuffer.append(".n-bordered tr td:last-child{border-right: none;}");
		HTMLSuitBuffer.append("</style>");
//		if (GetTestExecutionFailCounter() > 0) { // Fail
//			HTMLSuitBuffer.append("<body bgcolor='#FADBD8'>"); // bgcolor="#D5F5E3" #FADBD8
//		} else { // Pass
//			HTMLSuitBuffer.append("<body bgcolor='#D5F5E3'>"); // bgcolor="#D5F5E3" #FADBD8
//		}
		return HTMLSuitBuffer;
	}

	public StringBuffer HTMLTestSuitHeadBlock() {
		String hostName = null;
		try {
			TestTotalTime = getExecutionTime(strExecutionDate, endExecutionDate);
			java.net.InetAddress addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (Exception e) {
			System.out.println(strDate + "  " + endDate);
			e.printStackTrace();
		}

		int passcount = TestSuitPassCount();
		int failcount = testSuits.size() - TestSuitPassCount();

		HTMLSuitBuffer.append("<table id='MainTable'>");
		HTMLSuitBuffer.append("<tr id='ReportHeader'>");
		HTMLSuitBuffer.append("<td colspan='6' ><b>" + ApplicationName.toUpperCase() + " - EXECUTION DETAILS<b></td>");
		HTMLSuitBuffer.append("</tr>");

		HTMLSuitBuffer.append("<tr bgcolor='#F2F3F4' >");
		HTMLSuitBuffer.append("<td width='15%'>&nbsp; Application Name  </td>");
		HTMLSuitBuffer.append("<td ><b id='workspace'>" + ApplicationName + "</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; System Informaion </td>");
		HTMLSuitBuffer.append("<td ><b>" + hostName + "</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; Total Execution Time</td>");
		HTMLSuitBuffer.append("<td ><b>" + TestStartTime + "</b></td></tr>");

		HTMLSuitBuffer.append("<tr bgcolor='#F2F3F4'>");
		HTMLSuitBuffer.append("<td >&nbsp; Test Designer </td>");
		HTMLSuitBuffer.append("<td ><b>Saurabh Sawalapurkar</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; Test Executor</td>");
		HTMLSuitBuffer.append("<td ><b>Saurabh Sawalapurkar</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; Total Execution Time </td>");
		HTMLSuitBuffer.append("<td ><b>" + TestTotalTime + "</b></td></tr>");

		HTMLSuitBuffer.append("<tr bgcolor='#F2F3F4' >");
		HTMLSuitBuffer.append("<td >&nbsp; Total Test Count </td>");
		HTMLSuitBuffer.append("<td ><b>" + testSuits.size() + "</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; Pass Test Count </td>");
		HTMLSuitBuffer.append("<td ><b>" + passcount + "</b></td>");
		HTMLSuitBuffer.append("<td >&nbsp; Fail Test Count </td>");
		HTMLSuitBuffer.append("<td ><b>" + failcount + "</b></td>");
		HTMLSuitBuffer.append("<tr bgcolor='#F2F3F4'></tr>");

		HTMLSuitBuffer.append("</table>");
		HTMLSuitBuffer.append("<br></br>");

		return HTMLSuitBuffer;
	}

	public StringBuffer HTMLTestBlock() {
		String hostName = null;
		try {
			TestTotalTime = getExecutionTime(strDate, endDate);
			java.net.InetAddress addr = InetAddress.getLocalHost();
			hostName = addr.getHostName();
		} catch (Exception e) {
			System.out.println(strDate + "  " + endDate);
			e.printStackTrace();
		}
		HTMLSuitBuffer.append("<table id='MainTable'>");
		HTMLSuitBuffer.append("<tr id='ReportHeader'>");
		HTMLSuitBuffer.append("<td width='15%'><b>TESTCASENAME<b></td>");
		HTMLSuitBuffer.append("<td width='25%'><b>TEST DESCRIPTION<b></td>");
		HTMLSuitBuffer.append("<td width='7%'><b>STATUS<b></td>");
		HTMLSuitBuffer.append("<td width='25%'><b>OUTPUT DETAILS<b></td>");
		HTMLSuitBuffer.append("<td width='15%'><b>TOTAL EXECUTION TIME<b></td>");
		HTMLSuitBuffer.append("<td width='35%'><b>REMARKS<b></td>");
		HTMLSuitBuffer.append("</tr>");

		String TestCaseName, TestDescription, TestExecutionStatus, TestOutputDetails, TestExecutionTime, TestRemarks, LogPath, Output, TestKey;
		Map<String, ExcelReport> testAllDetails;
		String IOType;
		String IOData, IOLabel, IOValue, StrOutput = "", StrInput = "", IOLink;
		int IOCount = 0, OCount = 0, ICount = 0;

		for (Entry<String, testSuit> testSuit : testSuits.entrySet()) {
			TestKey = testSuit.getKey();
			TestCaseName = testSuit.getValue().getTestCaseName();
			TestDescription = testSuit.getValue().getTestDescription();
			TestExecutionStatus = testSuit.getValue().getTestExecutionStatus();
			TestOutputDetails = testSuit.getValue().getTestOutputDetails();
			TestExecutionTime = testSuit.getValue().getTestExecutionTime();
			TestRemarks = testSuit.getValue().getTestRemarks();
			testAllDetails = testSuit.getValue().getTestIODetails();
			LogPath = testSuit.getValue().getTestCaseLogPath();
			StrOutput = "";

//			System.out.println("TestKey : " + TestKey);
			for (Entry<String, ExcelReport> excelReport : testAllDetails.entrySet()) {
				Output = excelReport.getValue().getOutput();
//					PageRemark = excelReport.getValue().getTestRemarks();
				if (Output != null) {
					if (Output.contains("|")) {
						for (int io = 0; io < Output.split("\\|").length; io++) {
							IOData = Output.split("\\|")[io];
							if (IOData.contains(";")) {
								IOType = IOData.split(";")[0].trim();
								IOLabel = IOData.split(";")[1].trim();
								IOValue = IOData.split(";")[2].trim();
								IOLink = IOData.split(";")[3].trim();
								if (IOType.equalsIgnoreCase("output")) {
									if (!StrOutput.isEmpty() && StrOutput != "") {
										StrOutput = StrOutput + "\r\n" + IOLabel + " = " + IOValue;
									} else {
										StrOutput = IOLabel + " = " + IOValue;
									}
									OCount = OCount + 1;
								} else if (IOType.equalsIgnoreCase("input")) {
									if (!StrInput.isEmpty() && StrInput != "") {
										StrInput = StrInput + "\r\n" + IOLabel + " = " + IOValue;
									} else {
										StrInput = IOLabel + " = " + IOValue;
									}
									ICount = ICount + 1;
								}
							}
						}
						if (OCount >= ICount) {
							IOCount = OCount;
						} else {
							IOCount = ICount;
						}

					} else {
						IOData = Output;
						if (IOData.contains(";")) {
							IOType = IOData.split(";")[0].trim();
							IOLabel = IOData.split(";")[1].trim();
							IOValue = IOData.split(";")[2].trim();
							IOLink = IOData.split(";")[3].trim();
							if (IOType.equalsIgnoreCase("output")) {
								StrOutput = IOLabel + " = " + IOValue;
							} else if (IOType.equalsIgnoreCase("input")) {
								StrInput = IOLabel + " = " + IOValue;
							}
							IOCount = 1;
						}
					}
				}

			}

			if (TestRemarks == null) {
				TestRemarks = "";
			}

			if (TestExecutionStatus.equalsIgnoreCase("fail")) {
				HTMLSuitBuffer.append("<tr bgcolor='#FADBD8'>");
				HTMLSuitBuffer.append("<td width='15%' align='center'><b>" + TestCaseName + "<b></td>");
				HTMLSuitBuffer.append("<td width='25%' align='center'><b>" + TestDescription + "<b></td>");
				HTMLSuitBuffer.append("<td width='10%' align='center'><b>" + TestExecutionStatus + "<b></td>");
				HTMLSuitBuffer.append("<td width='20%'><b>" + StrOutput + "<b></td>");
				HTMLSuitBuffer.append("<td width='15%' align='center'><b>" + TestExecutionTime + "<b></td>");
				HTMLSuitBuffer.append("<td width='35%'><b>" + TestRemarks + "<b></td>");
				HTMLSuitBuffer.append("</tr>");
			} else {
				HTMLSuitBuffer.append("<tr bgcolor='#82FA58'>");
				HTMLSuitBuffer.append("<td width='15%' align='center'><b>" + TestCaseName + "<b></td>");
				HTMLSuitBuffer.append("<td width='25%' align='center'><b>" + TestDescription + "<b></td>");
				HTMLSuitBuffer.append("<td width='10%' align='center'><b>" + TestExecutionStatus + "<b></td>");
				HTMLSuitBuffer.append("<td width='20%'><b>" + StrOutput + "<b></td>");
				HTMLSuitBuffer.append("<td width='15%' align='center'><b>" + TestExecutionTime + "<b></td>");
				HTMLSuitBuffer.append("<td width='35%'><b>" + TestRemarks + "<b></td>");
				HTMLSuitBuffer.append("</tr>");

			}

		}

		HTMLSuitBuffer.append("</table>");
		HTMLSuitBuffer.append("<br></br>");

		return HTMLSuitBuffer;
	}

	public int TestSuitPassCount() {
		int passcount = 0;
		String TestExecutionStatus;
		for (Entry<String, testSuit> testSuit : testSuits.entrySet()) {
			TestExecutionStatus = testSuit.getValue().getTestExecutionStatus();
			if (TestExecutionStatus.equalsIgnoreCase("pass")) {
				passcount = passcount + 1;
			}

		}

		return passcount;
	}

}
