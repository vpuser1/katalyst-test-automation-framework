//package testEngine;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Collections;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.concurrent.TimeUnit;
//
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
//import org.openqa.selenium.NoSuchSessionException;
//import org.openqa.selenium.PageLoadStrategy;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxOptions;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.testng.Assert;
//import org.testng.asserts.SoftAssert;
//
//import com.google.common.base.Verify;
//
//import io.appium.java_client.MobileElement;
//import io.appium.java_client.android.AndroidDriver;
//import io.appium.java_client.remote.MobileCapabilityType;
//import testDrivers.ConfigUtil;
//import testDrivers.SecureUtil;
//
//@SuppressWarnings("unused")
//public class BaseMachineClass extends FileUtil {
//
//	public WebDriver driver;
//
//	public ConfigUtil configUtil;
//	public ExcelUtil excelUtil;// = new ExcelUtil(TestCaseName);
//	public EmailReportUtil emailReportUtil;
//	public ExcelReportUtil excelReportUtil;
//	public Date TestStartDate, TestEndDate, TestExecutionStartDate, TestExecutionEndDate; // removed static 10 March
//																							// 2020
//	public String strDate, endDate, PagestrDate, PageendDate; // removed static 10 March 2020
//	public static String strExecutionDate, endExecutionDate;
//	public String ExecutionStatus;
//	public Map<String, LoadInReports> loadInReports = new LinkedHashMap<String, LoadInReports>(); // removed static
//	public Map<String, ExcelReport> excelReports = new LinkedHashMap<String, ExcelReport>(); // removed static
//	public Map<String, String> testElement = new LinkedHashMap<String, String>(); // removed static
//	public Map<String, InputOutputUtil> IOMap = new LinkedHashMap<String, InputOutputUtil>(); // removed static
//	public static Map<String, testSuit> testSuits = new LinkedHashMap<String, testSuit>();
//	public Map<String, String> TestDataType = new LinkedHashMap<String, String>();
//	SecureUtil secure = new SecureUtil();
//	String ImportOSDFlag = "";
//
//	XSSFWorkbook ExcelWorkbook;
//	XSSFSheet ExcelSheet;
//	String AccessDataFrom = "";
//	String SourceTestCase, CurrentTestCase;
//
//	public String reportTimeStamp;
//	SoftAssert softAssertion = new SoftAssert();
//	public static String ApplicationName;
//	String BrowserName;
//
//	// Initial Setup
//	public void InitalSetup(String TestCaseName, String Browser) {
//		System.out.println("Called InitialSetup");
//
//		secure.SecureConfiguration();
////		String SourceTestCase, CurrentTestCase;
//		FollowedTestsAccessDataFrom(TestCaseName);
//		SourceTestCase = TestCaseName;
//		CurrentTestCase = getFirstTestCase();
//
//		if (getAccessDataFrom().equalsIgnoreCase("source")) {
//			CurrentTestCase = SourceTestCase;
//		}
//
////		excelUtil = new ExcelUtil(SourceTestCase, CurrentTestCase);
//
//		TestStartDate = new Date();
//		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
//		strDate = formatter.format(TestStartDate);
//
//		excelUtil.FollowedTestsRecursions(SourceTestCase);
//		Collections.reverse(excelUtil.testArray);
//		System.out.println("Test Execution Order : " + excelUtil.testArray + " - " + excelUtil.testArray.size());
//
//		excelUtil.SetGlobalObjects();
//		excelUtil.SetTestWorkflow(); // Adjusted based on source Test case
////		excelUtil.SetGlobalParameters();
//		excelUtil.SetGlobalParametersT();
//		excelUtil.GetEnvironmentDetails();
//		ValidateDuplicatedPagesInSourceFlow(SourceTestCase, getAccessDataFrom(), "initial");
//
////		reportTimeStamp = excelUtil.GetTimeStamp();
//
////		String BrowserName;
//		if (Browser == null) {
//			BrowserName = excelUtil.GetParameterValue(SourceTestCase, "BrowserName"); // CurrentTestCase SourceTestCase
//			System.out.println("Browser : " + BrowserName);
//		} else {
//			BrowserName = Browser;
//			System.out.println("Browser : " + BrowserName);
//		}
//
////		configUtil = new ConfigUtil(driver, excelUtil, loadInReports, SourceTestCase, CurrentTestCase, reportTimeStamp,
////				testElement, BrowserName); 
//
//		String URL;
//		ImportOSDFlag = excelUtil.GetParameterValue(CurrentTestCase, "ImportOSDFlag");
//		if (ImportOSDFlag.equalsIgnoreCase("yes")) {
//			URL = "https://apposcuat1.avaya.com/OSC/home/DirectLogin";
//		} else {
//			URL = excelUtil.GetParameterValue(CurrentTestCase, "URL");
//		}
//		driver = launchURL(BrowserName, URL);
//		System.out.println("[" + URL + "] URL Launched in Browser [" + BrowserName + "].");
//		System.out.println(driver.getWindowHandle());
//
//		int DriverLen = driver.getWindowHandle().length();
//		String BrowserDriverHandle = driver.getWindowHandle().substring(DriverLen - 10, DriverLen);
//		System.out.println(BrowserDriverHandle);
//		reportTimeStamp = BrowserDriverHandle;
//
//		ApplicationName = excelUtil.GetParameterValue(CurrentTestCase, "ApplicationName");
//
//		configUtil = new ConfigUtil(driver, excelUtil, loadInReports, SourceTestCase, CurrentTestCase, reportTimeStamp, testElement, BrowserName);
////		configUtil.DeleteSnapshots();
////		configUtil.DeleteFolderContents(SnapshotsPath);
////		configUtil.DeleteFolderFiles(home + "/Downloads/");
//		configUtil.CreateSnapsFolder(SourceTestCase);
////		**********************ADD New Object Here*********************************
//		configUtil.PagesConfigSetup(driver, excelUtil, loadInReports, SourceTestCase, CurrentTestCase, reportTimeStamp, testElement, BrowserName);
//
//	}
//
//	// Final Setup
//	public void FinalSetup(String TestCaseName, String Browser) {
//		System.out.println("Called FinalSetup");
//		String SourceTestCase, CurrentTestCase;
//		SourceTestCase = TestCaseName;
//		CurrentTestCase = SourceTestCase;
//
//		String BrowserName;
//		if (Browser == null) {
//			BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
//			System.out.println("Browser : " + BrowserName);
//		} else {
//			BrowserName = Browser;
//			System.out.println("Browser : " + BrowserName);
//		}
//
//		ValidateDuplicatedPagesInSourceFlow(SourceTestCase, getAccessDataFrom(), "final");
//
//		TestEndDate = new Date();
//		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
//		endDate = formatter.format(TestEndDate);
//
////		excelReportUtil = new ExcelReportUtil(excelUtil, loadInReports, TestStartDate, TestEndDate, reportTimeStamp, BrowserName, configUtil);
////		emailReportUtil = new EmailReportUtil(excelUtil, loadInReports, TestStartDate, TestEndDate, configUtil, BrowserName);
//
////		Removed HTMLSingleTestReport and added all content to HTMLTestReport		
////		String HTMLMainContent = emailReportUtil.HTMLSingleTestReport(SourceTestCase).toString();  
//		String HTMLContent = emailReportUtil.HTMLTestReport(SourceTestCase, reportTimeStamp).toString();
//		String EmailContent = emailReportUtil.EmailTestReport(SourceTestCase, reportTimeStamp).toString();
//
////		emailReportUtil.GenerateCharts(SourceTestCase);
//
////		configUtil.FileWriter(SourceTestCase, HTMLContent);  ''Comment on 18 Jan 2021
//		String TestCaseDescription = excelUtil.GetParameterValue(CurrentTestCase, "TestDescription");
//		String TestDesigner = excelUtil.GetParameterValue(CurrentTestCase, "TestDesigner");
//		String TestExecutor = excelUtil.GetParameterValue(CurrentTestCase, "TestExecutor");
//
//		String TotalExecutionTime = null;
//		try {
//			TotalExecutionTime = emailReportUtil.getExecutionTime(strDate, endDate);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		String PassPercent = emailReportUtil.GetTestExecutionPassPercentage();
//		configUtil.AddTestsDetailsToLog(ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, PassPercent, BrowserName);
//
//		String UserName = null, Account = null, AppVersion = null;
//
//		UserName = configUtil.getOutput("UserName");
//		Account = configUtil.getOutput("Account");
//		AppVersion = configUtil.getOutput("AppVersion");
//
////		, UserName, Account, AppVersion
//		configUtil.AddGeneralInformationToLog(SourceTestCase);
//		configUtil.AddModelInformationToLog(SourceTestCase);
//		excelReportUtil.ExcelPageContents();
////		excelReportUtil.ExcelReportCreator(ApplicationName, SourceTestCase, TestStartDate.toString(), TestEndDate.toString(), TotalExecutionTime, TestCaseDescription, TestDesigner, TestExecutor, PassPercent, configUtil);
//
//		String ExecutorTestCaseName, ExecutorTestExecutionTime, ExecutorTestRemarks = "-", ExecutorStrOutput = "-", ExecutorTestExecutionStatus = "-", ExecutorWExecutionStartTime, ExecutorLogTestPath = "-", ExecutorReportFilePath = "-",
//				ExecutorInput = "-", ExecutorExeQuoteStatus = "-";
//		ExecutorTestCaseName = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "TestCaseName");
//		ExecutorTestExecutionTime = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "TestExecutionTime");
//		ExecutorTestRemarks = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "TestRemarks");
//		if (ExecutorTestRemarks.contains("|")) {
//			ExecutorTestRemarks = ExecutorTestRemarks.replaceAll("\\|", "#");
//		}
//
//		String stroutput = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput");
//		if (stroutput.contains("\n")) {
//			ExecutorStrOutput = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput").replaceAll("\n", " ");
//		} else {
//			ExecutorStrOutput = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "StrOutput");
//		}
//		ExecutorTestExecutionStatus = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "TestExecutionStatus");
//		ExecutorWExecutionStartTime = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "ExecutionStartTime");
//		ExecutorLogTestPath = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "LogTestPath");
//		ExecutorReportFilePath = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "ReportFile");
//		ExecutorInput = configUtil.ExecutorUpdates.get(reportTimeStamp + "#" + "OSDQRN");
//
//		configUtil.UpdatedExecutorDetails();
//
//		try {
//			configUtil.writeExecutorDetails(ExecutorTestCaseName + "|" + ExecutorTestRemarks + "|" + ExecutorStrOutput + "|" + ExecutorTestExecutionStatus + "|" + ExecutorLogTestPath + "|" + ExecutorReportFilePath + "|" + ExecutorWExecutionStartTime
//					+ "|" + TestEndDate.toString() + "|" + ExecutorExeQuoteStatus + "|" + ExecutorInput + "|" + ExecutorTestExecutionTime);
//		} catch (IOException e) {
//			System.out.println("Unable To Write Content To Source File...");
//			e.printStackTrace();
//		}
//
////		emailReportUtil.SendEmail(SourceTestCase, reportTimeStamp);
//		configUtil.DeleteChartsTestFolder(SourceTestCase);
//
//		CloseWebDriver();
//
//	}
//
//	public void ModelExecution(String TestCaseName, String Browser) {
//		System.out.println("Called Modular Execution...");
//		String WorkflowTestPageValue = null, WorkflowTestPageName;
//		String ElementName = null, PageName = null, ObjectType, ObjectIdentification, ActionTaken = null, OnFail, Snapshot, TestCaseValue = null, MarkObject, LoadInReport = null, Wait, PageDescription = null, TestRemarks = null, TestCase = null,
//				IOType = null, PageCurrentTestCase, PageSourceTestCase, ElementDescription = "";
//		String WSSheetName = null, Output = null, CurrentTestCase = null, SourceTestCase;
//		String LiveActionPreview = "";
//		int TestRowNum = 0;
//		int AddRecordIndex = 1;
//		LiveActionPreview = excelUtil.getEnvironmentValue("LiveActionPreview");
//
//		for (Entry<String, TestWorkflowsUtil> TestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
//			WorkflowTestPageName = TestWorkflow.getKey().toString();
//			WorkflowTestPageValue = TestWorkflow.getValue().getPageValue().toString();
//			TestRowNum = TestWorkflow.getValue().getTestRowNum();
//			CurrentTestCase = WorkflowTestPageName.split("#")[0];
//			SourceTestCase = TestCaseName;
//			if (getAccessDataFrom().equalsIgnoreCase("source")) {
//				CurrentTestCase = SourceTestCase;
//			}
//
//			String BrowserName;
//			if (Browser == null) {
//				BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
//				System.out.println("Browser : " + BrowserName);
//			} else {
//				BrowserName = Browser;
//				System.out.println("Browser : " + BrowserName);
//			}
//
//			Date StartDate = new Date();
//			SimpleDateFormat formatter1 = new SimpleDateFormat("HH:mm:ss");
//			PagestrDate = formatter1.format(StartDate);
//
//			WSSheetName = excelUtil.GetSheetNameForPage(WorkflowTestPageValue);
//
//			// Set Page Details for all available Pages present in respective Test case
//			// workflow.
//			excelUtil.SetPageDetails(WSSheetName, WorkflowTestPageValue, SourceTestCase, CurrentTestCase);
//
//			By retElement;
//			String retOutput;
//			long timeOut;
//			int ElementCount;
//			ElementCount = excelUtil.GetPage().size();
//
//			String LogPageDescription = excelUtil.GetTestPageDescription();
//
//			if (!LogPageDescription.isEmpty() && LogPageDescription != "") {
//				configUtil.AddPageTextToLog(SourceTestCase, WSSheetName + " => " + WorkflowTestPageValue + " =>  " + LogPageDescription, AddRecordIndex, BrowserName);
//			} else {
//				configUtil.AddPageTextToLog(SourceTestCase, WSSheetName + " => " + WorkflowTestPageValue, AddRecordIndex, BrowserName);
//			}
//
//			for (Map.Entry<String, PageUtil> page : excelUtil.GetPage().entrySet()) {
////				ElementName = page.getKey();
//				ElementName = page.getValue().getElementName();
//				PageSourceTestCase = page.getValue().getSourceTestCase();
//				PageCurrentTestCase = page.getValue().getCurrentTestCase();
////				TestCase = page.getValue().getTestCase();
//				PageName = page.getValue().getPageName().toString();
//				ObjectType = page.getValue().getObjectType().toString();
//				ObjectIdentification = page.getValue().getObjectIdentification().toString();
//				ActionTaken = page.getValue().getActionTaken().toString();
//				OnFail = page.getValue().getOnFail().toString();
//				MarkObject = page.getValue().getMarkObject().toString();
//				LoadInReport = page.getValue().getLoadInReport().toString();
//				Snapshot = page.getValue().getSnapshot().toString();
//				Wait = page.getValue().getWait().toString();
//				TestCaseValue = page.getValue().getTestCaseValue().toString();
//				PageDescription = page.getValue().getPageDescription().toString();
//				ElementDescription = page.getValue().getElementDescription().toString();
//
//				// Not sure whether to use source or current test case here, trial needs to do
//				testElement.put(PageCurrentTestCase, ElementName);
//				ExecutionStatus = "Fail";
//
//				if (!ElementDescription.isEmpty() && ElementDescription != "" && !TestCaseValue.isEmpty() && TestCaseValue != "") {
//					configUtil.AddTextToLog(SourceTestCase, "INFO : " + ElementDescription);
//					if (LiveActionPreview.equalsIgnoreCase("yes")) {
//						configUtil.HighlightTestStepsDetails(SourceTestCase + " : " + ElementDescription);
//					}
//				}
//
//				loadInReports.put(PageCurrentTestCase + "#" + ElementName, new LoadInReports(PageSourceTestCase, PageCurrentTestCase, WSSheetName, WorkflowTestPageValue, ExecutionStatus, PageDescription, LoadInReport, ElementName, ActionTaken,
//						TestCaseValue, Output, PagestrDate, PageendDate, TestRemarks, ElementCount, OnFail, IOType, TestRowNum, ElementDescription));
//
//				timeOut = configUtil.DynamicWait(Wait);
//
////				System.out.println("ActionTaken " + ActionTaken);
//
//				if (ObjectType.equalsIgnoreCase("function")) {
//					retOutput = configUtil.getFunction(PageCurrentTestCase, ActionTaken, ObjectIdentification, TestCaseValue, timeOut);
//					ExecutionStatus = "Pass";
//
//				} else {
//					configUtil.ActionTaken(PageCurrentTestCase, ElementName, ActionTaken, ObjectIdentification, TestCaseValue, timeOut);
//					ExecutionStatus = "Pass";
//				}
//				configUtil.PostTestOperations(PageSourceTestCase, PageCurrentTestCase, TestCaseValue, PageName, ElementName, MarkObject, ObjectIdentification, Snapshot, ExecutionStatus, PagestrDate, PageendDate, ElementDescription);
//			}
//
//			AddRecordIndex++;
//		}
//
//	}
//
//	public void TestSuitCaller() {
//
//		TestExecutionEndDate = new Date();
//		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
//		endExecutionDate = formatter.format(TestExecutionEndDate);
//
//		String HTMLTestSuit = emailReportUtil.HTMLTestSuit().toString();
////		System.out.println(HTMLTestSuit);
//		configUtil.TestSuitCreator(HTMLTestSuit);
//
//		testSuitUtil testSuitUtil = new testSuitUtil();
//		testSuitUtil.GettestSuitDetails(reportTimeStamp);
//
//		configUtil.DeleteFolderContents(SnapshotsPath);
//		configUtil.DeleteFolderFiles(home + "/Downloads/");
//	}
//
//	public void CloseWebDriver() {
//		try {
//			driver.close();
//		} catch (Exception e) {
//			String BrowserName = excelUtil.GetParameterValue(CurrentTestCase, "BrowserName");
//			String URL = excelUtil.GetParameterValue(CurrentTestCase, "URL");
//			driver = configUtil.launchURL(BrowserName, URL);
//			System.out.println("[" + URL + "] URL Launched in Browser [" + BrowserName + "].");
//		}
//	}
//
//	public void QuiteWebDriver() {
//		driver.quit();
//	}
//
////********************************************************************************************************************************************************************************************
//
//	public synchronized void FollowedTestsAccessDataFrom(String PrimaryTestCaseName) {
//		ExcelSheet = GetWorkBook(testWorkbook).getSheet("TestWorkflow");
//		int RowCount = ExcelSheet.getPhysicalNumberOfRows();
//		String RetrievedTestCaseName = "", RetrievedFollowedTestCaseName = "";
//
//		try {
//			for (int ro = 1; ro <= RowCount; ro++) {
//				RetrievedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(0)).toString();
//				RetrievedFollowedTestCaseName = GetCellValue(ExcelSheet.getRow(ro).getCell(2)).toString();
//				AccessDataFrom = GetCellValue(ExcelSheet.getRow(ro).getCell(3)).toString();
//
//				if (RetrievedTestCaseName.equalsIgnoreCase(PrimaryTestCaseName)) {
//
//					if ((AccessDataFrom.isEmpty() && AccessDataFrom == "") || AccessDataFrom.equalsIgnoreCase("source")) {
//						AccessDataFrom = "source";
//					}
//					if (AccessDataFrom.equalsIgnoreCase("self")) {
//						AccessDataFrom = "self";
//					}
//
//					if (RetrievedFollowedTestCaseName != "" && !RetrievedFollowedTestCaseName.isEmpty()) {
//						TestDataType.put(RetrievedTestCaseName, AccessDataFrom);
//						FollowedTestsAccessDataFrom(RetrievedFollowedTestCaseName);
//					} else {
//						TestDataType.put(RetrievedTestCaseName, AccessDataFrom);
//					}
//				}
//			}
//		} catch (Exception e) {
//		}
//
//	}
//
//	protected XSSFWorkbook GetWorkBook(String WorkbookPath) {
//		try {
//			FileInputStream file = new FileInputStream(new java.io.File(WorkbookPath));
//			ExcelWorkbook = new XSSFWorkbook(file);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return ExcelWorkbook;
//	}
//
//	public Object GetCellValue(Cell cell) {
//		Object CellValue = "";
//		try {
//			switch (cell.getCellType()) {
//
//			case Cell.CELL_TYPE_STRING:
//				CellValue = cell.getStringCellValue();
//				break;
//			case Cell.CELL_TYPE_NUMERIC:
//				CellValue = cell.getNumericCellValue();
//				break;
//			case Cell.CELL_TYPE_BLANK:
//				CellValue = "";
//				break;
//			default:
//				CellValue = "";
//				break;
//			}
//		}
//
//		catch (Exception e) {
//			CellValue = "";
//		}
//		return CellValue;
//	}
//
//	public String getFirstTestCase() {
//		String FirstTestCase = null;
//		int testdatasize = TestDataType.size();
//		for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
//			FirstTestCase = tesdatatype.getKey();
//			tesdatatype.getValue();
////			break;
//		}
//
//		return FirstTestCase;
//	}
//
//	public String getSourceTestCase() {
//		String SourceTestCase = null;
//		int testdatasize = TestDataType.size();
//		for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
//			SourceTestCase = tesdatatype.getKey();
//			tesdatatype.getValue();
//			break;
//		}
//
//		return SourceTestCase;
//	}
//
//	public String getAccessDataFrom() {
//		String FirstTestCase = null, AccessDataFrom = null;
//
//		int testdatasize = TestDataType.size();
//
//		for (Map.Entry<String, String> tesdatatype : TestDataType.entrySet()) {
//			FirstTestCase = tesdatatype.getKey();
//			AccessDataFrom = tesdatatype.getValue();
//			break;
//		}
//
//		return AccessDataFrom;
//	}
//
//	public void ValidateDuplicatedPagesInSourceFlow(String SourceTestCase, String AccessForm, String LevelofExecution) {
//		String FirstTestCase = null, AccessDataFrom = null;
//		boolean DuplicateFlag = false;
//		String WorkflowOuterTestPageName, WorkflowOuterTestPageValue, WorkflowInnerTestPageName, WorkflowInnerTestPageValue, DuplicatePages = "";
//		int Count;
//
//		if (AccessForm.equalsIgnoreCase("source")) {
//			for (Entry<String, TestWorkflowsUtil> OuterTestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
//				WorkflowOuterTestPageName = OuterTestWorkflow.getKey().toString();
//				WorkflowOuterTestPageValue = OuterTestWorkflow.getValue().getPageValue().toString();
//				Count = 0;
//
//				for (Entry<String, TestWorkflowsUtil> InnerTestWorkflow : excelUtil.GetAllTestWorkflow().entrySet()) {
//					WorkflowInnerTestPageName = InnerTestWorkflow.getKey().toString();
//					WorkflowInnerTestPageValue = InnerTestWorkflow.getValue().getPageValue().toString();
//					if (WorkflowOuterTestPageValue.equalsIgnoreCase(WorkflowInnerTestPageValue)) {
//						Count++;
//					}
//
//				} // Inner Loop
//
//				if (Count > 1) {
//					if (!DuplicatePages.contains(WorkflowOuterTestPageValue)) {
//						DuplicatePages = DuplicatePages + ", " + WorkflowOuterTestPageValue;
//					}
//				}
//			} // Outer Loop
//
//			if (!DuplicatePages.isEmpty() && DuplicatePages != "") {
//				String Message = "Source testcase [" + SourceTestCase + "] workflow contains duplicate page operations [" + DuplicatePages.substring(1).trim() + "], Please update workflow pages.";
//
//				if (LevelofExecution.equalsIgnoreCase("initial")) {
//					System.out.println(Message);
//					Assert.fail(Message);
//				} else {
//					Assert.fail("");
//				}
//
//			}
//
//		}
//
//	}
//
//	public WebDriver launchURL(String BrowserName, String URL) {
//		Dimension targetSize = new Dimension(1920, 1080);
//		switch (BrowserName.toLowerCase()) {
//
//		case "chrome":
//			System.setProperty("webdriver.chrome.driver", ChromeDriverPath);
//			try {
//				ChromeOptions options = new ChromeOptions();
//				options.setExperimentalOption("w3c", false);
//
//////				options.addArguments("--headless");
////				options.addArguments("--window-size=1920,1080");
////				options.addArguments("--no-sandbox");
////				options.addArguments("--disable-extensions");
////				options.addArguments("--dns-prefetch-disable");
////				options.addArguments("--disable-gpu");
////				options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
//
////				options.addArguments("--incognito");
//				driver = new ChromeDriver(options);
//				driver.manage().window().maximize();
////				driver.manage().window().setSize(targetSize);
//				driver.get(URL);
//			} catch (Exception e1) {
//				System.out.println("Driver Launch Issue...");
////				e1.printStackTrace();
//			}
//			break;
//		case "ie":
//			System.setProperty("webdriver.ie.driver", IEDriverPath);
//			driver = new InternetExplorerDriver();
////			driver.manage().window().maximize();
//			driver.manage().window().setSize(targetSize);
//			driver.get(URL);
//
//		case "edge":
//			System.setProperty("webdriver.edge.driver", MSEdgeDriverPath);
//			driver = new EdgeDriver();
////			driver.manage().window().maximize();
//			driver.manage().window().setSize(targetSize);
//			driver.get(URL);
//			break;
//		case "firefox":
//			System.setProperty("webdriver.gecko.driver", GeckoDriverPath);
//			FirefoxOptions options = new FirefoxOptions();
//			options.setCapability("marionette", true);
////			options.setLegacy(true);
//			driver = new FirefoxDriver(options);
////			driver.manage().window().maximize();
//			driver.manage().window().setSize(targetSize);
//			driver.get(URL);
//			break;
//		case "mobilechrome":
//			DesiredCapabilities cap = new DesiredCapabilities();
//			// Browser Capability
//			cap.setCapability("chromedriverExecutable", "C:\\B2BAvayaEnterpriseStore\\driver\\chromedriver\\chromedriver.exe");
//			cap.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
//			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "vivo 1601");
//			cap.setCapability(MobileCapabilityType.UDID, "7HJJ9S5T99999999"); // 192.168.1.203:5555 //7HJJ9S5T99999999
//			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
//			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0");
//			ChromeOptions chromeOptions = new ChromeOptions();
//			chromeOptions.setExperimentalOption("w3c", false);
//			cap.merge(chromeOptions);
//
//			URL url;
//			try {
//				url = new URL("http://0.0.0.0:4723/wd/hub");
//				driver = new AndroidDriver<MobileElement>(url, cap);
//				System.out.println("URL Access");
//				driver.navigate().to("https://channelstore-stg.avaya.com/aes");
//				driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
//				System.out.println("Site Access");
//
//			} catch (MalformedURLException e) {
//				e.printStackTrace();
//			}
//
//			break;
//
//		default:
//			System.out.println("Browser [" + BrowserName + "] is not in list, Please validate...");
//			Assert.fail("Browser [" + BrowserName + "] is not in list, Please validate...");
//		}
//
//		if (driver != null)
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		return driver;
//	}
//
//	public void TestExecutionStartTime() {
//		TestExecutionStartDate = new Date();
//		SimpleDateFormat Exeformatter = new SimpleDateFormat("HH:mm:ss");
//		strExecutionDate = Exeformatter.format(TestExecutionStartDate);
//
//	}
//
//	public void DeleteLocalFiles() {
//		File localPath = new File(localfolder);
//		for (File file : localPath.listFiles()) {
//			if (!file.isDirectory())
//				file.delete();
//		}
//	}
//
//}
