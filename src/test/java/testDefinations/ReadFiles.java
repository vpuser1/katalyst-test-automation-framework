package testDefinations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class ReadFiles {

	public static void main(String[] args) {

		LinkedHashMap<String, String> ExcellogMap = new LinkedHashMap<String, String>();
		String FilePath = "D:\\IVL\\katalyst Automation Feamework\\OneTouchFramework\\reports\\Testcase3\\Testcase3_13032020195242.txt";
		String st, SheetName = null, PageName = null, PageDescr = null, logtxt;
		int logcount = 0;
		File file = new File(FilePath);
		if (file.exists()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				while ((st = br.readLine()) != null) {
					if (st.contains("|")) {
						st = st.replace("|", "").replace("+", "").replace("-", "");

						if (st.contains("=>")) {
							SheetName = st.split("=>")[0];
							PageName = st.split("=>")[1];
							PageDescr = st.split("=>")[2];
							logcount = 0;
						} else if (st.contains("::")) {
							logtxt = st;
							logcount = logcount + 1;
							String Key = SheetName + "#" + PageName + "#" + logcount;
							ExcellogMap.put(Key, logtxt);

						}
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		for (Entry<String, String> excellogmap : ExcellogMap.entrySet()) {
			System.out.println(excellogmap.getKey());
		}

	}

}
