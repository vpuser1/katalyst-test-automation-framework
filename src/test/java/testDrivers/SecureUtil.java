package testDrivers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import testEngine.ExcelUtil;
import testEngine.FileUtil;
import testEngine.GearBox;
import testEngine.LoadInReports;

public class SecureUtil extends GearBox {

	public WebDriver driver;
	public ExcelUtil excelUtil;
	public Map<String, LoadInReports> loadInReports;// = new LinkedHashMap<String, LoadInReports>(); // removed static
	public Map<String, String> testElement;// = new LinkedHashMap<String, String>(); // removed static
	public GearBox gearbox;
	public Date TestStartDate, TestEndDate;
	public String reportTimeStamp;
	public String SourceTestCase, CurrentTestCase;
	public String BrowserName;

	public SecureUtil(WebDriver driver, ExcelUtil excelUtil, Map<String, LoadInReports> loadInReports, Map<String, String> testElement, GearBox gearbox, Date testStartDate, Date testEndDate, String reportTimeStamp, String sourceTestCase,
			String currentTestCase, String browserName) {
		super(driver, excelUtil, loadInReports, testElement, gearbox, testStartDate, testEndDate, reportTimeStamp, sourceTestCase, currentTestCase, browserName);
		this.driver = driver;
		this.excelUtil = excelUtil;
		this.loadInReports = loadInReports;
		this.testElement = testElement;
		this.gearbox = gearbox;
		this.TestStartDate = testStartDate;
		this.TestEndDate = testEndDate;
		this.reportTimeStamp = reportTimeStamp;
		this.SourceTestCase = sourceTestCase;
		this.CurrentTestCase = currentTestCase;
		this.BrowserName = browserName;
	}

	// FileUtil
	private SecretKeySpec secretKey;
	private byte[] key;
	public LinkedHashMap<String, String> AuthMap = new LinkedHashMap<String, String>();
	public String secretVal, originalString, encryptedString, decryptedString;

	public void setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public String encrypt(String strToEncrypt, String secret) {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			OnErrorLoadInReport("Error while encrypting.");
//			System.out.println("Error while encrypting: " + e.toString());
		}
		return null;
	}

	public String decrypt(String strToDecrypt, String secret) {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			OnErrorLoadInReport("Error while decrypting.");
//			System.out.println("Error while decrypting: " + e.toString());
		}
		return null;
	}

	public void GetPropertyFileDetails() {
		String ApplicationName = null, UserName = null, Password, AppUser;
		AuthMap.clear();
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(new File(AuthenticationFilePath)));
			String line = reader.readLine();
			while (line != null) {
//				System.out.println(line);
				if (line.contains("=")) {
					AppUser = line.split("=")[0];
					Password = line.split("=")[1];
					if (AppUser.contains("_")) {
						ApplicationName = AppUser.split("_")[0];
						UserName = AppUser.split("_")[1];
					}
					AuthMap.put(AppUser, Password);
//					System.out.println(ApplicationName + " | " + UserName + " | " + Password);
				}
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			OnErrorLoadInReport("ERROR : Observing issue while retrieving details from [" + AuthenticationFilePath + "] file.");
			e.printStackTrace();
		}
	}

	public void SetSecureDetailsInMap() {
		String ApplicationName = null, UserName = null, Password, AppUser;
		try {
			for (Entry<String, String> auth : AuthMap.entrySet()) {
				AppUser = auth.getKey();
				Password = auth.getValue();
				if (!AppUser.contains("Format")) {
					if (AppUser.contains("_")) {
						ApplicationName = AppUser.split("_")[0];
						UserName = AppUser.split("_")[1];
						secretVal = ApplicationName;
					}
					if (!Password.toLowerCase().contains("secure")) {
						originalString = Password.trim();
						encryptedString = encrypt(originalString, secretVal);
						AuthMap.put(AppUser.toString(), ("Secure : " + encryptedString).toString());
					} else {
						AuthMap.put(AppUser, Password);
					}

				} else {
					AuthMap.put(AppUser, Password);
				}
			}
		} catch (Exception e) {
			OnErrorLoadInReport("ERROR : Observing issue while securing user details...");
		}
	}

	public void EncryptPropertyFileDetails() {
		String Password, AppUser;
		try {
			FileWriter myWriter = new FileWriter(new File(AuthenticationFilePath));
			myWriter.write("User should provide Authentication details in below format.");
			myWriter.write("\r\n");
			myWriter.write("If the 'Password' value shows 'Secure' keyword, It means its encrypted and safe to use.");
			myWriter.write("\r\n");
			myWriter.write("Format => [ApplicationName_UserName = Password]");
			myWriter.write("\r\n");
			myWriter.write("\r\n");

			for (Entry<String, String> auth : AuthMap.entrySet()) {
				AppUser = auth.getKey();
				Password = auth.getValue().toString();
				if (!AppUser.contains("Format")) {
					myWriter.write(AppUser.trim() + " = " + Password.trim());
					myWriter.write("\r\n");
				}
			}

			myWriter.close();
//			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
//			System.out.println("An error occurred.");
			OnErrorLoadInReport("ERROR : Observing issue while encryoting user details...");
//			e.printStackTrace();
		}

	}

	public String getPassword(String ApplicationName, String UserName) {
		String AppUser;
		String Password = null, encryptedString;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(new File(AuthenticationFilePath)));
			String line = reader.readLine();
			while (line != null) {

				if (line.contains("=")) {
					AppUser = line.split("=")[0];
					if (!AppUser.contains("Format")) {
						String ProvAppUser = ApplicationName + "_" + UserName;
						if (AppUser.trim().equalsIgnoreCase(ProvAppUser.trim())) {
							encryptedString = line.split("=")[1].replace("Secure :", "").trim();
							if (AppUser.contains("_")) {
								ApplicationName = AppUser.split("_")[0];
								UserName = AppUser.split("_")[1];
								secretVal = ApplicationName;
								decryptedString = decrypt(encryptedString, secretVal);
								Password = decryptedString.trim();
								break;
							}
						}
					}
				}
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {

			//Assert.fail("Incorrect Details Provided or Details may be Missing, Please validate it.");
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Incorrect Details Provided or Details may be Missing, Please validate it.");
//			e.printStackTrace();
		}

		if (Password == null) {
			//Assert.fail("Incorrect Details Provided, Please validate it.");
			ExecutionStatus = "Fail";
			OnErrorLoadInReport("Incorrect Details Provided, Please validate it.");
		}

		return Password;

	}

	public void SecureConfiguration() {
		GetPropertyFileDetails();
		SetSecureDetailsInMap();
		EncryptPropertyFileDetails();
	}

}
