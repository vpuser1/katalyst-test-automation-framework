package testDrivers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class DriverUtil {
	
	protected WebDriver driver;
//	protected ExtentReports extent;
//	protected ExtentTest test;
	public static String OBJECT_FILE_PATH = "./Object/ObjectRepository.xlsx";
	// public static String UniqueObjectId="ObjectName";
//	Map<String, String> objectsMap = readAllObjects();

	Map<String, String> objectMap;
	static String starttime, startdatetime;

	protected static String checkpoint, checkpointdesc, status, pagedata, pageremark;
	protected static int row=0;
	String ErrorMessage;
	//public static Map<String, String> SubscriptionsMap = new HashMap<String, String>();
	//public static Map<String, String> DevicesMap = new HashMap<String, String>();

//	public BaseMachineClass(WebDriver driver, ExtentReports extent, ExtentTest test) {
//		this.driver = driver;
//		this.extent = extent;
//		this.test = test;
//	}
//
//	public BaseMachineClass() {
//
//	}

//	protected static final Logger LOGGER = Logger.getLogger(BaseMachineClass.class.getName());
	Capabilities capability;

//	static Map<String, String> readAllObjects() {
//		ExcelUtil excelUtl = new ExcelUtil();
//		try {
//			excelUtl.setExcelFile(OBJECT_FILE_PATH, "Objects");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return excelUtl.getSheetDataObjects("Objects");
//
//	}

//	@SuppressWarnings("deprecation")
//	public WebDriver launchURL(String browserName, String url) {
//
//		// Create object of SimpleDateFormat class and decide the format
//		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//		DateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss");
//
//		// get current date time with Date()
//		Date date = new Date();
//
//		// Now format the date
//		startdatetime = dateFormat.format(date);
//		starttime = dateFormat1.format(date);
//
//		// Print the Date
//		System.out.println("Execution Start Time:" + starttime);
//
//		switch (browserName.toLowerCase()) {
//
//		case "chrome":
//
//			DesiredCapabilities cap = new DesiredCapabilities();
//			cap.setCapability("deviceName", "Android SDK built for x86");  //AOSP on IA Emulator
//			cap.setCapability("platformName", "Android");
//			cap.setCapability(CapabilityType.VERSION, "8.1.0"); //9.0
//			cap.setCapability("appPackage", "com.android.chrome");
//			cap.setCapability("appActivity", "com.google.android.apps.chrome.Main");
//			try {
//				driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), cap);
//			} catch (MalformedURLException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			
//			
////			System.setProperty("webdriver.chrome.driver", DRIVER_PATH + "chromedriver.exe");
////			ChromeOptions chromeoptions = new ChromeOptions();
////			// here "--start-maximized" argument is responsible to maximize chrome browser
////			chromeoptions.addArguments("--start-maximized");
////			driver = new ChromeDriver(chromeoptions);
//			
////			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
////			capability = ((RemoteWebDriver) driver).getCapabilities();
////			extent.addSystemInfo("BrowserName", capability.getBrowserName());
////			extent.addSystemInfo("Browser Version", capability.getVersion());
////			JavascriptExecutor js = (JavascriptExecutor) driver;
////			js.executeScript("document.body.style.zoom='80%'");
////
////			try {
////				driver.get(url);
////				LOGGER.info("URL Launched Successfully");
////				getScreenShot("URL Launched Successfully");
////				test.log(LogStatus.PASS, "URL: " + url + " Launched Successfully");
////				System.out.println("URL Launched Successfully");
////			} catch (Exception e) {
////				LOGGER.error("URL Launch Falied" + e);
////				getScreenShot("URL Launch Falied");
////				test.log(LogStatus.FAIL, "URL: " + url + " could not be launched -" + e);
////				Assert.fail("Error occured while launching url: " + e);
////			}
//			break;
//		case "firefox":
//			System.setProperty("webdriver.gecko.driver", DRIVER_PATH + "geckodriver.exe");
//			driver = new FirefoxDriver();
//			driver.manage().window().maximize();
//			capability = ((RemoteWebDriver) driver).getCapabilities();
//			// extent.addSystemInfo("BrowserName",capability.getBrowserName());
//			// extent.addSystemInfo("Browser Version", capability.getVersion());
//			try {
//				driver.get(url);
//				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//				LOGGER.info("URL Launched Successfully");
//				// getScreenShot("URL Launched Successfully");
//				// test.log(LogStatus.PASS, "URL: "+url+" Launched Successfully");
//				System.out.println("URL Launched Successfully");
//			} catch (Exception e) {
//				LOGGER.error("URL Launch Falied" + e);
//				// getScreenShot("URL Launch Falied");
//				// test.log(LogStatus.FAIL, "URL: "+url+" could not be launched -"+e);
//				Assert.fail("Error occured while launching url: " + e);
//			}
//			break;
//		case "ie":
//			System.setProperty("webdriver.ie.driver", DRIVER_PATH + "IEDriverServer.exe");
//			DesiredCapabilities returnCapabilities = DesiredCapabilities.internetExplorer();
//			returnCapabilities.setCapability("ignoreProtectedModeSettings", true);
//			returnCapabilities.setCapability("ie.ensureCleanSession", true);
//			returnCapabilities.setCapability("requireWindowFocus", true);
//			returnCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//			returnCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
//					true);
//			driver = new InternetExplorerDriver(returnCapabilities);
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			driver.manage().window().maximize();
//			capability = ((RemoteWebDriver) driver).getCapabilities();
//			// extent.addSystemInfo("BrowserName",capability.getBrowserName());
//			// extent.addSystemInfo("Browser Version", capability.getVersion());
//			try {
//				driver.get(url);
//				LOGGER.info("URL Launched Successfully");
//				// getScreenShot("URL Launched Successfully");
//				// test.log(LogStatus.PASS, "URL: "+url+" Launched Successfully");
//				System.out.println("URL Launched Successfully");
//			} catch (Exception e) {
//				LOGGER.error("URL Launch Falied" + e);
//				// getScreenShot("URL Launch Falied");
//				// test.log(LogStatus.FAIL, "URL: "+url+" could not be launched -"+e);
//				Assert.fail("Error occured while launching url: " + e);
//			}
//			break;
//
//		case "edge":
//			System.setProperty("webdriver.edge.driver", DRIVER_PATH + "MicrosoftWebDriver.exe");
//			DesiredCapabilities returnCapabilities1 = DesiredCapabilities.edge();
//			returnCapabilities1.setBrowserName("MicrosoftEdge");
//			returnCapabilities1.setPlatform(Platform.WIN10);
////			returnCapabilities1.setCapability("ignoreProtectedModeSettings", true);
////			returnCapabilities1.setCapability("edge.ensureCleanSession", true);
////			returnCapabilities1.setCapability("requireWindowFocus", true);
////			returnCapabilities1.setCapability(CapabilityType.ACCEPT_SSL_CERTS,true);
////			returnCapabilities1.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
//			driver = new EdgeDriver(returnCapabilities1);
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//			driver.manage().window().maximize();
//			capability = ((RemoteWebDriver) driver).getCapabilities();
//			// extent.addSystemInfo("BrowserName",capability.getBrowserName());
//			// extent.addSystemInfo("Browser Version", capability.getVersion());
//			try {
//				driver.get(url);
//				LOGGER.info("URL Launched Successfully");
//				// getScreenShot("URL Launched Successfully");
//				// test.log(LogStatus.PASS, "URL: "+url+" Launched Successfully");
//				System.out.println("URL Launched Successfully");
//			} catch (Exception e) {
//				LOGGER.error("URL Launch Falied" + e);
//				// getScreenShot("URL Launch Falied");
//				// test.log(LogStatus.FAIL, "URL: "+url+" could not be launched -"+e);
//				Assert.fail("Error occured while launching url: " + e);
//			}
//			break;
//		}
//
//		if (driver != null)
//			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		return driver;
//	}
//
//	public String getText(String fieldName) {
//		By locator;
//		locator = getObject(fieldName);
//		String result = null;
//		try {
//			result = driver.findElement(locator).getText();
//		} catch (Exception e) {
////			LOGGER.error("Error occured while getting text from " + fieldName);
//		}
//		return result;
//	}
//
//	public void printConsole(String message) {
//		System.out.println(message);
//	}
//
//	public void killBrowserInstance(WebDriver driver) {
//		try {
//			driver.quit();
////			LOGGER.info("Browser Instance successfully closed");
//		} catch (Exception e) {
////			LOGGER.error("Error occured while closing webdriver instance: " + e);
//			Assert.fail("Error occured while closing webdriver instance: " + e);
//		}
//	}

//	public By getObject(String objName) {
//		By ret = null;
//		String c = null;
//		try {
//			// setExcelFile(OBJECT_FILE_PATH, "data");
//			// objectMap = getSheetDataObject("ObjectName", "Login");
//			//
//			//// c = getExcelData(sheetName, columnName, rowname);
//			// c=objectMap.get(objName);
//
////			c = objectsMap.get(objName);
//			System.out.println();
//		} catch (Exception e) {
////			LOGGER.error("Error occured while getting data from excel: " + e);
//			Assert.fail("Error occured while getting data from excel: " + e);
//		}
//
//		String[] keyVal = c.split("~");
//		String key = keyVal[0].trim();
//		String value = keyVal[1].trim();
//
//		switch (key.toLowerCase()) {
//		case "class":
//			ret = By.className(value);
//			break;
//		case "css":
//			ret = By.cssSelector(value);
//			break;
//		case "id":
//			ret = By.id(value);
//			break;
//		case "link":
//			ret = By.linkText(value);
//			break;
//		case "name":
//			ret = By.name(value);
//			break;
//		case "partiallink":
//			ret = By.partialLinkText(value);
//			break;
//		case "tagname":
//			ret = By.tagName(value);
//			break;
//		case "xpath":
//			ret = By.xpath(value);
//			break;
//		}
//
//		return ret;
//	}
//
//	public By getObjectReplace(String objName, String rKey, String rValue) {
//		By ret = null;
//		String c = objectsMap.get(objName);
//
//		String[] keyVal = c.split("\\~");
//		String[] replaceKey = rKey.split("\\~");
//		String[] replaceValue = rValue.split("\\~");
//
//		for (int i = 0; i < replaceKey.length; i++)
//			keyVal[1] = keyVal[1].replace(replaceKey[i], replaceValue[i]);
//
//		String value = keyVal[1];
//
//		switch (keyVal[0].toLowerCase()) {
//		case "class":
//			ret = By.className(value);
//			break;
//		case "css":
//			ret = By.cssSelector(value);
//			break;
//		case "id":
//			ret = By.id(value);
//			break;
//		case "link":
//			ret = By.linkText(value);
//			break;
//		case "name":
//			ret = By.name(value);
//			break;
//		case "partiallink":
//			ret = By.partialLinkText(value);
//			break;
//		case "tagname":
//			ret = By.tagName(value);
//			break;
//		case "xpath":
//			ret = By.xpath(value);
//			break;
//		}
//
//		return ret;
//	}
//
//	public String getConfigValue(String key) {
//		Properties props = new Properties();
//		File file = new File(".//data//Input_Data.properties");
//		FileReader fileR = null;
//		String retVal = null;
//		try {
//			fileR = new FileReader(file);
//			props.load(fileR);
//			retVal = props.getProperty(key);
//			LOGGER.debug("Key: " + key + " " + "Value: " + retVal);
//
//			fileR.close();
//		} catch (Exception e) {
//			LOGGER.error("Error occured while getting value from propertie file: " + e);
//			Assert.fail("Error occured while getting value from propertie file: " + e);
//		}
//		return retVal;
//	}
//
//	public void setText(String objName, String value) {
//		try {
//			By locator;
//			locator = getObject(objName);
//			driver.findElement(locator).clear();
//			driver.findElement(locator).sendKeys(value);
//			LOGGER.info(objName + " The text: " + value + " entered successfully");
//			getScreenShot("login");
//			test.log(LogStatus.PASS, objName + " The text: " + value + " entered successfully");
////			throw new ArithmeticException("Explicitly added exception................");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while entering value in textbox [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while entering value in textbox: " + e);
//			test.log(LogStatus.FAIL, "Error occured while entering value in textbox: " + e);
//			Assert.fail("Error occured while entering value in textbox: " + e);
//		}
//	}
//
//	public void clickButton(String objName) {
//		try {
//			wait(2);
//			By locator;
//			locator = getObject(objName);
//			driver.findElement(locator).click();
//			LOGGER.info(objName + ":Button successfully clicked");
//			test.log(LogStatus.PASS, objName + ":Button successfully clicked");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while clicking button [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while clicking button " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while clicking button " + objName + ": " + e);
//			Assert.fail("Error occured while clicking button: " + e);
//		}
//	}
//
//	public void pressTAB(String objName) {
//		try {
//			wait(2);
//			By locator;
//			locator = getObject(objName);
//			driver.findElement(locator).sendKeys(Keys.TAB);
//			LOGGER.info("Pressed TAB Succesfully");
//			test.log(LogStatus.PASS, "Pressed TAB Key Succesfully");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while pressing TAB Key  [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while pressing TAB Key " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while pressing TAB Key  " + objName + ": " + e);
//			Assert.fail("Error occured while pressing TAB Key: " + e);
//		}
//	}
//
//	public boolean selectRadioButton_WE(String objName, String key, String value) {
//
//		try {
//			WebElement ele = driver.findElement(getObjectReplace(objName, key, value));
//			if (!ele.isSelected()) {
//				ele.click();
//				LOGGER.info(objName + " has been selected");
//				test.log(LogStatus.PASS, "EXPECTECD: Element " + objName + " should be selected",
//						"Usage: <span style='font-weight:bold;'>ACTUAL:: " + objName + " has been selected</span>");
//			} else {
//				LOGGER.info(objName + " radio button already checked");
//				test.log(LogStatus.INFO, "EXPECTECD: Element " + objName + " already checked",
//						"Usage: <span style='font-weight:bold;'>ACTUAL:: " + objName + " already checked</span>");
//			}
//			return true;
//
//		} catch (Exception e) {
//			ErrorMessage = "Selecting radio button failed, Selecting radio button failed  for  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			getScreenShot("selecting radio button " + objName);
//			LOGGER.error("Selecting radio button failed..." + e);
//			LOGGER.error("Selecting radio button failed..." + e);
//			test.log(LogStatus.FAIL, "Selecting radio button failed", "Selecting radio button failed  because  -" + e);
//			Assert.fail("Selecting radio button failed..." + e);
//			return false;
//		}
//	}
//
//	public boolean selectDropDownByVisibleText(String objName, String value) {
//		try {
//			if (value != null && value.trim().length() > 0) {
//				WebElement supplierName = driver.findElement(getObject(objName));
//
//				boolean isOptionFound = false;
//				Select oSelect = new Select(supplierName);
//				List<WebElement> optionelems = oSelect.getOptions();
//				for (int i = 0; i < optionelems.size(); i++)
//					// if(optionelems.get(i).getText().trim().equals(value.trim()))
//					if (optionelems.get(i).getText().trim().contains(value.trim())) {
//						oSelect.selectByIndex(i);
//						isOptionFound = true;
//						break;
//					}
//				if (isOptionFound) {
//					LOGGER.info("Value -'" + value + "'- selected from Drop down " + objName + " sucessfully");
//					test.log(LogStatus.PASS, "Drop down should be selected",
//							"Value -'" + value + "'- selected from Drop down " + objName + " sucessfully");
//				} else {
//					getScreenShot("Unable to find Value -'" + value + "'- from Drop down " + objName);
//					LOGGER.info("Unable to find Value -'" + value + "'- from Drop down " + objName);
//					test.log(LogStatus.FAIL, "Drop down should be selected",
//							"Unable to find Value -'" + value + "'- from Drop down " + objName);
//					Assert.fail("Unable to find Value -'" + value + "'- from Drop down " + objName);
//				}
//			}
//			return true;
//		} catch (Exception e) {
//			ErrorMessage = "Dropdown selection failed for  [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			getScreenShot("Selecting  " + value);
//			LOGGER.error("Dropdown selection failed..." + e);
//			test.log(LogStatus.FAIL, "Dropdown selection failed..." + e);
//			Assert.fail("Dropdown selection failed..." + e);
//			return false;
//		}
//	}
//
//	public void clickLink(String objName) {
//		try {
//			By locator;
//			locator = getObject(objName);
//			driver.findElement(locator).click();
//			LOGGER.info("Link successfully clicked");
//			test.log(LogStatus.PASS, "Link successfully clicked");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while clicking Link:  [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while clicking Link: " + e);
//			test.log(LogStatus.FAIL, "Error occured while clicking Link: " + e);
//			Assert.fail("Error occured while clicking Link: " + e);
//		}
//	}
//
//	public void mouseHover(String objName) {
//		try {
//
//			WebElement elem = driver.findElement(getObject(objName));
//			Actions actions = new Actions(driver);
//			actions.moveToElement(elem).build().perform();
//			LOGGER.info(objName + " element is clicked");
//			test.log(LogStatus.PASS, objName + " element is clicked");
//
//		} catch (Exception e) {
//			ErrorMessage = "Exception occured while looking for element :  [ " + objName + " ]" + e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.info("Exception occured while looking for element : " + objName + "  - " + e);
//			LOGGER.error("Exception occured while looking for element : " + objName + "  - " + e);
//			test.log(LogStatus.FAIL, objName + " element is not clicked");
//
//		}
//	}
//
//	public void clickManageBlueprint(String objName) {
//		try {
//			WebElement ele1, ele2, ele3;
//			String[] object = objName.split("~");
//			ele1 = driver.findElement(getObject(object[0]));
//			ele2 = driver.findElement(getObject(object[1]));
//			ele3 = driver.findElement(getObject(object[2]));
//
//			Actions actions = new Actions(driver);
//			actions.moveToElement(ele1).pause(500).moveToElement(ele2).pause(500).click(ele3).build().perform();
//
//			LOGGER.info("Blue print link successfully clicked");
//			test.log(LogStatus.PASS, "Blue print Link successfully clicked");
//
//		} catch (Exception e) {
//			LOGGER.error("Error occured while clicking Blue print Link: " + e);
//			test.log(LogStatus.FAIL, "Error occured while clicking Blue print Link: " + e);
//			Assert.fail("Error occured while clicking Blue print Link: " + e);
//		}
//	}
//
//	public void mouseHoverAndClick(String objName) {
//		try {
//
//			WebElement elem = driver.findElement(getObject(objName));
//			Actions actions = new Actions(driver);
//			actions.moveToElement(elem).pause(1000).click().perform();
//			LOGGER.info(objName + " element is clicked");
//			test.log(LogStatus.PASS, objName + " element is clicked");
//
//		} catch (Exception e) {
//			LOGGER.error("Exception occured while looking for element : " + objName + "  - " + e);
//			test.log(LogStatus.FAIL, objName + " element is not clicked");
//
//		}
//	}
//
//	public void wait(int seconds) {
//		try {
//			int sec = seconds * 1000;
//			Thread.sleep(sec);
//		} catch (Exception e) {
//			LOGGER.error("Error Occured while wait");
//			test.log(LogStatus.FAIL, "Error Occured while wait" + e);
//		}
//	}
//
//	public void getScreenShot(String testStep) {
//		try {
//			String screenshotB64 = "data:image/png;base64,"
//					+ ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64);
//
//			test.log(LogStatus.INFO, "Snapshot for  " + testStep + "  : " + test.addBase64ScreenShot(screenshotB64));
//
//		} catch (Exception e) {
//			LOGGER.info("ERROR IN SCREENSHOT." + e);
//			Assert.fail("ERROR IN SCREENSHOT." + e);
//		}
//	}
//
//	public WebDriver switchToWindow(WebDriver driver) {
//
//		try {
//
//			String parentWindowHandle = driver.getWindowHandle();
//			Set<String> windowId = driver.getWindowHandles();
//
//			for (String id : windowId) {
//
//				if (!id.equalsIgnoreCase(parentWindowHandle)) {
//					driver.switchTo().window(id);
//				}
//				LOGGER.info("Window Switched successfully");
//			}
//
//		} catch (Exception e) {
//			LOGGER.error("Error Occured while switching window: " + e);
//		}
//		return driver;
//	}
//
//	public WebElement fluentWait(WebDriver driver, String objName, long timeout, long polling) {
//		WebElement element = null;
//		// try {
//		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(timeout, TimeUnit.SECONDS) // set the
//																												// timeout
//				.pollingEvery(polling, TimeUnit.SECONDS) // set the interval between every 2 tries
//				.ignoring(NoSuchElementException.class); // don't throw this exception
//		// Then - declare the webElement and use a function to find it
//		try {
//			element = wait.until(new Function<WebDriver, WebElement>() {
//				public WebElement apply(WebDriver driver) {
//					return driver.findElement(getObject(objName));
//				}
//			});
//		} catch (TimeoutException TE) {
//			System.out.println(TE.getMessage());
//			LOGGER.error(TE.getMessage());
//			throw TE;
//		}
//		return element;
//
//	}
//
//	public WebElement fluentWait(WebDriver driver, String objName) {
//		WebElement element = null;
//		// try {
//		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS) // set the
//																											// timeout
//				.pollingEvery(3, TimeUnit.SECONDS) // set the interval between every 2 tries
//				.ignoring(NoSuchElementException.class); // don't throw this exception
//		// Then - declare the webElement and use a function to find it
//		try {
//			element = wait.until(new Function<WebDriver, WebElement>() {
//				public WebElement apply(WebDriver driver) {
//					return driver.findElement(getObject(objName));
//				}
//			});
//		} catch (TimeoutException TE) {
//			System.out.println(TE.getMessage());
//			LOGGER.error(TE.getMessage());
//			throw TE;
//		}
//		return element;
//	}
//
//	public WebElement waitElementClickable(WebDriver driver, String objName, long timeout) {
//		WebElement element = null;
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, timeout);
//			element = wait.until(ExpectedConditions.elementToBeClickable(locator));
//			LOGGER.info(objName + ":Element is clickable");
//			test.log(LogStatus.PASS, objName + ":Element is clickable");
//
//		} catch (Exception e) {
//			LOGGER.error("Error occured while waiting for Element to be clickable " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be clickable  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be clickable : " + e);
//		}
//		return element;
//
//	}
//
//	public WebElement waitElementClickable(WebDriver driver, String objName) {
//		WebElement element = null;
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, 60);
//			element = wait.until(ExpectedConditions.elementToBeClickable(locator));
//			LOGGER.info(objName + ":Element is clickable");
//			test.log(LogStatus.PASS, objName + ":Element is clickable");
//
//		} catch (Exception e) {
//			LOGGER.error("Error occured while waiting for Element to be clickable " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be clickable  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be clickable : " + e);
//		}
//		return element;
//	}
//
//	public WebElement waitElementVisible(WebDriver driver, String objName, long timeout) {
//		WebElement element = null;
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, timeout);
//			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
//			LOGGER.info(objName + ":Element is visible");
//			test.log(LogStatus.PASS, objName + ":Element is visible");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while waiting for Element to be visible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be visible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be visible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be visible : " + e);
//		}
//		return element;
//
//	}
//
//	public WebElement waitElementVisible(WebDriver driver, String objName) {
//		WebElement element = null;
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, 200);
//			element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
//			LOGGER.info(objName + ":Element is visible");
//			test.log(LogStatus.PASS, objName + ":Element is visible");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while waiting for Element to be visible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be visible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be visible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be visible : " + e);
//		}
//		return element;
//	}
//
//	public void waitElementInvisible(WebDriver driver, String objName, long timeout) {
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, timeout);
//			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
//			LOGGER.info(objName + ":Element is invisible");
//			test.log(LogStatus.PASS, objName + ":Element is invisible");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while waiting for Element to be invisible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be invisible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be invisible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be invisible : " + e);
//		}
//	}
//
//	public void waitElementInvisible(WebDriver driver, String objName) {
//		try {
//			By locator;
//			locator = getObject(objName);
//			WebDriverWait wait = new WebDriverWait(driver, 300);
//			wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
//			LOGGER.info(objName + ":Element is invisible");
//			test.log(LogStatus.PASS, objName + ":Element is invisible");
//
//		} catch (Exception e) {
//			ErrorMessage = "Error occured while waiting for Element to be invisible  [ " + objName + " ]"
//					+ e.getMessage();
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, ErrorMessage, row);
//			LOGGER.error("Error occured while waiting for Element to be invisible " + objName + ": " + e);
//			test.log(LogStatus.FAIL, "Error occured while waiting for Element to be invisible  " + objName + ": " + e);
//			Assert.fail("Error occured while waiting for Element to be invisible : " + e);
//		}
//	}
//
//	public WebDriver switchToIFrame(WebDriver driver) {
//		driver.switchTo().frame(1);
//		return driver;
//	}
//
//	public WebDriver switchToWindowQuoteCenterPage(WebDriver driver) {
//
//		try {
//
//			String parentWindowHandle = driver.getWindowHandle();
//			Set<String> windowId = driver.getWindowHandles();
//			boolean targetWindowFOund = false;
//			for (String id : windowId) {
//				if (!id.equalsIgnoreCase(parentWindowHandle)) {
//					driver.switchTo().window(id);
//					if (driver.getTitle().equalsIgnoreCase("A1S Quote Center")) {
//						targetWindowFOund = true;
//						break;
//					}
//					LOGGER.info("Window Switched successfully");
//				}
//			}
//
//			if (targetWindowFOund)
//				driver.switchTo().frame(0);
//			else
//				Assert.fail("Quote Center Window not opened/found");
//		} catch (Exception e) {
//			LOGGER.error("Error Occured while switching window: " + e);
//		}
//
//		return driver;
//	}
//
//	public String getKey(Map<String, String> map, String value) {
//
//		Set<String> keys = map.keySet();
//		String res = null;
//		for (String myKeys : keys) {
//			if (myKeys.equalsIgnoreCase(value)) {
//				res = myKeys;
//			}
//		}
//		return res;
//	}
//
//	public String verifyLandingPage(String objName) {
//
//		String text = "";
//		try {
//			By locator;
//			locator = getObject(objName);
//			text = driver.findElement(locator).getText();
//			LOGGER.info("Text successfully captured");
//			test.log(LogStatus.PASS, "Heading text captured successfully");
//
//		} catch (Exception e) {
//			verifyPageStatus(checkpoint, checkpointdesc, pagedata, e.toString(), row);
//			LOGGER.error("Error occured while getting text: " + e);
//			test.log(LogStatus.FAIL, "Error occured while getting text: " + e);
//			Assert.fail("Error occured while getting text: " + e);
//		}
//		return text;
//	}
//
//	public String VerifyQuoteCenter(String objName) {
//
//		String text = "";
//		try {
//			By locator;
//			locator = getObject(objName);
//			text = driver.findElement(locator).getText();
//			LOGGER.info("Text successfully captured");
//			test.log(LogStatus.PASS, "Heading text captured successfully");
//
//		} catch (Exception e) {
//			LOGGER.error("Error occured while getting text: " + e);
//			test.log(LogStatus.FAIL, "Error occured while getting text: " + e);
//			Assert.fail("Error occured while getting text: " + e);
//		}
//		return text;
//	}
//
//	/**
//	 * Round to certain number of decimals
//	 * 
//	 * @param d
//	 * @param decimalPlace
//	 * @return
//	 */
//	public static float round(float d, int decimalPlace) {
//		BigDecimal bd = new BigDecimal(Float.toString(d));
//		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
//		return bd.floatValue();
//	}
//
//	public void WriteEmailReportDetails(String reportkey, String reportvalue) {
//		String filepath = "/B2BAvayaEnterpriseStore/data/EmailReportDetails.properties";
//		File newfile = new File(filepath);
//		try {
//			newfile.createNewFile();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		try {
//			OutputStream output = new FileOutputStream("/B2BAvayaEnterpriseStore/data/EmailReportDetails.properties",
//					true);
//			Properties prop = new Properties();
//
//			// set the properties value
//			prop.setProperty(reportkey, reportvalue);
////            prop.setProperty("db.user", "mkyong");
////            prop.setProperty("db.password", "password");
//
//			// save properties to project root folder
//			prop.store(output, null);
//
//			System.out.println(prop);
//
//		} catch (IOException io) {
//			io.printStackTrace();
//		}
//	}
//
////	public void ReadEmailReportDetails() {
////
////		try (InputStream input = new FileInputStream("/B2BAvayaEnterpriseStore/data/EmailReportDetails.properties")) {
////
////			Properties prop = new Properties();
////
////			// load a properties file
////			prop.load(input);
////			Set<Object> keys = prop.keySet();
//////			 get the property value and print it out
////			for (Object k : keys) {
////				String key = (String) k;
////				System.out.println(key + " = > " + prop.getProperty(key));
////
////			}
////
////		} catch (IOException ex) {
////			ex.printStackTrace();
////		}
////
////	}
//
//	public void DeleteEmailReportDetails() {
//
//		String filepath = "/B2BAvayaEnterpriseStore/data/EmailReportDetails.properties";
//		boolean fileflag = true;
//		File file = new File(filepath);
//		if (file.exists()) {
//			file.delete();
//			fileflag = false;
//		}
//		if (fileflag == false) {
//			File newfile = new File(filepath);
//			try {
//				newfile.createNewFile();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//	}
//
//	public void verifyPageStatus(String checkpoint, String checkpointdesc, String Data, String Remark, int row) {
//		WriteEmailReportDetails(row + "_1_Checkpoint", checkpoint);
//		WriteEmailReportDetails(row + "_2_Status", "Failed");
//		WriteEmailReportDetails(row + "_3_Description", checkpointdesc);
//		WriteEmailReportDetails(row + "_4_Testdata", Data);
//		WriteEmailReportDetails(row + "_5_Remark", Remark);
//		//getScreenShotForEmailReport();
//
//	}
//
////	public void getScreenShotForEmailReport() {
////		try {
////			// Take screenshot and store as a file format
////			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
////			// now copy the screenshot to desired location using copyFile //method
////			FileUtils.copyFile(src, new File("/B2BAvayaEnterpriseStore/images/B2BError.png"));
////
////		} catch (Exception e) {
////			LOGGER.info("ERROR IN SCREENSHOT." + e);
////			Assert.fail("ERROR IN SCREENSHOT." + e);
////		}
////
////	}

	

}
